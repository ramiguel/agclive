function deleteConfirm(url,id) {
    BootstrapDialog.show({
        title: 'Confirm',
        message:"Are you sure you want to delete this item?",
        buttons:[{
            label:"Yes",
            action:function(dialog) {
                dialog.close();
                var data = {
                    id:id
                };
                $.post(url,data,function(response) {
                    console.log(response);
                    var data = JSON.parse(response);
                    if (data.response_code == 0) {
                        BootstrapDialog.show({
                            title: 'Success',
                            message:data.response_msg,
                            buttons:[{
                                label:"Close",
                                action:function(dialog) {
                                    dialog.close();
                                    $table.bootstrapTable('refresh');
                                }
                            }] 
                        });
                    } else {
                        BootstrapDialog.show({
                            title: 'Error',
                            type: BootstrapDialog.TYPE_WARNING,
                            message:data.response_msg,
                            buttons:[{
                                label:"Close",
                                action:function(dialog) {
                                    dialog.close();
                                }
                            }] 
                        });
                    }
                    return false;
                });
            }
        },{
            label:"Cancel",
            action:function(dialog) {
                dialog.close();
            }
        }]
    });    
}

function submitForm(data,url) {
    var spinner = new Spinner().spin($('body')[0]);
    $('button[type="submit"]').attr("disabled", "disabled");
    $.post(url,data, function(response) {
        var data = JSON.parse(response);
        if (data.response_code == 0) {
            BootstrapDialog.show({
                title: 'Success',
                message:data.response_msg,
                buttons: [{
                    label: 'Close',
                    action: function(dialog) {
                        spinner.stop();
                        dialog.close();
                        window.location = data.redirect_url;
                    }                            
                }]
            });

        } else {
            BootstrapDialog.show({
                title: 'Error',
                type: BootstrapDialog.TYPE_WARNING,
                message:data.response_msg,
                buttons: [{
                    label: 'Close',
                    action: function(dialog) {
                        spinner.stop();
                        $('button[type="submit"]').removeAttr("disabled");
                        dialog.close();
                    }                            
                }]
            });
        }
        return false;
    });
}