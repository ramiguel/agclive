var id = 0;
var $table = $('#table');

function operateFormatter(value, row, index) {
    return [
        '<a class="btn btn-default action-button-item edit" href="javascript:void(0)" title="Edit"><i class="fa fa-edit"></i></a>',
        '<a class="btn btn-default action-button-item delete" href="javascript:void(0)" title="Delete"><i class="fa fa-trash"></i></a>'
    ].join('');
}

window.operateEvents = {
    'click .edit': function (e, value, row, index) {
        redirectTo(requestPath()+"backoffice/site/edit/"+row.id);
    },
    'click .delete': function (e, value, row, index) {
        BootstrapDialog.show({
            title: 'Confirm',
            message:"Are you sure you want to delete this item?",
            buttons:[{
                label:"Yes",
                action:function(dialog) {
                    dialog.close();
                    $.get(requestPath()+"backoffice/site/delete/"+row.id,function(response) {
                        var data = JSON.parse(response);
                        if (data.response_code == 0) {
                            showSuccessDialog(data.response_msg);
                        } else {
                            showWarningDialog(data.response_msg);
                        }
                        return false;
                    });
                }
            },{
                label:"Cancel",
                action:function(dialog) {
                    dialog.close();
                }
            }]
        });
    }
};

$(function() {
    $table.on('load-success.bs.table',function(e,data) {
        $table.bootstrapTable('hideLoading');
    })
        .on('load-error.bs.table',function(e,status) {
            $table.bootstrapTable('hideLoading');
        });
});

function requestPath() {
    split_path = window.location.pathname.split('/');
    if($.trim(split_path[1]) == 'index.php') {
        return window.location.origin+'/';
    } else {
        return window.location.origin+'/'+split_path[1]+'/';
    }
}

function redirectTo(url) {
    window.location = url;
}

function postQueryParams(params) {
    params.id = id;
    return JSON.stringify(params);
}

function showSuccessDialog(msg) {
    BootstrapDialog.show({
        title: 'Success',
        message: msg,
        buttons:[{
            label:"Close",
            action:function(dialog) {
                dialog.close();
                $table.bootstrapTable('refresh');
            }
        }]
    });
}

function showWarningDialog(msg) {
    BootstrapDialog.show({
        title: 'Error',
        type: BootstrapDialog.TYPE_WARNING,
        message: msg,
        buttons:[{
            label:"Close",
            action:function(dialog) {
                dialog.close();
            }
        }]
    });
}