function requestPath() {
	split_path = window.location.pathname.split('/');
	if($.trim(split_path[1]) == 'index.php') {
		return window.location.origin+'/';
	} else { 
		return window.location.origin+'/'+split_path[1]+'/';
	}
}

$(function() {
	$(document).on('click', '.broadcast-send', function(e) {
		var msg = $('.broadcast-msg').val();
		var select = $('.broadcast-select option:selected').text();
		
		$.post(requestPath()+'dashboard/broadcast', {msg:msg, select:select}, function(data) {
			console.log(data);
		});
	});
});