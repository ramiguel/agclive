function requestPath() {
	split_path = window.location.pathname.split('/');
	if($.trim(split_path[1]) == 'index.php') {
		return window.location.origin+'/';
	} else { 
		return window.location.origin+'/'+split_path[0];
	}
}

// START JAVASCRIPT HTML FUNCTIONS
// DECLARE DISASTER FORM
function declareDisaster(companies, disaster_type) {
	var row = '<div class="row">';
	row += '<form class="form-horizontal" id="form-declare-disaster">';
	row += '<div class="col-sm-12">';
	row += '<div class="form-group col-sm-6">';
	row += '<label for="disaster_name" class="control-label">Disaster Name</label>';
	row += '<input type="text" id="disaster_name" placeholder="Enter Disaster Name" name="disaster_name" />';
	row += '</div>';
	row += '<div class="form-group col-sm-6">';
	row += '<label for="disaster_type" class="control-label">Disaster Type</label>';
	row += '<select name="disaster-type-option" class="form-control dark-dropdown disaster-type-option"><option value="">--- Select Disaster Type ---</option><option value="0">All</option>'+ disaster_type +'</select>';
	row += '<input type="checkbox" name="disaster-type-check" />';
	row += '</div>';
	row += '<div class="form-group col-sm-6">';
	row += '<label for="disaster_details" class="control-label">Disaster Details</label>';
	row += '<input type="text" id="disaster_details" placeholder="Enter Disaster Details" name="disaster_details">';
	row += '</div>';
	row += '<div class="form-group col-sm-6">';
	row += '<label for="disaster_details" class="control-label">Company Name</label>';
	row += '<select name="declare-disaster-option" class="form-control dark-dropdown declare-disaster-option"><option value="">--- Select Company ---</option><option value="0">All</option>'+ companies +'</select>';
	row += '</div>';
	row += '<div class="form-group col-sm-12">';
	row += '<div class="form-group col-sm-6 sub-comp-container">';
	row += '</div>';
	row += '<div class="form-group col-sm-6 select-comp-container">';
	row += '</div>';
	row += '</div>';
	row += '</div>';
	row += '</form>';
	row += '</div>';
	
	return row;
}

// CURRENT DISASTER HTML
function showCurrentDisaster(disaster) {
	var row = '<button class="back-to-dashboard">Back</button>';
	row += '<table class="tbl-disaster">';
	row += '<tr><th>ID</th><th>Disaster Name</th><th>Disaster Details</th><th>Company</th><th>Date Created</th><th>Date Closed</th><th>Action</th></tr>';
	row += disaster;
	row += '</table>';
	
	return row;
}

// SELECT COMPANY HTML LEFT SIDE BROADCAST
function broadcastCompany(obj) {
	var row = '<li class="company-sites-left" id="'+ obj.id +'"><a>'+ obj.site +'</a></li>';
	
	return row;
}

// SELECT COMPANY HTML LEFT SIDE DISASTER
function disasterCompany(obj) {
	var row = '<li class="declare-company-sites-left" id="'+ obj.id +'"><a>'+ obj.site +'</a></li>';
	
	return row;
}

// SELECT COMPANY HTML LEFT SIDE EDIT DISASTER
function disasterEditCompany(obj) {
	var row = '<li class="declare-edit-company-sites-left" id="'+ obj.id +'"><a>'+ obj.site +'</a></li>';
	
	return row;
}

// SELECTED COMPANY HTML RIGHT SIDE BROADCAST
function selectCompany(site_id, site_name, company_id, company_name) {
	var row = '<div class="company-list-group"><div class="company-list-group__name" id="'+ company_id +'">'+ company_name +'</div><ul class="list-unstyled company-list"><li class="company-sites-right-select" id="'+ site_id +'"><a>'+ site_name +' <button class="delete-company remove-selected-sites" data-location="1"></button></a></li></ul></div>';
	
	return row;
}

// SELECTED COMPANY HTML RIGHT SIDE DISASTER
function selectDeclareCompany(site_id, site_name, company_id, company_name) {
	var row = '<div class="declare-company-list-group"><div class="declare-company-list-group__name" id="'+ company_id +'">'+ company_name +'</div><ul class="list-unstyled company-list"><li class="declare-company-sites-right-select" id="'+ site_id +'"><a>'+ site_name +' <button class="delete-company remove-selected-sites" data-location="2"></button></a></li></ul></div>';
		
	return row;
}

// SELECTED COMPANY HTML RIGHT SIDE EDIT DISASTER
function selectEditDeclareCompany(site_id, site_name, company_id, company_name) {
	var row = '<div class="declare-edit-company-list-group"><div class="declare-edit-company-list-group__name" id="'+ company_id +'">'+ company_name +'</div><ul class="list-unstyled company-list"><li class="declare-edit-company-sites-right-select" id="'+ site_id +'"><a>'+ site_name +' <button class="delete-company remove-selected-sites" data-location="3"></button></a></li></ul></div>';
	
	return row;
}

// OTHER DISASTER TYPE HTML
function otherDisasterType() {
	var row = '<input type="text" name="disaster-type-other" />';
	
	return row;
}

// VIEW/EDIT CURRENT DISASTER HTML
function viewCurrentDisaster(data, companies, disaster_type) {
	if(data) {
		var row = '<div class="form-horizontal declare-disaster-container">';
		
			// DISASTER NAME TEXTBOX
			row += '<div class="form-group clearfix"><label for="disaster_name" class="col-sm-2 control-label">Disaster Name</label>';
			row += '<div class="col-sm-4"><input type="text" name="declare_edit_disaster_name" class="form-control" id="disaster_name" value="'+ data.data.disaster_name +'" /></div></div>';
			
			// DISASTER TYPE OPTION
			row += '<div class="form-group clearfix"><label for="disaster_type" class="col-sm-2 control-label">Disaster Type</label>';
			row += '<div class="col-sm-4"><select id="disaster_type" class="form-control select-dark declare-edit-disaster-type-option"><option value="'+ data.data.type +'">'+ data.data.type +'</option>'+ disaster_type +'</select></div>';
			
			// DISASTER TYPE OTHER
			row += '<div class="form-group clearfix"><label class="control-label">&nbsp;</label><div class="custom-checkbox-label"><div class="custom-checkbox"><div class="row"><input id="others-edit" type="checkbox" value="None" class="declare-edit-other-type" name="check" /><label for="others-edit"></label>Others<input type="text" name="declare-edit-disaster-type-option" class="form-control others-textbox hide declare-edit-other-type-textbox"></div></div></div></div>';
			
			// DISASTER DETAILS TEXTAREA
			row += '<div class="form-group clearfix"><label for="disaster_details" class="col-sm-2 control-label">Disaster Details</label>';
			row += '<div class="col-sm-4"><textarea class="form-control disaster_details-textarea" name="declare_edit_disaster_details" id="disaster_details" cols="30" rows="10">'+ data.data.disaster_details +'</textarea></div></div></div>';
			
			// DISASTER COMPANY OPTION
			row += '<div class="row"><div class="col-md-5"><label for="" class="text-uppercase">Companies/Sites</label><select name="" class="form-control select-dark declare-edit-company-option"><option value="">- Please Select Companies -</option>'+ companies +'</select>';
			
			// DISASTER SITES SELECTOR
			row += '<div class="declare-company-list-container"><ul class="list-unstyled company-list declare-edit-list-company-sites"></ul></div></div>';
			row += '<div class="col-md-5"><label for="" class="text-uppercase">selected</label><div class="company-list-container--selected declare-edit-company-sites-right">';
			$.each(data.data.companies, function(idx, obj) {
				row += '<div class="declare-edit-company-list-group"><div class="declare-edit-company-list-group__name" id="'+ obj.company_id +'">'+ obj.company_name +'</div><ul class="list-unstyled company-list">';
				
				$.each(obj.sites, function(i, o) {
					row += '<li class="declare-edit-company-sites-right-select" id="'+ o.site_id +'"><a>'+ o.site +'<button class="delete-company remove-selected-sites" data-location="3"></button></a></li>';
				});
				
				row += '</ul></div>';
			});
			
			row += '</div></div></div>';
			
			// DISASTER BROADCAST TEXTAREA
			row += '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">Broadcast</label><textarea class="form-control textarea-custom declare-edit-broadcast-msg" rows="5" placeholder="Example: Fire in Mandaluyong Warehouse"></textarea></div></div>';

			
			// DISASTER BROADCAST MODE
			row += '<div class="row"><div class="col-md-12 form-group"><div class="custom-checkbox-label"><div class="custom-checkbox"><input type="checkbox" value="1" id="edit-disaster-check-sms" name="broadcast" class="view-disaster-check" /><label for="edit-disaster-check-sms"></label></div>Broadcast via SMS</div><span class="pull-right"><button class="btn btn-default declare-edit-broadcast">Broadcast</button></span></div></div>';
			
			
		return row;
	}
}

function filterEmp(affected_emp_companies, status, datefrom, dateto) {
	var row = '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">By Company</label><select class="form-control select-dark modal-select-company"><option value="">---Please Select---</option>'+ affected_emp_companies +'</select></div></div>';
	row += '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">By Status</label><select class="form-control select-dark modal-select-status"><option value="">---Please Select---</option>'+ status +'</select></div></div>';
	row += '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">Date Range</label><div><label>From</label><input type="date" class="`" id="signin_datefrom" name="by-date-from" value="'+ datefrom +'" /><label>To</label><input type="date" id="signin_dateto" class="modal-select-date-to" name="by-date-to" value="'+ dateto +'" /></div></div></div>';
		
	return row;
}

function filterBroadcast(companies, date) {
	var row = '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">By Company</label><select class="form-control select-dark modal-select-company"><option value="">---Please Select---</option>'+ companies +'</select></div></div>';
	row += '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">Date Range</label><div><label>From</label><input type="date" class="modal-select-date-from" name="by-date-from" value="'+ date +'" /><label>To</label><input type="date" class="modal-select-date-to" name="by-date-to" value="'+ date +'" /></div></div></div>';
	
	return row;
}

function filterIncident(companies, date) {
	var row = '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">By Company</label><select class="form-control select-dark modal-select-company"><option value="">---Please Select---</option>'+ companies +'</select></div></div>';
	row += '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">Date Range</label><div><label>From</label><input type="date" class="modal-select-date-from" name="by-date-from" value="'+ date +'" /><label>To</label><input type="date" class="modal-select-date-to" name="by-date-to" value="'+ date +'" /></div></div></div>';
	
	return row;
}

function filterSocial(screen_names) {
	var row = '<div class="row"><div class="col-md-12 form-group"><label for="" class="text-uppercase">By Screen name</label><select class="form-control select-dark modal-select-screen-name"><option value="">---Please Select---</option>'+ screen_names +'</select></div></div>';
	return row;
}

function filterDisaster(obj) {
	var row = '<tr id="'+ obj.id +'"><td>'+ obj.disaster_name +'</td><td>'+ obj.type +'</td><td>'+ obj.disaster_details +'</td><td><button type="button" class="fa fa-search view-current-disaster"></button><button type="button" class="fa fa-times delete-current-disaster" id="'+ obj.id +'" data-toggle="modal" data-target="#confirmDelete"></button></td></tr>';
	
	return row;
}
// END JAVASCRIPT HTML FUNCTIONS

// JQUERY/AJAX FUNCTIONS
$(function() {
	var affected_emp_companies = "";
	var selected_company_id = 0;
	var selected_status = 0;
	var filter_datefrom = "";
	var filter_dateto = "";

	// FILTER EMPLOYEE SIGN-INS
	$(document).on('click', '.filter-emp', function(e) {
		BootstrapDialog.show({
			title: 'Employee Sign-Ins Filter',
			message: filterEmp(affected_emp_companies, status, filter_datefrom, filter_dateto),
			buttons: [{
				label: 'Filter',
				action: function(dialog) {
					var filter_type = 'search_disaster';
					filter_datefrom = $("#signin_datefrom").val();
					filter_dateto = $("#signin_dateto").val();
					selected_status = $('.modal-select-status>option:selected').val();
					selected_company_id = $('.modal-select-company>option:selected').val();
					var disaster_id = ($(".select-disaster").val() == "") ? 0 : $(".select-disaster").val();

					var post_data = {
										type:filter_type,
										company_id:selected_company_id,
										id:disaster_id,
										status:selected_status,
										datefrom:filter_datefrom,
										dateto:filter_dateto
									};

					$.post(requestPath()+"dashboard/search_emp_db", post_data, function(data) {
						display_signin(data['data']);
						signin_filter = post_data;
					});

					dialog.close();
				}
			}, {
				label: 'Cancel',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// FILTER BROADCAST TRAIL
	$(document).on('click', '.filter-incident', function(e) {
		// $('.modal-top>div>div>span>label').text('BROADCAST TRAIL');
		// $('.cmd-filter').attr('data-from', 'incident');
		// $('.modal-status-container').hide(function(){});
		// $('#cmd-modal').modal('show');
		BootstrapDialog.show({
			title: 'Broadcast History Filter',
			message: filterBroadcast(companies, date),
			buttons: [{
				label: 'Filter',
				action: function(dialog) {
					var filter_type = "incident";
					var datefrom = $(".modal-select-date-from").val();
					var dateto = $(".modal-select-date-to").val();
					var company_id = $('.modal-select-company>option:selected').val();

					var post_data = {
										"filter_type":filter_type, 
										"datefrom":datefrom, 
										"dateto":dateto, 
										"company_id":company_id
									};
					
					 $.post(requestPath()+"dashboard/post_filter", post_data, function(data) {		
						 display_broadcast(data);
						 broadcast_filter = post_data;
					});
					dialog.close();
				}
			}, {
				label: 'Cancel',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// FILTER INCIDENTS REPORTED
	$(document).on('click', '.filter-report', function(e) {
		// $('.modal-top>div>div>span>label').text('INCIDENTS REPORTED');
		// $('.cmd-filter').attr('data-from', 'report');
		// $('.modal-status-container').hide(function(){});
		// $('#cmd-modal').modal('show');
		BootstrapDialog.show({
			title: 'Incidents Reported Filter',
			message: filterIncident(companies, date),
			buttons: [{
				label: 'Filter',
				action: function(dialog) {
					var filter_type = "report";
					var datefrom = $(".modal-select-date-from").val();
					var dateto = $(".modal-select-date-to").val()
					var company_id = $('.modal-select-company>option:selected').val();

					var post_data = {
										"filter_type":filter_type, 
										"datefrom":datefrom, 
										"dateto":dateto, 
										"company_id":company_id
									};
					
					 $.post(requestPath()+"dashboard/post_filter", post_data, function(data) {		
						display_report_incident(data);
						incident_filter = post_data;
					});
					dialog.close();
				}
			}, {
				label: 'Cancel',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// FILTER PUBLIC FEEDS
	$(document).on('click', '.filter-social', function(e) {
		// $('.modal-top>div>div>span>label').text('PUBLIC TWITTER FEEDS');
		// $('.cmd-social-filter').attr('data-from', 'social');
		// $('.modal-status-container').show();
		// $('#cmd-social-modal').modal('show');

		BootstrapDialog.show({
			title: 'Public feeds Filter',
			message: filterSocial(screen_names),
			buttons: [{
				label: 'Filter',
				action: function(dialog) {
					var screen_name = $('.modal-select-screen-name>option:selected').val();

			        //if (screen_name != '') {
			        	$.post(requestPath()+"dashboard/get_twitter_feed", {screen_name:screen_name}, function(data){

							$('.table-twitter-rss>tbody').html('');

							var feeds = JSON.parse(data);
					
							for(feed in feeds) {				
								$('.table-twitter-rss>tbody').append('<tr><td class="image-container employee"><div><img src="'+feeds[feed]['profile_image']+'" alt=""></div></td><td><div class="acctname">'+feeds[feed]['user_name']+'</div><div class="time"><small>'+$.timeago(feeds[feed]['created_at'])+' @'+feeds[feed]['screen_name']+'</small></div><div class="incident">'+feeds[feed]['text']+'</div></td></tr>');
							}
						});
			        //}
					dialog.close();
				}
			}, {
				label: 'Cancel',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// DONE SELECT SITES
	$(document).on('click', '.select-sites-done', function(e) {
		var sites_selector = $('.company-sites-right-select');
		var sites = [];
		$.each(sites_selector, function(idx, obj) {
			sites.push($(this).attr('id'));
		});
		var type = $('.broadcast-type-option').val();
		var company = $('.broadcast-company-option>option:selected').val();
		
		if(sites.length != 0) {
			$('.confirm-broadcast').removeAttr('disabled');
			$('#companysites').modal('hide');
		} else {
			BootstrapDialog.alert("Please select company/sites.");
		}
	});
	
	// CLEAR SELECTED SITES
	$(document).on('click', '.clear-sites', function(e) {
		$('.company-list').html('');
		$('.view-selected-companies_container').html('');
		$('.btn-select-sites').removeAttr('disabled');
	});
	
	// REMOVE SITES
	$(document).on('click', '.remove-selected-sites', function(e) {
		if($(this).attr('data-location') == '1') {
			var li_group = $(this).parent().parent().parent().children();
			
			if(li_group.length == 1) {
				$(this).parent().parent().parent().parent().remove();
			} else {
				$(this).parent().parent().remove();
			}
			
			var site_id = $(this).parent().parent().attr('id');
			var y = rightIds;
			var removeItem = site_id;
			
			y.splice($.inArray(removeItem,y), 1);
		} else if($(this).attr('data-location') == '2') {
			var li_group = $(this).parent().parent().parent().children();
			
			if(li_group.length == 1) {
				$(this).parent().parent().parent().parent().remove();
			} else {
				$(this).parent().parent().remove();
			}
			
			var site_id = $(this).parent().parent().attr('id');
			var y = rightIds2;
			var removeItem = site_id;
			
			y.splice($.inArray(removeItem,y), 1);
		} else if($(this).attr('data-location') == '3' ) {
			var li_group = $(this).parent().parent().parent().children();
			
			if(li_group.length == 1) {
				$(this).parent().parent().parent().parent().remove();
			} else {
				$(this).parent().parent().remove();
			}
			
			var site_id = $(this).parent().parent().attr('id');
			var y = rightIds3;
			var removeItem = site_id;
			
			y.splice($.inArray(removeItem,y), 1);
			
			// if(y.length == 1) {
			// } else {
				// $(this).parent().parent().parent().parent().remove();
				// var removeItem = site_id;
				
				// y.splice($.inArray(removeItem,y), 1);
			// }
		}
	});
	
	// COMPANY SITES LEFT SIDE BROADCAST
	var rightIds = [];
	$(document).on('click', '.company-sites-left', function(e) {
		var site_id = $(this).attr('id'); 
		var site_name = $(this).text();
		var company_id = $('.broadcast-company-option>option:selected').val();
		var company_name = $('.broadcast-company-option>option:selected').text();
		var leftIds = site_id;
		var index = $.inArray(leftIds, rightIds);
		var search = $('div').find("#"+company_id+'.company-list-group__name');
		
		if(site_id == 0) {
			if (index >= 0) {
			} else {
				rightIds = [];
				var li_group = $(this).parent().children();
				$('div#'+company_id+'.company-list-group__name').parent().html('');
				
				$('.company-sites-right').append('<div class="company-list-group"><div class="company-list-group__name" id="'+ company_id +'">'+ company_name +'</div><ul class="list-unstyled company-list"></ul></div>');
				
				$.each(li_group, function(idx, obj) {
					if(company_id == 0 || $(this).attr('id') != 0) {
						$('div#'+company_id+'.company-list-group__name').next().append('<li class="company-sites-right-select" id="'+ $(this).attr('id') +'"><a>'+ $(this).text() +' <button class="delete-company remove-selected-sites" data-location="1"></button></a></li>');
						
						rightIds.push($(this).attr('id'));
					}
				});
			}
		} else {
			if (index >= 0) {
			} else {
				if(search.length == 0) {
					$('.company-sites-right').append(selectCompany(site_id, site_name, company_id, company_name));
				} else {
					$('div#'+company_id+'.company-list-group__name').next().append('<li class="company-sites-right-select" id="'+ site_id +'"><a>'+ site_name +' <button class="delete-company remove-selected-sites" data-location="1"></button></a></li>');
				}
				rightIds.push(leftIds);
			}
		}
	});
	
	// COMPANY SITES LEFT SIDE DISASTER
	var rightIds2 = [];
	$(document).on('click', '.declare-company-sites-left', function(e) {
		var site_id = $(this).attr('id'); 
		var site_name = $(this).text();
		var company_id = $('.declare-company-option>option:selected').val();
		var company_name = $('.declare-company-option>option:selected').text();
		var leftIds = site_id;
		var index = $.inArray(leftIds, rightIds2);
		var search = $('div').find("#"+company_id+'.declare-company-list-group__name');
		
		if(site_id == 0) {
			if (index >= 0) {
			} else {
				rightIds2 = [];
				var li_group = $(this).parent().children();
				$('div#'+company_id+'.declare-company-list-group__name').parent().html('');
				
				$('.declare-company-sites-right').append('<div class="declare-company-list-group"><div class="declare-company-list-group__name" id="'+ company_id +'">'+ company_name +'</div><ul class="list-unstyled company-list"></ul></div>');
				
				$.each(li_group, function(idx, obj) {
					if(company_id == 0 || $(this).attr('id') != 0) {
						$('div#'+company_id+'.declare-company-list-group__name').next().append('<li class="declare-company-sites-right-select" id="'+ $(this).attr('id') +'"><a>'+ $(this).text() +' <button class="delete-company remove-selected-sites" data-location="2"></button></a></li>');
						
						rightIds2.push($(this).attr('id'));
					}
				});
			}
		} else {
			if (index >= 0) {
			} else {
				if(search.length == 0) {
					$('.declare-company-sites-right').append(selectDeclareCompany(site_id, site_name, company_id, company_name));
				} else {
					$('div#'+company_id+'.declare-company-list-group__name').next().append('<li class="declare-company-sites-right-select" id="'+ site_id +'"><a>'+ site_name +' <button class="delete-company remove-selected-sites" data-location="2"></button></a></li>');
				}
				rightIds2.push(leftIds);
			}
		}
	});
	
	// COMPANY SITES LEFT SIDE EDIT DISASTER
	var rightIds3 = [];
	$(document).on('click', '.declare-edit-company-sites-left', function(e) {
		var site_id = $(this).attr('id'); 
		var site_name = $(this).text();
		var company_id = $('.declare-edit-company-option>option:selected').val();
		var company_name = $('.declare-edit-company-option>option:selected').text();
		var leftIds = site_id;
		var index = $.inArray(leftIds, rightIds3);
		var search = $('div').find("#"+company_id+'.declare-edit-company-list-group__name');
		
		if(site_id == 0) {
			if (index >= 0) {
			} else {
				rightIds3 = [];
				var li_group = $(this).parent().children();
				$('div#'+company_id+'.declare-edit-company-list-group__name').parent().html('');
				
				$('.declare-edit-company-sites-right').append('<div class="declare-edit-company-list-group"><div class="declare-edit-company-list-group__name" id="'+ company_id +'">'+ company_name +'</div><ul class="list-unstyled company-list"></ul></div>');
				
				$.each(li_group, function(idx, obj) {
					if($(this).attr('id') != 0) {
						$('div#'+company_id+'.declare-edit-company-list-group__name').next().append('<li class="declare-edit-company-sites-right-select" id="'+ $(this).attr('id') +'"><a>'+ $(this).text() +' <button class="delete-company remove-selected-sites" data-location="3"></button></a></li>');
						
						rightIds3.push($(this).attr('id'));
					}
				});
			}
		} else {
			if (index >= 0) {
			} else {
				if(search.length == 0) {
					$('.declare-edit-company-sites-right').append(selectEditDeclareCompany(site_id, site_name, company_id, company_name));
				} else {
					$('div#'+company_id+'.declare-edit-company-list-group__name').next().append('<li class="declare-edit-company-sites-right-select" id="'+ site_id +'"><a>'+ site_name +' <button class="delete-company remove-selected-sites" data-location="3"></button></a></li>');
				}
				
				rightIds3.push(leftIds);
			}
		}
	});
	
	// BROADCAST COMPANY OPTION
	$(document).on('change', '.broadcast-company-option', function(e) {
		var company_id = $(this).val();
		
		$('.list-company-sites').html('');
		$.post(requestPath()+'dashboard/get_disaster_company', {company_id:company_id}, function(data) {
			$('.list-company-sites').append('<li class="company-sites-left" id="0"><a>All Sites</a></li>');
			
			$.each(data.data, function(idx, obj) {
				$('.list-company-sites').append(broadcastCompany(obj));
			});
		});
	});
	
	// DISASTER COMPANY OPTION
	$(document).on('change', '.declare-company-option', function(e) {
		var company_id = $(this).val();
		
		$('.declare-list-company-sites').html('');
		$.post(requestPath()+'dashboard/get_disaster_company', {company_id:company_id}, function(data) {
			$('.declare-list-company-sites').append('<li class="declare-company-sites-left" id="0"><a>All Sites</a></li>');
			
			$.each(data.data, function(idx, obj) {
				$('.declare-list-company-sites').append(disasterCompany(obj));
			});
		});
	});	
	
	// EDIT DISASTER COMPANY OPTION
	$(document).on('change', '.declare-edit-company-option', function(e) {
		var company_id = $(this).val();
		
		$('.declare-edit-list-company-sites').html('');
		$.post(requestPath()+'dashboard/get_disaster_company', {company_id:company_id}, function(data) {
			$('.declare-edit-list-company-sites').append('<li class="declare-edit-company-sites-left" id="0"><a>All Sites</a></li>');
			
			$.each(data.data, function(idx, obj) {
				$('.declare-edit-list-company-sites').append(disasterEditCompany(obj));
			});
		});
	});
	
	// LOADING SCREEN FADEOUT IF FINISHED LOADING
	$(document).ajaxComplete(function(event, jqxhr, settings) {
		if(settings.url !== requestPath()+'dashboard/get_twitter_feed') return;
		$('.spinner-container').fadeOut(1000);
	});
	
	// MODAL CLOSE
	$(document).on('click', '.modal-close', function(e) {
		$('#cmd-modal').modal('hide');
	});

	// SOCIAL FEEDS MODAL CLOSE
	$(document).on('click', '.social-modal-close', function(e) {
		$("#cmd-social-modal").modal("toggle");
	});
	
	// CANCEL DECLARE DISASTER
	$(document).on('click', '.btn-cancel-declare', function(e) {
		$('input[name=declare_disaster_name]').val('');
		$('.declare-disaster-type-option').val('');
		$('textarea[name=declare_disaster_details]').val('');
		$('.declare-company-option').val('');
		$('.declare-company-sites-left').html('');
		$('.declare-company-sites-right').html('');
		activaTab('dashboard');
	});

	function activaTab(tab){
			$('.nav-tabs--custom-dash a[href="#' + tab + '"]').tab('show');
	}
	
	$(document).on('click','.cmd-social-filter', function(e) {
        var screen_name = $('.select-screen-name>option:selected').val();
        console.log(screen_name);
        if (screen_name != '') {
	    	$.post(requestPath()+'dashboard/get_twitter_feed', {screen_name:screen_name}, function(data) {
        		$("cmd-social-modal").modal("toggle");
		        if(data) {
		        	$('.table-twitter-rss>tbody').html('');
					$.each(JSON.parse(data), function(idx, obj) {
						$.each(obj, function(i, o) {
							$('.table-twitter-rss>tbody').append('<tr><td class="image-container employee"><div><img src="'+o['user']['profile_image_url']+'" alt=""></div></td><td><div class="acctname">'+o['user']['name']+'</div><div class="time"><small>'+$.timeago(o['created_at'])+' @'+o['user']['screen_name']+'</small></div><div class="incident">'+o['text']+'</div></td></tr>');
						});
					});
				}
        	});
        }
	});
	

	var signin_filter = {"filter_type" : "employee", "datefrom" : $('.select-date-from').val(), "dateto" : $('.select-date-to').val(), "status":"", "company_id":""};
	var incident_filter = {"filter_type" : "report", "datefrom" : $('.select-date-from').val(), "dateto" : $('.select-date-to').val(), "company_id":""};
	var broadcast_filter = {"filter_type" : "incident", "datefrom" : $('.select-date-from').val(), "dateto" : $('.select-date-to').val(), "company_id":""};

	$(document).on('click', '.cmd-filter', function(e) {
		var filter_type = $(this).attr('data-from');
		var datefrom = $('.select-date-from').val();
		var dateto = $('.select-date-to').val();
		var status = $('.select-status>option:selected').val();
		var company_id = $('.select-company>option:selected').val();
		
		$.post(requestPath()+"dashboard/post_filter", {filter_type:filter_type, datefrom:datefrom, dateto:dateto, status:status, company_id:company_id}, function(data) {
			if (filter_type == "employee") {
				display_signin(data);
				signin_filter={"filter_type":"employee","datefrom":datefrom,"dateto":dateto,"status":status,"company_id":company_id};
			} else if (filter_type == "report") {
				display_report_incident(data);
				incident_filter={"filter_type":"report","datefrom":datefrom,"dateto":dateto,"company_id":company_id};
			} else if (filter_type == "incident") {
				display_broadcast(data);
				broadcast_filter={"filter_type":"incident","datefrom":datefrom,"dateto":dateto,"company_id":company_id};
			}
		});

		$('#cmd-modal').modal('hide');
	});

	$.post(requestPath()+"dashboard/get_emergency", {"disaster_id":($('.select-disaster').val() == "") ? 0 : $('.select-disaster').val()}, function(data){
		display_employee_status_count(data['data']);
	});

	//$.post(requestPath()+"dashboard/post_filter", signin_filter, function(data){
		//display_signin(data);
	//});


	var init_request_loaded = 0;

	setInterval(function(e){
		if(init_request_loaded == 1) {
			$.post(requestPath()+"dashboard/post_filter", incident_filter, function(data){
				display_report_incident(data);
			});
			$.post(requestPath()+"dashboard/post_filter", broadcast_filter, function(data){
				display_broadcast(data);
			});
			
			if($('input[name=disaster-checkbox]').is(':checked')) {
				// GET SIGNINS
				$.post(requestPath()+"dashboard/post_filter", signin_filter, function(data){
					display_signin(data);
				});

				// GET EMP STATUS COUNTS
				$.ajax({
					type: 'GET',
					url: requestPath()+'dashboard/get_emergency',
					success: function(data) {
						if(data['response_code'] == 0) {
							display_employee_status_count(data);
						}
					}
				});
			} else {
				var data = 0;
				display_employee_status_count(data);
				$.ajax({
					type: 'GET',
					url: requestPath()+'dashboard/get_emergency',
					success: function(data) {
					}
				});
			}
			$('.spinner-container').fadeOut(1000);
		}

		//SIGNIN+COUNT
		if ($("input[name=search_emp_db]").is(":visible")) {
			if ($("input[name=search_emp_db]").val() == 0) {
				$(".select-disaster").trigger("change");
			}
		} else {
			$(".select-disaster").trigger("change");
		}

		$.post(requestPath()+"dashboard/post_filter", incident_filter, function(data){
			display_report_incident(data);
		});

		$.post(requestPath()+"dashboard/post_filter", broadcast_filter, function(data){
			display_broadcast(data);
		});

	}, 10000);

	function display_employee_status_count(data) {
		$('.number-employee').html('');
		$('.graph-status-container').html('');

		var nostatus_count = data['nostatus_count'];
		var safe_count = data['safe_count'];
		var threat_count = data['threat_count'];
		var urgent_count = data['urgent_count'];
		var emp_count = data['employee_count'];
		//var calamity_count = (data != 0 ? data['data']['calamity_count'][0]['calamity_count'] : 0);
		//var reportin_count = (data != 0 ? data['data']['reportin_count'][0]['reportin_count'] : 0);
		
		$('.number-employee').prepend(emp_count+' <small>employees</small>');
		$('.th-nostatus').text(''+nostatus_count+'');
		$('.th-safe').text(''+safe_count+'');
		$('.th-threat').text(''+threat_count+'');
		$('.th-urgent').text(''+urgent_count+'');
		//$('.th-calamities').text(''+calamity_count+'');
		//$('.th-reportins').text(''+reportin_count+'');
		
		$('.graph-status-container').append('<ul class="pie"><li class="visualize pie-safe" data-value="'+safe_count+'" data-color="#9cbf3d"></li><li class="visualize pie-threat" data-value="'+threat_count+'" data-color="#f6b63a"></li><li class="visualize pie-urgent" data-value="'+urgent_count+'" data-color="#e15544"></li></li><li class="visualize pie-nostatus" data-value="'+nostatus_count+'" data-color="#333"></li></ul>');
		$('.pie').visualize({
			width: 300,
			type: 'pie',
			legend: true
		});
	}

	function display_signin (data) {
		$('.table-container-emp-status>table>tbody').html('');
		if (data.data.length > 0) {
			$.each(data['data'], function(idx, obj) {
				if(obj.img_path == '' || !obj.img_path) {
					obj.img_path = requestPath()+"assets/photo/default-profile.png";
				}
				var status = obj.status;
				if (status != "safe") {
					var append = '<tr id="'+obj.id+'"><td class="image-container employee"><div><img src="'+obj.img_path+'" alt></div></td><td>'+obj.emp_name+'</td><td><div class="employee-status status-'+obj.status+'">'+obj.status+'</div></td><td class="status-cell"><div class="dropdown"><button id="dLabel" type="button" class="btn btn-toggledown btn-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button><ul class="dropdown-menu dropdown dropdown-emp-db" roloe="menu" aria-labelledby="dLabel"><li class="status-safe" value="1">safe</li><li class="status-threat" value="2">threat</li><li class="status-urgent" value="3">urgent</li><li class="status-nostatus" value="0">no status</li></ul></div></td></tr>';
					$('.table-status>tbody').append(append);
				}
			});
		} else {
			var append = "<tr><td colspan=3 align='center'>No Sign-Ins yet</td>";
			$('.table-status>tbody').append(append);
		}
	}

	function display_report_incident (data) {
		$('.table-container-reports>table>tbody').html('');
		$.each(data['data'], function(idx, obj) {
			var append = '<tr><td><div class="name"><strong>'+ obj.emp_name +'</strong></div><div><small>'+ obj.location +'</small></div><div><small>'+ obj.timeago +'</small></div><div class="incident">'+ obj.message +'</div></td><td class="status-cell"></td></tr>';
			$('.table-report-in>tbody').append(append);
		});
	}

	function display_broadcast (data) {
		$('.table-container-broadcast>table>tbody').html('');
		$.each(data['data'], function(idx, obj) {
			var append = '<tr><td><div class="name">'+ obj.name +'</div><div class="incident">'+ obj.message +'</div></td></tr>';
			$('.table-broadcast>tbody').append(append);
		});
	}
	

	var emp_count_loaded = 0;
	$(document).on('change', '.select-disaster', function(e) {
		var selected_disaster = ($(".select-disaster").val() == "") ? 0 : $(".select-disaster").val();
		var type = 'search_disaster';

		if (signin_filter["id"] != selected_disaster)  {
			emp_count_loaded = 0;
			signin_filter["datefrom"] = "";
			signin_filter["dateto"] = "";
			signin_filter["company_id"] = "";
			signin_filter["status"] = "";
		} else {
			signin_filter["datefrom"] = filter_datefrom;
			signin_filter["dateto"] = filter_dateto;
			signin_filter["company_id"] = selected_company_id;
			signin_filter["status"] = selected_status;
		}

		signin_filter["id"] = selected_disaster;
		signin_filter["type"] = type;

		$.post(requestPath()+'dashboard/search_emp_db', signin_filter, function(data) {
			display_signin(data['data']);
			//if (emp_count_loaded == 0) {
				if (signin_filter["status"] != "nostatus") {
					display_employee_status_count(data['data']['count']);
				}
				affected_emp_companies = data['data']['company_list'];
				filter_datefrom = data["data"]["date_min"];
				filter_dateto = data["data"]["date_max"];
				$("#signin_datefrom").val(filter_datefrom);
				$("#signin_dateto").val(filter_dateto);
				emp_count_loaded = 1;
			//}
		//	signin_filter = post_data;
		});
	});

	$(".by-status").on("click", function(){
		check = $(this).is(":checked");
		if(check) {
			$('.select-status').removeAttr('disabled');
			$('.select-company').attr('disabled','disabled');
		} else {
			$('.select-status').attr('disabled','disabled');
			$('.select-company').removeAttr('disabled');
		}
	});
	
	$(".by-date").on("click", function(){
		check = $(this).is(":checked");
		if(check) {
			$('.select-date').removeAttr('disabled');
			$('.select-company').attr('disabled','disabled');
		} else {
			$('.select-date').attr('disabled','disabled');
			$('.select-company').removeAttr('disabled');
		}
	});
	
	// DROPDOWN FUNCTIONS
	$(document).on('click', '.dropdown-emp-db>li', function(e) {
		var id = $(this).parent().parent().parent().parent().attr('id');
		var status = $(this).val();
		var type = $(this).attr('class');

		if(id != 0) {		
			$('tr[id='+id+']>td>.employee-status').fadeOut(function() {
				$.post(requestPath()+'dashboard/change_status', {status:status,id:id}, function(data) {
					if(data['data'][0]['status'] == 0) {
						$('tr[id='+data['data'][0]['id']+']>td>.employee-status').css('background', '#666666').text('no status');
					} else if(data['data'][0]['status'] == 1) {
						$('tr[id='+data['data'][0]['id']+']>td>.employee-status').css('background', '#b6d7a8').text('safe');
					} else if(data['data'][0]['status'] == 2) {
						$('tr[id='+data['data'][0]['id']+']>td>.employee-status').css('background', '#fcca9b').text('threat');
					} else if(data['data'][0]['status'] == 3) {
						$('tr[id='+data['data'][0]['id']+']>td>.employee-status').css('background', '#e89999').text('urgent');
					}
				});
			}).fadeIn();
		} else {
			alert("Employee has not signed-in yet.");
		}
	});
	
	// SEARCH FUNCTIONS
	$(document).on('keypress', 'input[name=search_emp_db]', function(e) {
		var search = $(this).val();
		var type = 'search_disaster';
		var disaster_id = ($(".select-disaster").val() == "") ? 0 : $(".select-disaster").val();
		
		if(e.keyCode == 13)	 {
			signin_filter["search"] = search;

			$.post(requestPath()+'dashboard/search_emp_db', signin_filter, function(data) {
				display_signin(data['data']);
			});
		}
	});
	
	$(document).on('keypress', 'input[name=search_report_in]', function(e) {
		var search = $(this).val();
		var type = 'report';
		
		if(e.keyCode == 13) {
			incident_filter['search'] = search;
			$.post(requestPath()+"dashboard/post_filter", incident_filter, function(data){
				display_report_incident(data);
			});	
		}
	});
	
	$(document).on('keypress', 'input[name=search-incident]', function(e) {
		var search = $(this).val();
		var type = 'incident';
		
		if(e.keyCode == 13) {
			broadcast_filter['search'] = search;
			$.post(requestPath()+"dashboard/post_filter", broadcast_filter, function(data){
				display_broadcast(data);
			});
		}
	});

	$(document).on('keypress', 'input[name=search_social]', function(e) {
		var search = $(this).val();
		//var type = 'incident';
		
		if(e.keyCode == 13) {
			$.post(requestPath()+"dashboard/get_twitter_feed", {search:search}, function(data){
				$('.table-twitter-rss>tbody').html('');

				var feeds = JSON.parse(data);
		
				for(feed in feeds) {				
					$('.table-twitter-rss>tbody').append('<tr><td class="image-container employee"><div><img src="'+feeds[feed]['profile_image']+'" alt=""></div></td><td><div class="acctname">'+feeds[feed]['user_name']+'</div><div class="time"><small>'+$.timeago(feeds[feed]['created_at'])+' @'+feeds[feed]['screen_name']+'</small></div><div class="incident">'+feeds[feed]['text']+'</div></td></tr>');
				}
			});
		}
	});
	
	// VIEW/EDIT CURRENT DISASTER
	$(document).on('click', '.view-current-disaster', function(e) {
		var $this = $(this);
		var disaster_id = $(this).parent().parent().attr('id');
		var type = 'view';
		var mode = '';
		
		$.post(requestPath()+'dashboard/disaster_mode', {disaster_id:disaster_id, type:type, mode:mode}, function(data) {
			var leftIds = $.each(data.data.companies, function(idx, obj) { $.each(obj.sites, function(idx, obj) { rightIds3.push(obj.site_id) }); });
			
			BootstrapDialog.show({
				title: 'View Current Disaster',
				message: viewCurrentDisaster(data, companies, disaster_type),
				buttons: [{
					label: 'Save Disaster',
                    cssClass: 'btn-primary',
					action: function(dialog) {
						var $text = $('<div></div>');
						$text.append('<i class="fa fa-hand-paper-o fa-2x"></i> You are about to save this disaster. Are you sure you want to continue?');
						BootstrapDialog.show({
							type: BootstrapDialog.TYPE_PRIMARY,
							title: 'Save Edited Disaster',
							message: $text,
							buttons: [{
								label: 'Confirm',
								cssClass: 'btn-primary',
								autospin: true,
								action: function(d) {
									var type = 'edit';
									var name = $('input[name=declare_edit_disaster_name]').val();
									if($('.declare-edit-other-type').is(':checked')) {
										var disaster_type = $('.declare-edit-other-type-textbox').val();
									} else {
										var disaster_type = $('.declare-edit-disaster-type-option>option:selected').val();
									}
									var details = $('textarea[name=declare_edit_disaster_details]').val();
									var sites_selector = $('.declare-edit-company-sites-right-select');
									var sites = [];
									$.each(sites_selector, function(idx, obj) {
										sites.push({
											site_id: $(this).attr('id'), 
											company_id: $(this).parent().prev().attr('id')
										});
									});
									
									if(disaster_id == '' || type == '' || name == '' || disaster_type == '' || details == '' || sites == '') {
										BootstrapDialog.alert('Unable to edit blank fields.');
										d.close();
									} else {
										d.enableButtons(false);
										d.setClosable(false);
										d.getModalBody().html('Saving Disaster...');
										$.post(requestPath()+'dashboard/disaster_mode', {disaster_id:disaster_id, type:type, name:name, disaster_type:disaster_type, details:details, sites:sites}, function(data) {
											$(document).ajaxComplete(function(event, xhr, settings) {
												if(settings.url !== requestPath()+'dashboard/disaster_mode') return;
												d.close();
												dialog.close();
												rightIds2 = [];
											});
											BootstrapDialog.alert('Disaster has been successfully saved!');
											$this.parent().parent().fadeIn();
										});
									}
								}
							}, {
								label: 'Cancel',
								action: function(dialog) {
									dialog.close();
								}
							}]
						});
					}
				}, {
					label: 'End Disaster Mode',
                    cssClass: 'btn-danger',
					action: function(dialog) {
						var $text = $('<div></div>');
						$text.append('<i class="fa fa-trash-o fa-2x"></i> You are about to close this disaster. Are you sure you want to continue?');
						BootstrapDialog.show({
							type: BootstrapDialog.TYPE_DANGER,
							title: 'Close Disaster',
							message: $text,
							buttons: [{
								label: 'Confirm',
								cssClass: 'btn-danger',
								autospin: true,
								action: function(d) {
									var input_type = 'delete';
									d.enableButtons(false);
									d.setClosable(false);
									d.getModalBody().html('Closing Disaster...');
									$.post(requestPath()+'dashboard/disaster_mode', {disaster_id:disaster_id, type:input_type}, function(data) {
										$(document).ajaxComplete(function(event, xhr, settings) {
											if(settings.url !== requestPath()+'dashboard/disaster_mode') return;
											d.close();
											dialog.close();
										});
										BootstrapDialog.alert('Disaster has been closed!');
										$this.parent().parent().fadeOut();
									});
								}
							}, {
								label: 'Cancel',
								action: function(dialog) {
									dialog.close();
								}
							}]
						});
					}
				}, {
					label: 'Cancel',
					action: function(dialog) {
						dialog.close();
					}
				}]
			});
		});
	});
	
	$(document).on('click', '.declare-other-type', function(e) {
		if($(this).is(':checked')) {
			$('.declare-disaster-type-option').attr('disabled', true);
			$('.declare-other-type-textbox').removeClass('hide');
		} else {
			$('.declare-disaster-type-option').removeAttr('disabled');
			$('.declare-other-type-textbox').addClass('hide');
		}
	});
	
	$(document).on('click', '.declare-edit-other-type', function(e) {
		if($(this).is(':checked')) {
			$('.declare-edit-disaster-type-option').attr('disabled', true);
			$('.declare-edit-other-type-textbox').removeClass('hide');
		} else {
			$('.declare-edit-disaster-type-option').removeAttr('disabled');
			$('.declare-edit-other-type-textbox').addClass('hide');
		}
	});
	
	// DELETE CURRENT DISASTER
	$(document).on('click', '.delete-current-disaster', function(e) {
		var $this = $(this);
		var disaster_id = $(this).attr('id');
		var type = 'delete';
		var name = $this.parent().prev().prev().prev().text();
		var sites = [];
		$.each($this, function(idx, obj) {
			sites.push({
				site_id: $(this).attr('id')
			});
		});
		var $text = $('<div></div>');
        $text.append('<i class="fa fa-trash-o fa-2x"></i> You are about to delete this disaster. Are you sure you want to continue?');
		
		BootstrapDialog.show({
			type: BootstrapDialog.TYPE_DANGER,
			title: 'Confirm Delete',
			message: $text,
			buttons: [{
				label: 'Delete',
				cssClass: 'btn-danger',
				autospin: true,
				action: function(dialog) {
					dialog.enableButtons(false);
					dialog.setClosable(false);
					dialog.getModalBody().html('Deleting...');
					$.post(requestPath()+'dashboard/disaster_mode', {disaster_id:disaster_id, type:type}, function(data) {
						send_broadcast(name, sites);
						$(document).ajaxComplete(function(event, xhr, settings) {
							if(settings.url !== requestPath()+'dashboard/disaster_mode') return;
							dialog.close();
						});
						BootstrapDialog.alert('Disaster has been closed!');
						$this.parent().parent().fadeOut();
					});
				}
			}, {
				label: 'Cancel',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// DECLARE DISASTER FUNCTION
	$(document).on('click', '.btn-confirm-declare', function(e) {
		var name = $('input[name=declare_disaster_name]').val();
		var type = $('.declare-disaster-type-option>option:selected').val();
		if($('.declare-other-type').is(':checked')) {
			var type = $('.declare-other-type-textbox').val();
		} else {
			var type = $('.declare-disaster-type-option>option:selected').val();
		}
		var details = $('textarea[name=declare_disaster_details]').val();
		var sites_selector = $('.declare-company-sites-right-select');
		if($('.declare-company-option').val() == 0) {
			var sites = {company_id:'0'};
		} else {
			var sites = [];
			$.each(sites_selector, function(idx, obj) {
				sites.push({
					site_id: $(this).attr('id'), 
					company_id: $(this).parent().prev().attr('id')
				});
			});
		}
		
		if(name == '' || type == '' || details == '' || sites == '') {
			BootstrapDialog.alert('Please fill out the form completely.');
		} else {
			BootstrapDialog.show({
				type: BootstrapDialog.TYPE_PRIMARY,
				title: 'Confirm Declare Disaster',
				message: 'Are you sure you want to declare this disaster?',
				buttons: [{
					label: 'Send',
					cssClass: 'btn-primary',
					autospin: true,
					action: function(dialog) {
						dialog.enableButtons(false);
						dialog.setClosable(false);
						dialog.getModalBody().html('Sending...');
						$.post(requestPath()+'dashboard/disaster_mode', {name:name,type:type,details:details,sites:sites}, function(data) {
							if($('#declare-disaster-check-sms').is(':checked')) {
								send_declare_disaster(name, sites);
							}
							$(document).ajaxComplete(function(event, xhr, settings) {
								if(settings.url !== requestPath()+'dashboard/disaster_mode') return;
								$('input[name=declare_disaster_name]').val('');
								$('.declare-disaster-type-option').val('');
								$('.declare-other-type-textbox').val('');
								$('textarea[name=declare_disaster_details]').val('');
								$('.declare-company-sites-right').html('');
								$('.declare-company-option').val('');
								$('.declare-list-company-sites').html('');
								dialog.close();
							});
							BootstrapDialog.alert('Disaster has been declared!');
							$("#current-disaster-table>tbody").load(location.href + " #current-disaster-table>tbody>tr");
						});
					}
				}, {
					label: 'Cancel',
					action: function(dialog) {
						dialog.close();
					}
				}]
			});
		}
	});
	
	// BROADCAST FUNCTION
	$(document).on('click', '.broadcast-all-companies-check', function(e) {
		if($(this).is(":checked")) {
			$('.btn-select-sites').attr('disabled', true);
			$('.confirm-broadcast').removeAttr('disabled');
		} else {
			$('.btn-select-sites').removeAttr('disabled');
			$('.confirm-broadcast').attr('disabled', true);
		}
	});
	
	$(document).on('click', '.confirm-broadcast', function(e) {
		var msg = $('.broadcast-msg').val();
		var disaster_type = $('.broadcast-disaster-type').val();
		var sites_selector = $('.company-sites-right-select');
		// if($('.broadcast-company-option').val() == 0) {
			// var sites = {company_id:'0'};
		// } else {
			var sites = [];
			$.each(sites_selector, function(idx, obj) {
				sites.push({
					site_id: $(this).attr('id'), 
					company_id: $(this).parent().prev().attr('id')
				});
			});
		// }
        var ccb_mode = $('#ccb_mode').val();
        var ccb_mode_text = $('#ccb_mode option:selected').text();
		var $text = $('<div></div>');
        $text.append('You are about to send a broadcast. Are you sure you want to continue?');

		if(msg == '') {
			BootstrapDialog.alert("Please enter broadcast message.");
		} else {
			BootstrapDialog.show({
				type: BootstrapDialog.TYPE_PRIMARY,
				title: 'Confirm Broadcast',
				message: $text,
				buttons: [{
					label: 'Send',
					cssClass: 'btn-primary',
					autospin: true,
					action: function(dialog) {
						if($('.broadcast-sms-check').is(":checked")) {
							send_broadcast(msg, sites);
						}
						dialog.enableButtons(false);
						dialog.setClosable(false);
						dialog.getModalBody().html('Sending...');
						$.post(requestPath()+'dashboard/broadcast', {msg:msg, sites:sites}, function(data) {
							$(document).ajaxComplete(function(event, xhr, settings) {
								if(settings.url !== requestPath()+'dashboard/broadcast') return;
								dialog.close();
								$('.broadcast-msg').val('');
								$('.broadcast-company-option').val('');
								$('.company-sites-right').html('');
								$('.company-list-container').html('');
							});
							BootstrapDialog.alert('Broadcast sent successfully!');
						});
						console.log(sites)
					}
				}, {
					label: 'Cancel',
					action: function(dialog) {
						dialog.close();
					}
				}]
			});
		}
	});
	
	$(document).on('click', '.broadcast-send', function(e) {
		BootstrapDialog.show({
			title: 'Broadcast',
			message: 'Are you sure you want to broadcast this message?',
			buttons: [{
				label: 'Broadcast',
				cssClass: 'btn-confirm-broadcast',
				action: function(dialog) {
					dialog.close();
				}
			}, {
				label: 'Cancel',
				cssClass: 'btn-cancel-broadcast',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// SET INTERVAL FUNCTIONS
	var last = ($('.table-status>tbody>tr').first().attr('id') === undefined) ? '0': $('.table-status>tbody>tr').first().attr('id');
	
	// GET SOCIAL MEDIA FEEDS
	$('.table-twitter-rss>tbody').html('');
	$.get(requestPath()+'dashboard/get_twitter_feed', function(data) {
		var feeds = JSON.parse(data);
		
		for(feed in feeds) {
			var start = feeds[feed]["created_at"];
			var end = Date();

			// end - start returns difference in milliseconds 
			var diff = new Date(Date.parse(end) - Date.parse(start));

			// get days
			var days = diff/1000/60/60/24;

			
			$('.table-twitter-rss>tbody').append('<tr><td class="image-container employee"><div><img src="'+feeds[feed]['profile_image']+'" alt=""></div></td><td><div class="acctname">'+feeds[feed]['user_name']+'</div><div class="time"><small>'+$.timeago(feeds[feed]['created_at'])+' @'+feeds[feed]['screen_name']+'</small></div><div class="incident">'+feeds[feed]['text']+'</div></td></tr>');

		}
	});
	
	$(document).on('click', '.current-disaster', function(e) {
		$.get(requestPath()+'dashboard/get_current_disaster', function(data) {
			// BootstrapDialog.show({
				// title: 'Current Disaster',
				// message: showCurrentDisaster(current_disaster),
				// buttons: [{
					// label: 'Cancel',
					// action: function(dialog) {
						// dialog.close();
					// }
				// }]
			// });
			$('.current-disaster-table').html('');
			$('.current-disaster-table').append(showCurrentDisaster(current_disaster));
			$('.current-disaster-table').show();
			$('.main-container').hide();
		});
	});
	
	$(document).on('click', '.back-to-dashboard', function(e) {
		$('.main-container').show();
		$('.current-disaster-table').hide();
	});
	
	$(document).on('click', '.disaster-edit', function(e) {
		var disaster_id = $(this).parent().parent().attr('id');
		var type = 'edit';
		
		$.post(requestPath()+'dashboard/disaster_mode', {disaster_id:disaster_id, type:type}, function(data) {
			BootstrapDialog.show({
				title: 'Edit Current Disaster',
				message: currentDisaster(data, type),
				buttons: [{
					label: 'Save',
					action: function(dialog) {
						var name = $('input[name=edit_disaster_name]').val();
						var details = $('input[name=edit_disaster_details]').val();
						var company = $('input[name=edit_disaster_company]').val();
						
						$.post(requestPath()+'dashboard/disaster_mode', {type:type, disaster_id:disaster_id, name:name, details:details, company:company}, function(data) {
							console.log(data);
						});
					}
				}, {
					label: 'Cancel',
					action: function(dialog) {
						dialog.close();
					}
				}]
			});
		});
	});
	
	$(document).on('click', '.disaster-delete', function(e) {
		var id = $(this).parent().parent().attr('id');
		var type = 'delete';
		
		BootstrapDialog.show({
			title: 'Delete Current Disaster',
			message: 'Are you sure you want to delete this disaster?',
			buttons: [{
				label: 'Delete',
				action: function(dialog) {
					$.post(requestPath()+'dashboard/disaster_mode', {id:id, type:type}, function(data) {
						console.log(data)
					});
				}
			}, {
				label: 'Cancel',
				action: function(dialog) {
					dialog.close();
				}
			}]
		});
	});
	
	// EDIT DISASTER BROADCASTING
	$(document).on('click', '.declare-edit-broadcast', function(e) {
		var msg = $('.declare-edit-broadcast-msg').val();
		var sites_selector = $('.declare-edit-company-sites-right-select');
		var sites = [];
		$.each(sites_selector, function(idx, obj) {
			sites.push({
				site_id: $(this).attr('id'), 
				company_id: $(this).parent().prev().attr('id')
			});
		});
		
		if(msg == '' || sites == 0) {
			alert("Please add broadcast message/sites.");
		} else {
			BootstrapDialog.show({
				title: 'Edit Disaster Broadcast',
				message: 'Are you sure you want to broadcast this message?',
				buttons: [{
					label: 'Send',
					autospin: true,
					action: function(dialog) {
						dialog.enableButtons(false);
						dialog.setClosable(false);
						dialog.getModalBody().html('Sending...');
						$.post(requestPath()+'dashboard/broadcast', {msg:msg, sites:sites}, function(data) {
							if($('.view-disaster-check').is(':checked')) {
								send_broadcast(name, sites);
							}
							$(document).ajaxComplete(function(event, xhr, settings) {
								if(settings.url !== requestPath()+'dashboard/broadcast') return;
								dialog.close();
							});
							BootstrapDialog.alert('Broadcast sent successfully!');
						});
					}
				}, {
					label: 'Cancel',
					action: function(dialog) {
						dialog.close();
					}
				}]
			});
		}
	});
	
	// FILTER DISASTER NAME
	$(document).on('change', '.filter-type-disaster-option', function(e) {
		var disaster_type = $('.filter-type-disaster-option>option:selected').val();
		var type = 'view';
		var mode = 'filter';
		
		$.post(requestPath()+'dashboard/disaster_mode', {disaster_type:disaster_type, type:type, mode:mode}, function(data) {
			$('.current-disaster-table>tbody').html('');
			$.each(data.data, function(idx, obj) {
				$('.current-disaster-table>tbody').append(filterDisaster(obj));
			});
		});
	});
	
	$(document).on('click', '.current-search-go', function(e) {
		var search = $('.current-search-text').val();
		var type = 'view';
		var mode = 'search';
		
		$.post(requestPath()+'dashboard/disaster_mode', {search:search, type:type, mode:mode}, function(data) {
			$('.current-disaster-table>tbody').html('');
			$.each(data.data, function(idx, obj) {
				$('.current-disaster-table>tbody').append(filterDisaster(obj));
			});
		});
	});

    /*--- SMS Integration ---*/
    function send_broadcast(msg, sites) {
        $.post(
            requestPath() + 'sms/commandcenter/broadcast',
            {data:msg, sites:sites},
            function(data) {
                if (data == "ok") {
					console.log("Successfully sent via SMS.");
				}
            });
    }
	
	function send_declare_disaster(msg, sites) {
		$.post(
			requestPath() + 'sms/commandcenter/disaster-declared',
			{data:msg, sites:sites},
			function(data) {
				if (data == "ok") {
					console.log("Successfully sent via SMS.");
				}
			});
	}

    function send_declare_disaster(msg, sites) {
        $.post(
            requestPath() + 'sms/commandcenter/disaster-declared',
            {data:msg, sites:sites},
            function(data) {
                if (data == "ok") {
                    console.log("Successfully sent via SMS.");
                }
            });
    }

    function send_disaster_toggled_on_all() {
        $.ajax({
            method: "GET",
            url: requestPath() + "sms/commandcenter/toggle-on"
        }).done(function(data){
            if(data != "none"){
                BootstrapDialog.show({
                    title: 'SMS Notification',
                    type: BootstrapDialog.TYPE_WARNING,
                    message:data,
                    buttons: [{
                        label: 'Close',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
            }
        });
    }

    function send_disaster_toggled_off_all() {
        $.ajax({
            method: "GET",
            url: requestPath() + "sms/commandcenter/toggle-off"
        }).done(function(data){
            if(data != "none"){
                BootstrapDialog.show({
                    title: 'SMS Notification',
                    type: BootstrapDialog.TYPE_WARNING,
                    message:data,
                    buttons: [{
                        label: 'Close',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
            }
        });
    }

});