/*
Project Name: AGC Locator
Project Version: 1
Owner: Yondu
Website: http://yondu.com
front-end Developer: UXD-Team
                     Reynier Carlo Nebres
*/
(function ($) {

	/**** TOGGLE STATUS *******/
	$('.dropdown li').each(function(){
		$(this).click(function(){

			var text = $(this).html();
			$(this).parent().next('input').val(text);
		});
	});




	/**** TOGGLE SEARCH *******/
	$('.btn[data-toggle="search"]').each(function(){
		$(this).click(function(){
			$(this).next(".search-holder").slideToggle();
		})
	})





	$.fn.currentDateTime = function(){

		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;

		var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

		var date = new Date();

		var month = months[date.getMonth()];

		var day = date.getDate();

		var weekday = weekdays[date.getDay()];

		var fullDate = month + ' '+(day<10 ? '0' + day : day) +', '+date.getFullYear();

		$('.dayToday').text(weekday+', ');

		$('.dateToday').text(fullDate);

		setInterval(function(){

			var currentTime = new Date ();

			var currentHours = currentTime.getHours ();

			var currentMinutes = currentTime.getMinutes ();

			var currentSeconds = currentTime.getSeconds ();

			currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;

			currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

			//currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

			//currentHours = ( currentHours == 0 ) ? 12 : currentHours;

			var currentTimeString = currentHours + ":" + currentMinutes;
			 
			$(".currentTime").text(currentTimeString);

		}, 1000);

	}
	
	$('.btn-tab').each(function(){
        $(this).click(function(){
            var nav_reference = $(this).attr('data-navref');
            
             $('.nav-tabs li:nth-child('+nav_reference+')').find('a').trigger('click');
        });
    });
    

    // $('.trigger-show').click(function() {
        // var $this = $(this);
        // var triggershow = $this.attr('data-triggershow');
        
        // if ($this.is(':checked')) {
            // $('.'+triggershow).removeClass('hide');    
        // } else {
           // $('.'+triggershow).addClass('hide'); 
        // }
    // });
	
	// $('.declare-sms-check').click(function() {
        // var $this = $(this);
        // var triggershow = $this.attr('data-triggershow');
        
        // if ($this.is(':checked')) {
            // $('.'+triggershow).removeClass('hide');    
        // } else {
           // $('.'+triggershow).addClass('hide'); 
        // }
    // });

})(jQuery);