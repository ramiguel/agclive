var companies = [];
var reports = [];
var sql_statement = "";
var headers = "";

getCompanyList();

var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        matches = [];

        substrRegex = new RegExp(q, 'i');

        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};

$(".btn-go").on("click", function(){
    if($("#filter").val() != "") {
        getSearchedReportsOf();
        $(".btn-submit").removeAttr("disabled");
    } else {
        $("#filter").focus();
    }

});

$("#generate_report").validate({
    rules: {
        reports: {
            required: true
        }
    }, errorPlacement: function(error, element) {
        error.insertAfter(element.parent());
    }, submitHandler: function(form) {
        var spinner = new Spinner().spin($("body")[0]);
        var report = $("#reports").val();

        if(report == null || report === undefined) {
            showWarningDialog("Please select a specific report to be generated.", "#reports");
        } else {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: requestPath() + "backoffice/reports/get-selected",
                data: {"data": JSON.stringify(report)},
                success: function (data, status, xhr) {
                    if(data.response_code == 0) {
                        $(".btn-export").removeAttr("disabled");
                        $(".btn-export-xls").removeAttr("disabled");
                        sql_statement = data.sql_statement;
                        sql_statement = sql_statement.replace(/\n|\r/g, " ");
                        headers = data.headers;
                        appendHeader();
                        spinner.stop();
                    } else {
                        console.log("error");
                        spinner.stop();
                    }
                }, error: function(data) {
                    console.log(data);
                    spinner.stop();
                }
            });

        }
    }
});

$(".btn-export").on('click',function() {
    var id = $("#reports").val();
    redirectTo(requestPath()+"backoffice/reports/export/" + id);
//    var spinner = new Spinner().spin($("body")[0]);
//    $.ajax({
//        type: "POST",
//        dataType: "json",
//        url: requestPath() + "backoffice/reports/export",
//        data: {
//            "sql_statement" : JSON.stringify(sql_statement),
//            "headers": JSON.stringify(headers)
//        },
//        success: function(data, status, xhr) {
//            console.log("success");
//            spinner.stop();
//        },
//        error: function (xhr, status, error) {
//            console.log(error);
//            spinner.stop();
//        }
//    });
});

$(".btn-export-xls").on("click", function() {
    console.log('click');
    var spinner = new Spinner().spin($("body")[0]);
    $.ajax({
        type: "POST",
        dataType: "json",
        url: requestPath() + "backoffice/reports/export-xls",
        data : {
            "sql_statement" : JSON.stringify(sql_statement),
            "headers": JSON.stringify(headers)
        }, success: function(data, status, xhr) {
            spinner.stop();
        }, error: function (xhr, status, error) {
            console.log(error);
            spinner.stop();
        }
    });
});

function postQueryParams(params) {
    return JSON.stringify(params);
}

function redirectTo(url) {
    window.location = url;
}

function appendHeader() {
    var spinner = new Spinner().spin($("body")[0]);
    var header_split = headers.split(",");
    var head = "<tr>";
    var head_close = "</tr>";
    var head_items = "";

    $.each(header_split, function (i, item) {
        head_items += "<th data-field='"+ item +"'>"+toTitleCase(item)+"</th>";
    });

    $(".no-records-found").empty();
    $("#table thead").empty();
    $("#table thead").append(head + head_items + head_close);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: requestPath() + "backoffice/reports/get-items",
        data: {"data": JSON.stringify(sql_statement)},
        success: function (data, status, xhr) {
            if(data.response_code == 0) {
                fillReportDetails(data.result);
                console.log(sql_statement);
                console.log(data.result);
                spinner.stop();
            } else {
                console.log("error");
                spinner.stop();
            }
        }, error: function(data) {
            console.log(data);
            spinner.stop();
        }
    });
}

function fillReportDetails(result) {
    var header_split = headers.split(",");
    var append_items = "";

    $("#table tbody").empty();

    $.each(result, function (i, item) {
        var counter = 0;
        var item_length = Object.keys(item).length;

        $.each(item, function (j, jitem) {
            counter += 1;
            append_items +=
                "<td style>" +
                jitem +
                "</td>";
        });

        $('#table tbody').append("<tr data-index='"+ i +"'>" +
            append_items +
        "</tr>");

        append_items = "";
    });
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function requestPath() {
    split_path = window.location.pathname.split('/');
    if($.trim(split_path[1]) == 'index.php') {
        return window.location.origin+'/';
    } else {
        return window.location.origin+'/'+split_path[1]+'/';
    }
}

function getCompanyList() {
    var filter_string = $("#filter").val();
    var spinner = new Spinner().spin($("body")[0]);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: requestPath() + "backoffice/reports/get-list",
        data: JSON.stringify(filter_string),
        success: function (data, status, xhr) {
            companies = data;


            $('#the-basics .typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'companies',
                    source: substringMatcher(companies)
                });

            spinner.stop();
        }, error: function(xhr, status, error) {
            console.log(error);
            spinner.stop();
        }
    });
}

function getSearchedReportsOf() {
    var filter_string = $("#filter").val();
    var spinner = new Spinner().spin($("body")[0]);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: requestPath() + "backoffice/reports/get-searched",
        data: {"data": JSON.stringify(filter_string)},
        success: function (data, status, xhr) {
            if(data.response_code == 0) {
                $('#reports').empty();
                $.each(data.report_list, function (i, item) {
                    $('#reports').append($('<option>', {
                        value: item.id,
                        text : item.name
                    }));
                });

                spinner.stop();
            } else {
                console.log("error");
            }
            spinner.stop();
        }, error: function(data) {
            console.log(data);
            spinner.stop();
        }
    });
}

function showWarningDialog(msg, location) {
    BootstrapDialog.show({
        title: 'Error',
        type: BootstrapDialog.TYPE_WARNING,
        message:msg,
        buttons: [{
            label: 'Close',
            action: function(dialog) {
                dialog.close();
                $(location).focus();
            }
        }]
    });
}
