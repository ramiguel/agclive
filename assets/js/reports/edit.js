function redirectTo(url) {
    window.location = url;
}

function requestPath() {
    split_path = window.location.pathname.split('/');
    if($.trim(split_path[1]) == 'index.php') {
        return window.location.origin+'/';
    } else {
        return window.location.origin+'/'+split_path[1]+'/';
    }
}

$(document).ready(function(){
    $("#report_maintenance").validate({
        rules: {
            name: {
                required: true
            },
            sql_statement: {
                required: true
            },
            headers: {
                required: true
            }
        }, errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        }, submitHandler:function(form) {

            //$('button[type="submit"]').attr("disabled", "disabled");

            $.post(requestPath() + "backoffice/maintenance/edit",
                $(form).serialize(),
                function(response) {
                    var data = JSON.parse(response);
                    if(data.response_code == 0) {
                        BootstrapDialog.show({
                            title: 'Success',
                            closable: false,
                            message:data.response_msg,
                            buttons: [{
                                label: 'Close',
                                action: function(dialog) {
                                    dialog.close();
                                    window.location = "{{base_url}}/backoffice/maintenance";
                                }
                            }]
                        });
                    } else {
                        BootstrapDialog.show({
                            title: 'Error',
                            type: BootstrapDialog.TYPE_WARNING,
                            message:data.response_msg,
                            closable: false,
                            buttons: [{
                                label: 'Close',
                                action: function(dialog) {
                                    $('button[type="submit"]').removeAttr("disabled");
                                    dialog.close();
                                }
                            }]
                        });
                    }
                })
                .error(function() {
                    BootstrapDialog.show({
                        title: 'Error',
                        type: BootstrapDialog.TYPE_WARNING,
                        message:data.response_msg,
                        closable: false,
                        buttons: [{
                            label: 'Close',
                            action: function(dialog) {
                                $('button[type="submit"]').removeAttr("disabled");
                                dialog.close();
                            }
                        }]
                    });
                });

        }
    });
});