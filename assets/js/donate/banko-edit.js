$(document).ready(function() {
    $("#company").on("change", function () {
        company_id = $(this).val();
        var data = {
            company_id: company_id
        };
        var spinner = new Spinner().spin($("body")[0]);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: requestPath() + "backoffice/gcash/get-department",
            data: JSON.stringify(data),
            success: function (data, status, xhr) {
                if (data.response_code == 0) {
                    var body = "<option value=\"\">Select Department</option>";
                    $.each(data.data, function (index, item) {
                        body = body + "<option value=\"" + item.id + "\">" + item.department_name + "</option>";
                    });
                    $("#department").html(body);
                    get_employee(company_id);
                } else {
                    $("#department").html("<option value=\"\">Select Department</option>");

                    $("#employee").html("<option value=\"\">Select Employee</option>");
                }
                spinner.stop();
            },
            error: function (xhr, status, error) {
                console.log(error);
                spinner.stop();
            }
        });
    });

    $("#employee").on("change", function () {
        //clearAll();
    });

    $("#banko-donate").validate({
        rules: {
            account_no: {
                required: true
            }
        }, errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        }, submitHandler: function(form) {
            var spinner = new Spinner().spin($("body")[0]);
            var account_no = $("#account_no").val();
            var employee_id = $("#employee").val();

            $.post(
                requestPath()+ "backoffice/banko/check-if-existing-edit", JSON.stringify({
                    data: account_no,
                    employee_id: employee_id
                }),
                function(data) {
                    if(data.trim() == "ok") {

                        $('button[type="submit"]').attr("disabled", "disabled");
                        $.post(
                            requestPath() + "backoffice/banko/edit/"+requestId(), $(form).serialize(), function (response) {
                                var data = JSON.parse(response);
                                spinner.stop();

                                if (data.response_code == 0) {
                                    showSuccessDialog("Banko account successfully added.");
                                } else {
                                    showWarningDialog("Failed to add Banko account.", "#banko-donate");
                                }
                                $('button[type="submit"]').removeAttr("disabled");
                            }
                        );

                    } else {
                        spinner.stop();
                        showWarningDialog("Banko Account is already in used. Please try again.", "#account_no");
                    }
                }
            );

        }
    });

    function requestPath() {
        split_path = window.location.pathname.split('/');
        if($.trim(split_path[1]) == 'index.php') {
            return window.location.origin+'/';
        } else {
            return window.location.origin+'/'+split_path[1]+'/';
        }
    }

    function requestId() {
        split_path = window.location.pathname.split('/');
        return split_path[5];
    }

    function get_employee(company_id) {
        $.post(requestPath()+"backoffice/gcash/get-employees", JSON.stringify({
            company_id: company_id
        }), function (data) {
            if (data.response_code == 0) {
                var body = "<option value=\"\">Select Employees</option>";
                $.each(data.data, function (index, item) {
                    body = body + "<option value=\"" + item.id + "\">" + item.employee_name + "</option>";
                });

                $("#employee").html(body);
            } else {
                $("#employee").html("<option value=\"\">Select Employees</option>");
            }
        });
    }

    function showWarningDialog(msg, location) {
        BootstrapDialog.show({
            title: 'Error',
            type: BootstrapDialog.TYPE_WARNING,
            message:msg,
            buttons: [{
                label: 'Close',
                action: function(dialog) {
                    dialog.close();
                    $(location).focus();
                }
            }]
        });
    }

    function showSuccessDialog(msg) {
        BootstrapDialog.show({
            title: 'Success',
            message:msg,
            buttons: [{
                label: 'Close',
                action: function(dialog) {
                    dialog.close();
                    window.location = requestPath() + "backoffice/banko";
                }
            }]
        });
    }
});