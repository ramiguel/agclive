function redirectTo(url) {
    window.location = url;
}

function postQueryParams(params) {
    return JSON.stringify(params);
}

function requestPath() {
    split_path = window.location.pathname.split('/');
    if($.trim(split_path[1]) == 'index.php') {
        return window.location.origin+'/';
    } else {
        return window.location.origin+'/'+split_path[1]+'/';
    }
}

