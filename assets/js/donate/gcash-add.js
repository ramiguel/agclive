$(document).ready(function() {
    $("#company").on("change", function () {
        company_id = $(this).val();
        var data = {
            company_id: company_id
        };
        var spinner = new Spinner().spin($("body")[0]);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: requestPath() + "backoffice/gcash/get-department",
            data: JSON.stringify(data),
            success: function (data, status, xhr) {
                if (data.response_code == 0) {
                    var body = "<option value=\"\">Select Department</option>";
                    $.each(data.data, function (index, item) {
                        body = body + "<option value=\"" + item.id + "\">" + item.department_name + "</option>";
                    });
                    $("#department").html(body);
                    get_employee(company_id);
                } else {
                    $("#department").html("<option value=\"\">Select Department</option>");

                    $("#employee").html("<option value=\"\">Select Employee</option>");
                }
                spinner.stop();
            },
            error: function (xhr, status, error) {
                console.log(error);
                spinner.stop();
            }
        });
    });

    $("#employee").on("change", function() {
        clearAll();
    });

    $("#current").on("click", function() {
        if($("#employee").val() != "0") {
            if ($(this).is(":checked")) {
                //$("#mobile_no").attr('disabled', 'disabled');

                get_existing_mobile($("#employee").val());

            } else {
                $("#mobile_no").removeAttr("disabled");
            }
        } else {
            showWarningDialog("Please select an employee.", "#employee");
            $(this).prop("checked", false);
        }
    });

    $("#gcash-donate").validate({
        rules: {
            mobile_no: {
                required: true
            }
        }, errorPlacement: function(error, element){
            error.insertAfter(element.parent());
        }, submitHandler: function(form) {
            var spinner = new Spinner().spin($("body")[0]);
            var globe = false;
            var mobile_no = $("#mobile_no").val();
            var employee_id = $("#employee").val();

            $.post(
                requestPath()+ "backoffice/gcash/check-prefix",JSON.stringify({
                    data : mobile_no
                }),
                function(data) {
                    if(data.trim() == "ok") {

                        $.post(
                            requestPath()+ "backoffice/gcash/check-if-existing",JSON.stringify({
                                data : mobile_no
                            }),
                            function(data) {
                                if (data.trim() == "ok") {

                                    $.post(
                                        requestPath()+ "backoffice/gcash/check-if-already-added",JSON.stringify({
                                            data : mobile_no,
                                            employee_id : employee_id
                                        }),
                                        function(data) {
                                            if (data.trim() == "ok") {
                                                $('button[type="submit"]').attr("disabled", "disabled");
                                                $.post(
                                                    requestPath()+"backoffice/gcash/add", $(form).serialize(), function(response){
                                                    var data = JSON.parse(response);
                                                    spinner.stop();

                                                    if(data.response_code == 0) {
                                                        showSuccessDialog("GCash account added successfully.");
                                                    } else {
                                                        showWarningDialog("Failed to add GCash account","#gcash-donate");
                                                    }
                                                    $('button[type="submit"]').removeAttr("disabled");
                                                })
                                            } else {
                                                spinner.stop();
                                                showWarningDialog("GCash account for this employee is already registered. Please try again.", "#employee");
                                            }
                                        }
                                    );
                                } else {
                                    spinner.stop();
                                    showWarningDialog("GCash account number is already in used. Please try again.", "#mobile_no");
                                }
                            }
                        );
                    } else {
                        spinner.stop();
                        showWarningDialog("Mobile number used should be Globe-issued. Please try again.", "#mobile_no");
                    }
                }
            );
        }
    });

    function requestPath() {
        split_path = window.location.pathname.split('/');
        if($.trim(split_path[1]) == 'index.php') {
            return window.location.origin+'/';
        } else {
            return window.location.origin+'/'+split_path[1]+'/';
        }
    }

    function get_employee(company_id) {
        $.post(requestPath()+"backoffice/gcash/get-employees", JSON.stringify({
            company_id: company_id
        }), function (data) {
            if (data.response_code == 0) {
                var body = "<option value=\"\">Select Employees</option>";
                $.each(data.data, function (index, item) {
                    body = body + "<option value=\"" + item.id + "\">" + item.employee_name + "</option>";
                });

                $("#employee").html(body);
            } else {
                $("#employee").html("<option value=\"\">Select Employees</option>");
            }
        });
    }

    function get_existing_mobile(employee_id) {
        var spinner = new Spinner().spin($("body")[0]);
        $.post(requestPath()+"backoffice/gcash/get-mobile", JSON.stringify({
            employee_id : employee_id
        }), function(data) {
            if(data.response_code == 0) {
                $("#mobile_no").val(data.data[0].mobile_no);
                $("#mobile_no").text(data.data[0].mobile_no);
            } else {
                $("#mobile_no").val('');
            }
            spinner.stop();
        });
    }

    function clearAll() {
        $("#mobile_no").val('');
        $("#mobile_no").removeAttr("disabled");
        $("#current").prop("checked", false);
    }

    function showWarningDialog(msg, location) {
        BootstrapDialog.show({
            title: 'Error',
            type: BootstrapDialog.TYPE_WARNING,
            message:msg,
            buttons: [{
                label: 'Close',
                action: function(dialog) {
                    dialog.close();
                    $(location).focus();
                }
            }]
        });
    }

    function showSuccessDialog(msg) {
        BootstrapDialog.show({
            title: 'Success',
            message:msg,
            buttons: [{
                label: 'Close',
                action: function(dialog) {
                    dialog.close();
                    window.location = requestPath() + "backoffice/gcash";
                }
            }]
        });
    }
});