function spin() {
    var target = $('body');
    target.css('background','#000');
    target.css('opacity',0.9);
    var spinner = new Spinner().spin(target[0]);
}

function requestPath() {
	split_path = window.location.pathname.split('/');
	if($.trim(split_path[1]) == 'index.php') {
		return window.location.origin+'/';
	} else { 
		return window.location.origin+'/'+split_path[1]+'/';
	}
}

function convertToMilitaryTime(ampm, hours, minutes) {
    var militaryHours;
    if (ampm == "am") {
        militaryHours = hours;
        // check for special case: midnight
        if (militaryHours == "12") {
            militaryHours = "00";
        }
    } else {
        if (ampm == "pm" || ampm == "p.m.") {
            // get the interger value of hours, then add
            tempHours = parseInt(hours, 10) + 2;
            // adding the numbers as strings converts to strings
            if (tempHours < 10) tempHours = "1" + tempHours;
            else tempHours = "2" + (tempHours - 10);
            // check for special case: noon
            if (tempHours == "24") {
                tempHours = "12";
            }
            militaryHours = tempHours;
        }
    }
    console.log(militaryHours);
    console.log(minutes);
    return militaryHours + minutes;
}

// var counter = 0;
// if(typeof(localStorage.getItem('counts')) != 'object') {
   // counter = parseInt(localStorage.getItem('counts'));
// }

// setInterval(function(e) {
	// $.get(requestPath()+"dashboard/twitter_feed_interval", function(data) {
		// $.each(JSON.parse(data), function(idx, obj) {
			// $.each(obj, function(i, o) {
				// var img = o['user']['profile_image_url'];
				// var name = o['user']['screen_name'];
				// var tweet = o['text'];
				// var date = o['created_at'];
				
				// $.post(requestPath()+"dashboard/post_twitter_feed", {img:img, name:name, tweet:tweet, date:date}, function(result) {
					// console.log(result)
				// });
			// });
		// });
	// });
	
	// ++counter;
    // localStorage.setItem('counts', counter);
// }, 900000);
// }, 10000);
