 <?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * User: Cris del Rosario
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

define ("DB_HOST","localhost");
define ("DB_USER","agc");
define ("DB_PASS","@gcU$3r");
define ("DB_NAME","agcdb");

/* end of database configuration */

define ("BASE_URL","http://52.74.229.97/");

define ("FILE_PATH", $_SERVER['DOCUMENT_ROOT']."assets/files/");

/***********************************************************************************************************
 *
 * This can be our first page/redirect page/first controller
 *
 **********************************************************************************************************/

define ("ENTRY_POINT", "/checkpoint/index");

/***********************************************************************************************************
 * Email/SMTP Configuration
 *
 ***********************************************************************************************************/

define ("MAIL_HOST","smtp.gmail.com");
define ("MAIL_PORT",587);

define ("MAIL_USER","assist.agc@gmail.com");
define ("MAIL_PASS","fr@nky3e@ll!$!$");

define ("MAIL_FROM","assist.agc@yondu.com");
define ("MAIL_FROM_NAME","ASSIST AGC Employee Locator");

define ("MAIL_REPLY_TO","assist.agc@yondu.com");
define ("MAIL_REPLY_TO_NAME","ASSIST AGC Employee Locator");

/***********************************************************************************************************
 * IOS Push Notification Certificate URL
 *
 ***********************************************************************************************************/
 
define ("IOS_PUSH_URL", BASEPATH . "cert/AGC_APNS_certificates/pem/AGCAssist_prod_apns.pem");
define ("IOS_PUSH_PASSWORD", "agclocator");

/***********************************************************************************************************
 * Google API KEY for GCM push notifications
 *
 ***********************************************************************************************************/

define("GOOGLE_API_KEY", "AIzaSyCtCUlgY3lD76nsfH7bZMZFeGZ2Y-NP3Nc"); // Place your Google API Key

/***********************************************************************************************************
 * Twitter
 *
 ***********************************************************************************************************/

define ("CONSUMER_KEY","Orwwqcl3MDTWAI5LzlCNsFMrn");
define ("CONSUMER_SECRET"," YtyPjJLJ5Easv7PwnOZ6rSifQAC2BgYsdOkG3pZkHbIQUn72Io");
define ("ACCESS_TOKEN","446935197-nc1O62y3DYBBWkyJbupnbAP8mJsrDmzkKjvlg5e1");
define ("ACCESS_TOKEN_SECRET","xJZfegUr3J2BSD2476Hml5jhOkejCcHR0WJevJ4gdrGeT");

/***********************************************************************************************************
  * Globelabs
  *
  ***********************************************************************************************************/
 define ("SMS_KEYWORD_REG", "REG");
 define ("SMS_KEYWORD_SIGNIN", "SIGNIN");
 define ("SMS_KEYWORD_HELP", "HELP");
 define ("SMS_KEYWORD_GETPASS", "GETPASSWORD");
 define ("SMS_SAFE", "SAFE");
 define ("SMS_THREAT", "THREAT");
 define ("SMS_URGENT", "URGENT");
 define ("SMS_YES", "YES");
 define ("SMS_NO", "NO");

 /* --- Test Account 1 --- */
 // define ("SHORT_CODE", "0515");
 // define ("SHORT_CODE_CROSS", "0580515");
 // define ("APP_ID", "oorgSgXxGEu67Ten78ixxzu8ooRaS6Rz");
 // define ("APP_SECRET", "4878f4839657529be3f07fd489f33c1a6bce6d0d4dcada862689bfc491b153b3");
 // define ("GL_VERSION", "v1");
 // define ("PASSPHRASE", "AyalaASSIST2015");
 // define ("CORRELATOR", "12345");

 // /* --- Production Account --- */
 define ("SHORT_CODE", "4357");
 define ("SHORT_CODE_CROSS", "0584357");
 define ("APP_ID", "qRkECx8RKpu4RTonALiRaxuGzR95Ca67");
 define ("APP_SECRET", "18376adf2280c4be8c41cfa7192e37605036c33f09ebe6818b6181129eeae977");
 define ("GL_VERSION", "v1");
 define ("PASSPHRASE", "27L99L2W5b");
 define ("CORRELATOR", "12345");

 define ("MSG_CCB_PREFIX", "Command Center Broadcast: ");
 define ("MSG_DISASTER_TOGGLED_ON", "Disaster Mode is toggled on");
 define ("MSG_DISASTER_TOGGLED_OFF", "Disaster Mode is toggled off");

 define ("MSG_REG_SUCCESS", "Congratulations! You have successfully registered to ASSIST!");
 define ("MSG_REG_WRONG_FORMAT", "You have used the wrong format. Please follow the format below:\n\nREG\<sp\>\<LastName\>");
 define ("MSG_REG_NOT_WHITELISTED", "The credentials that you have provided are not whitelisted. Please try again.");
 define ("MSG_REG_ALREADY", "The credentials that you have provided are already registered.");
 define ("MSG_REG_NON_GLOBE", "The mobile number that you have provided is a non-globe number. Please try again.");
 define ("MSG_REG_ERROR", "There was a problem encountered. Please try again.");
 define ("MSG_REG_INVALID", "The credentials that you have provided is invalid. Please try again.");

 define ("MSG_SIGNIN_SUCCESS", "You have successfully sent your Sign-In entry for ASSIST. Always be safe!");
 define ("MSG_SIGNIN_UNSUCCESS", "You have failed to send your Sign-In entry for ASSIST. Please try again later. Always be safe!");
 define ("MSG_SIGNIN_WRONG_FORMAT", "You have used the wrong format. Please follow the format below: \n\nSIGNIN\<sp\>\<SAFE\/THREAT\/URGENT\>/\<Location\>/\<Message\>");
 define ("MSG_SIGNIN_DISASTER_OFF", "You may not Sign-In while Disaster Mode is off. Please wait for further announcements.");
 define ("MSG_SIGNIN_ERROR", "There was a problem encountered. Please try again.");
 define ("MSG_SIGNIN_NOT_WHITELISTED", "The mobile number you are using is not registered in ASSIST. Please contact your HR Administrator."); 

 define ("MSG_HELP_SUCCESS", "Your distress call has been received by ASSIST. You will be contacted ASAP.");
 define ("MSG_HELP_WRONG_FORMAT", "You have used the wrong format. Please follow the format below:\n\nHELP\<sp\>\<Location\>/\<Message\>");
 define ("MSG_HELP_DISASTER_OFF", "You may not Ask for Help while Disaster Mode is off. Please contact concerned authorities directy.");
 define ("MSG_HELP_NOT_REGISTERED", "The mobile number you are using is not registered in ASSIST. Please contact your HR Administrator.");

 define ("MSG_GET_PASS", "Here's your current password: ");

 define ("MSG_LOCATION_MISSING", "You have to indicate your location. Please try again.");
 define ("MSG_WRONG_FORMAT", "You have used the wrong format. Please follow the format below:  \nREG\<sp\>\<LastName\>\n\nOR\n\nSIGNIN\<sp\>\<SAFE\/THREAT\/URGENT\>/\<Location\>/\<Message\>\n\nOR\n\nHELP\<sp\>\<Location\>/\<Message\>\n\nOR\n\nGETPASSWORD");

 define ("MSG_DISASTER_DECLARED", "A disaster has been declared!");