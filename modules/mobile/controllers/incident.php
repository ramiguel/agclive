<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Incident extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("input", "iospushnotif", "androidpushnotif"));
        $this->load->model("incidents");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "report") {
            $this->_report_checkins();
        } else if ($request_method == "get_incident_count") {
			$this->_get_incident_count();
		} else if ($request_method == "reply") {
            $this->_reply();
        } else if ($request_method == "confirm") {
            $this->_confirm();
        }
        else {
            method_not_supported();
        }
    }

    private function _confirm() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Failed to confirm");
        if ($request->isValid()) {
            $data = $request->get_data();

            $result = $this->incidents->confirm($data->{'report_incident_id'},$data->{'confirm'},$data->{'user_id'});

            if($result) {
                $response = create_response(0,"Success");
            }
        }
        notify($response);
    }

    private function _reply() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Failed to reply");
        if ($request->isValid()) {
            $data = $request->get_data();

            $result = $this->incidents->reply($data->{'report_incident_id'},$data->{'user_id'},$data->{'message'});

            if ($result) {
                $response = create_response(0, "Success");
            }
        }

        notify($response);
    }

    //request = {"user_id": 1}
 	private function _report_checkins() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Could not get feeds");
        if ($request->isValid()) {
            $data = $request->get_data();
            $report_incident_id = $this->incidents->save_report($data);
            $info = $this->incidents->get_information($data->{'user_id'});
            if(count($info) > 0) {
                $device_keys = $this->incidents->get_device_keys($data->{'user_id'}, $info[0]->company_id);
                //if (count($device_keys)) {

                    $date = date_create();
                    $message = array(
                        "message" => $info[0]->emp_name . ": " . $data->{'message'},
                        "flag" => "Report incident",
                        "timestamp" => date_format($date, 'U')
                    );

                    $devices = array();

                    foreach ($device_keys as $device) {
                        if($device->{'device_key'} != "") {
                            $devices[($device->{'os_type'} == 1 ? "ios" : "android")][] = $device->{'device_key'}; 
                            $result = $this->incidents->save_to_report_incidents_is_read($report_incident_id, $device->{'owner'});
                        }
                    }

                    $this->androidpushnotif->push($devices['android'], $message);
                    $this->iospushnotif->pushIncident($devices['ios'], $message);

                    $response = create_response(0, "Success");
                //}
            }
        }
        notify($response);
 	}
}