<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Event extends Controller {
    public function __construct() {
        parent::__construct();

		$this->load->library(array("input","db"));
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "list") {
            $this->_event_list();
        } else if ($request_method == "view") {
        	$this->_event_details();
        } else if ($request_method == "volunteer") {
			$this->_volunteer();
		} else {
            method_not_supported();
        }
	}

	private function _event_details() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");

		if($request->isValid()) {
			$data = $request->data;

			$event_id = $data->{'event_id'};
			$user_id = $data->{'user_id'};

			$result = $this->db->query("SELECT id AS event_id, event_name, 
										CONCAT(
										DATE_FORMAT(event_date_from, '%b %d %Y %h:%i %p'),' - ',
										DATE_FORMAT(event_date_to, '%b %d %Y %h:%i %p'))
										AS event_date, 
										details, address, 
										(SELECT COUNT(*) FROM event_volunteers WHERE event_id = ".$event_id.") AS no_of_volunteers
										FROM events WHERE id = ". $event_id);

			if(count($result) > 0) {
				$selected_skills_array = array();
				$event_volunteered = $this->db->query("SELECT id FROM event_volunteers WHERE user_id = ".$user_id." AND event_id = ".$event_id. " ORDER BY id DESC LIMIT 1");
				if (count($event_volunteered) > 0) {
					$event_volunteer_id = $event_volunteered[0]->{'id'};
					$selected_skills = $this->db->query("SELECT skill_id FROM event_volunteer_skills WHERE event_volunteer_id = ". $event_volunteer_id);
					foreach ($selected_skills as $key => $value) {
						array_push($selected_skills_array, $value->skill_id);
					}
				}

				$event_skills = $this->db->query("SELECT skill_id, description FROM event_skills 
													LEFT JOIN skills ON skills.id = event_skills.skill_id
													WHERE event_id = ".$event_id);
				$result[0]->skills = array();
				foreach ($event_skills as $skey => $svalue) {
					$is_selected = false;
					if (in_array($svalue->{'skill_id'}, $selected_skills_array)) {
						$is_selected = true;
					}
					$result[0]->skills[] = array("skill_id"=>intval($svalue->{'skill_id'}),"description"=>$svalue->{'description'},"is_selected"=>$is_selected);
				}

				$response = create_response(0, "Success");
				$response->data = $result[0];
			}
		}
		notify($response);
	}

	private function _event_list() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");

		//if($request->isValid()) {
			//$data = $request->data;

			$datenow = date("Y-m-d H:i:s");
			$result = $this->db->query("SELECT id AS event_id, event_name,
										CONCAT(
										DATE_FORMAT(event_date_from, '%b %d %Y %h:%i %p'),' - ',
										DATE_FORMAT(event_date_to, '%b %d %Y %h:%i %p'))
										AS event_date 
										FROM `events` WHERE is_closed <> 1 AND event_date_to >= '$datenow'");

			//if(count($result) > 0) {
				$response = create_response(0, "Success");
				$response->data = $result;
			//}
		//}
		notify($response);
	}

	private function _volunteer() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Invalid format");

		if($request->isValid()) {
			$data = $request->data;

			$event_id = intval($data->{'event_id'});
			$user_id = intval($data->{'user_id'});
			$date = $this->db->quote(date('Y-m-d H:i:s'));
			$skills = $data->{'skills'};

			$result = $this->db->query("SELECT id FROM event_volunteers WHERE event_id=".$event_id." AND user_id=".$user_id);

			if(count($result) == 0) {
				$result = $this->db->insert("INSERT INTO event_volunteers(event_id, user_id, date_volunteered) 
												VALUES(".$event_id.", ".$user_id.",".$date." )");

				if($result) {
					$event_volunteer_id = $this->db->getLastInsertId();

					foreach ($skills as $skill_id) {
	                    $event_volunteer_skills = $this->db->create(Table::EVENT_VOLUNTEER_SKILLS);
	                    $event_volunteer_skills->event_volunteer_id = $event_volunteer_id;
	                    $event_volunteer_skills->skill_id = $skill_id;
	                    $result = $this->db->insert($event_volunteer_skills);
	                }

					$response = create_response(0, "Success");
				}
			} else {
				$event_volunteer_id = $result[0]->{'id'};
				$result = $this->db->update("UPDATE event_volunteers 
											SET date_volunteered=".$date." WHERE id=".$event_volunteer_id);

				if ($result) {
					$result = $this->db->update("DELETE FROM event_volunteer_skills WHERE event_volunteer_id=".$event_volunteer_id);
					if ($result) {
						foreach ($skills as $skill_id) {
		                    $event_volunteer_skills = $this->db->create(Table::EVENT_VOLUNTEER_SKILLS);
		                    $event_volunteer_skills->event_volunteer_id = $event_volunteer_id;
		                    $event_volunteer_skills->skill_id = $skill_id;
		                    $result = $this->db->insert($event_volunteer_skills);
		                }

		                $response = create_response(0, "Success");
					}
				}

				if (count($skills) == 0) {
					$this->db->update("DELETE FROM event_volunteers WHERE id=".$event_volunteer_id);
				}
			}
		}
		notify($response);
	}
}