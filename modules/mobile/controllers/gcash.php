<?php

class Gcash extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("db","user","input","hash","mail","util","session"));
        $this->load->model("gcashaccounts");
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "donate") {
            $this->_donate();
        } else {
            method_not_supported();
        }
    }

    private function _donate() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Error saving data");

        if($request->isValid()) {
            $data = $request->data;
            $result = $this->gcashaccounts->save_gcash($data);

            if($result) {
                $response = create_response(0, "Success");
            }
        }

        notify($response);
    }
}