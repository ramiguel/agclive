<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * User: Cris del Rosario
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

/*
 * Authentication API
 *
 * Methods available:
 *      check
 *      verify
 *      login
 */

class Auth extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("db","user","input","hash","mail","util","session"));
    }

    public function request_handler($request_method,$request_params) {
        if ($request_method == "check") {
            $this->_check_device_registration();
        } else if ($request_method == "verify") {
        } else if ($request_method == "login") {
            $this->_login();
        } else if ($request_method == "forgotpass") {
            $this->_change_user_password();
        } else {
            method_not_supported();
        }
    }

/*
 * Description:
 *       Check if device is already registered.
 *
 * Returns:
 *       Returns 0 is successful, otherwise, any positive number on error.
 *
 * Parameters:
 *
 *       device_id          - device identification
 *       display            - screen display information
 *           width
 *           height
 *           dpi
 *       app_id             -
 */

    private function _check_device_registration() {
        $response = create_response(1,"Device is not recognized yet");

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            if (isset($data->{'app_id'})) {
            }
        } else {
        }

        notify($response);
    }

/*
 * Description:
 *       Login
 *
 * Returns:
 *       Returns 0 is successful, otherwise, any positive number on error.
 *
 * Parameters:
 *
 *       username
 *       password
 *
 */

    private function _login() {
        $response = create_response(1,"Invalid username/password");
        $response->data = new sdgClass();

        $request = $this->input->get_data(Input::STREAM);

        if ($request->isValid()) {
            $data = $request->get_data();

            $username = isset($data->{'username'}) ? trim($data->{'username'}) : "";
            $password = isset($data->{'password'}) ? trim($data->{'password'}) : "";

            $result = null;

            if ($this->user->emaillogin($username,$password)) {
                $result = $this->db->query("SELECT e.id,e.is_registered,e.img_path,CONCAT(e.first_name,' ',e.last_name) AS employee_name,c.company_name,d.department_name,e.password,e.company_id,e.site_id,e.is_supervisor,e.is_rescuer,e.has_changed_init_pass FROM employees AS e
                                            LEFT JOIN companies AS c ON c.id = e.company_id
                                            LEFT JOIN department AS d ON d.id = e.department_id
                                            WHERE e.email = ".$this->db->quote($username)." AND e.is_deleted <> 1 LIMIT 0,1");
            }

            if ($result == null && $this->user->mobileLogin($username,$password)) {
                $result = $this->db->query("SELECT e.id,e.is_registered,e.img_path,CONCAT(e.first_name,' ',e.last_name) AS employee_name,c.company_name,d.department_name,e.password,e.company_id,e.site_id,e.is_supervisor,e.is_rescuer,e.has_changed_init_pass FROM employees AS e
                                            LEFT JOIN companies AS c ON c.id = e.company_id
                                            LEFT JOIN department AS d ON d.id = e.department_id
                                            WHERE e.mobile_no = ".$this->db->quote($username)." AND e.is_deleted <> 1 LIMIT 0,1");
            }

            if ($result != null && count($result) > 0) {
                $result = $result[0];
                if (Hash::CheckPassword($password,$result->password)) {
                    if ($result->is_registered == 1) {
                        $response = create_response(0,"Success");
                        $response->data = new sdgClass();

                        $current_date = date('Y-m-d H:i:s');

                        $session = $this->db->create("session");
                        $session->session_id = $this->session->get_session_id();
                        $session->user_id = $result->id;
                        $session->token_id = $token = $result->id.".".sha1($result->id.date("YmdHis"));
                        $session->date_started = $current_date;
                        $session->username = "emp_" . $result->id;
                        $session->state = 0;
                        $session->logged_in_via = 2;
                        $session->remote_addr = $this->input->get_remote_addr();
                        $session->auth_type = 2;
                        $session->gecos = json_encode($_SERVER);

                        $this->db->insert($session);

                        $disaster_mode = 0;

                        $cmdctr_result = $this->db->query("SELECT c.id FROM cmdctr c
                                        LEFT JOIN cmdctr_sites cs ON cs.disaster_id = c.id
                                        WHERE c.disaster_mode = 1 AND cs.company_id = $result->company_id 
                                        AND cs.site_id = $result->site_id");
                        if (count($cmdctr_result) > 0) {
                            $disaster_mode = 1;
                            foreach ($cmdctr_result as $key) {
                                $check_user_is_read = $this->db->query("SELECT COUNT(id) AS count FROM cmdctr_status 
                                                                    WHERE notification_id=".$key->id."
                                                                    AND user_id=".$session->user_id);
                                if(count($check_user_is_read) > 0) {
                                    if ($check_user_is_read[0]->count == 0) {
                                        $this->db->insert("INSERT INTO cmdctr_status(notification_id,user_id,is_read)
                                                            VALUES(".$key->id.",".$session->user_id.",0)");
                                    }
                                }
                            }
                        }

                        $role = 6;
                        if ($result->is_supervisor == 1) {
                            $role = 4;
                        } else if ($result->is_rescuer == 1) {
                            $role = 5;
                        }

                        $response->data = array(
                            "user_id" => $result->id,
                            "employee_name" => $result->employee_name,
                            "company_name" => $result->company_name,
                            "company_site_id" => $result->site_id,
                            "department" => $result->department_name,
                            "company_id" => $result->company_id,
                            "role" => $role,
                            "image_url" => $result->img_path,
                            "is_rescuer" => $result->is_rescuer,
                            "disaster_mode" => $disaster_mode,
                            "has_changed_init_pass" => $result->has_changed_init_pass
                        );
                    }
                }
            }
        }

        notify($response);
    }

    private function _change_user_password() {
        $response = create_response(1,"Unable to change user password");
        $response->data = new sdgClass();
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            if (isset($data->{'mobile'})) {
                $response = create_response(0,"Success");
                $response->data = new sdgClass();
            } else if ($data->{'email'}) {
                $subject = "AGC - Change Password Request";
                $recipient = $data->{'email'};

                $body = "Your new password is: " . $this->util->generate_password();

                if ($this->mail->send($recipient,$subject,$body)) {
                    $response = create_response(0,"Success");
                    $response->data = new sdgClass();
                }
            }
        }

        notify($response);
    }
}