<?php
 
class Safezone extends Controller {
    public function __construct() {
        parent::__construct();

		$this->load->library(array("input"));
        $this->load->model("safezones");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "list") {
            $this->_get_list();
        } else if ($request_method == "nominate") {
			$this->_save_safezone();
        } else if ($request_method == "provinces") {
            $this->_get_provinces();
        } else if ($request_method == "count-per-province") {
            $this->_get_count_per_province();
        } else {
            method_not_supported();
        }
	}

    private function _get_provinces() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not get list");

        $result  = $this->safezones->get_provinces();
        if(count($result) > 0) {
            $response = create_response(0,"Success");
            $response->data = $result;
        }
        notify($response);
    }

    private function _get_count_per_province() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not get data");
        
        if ($request->isValid()) {
            $data = $request->data;
            $result = $this->safezones->get_count_per_province();
            
            if(count($result) > 0){
                $response = create_response(0, "Success");
                $response->data = $result;
            }
        }
        notify($response);
    }
	
	private function _get_list() {
		$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not get data");
		
        if ($request->isValid()) {
            $data = $request->data;
            $result = $this->safezones->get_list($data);
			
            //if(count($result) > 0){
            	foreach ($result as $key => $value) {
            		$safezone_supplies = $this->safezones->get_safezone_supplies($result[$key]->{'id'});
            		$result[$key]->supply_list = array();
            		foreach ($safezone_supplies as $skey => $svalue) {
            			$result[$key]->supply_list[] = intval($svalue->{'item_id'});
            		}
            	}

            	$response = create_response(0, "Success");
            	$response->data = $result;
            //}
        }
        notify($response);
	}
	
	private function _save_safezone() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Error saving data");

		if($request->isValid()) {
			$data = $request->data;
			$result = $this->safezones->save_safezone($data);

			if($result){
				$response = create_response(0, "Success");
			}
		}
		notify($response);
	}
}