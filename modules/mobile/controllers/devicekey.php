<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Devicekey extends Controller {
    public function __construct() {
        parent::__construct();

		$this->load->library(array("input"));
        $this->load->model("devicekeys");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "register") {
            $this->_register_device_key();
		} else {
            method_not_supported();
        }
	}
	
	private function _register_device_key() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");
		
		if($request->isValid()) {
			$data = $request->data;
			$result = $this->devicekeys->save_device_key($data);

			if($result) {
				$response = create_response(0, "Success");
				$response->data = $result;
			}
		}
		notify($response);
		// var_dump($result);
	}
}