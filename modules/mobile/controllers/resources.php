<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Resources extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array('db'));
    }

    public function request_handler($request_method,$request_params) {
        $this->_list_resources();
    }

    private function _list_resources() {
        $result = $this->db->query("SELECT id,description,url FROM resources WHERE is_deleted <> 1");
        $response = create_response(1,"No resources available as of this time");

        if (count($result) > 0) {
            $response = create_response(0,"Success");
            $response->data = $result;
        }

        notify($response);
    }
}