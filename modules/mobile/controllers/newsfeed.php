<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Newsfeed extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("input","timeago","twitterapi"));
        $this->load->model("newsfeeds");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "cmdcenter") {
            
        } else if ($request_method == "official") {
            
        } else if ($request_method == "employees") {
            $this->_getEmployeeFeeds();
        } else if ($request_method == "get-replies") {
            $this->_get_replies();
        } else if ($request_method == "get-confirm-and-reply-count") {
        	$this->_get_confirm_and_reply_count();
        } else if($request_method == "get_twitter_screen_name") {
			$this->_get_twitter_screen_name();
		} else if($request_method == "get_twitter_feeds") {
			$this->_get_twitter_feeds();
		} else {
            method_not_supported();
        }
    }

    private function _get_confirm_and_reply_count() {
    	$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Could not get count");
        if ($request->isValid()) {
        	$data = $request->data;

        	$result = $this->newsfeeds->get_confirm_and_reply_count($data);

        	if (count($result) > 0) {
	        	$response = create_response(0, "Success");
	        	$response->data = $result[0];
	        }
        }
        notify($response);
    }

    private function _get_replies() {
    	$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Could not get replies");
        if ($request->isValid()) {
            $data = $request->data;

			$result = $this->newsfeeds->get_replies($data);
			
			//if($result) {
				foreach($result['message'] as $key=>$value) {
					$date = $value->{'date_replied'}; //date from db
					$days = $this->timeago->input_date($date);
					$result['message'][$key]->date_diff = $days;
 
                    $result['message'][$key]->message = $result['message'][$key]->message;
                    $result['message'][$key]->employee_name = $result['message'][$key]->employee_name;
				}
				$result['has_reached_last_row'] = $result['has_reached_last_row'];
				
				$response = create_response(0, "Success");
				$response->data = $result;
			//}
		}	
		notify($response);
    }
	
	private function _get_twitter_screen_name() {
		$response = create_response(1, "Could not get feeds");
		$result = $this->newsfeeds->get_screen_name();
		
		if($result) {
			$screen_name = array();
			
			foreach($result as $key => $value) {
				// $result[$key]->screen_name = $value->{'screen_name'};
				$screen_name['screen_name'][] = $value->{'screen_name'};
			}
			
			$response = create_response(0, "Success");
			$response->data = $screen_name;
		}
		
		notify($response);
	}

    //request = {"user_id": 1}
 	private function _getEmployeeFeeds() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Could not get feeds");
        if ($request->isValid()) {
            $data = $request->data;
            $result = $this->newsfeeds->getEmployeeFeeds($data);

            //if ($result) {		
				foreach($result['message'] as $key=>$value) {
					$date = $value->{'date_reported'}; //date from db
					$days = $this->timeago->input_date($date);
					$result['message'][$key]->date_diff = $days;

                    $result['message'][$key]->message = $result['message'][$key]->message;
                    $result['message'][$key]->location = $result['message'][$key]->location;
                    $result['message'][$key]->emp_name = $result['message'][$key]->emp_name;
				}
				$result['has_reached_last_row'] = $result['has_reached_last_row'];
				
                $response = create_response(0, "Success");
                $response->data = $result;
            //}
        }
        notify($response);
 	}
	
	private function _get_twitter_feeds() {
		$response = create_response(1, "Could not get feeds");
		$query = $this->newsfeeds->get_date_diff();
		
		$to_time = strtotime(date("Y-m-d H:i"));
		$from_time = strtotime(date("Y-m-d H:i", strtotime($query[0]->{'date_inserted'})));
		$minute_diff = round(abs($to_time - $from_time) / 60,2);
		
		if($minute_diff >= 15) {
			$screen_names = $this->newsfeeds->get_twitter_interval();
			$twitter = $this->twitterapi->post_twitter_interval($screen_names);
			
			foreach($twitter as $key => $value) {
				foreach($value as $data) {
					$this->newsfeeds->insert_twitter_feed($data);
				}
			}
			
			$result = $this->newsfeeds->get_twitter_feeds();
			
			$feeds = array();
			foreach($result as $key => $value) {
				$str = date("Y-m-d", strtotime($value->date_created));
				$str = strtotime(date("Y-m-d")) - (strtotime($str));
				$diff = floor($str/3600/24);
				
				if($diff >= 3) {
					$this->newsfeeds->validate_date($value->date_created);
				} else {
					$feeds[] = $value;
				}
			}
			
			$response = create_response(0, "Success");
			$response->data = $feeds;
		} else {
			$result = $this->newsfeeds->get_twitter_feeds();
			
			$feeds = array();
			foreach($result as $key => $value) {
				$str = date("Y-m-d", strtotime($value->date_created));
				$str = strtotime(date("Y-m-d")) - (strtotime($str));
				$diff = floor($str/3600/24);
				
				if($diff >= 3) {
					$this->newsfeeds->validate_date($value->date_created);
				} else {
					$feeds[] = $value;
				}
			}
			
			$response = create_response(0, "Success");
			$response->data = $feeds;
		}
		
		notify($response);
	}
}