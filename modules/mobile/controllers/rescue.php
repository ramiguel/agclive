<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Rescue extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("input","db"));
        $this->load->model("rescues");
    }

    public function request_handler($request_method,$request_params) {
        if ($request_method == "emp_per_location") {
            $this->_get_employee_per_location();
        } else if ($request_method == "get-rescues") {
            $this->_get_rescues();
        } else if ($request_method == "saved") {
            $this->_saved();
        } else {
            method_not_supported();
        }
    }
    
    private function _saved() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not save data");

        if ($request->isValid()) {
            $data = $request->data;

            $checkin_id = $data->{'checkin_id'};
            $rescuer_id = $data->{'rescuer_id'};
            $date_rescued = $this->db->quote(date("Y-m-d H:i:s"));
            $is_rescued = 1;

            $checkin_details = $this->db->query("SELECT c.user_id,emp.company_id,emp.site_id FROM checkins c
                                                    LEFT JOIN employees emp ON emp.id = c.user_id
                                                    WHERE c.id=".$checkin_id);

            if (count($checkin_details) > 0) {
                $user_id = $checkin_details[0]->user_id;
                $company_id = $checkin_details[0]->company_id;
                $site_id = $checkin_details[0]->site_id;

                $disaster_ids = $this->db->query("SELECT cmd.id FROM cmdctr_sites cs
                                    LEFT JOIN cmdctr cmd ON cmd.id = cs.disaster_id
                                    WHERE cs.company_id = $company_id AND cs.site_id = $site_id AND cmd.disaster_mode = 1");

                foreach ($disaster_ids as $val) {
                    $checkin = $this->db->query("SELECT id FROM checkins WHERE disaster_id=".$val->{'id'}." 
                                                    AND user_id=".$user_id);

                    foreach ($checkin as $value) {
                        $this->db->update("UPDATE checkins SET status=1,need_help=0,no_of_persons=0,rescuer_id=".$rescuer_id.",
                                        date_rescued=".$date_rescued.",is_rescued=".$is_rescued." 
                                        WHERE id=".$value->{'id'});

                        $this->db->update("DELETE FROM checkin_items WHERE checkin_id=".$value->{'id'});
                    }
                }

                $emp_signins = $this->db->query("SELECT ch.id FROM checkins ch 
                                                LEFT JOIN cmdctr cmd ON cmd.id = ch.disaster_id 
                                                WHERE cmd.disaster_mode = 1 AND ch.user_id = ".$user_id);

                foreach ($emp_signins as $val) {
                    $this->db->update("UPDATE checkins SET status=1,need_help=0,no_of_persons=0,rescuer_id=".$rescuer_id.",
                                        date_rescued=".$date_rescued.",is_rescued=".$is_rescued." 
                                        WHERE id=".$val->{'id'});

                    $this->db->update("DELETE FROM checkin_items WHERE checkin_id=".$val->{'id'});
                }

                $this->db->update("UPDATE employees SET status=1 WHERE id=".$user_id);

                $response = create_response(0, "Success");
            }
        }
        notify($response);
    }

    private function _get_employee_per_location() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not get data");
        
        if ($request->isValid()) {
            $data = $request->data;
            // $result = $this->rescues->get_emp_per_location($data);

            $location = $data->{'location'};
            $user_id = $data->{'user_id'};

            $others = "";
            if ($location == "Others") {
                $others = " OR (city NOT IN ('Biñan','Dasmariñas','Las Piñas','Muñoz','Parañaque') AND city NOT IN (SELECT city_name FROM cities))";
            }

            $sql = "SELECT checkin_id,user_id,emp_name,location,`status`,mobile_no,blood_type,img_path,date_checkedin,city FROM 
                    (
                        SELECT checkin_id,user_id,emp_name,location,`status`,mobile_no,blood_type,img_path,date_checkedin,city FROM 
                        (
                            SELECT c.id AS checkin_id,c.user_id,CONCAT(e.first_name,' ',e.last_name) AS emp_name,
                            c.location,c.status,e.mobile_no,e.blood_type,e.img_path ,date_checkedin, c.city
                            FROM checkins c 
                            INNER JOIN employees e ON e.id = c.user_id
                            LEFT JOIN cmdctr ON cmdctr.id = c.disaster_id
                            WHERE c.date_checkedin = DATE(CONVERT_TZ(NOW(),@@session.time_zone,'+8:00'))
                            AND cmdctr.disaster_mode = 1
                            AND c.need_help = 1 AND c.is_deleted <> 1 
                            AND c.user_id <> $user_id
                            ORDER BY c.date_checkedin,c.time_checkedin DESC
                        )t1 GROUP BY user_id ORDER BY `status` ASC
                    )t2 WHERE city = '$location' $others";


            $result = $this->db->query($sql);

            $list = array();

            if (count($result) > 0) {
                foreach($result as $key=>$data) {
                    //$location_check = str_ireplace("Metro Manila","",$data->location);
                    //if (stristr($location_check,$location)) {
                        $data->location = $data->location;
                        $data->emp_name = $data->emp_name;                        
                        $list[] = $data;
                    //}
                }

                $response = create_response(0, "Success");
                $response->data = $list;
            }
        }
        notify($response);
    }

    private function _get_rescues() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Error getting data");

        if ($request->isValid()) {
            $data = $request->data;

            $user_id = $data->{'user_id'};

            $cities = array();

            $result = $this->db->query("SELECT city_name FROM cities ORDER BY city_name ASC");
            foreach ($result as $data) {
                $city_name = utf8_encode($data->city_name);
                $cities[$city_name] = new stdClass();
                $cities[$city_name]->city_name = $city_name;
                $cities[$city_name]->total = 0;
            }

            $current_date = date("Y-m-d H:i:s");

            $result = $this->db->query("SELECT COUNT(id) AS total,city FROM
                                        (
                                            SELECT id,user_id,city
                                            FROM (
                                                SELECT c.id,c.user_id,c.city FROM checkins c 
                                                LEFT JOIN cmdctr ON cmdctr.id = c.disaster_id
                                                WHERE c.date_checkedin = DATE(CONVERT_TZ(NOW(),@@session.time_zone,'+8:00')) 
                                                AND cmdctr.disaster_mode = 1
                                                AND c.need_help = 1 AND c.is_deleted <> 1
                                                AND c.user_id <> $user_id
                                                ORDER BY c.date_checkedin,c.time_checkedin DESC
                                            )t1
                                            GROUP BY user_id
                                        )t2
                                        GROUP BY city");

            $others_count = 0;
            foreach ($result as $data) {
                if ($data->city == "Others") {
                    $others_count += $data->total;
                } else {
                    $city_found = 0;
                    foreach ($cities as $city) {
                        $result_city = $data->city;
                        $city_name = $city->city_name;

                        if ($result_city == $city_name) {
                            $cities[$result_city]->total += $data->total;
                            $city_found = 1;
                        } 
                    }

                    if ($city_found == 0) {
                        $others_count += $data->total;
                    }
                }
            }

            $cities["Others"]->total = $others_count;

            $result = array();

            foreach ($cities as $city) {
                if ($city->total > 0) {
                    $result[] = $city;
                }
            }

            $response = create_response(0,"Success");
            $response->data = $result;
        }

        notify($response);
    }

    function sanitize($text) {
        return str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç','á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä','é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë','í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î','ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô','ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('n', 'N', 'c', 'C','a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A','e', 'e', 'e', 'e', 'E', 'E', 'E', 'E','i', 'i', 'i', 'i', 'I', 'I', 'I', 'I','o', 'o', 'o', 'o', 'O', 'O', 'O', 'O','u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $text
        );
    }
}