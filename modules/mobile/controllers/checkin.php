<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Checkin extends Controller {
    public function __construct() {
        parent::__construct();

		$this->load->library(array("input", "iospushnotif", "androidpushnotif"));
        $this->load->model("checkins");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "items") {
            $this->_get_items();
        } else if ($request_method == "status") {
			$this->_get_status();
		} else if ($request_method == "save") {
			$this->_save_checkin();
		} else if ($request_method == "disaster_list") {
			$this->_disaster_list();
		} else {
            method_not_supported();
        }
	}
	
	private function _disaster_list() {
		$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not save data");		

        if ($request->isValid()) {
        	$data = $request->data;
        	$result = $this->checkins->getDisasterList($data);

        	if (count($result) > 0) {
				$response = create_response(0, "Success");
				$response->data = $result;
			}
        }
        notify($response);
	}

	private function _get_items() {
		$response = create_response(1,"Could not get data");
	
		$result = $this->checkins->getItems();
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		}
		
		notify($response);
	}
	
	private function _get_status() {
        $response = create_response(1,"Could not get data");

        $result = $this->checkins->getStatus();
		
        if ($result){
        	$response = create_response(0, "Success");
        	$response->data = $result;
        }
    
        notify($response);
	}
	
	private function _save_checkin() {
		$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not save data");		

        if ($request->isValid()) {
        	$data = $request->data;
        	$result = $this->checkins->saveCheckin($data);

        	if ($result) {
				$response = create_response(0, "Success");
				$response->data = "";
			}
        }
        notify($response);
	}
}