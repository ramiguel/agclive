<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Employee extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("input","timeago","upload"));
        $this->load->model("employees");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "info") {
            $this->_getInfo();
        } else if ($request_method == "update_info") {
            $this->_updateInfo();
        } else if ($request_method == "get_check_ins") {
			$this->_get_check_ins();
		} else if ($request_method == "upload") {
            $this->_uploadImage();
        } else if ($request_method == "get-regions") {
            $this->_get_regions(); 
        } else if ($request_method == "get-cities") {
            $this->_get_cities(); 
        } else {
            method_not_supported();
        }
    }
	
	private function _get_check_ins() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not get info");
		
        if ($request->isValid()) {
            $data = $request->data;
			$result = $this->employees->getCheckIns($data);
			
            if($result) {
				foreach($result as $key=>$value) {
					$date = $value->{'date_checkedin'}; //date from db
					$time = $value->{'time_checkedin'}; //time from db
					$days = $this->timeago->input_date($date. " " .$time);
					$result[$key]->date_diff = $days;
					$result[$key]->message = $result[$key]->message;
                    $result[$key]->location = $result[$key]->location;
                    $result[$key]->emp_name = $result[$key]->emp_name;
				}
				
                $response = create_response(0, "Success");
                $response->data = $result;
            }
        }

        notify($response);
    }

    //request = {"user_id": 1}
 	private function _getInfo() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Could not get info");
        if ($request->isValid()){
            $data = $request->data;
            $result = $this->employees->getInfo($data);
            if($result){
                foreach ($result[0] as $key => $value) {
                    $result[0]->$key = $result[0]->$key;
                }
                
                $response = create_response(0, "Success");
                $response->data = $result;
            }
        }
        notify($response);
 	}   

 	//request = {"user_id": 1, "employee_no": 123}
 	private function _updateInfo() {
		$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not update info");
        if ($request->isValid()) {
            $data = $request->data;

            $mobile_no = $data->{'mobile_no'};

            if (ctype_digit($mobile_no)) {
                if (strlen($mobile_no) == 11) {
                    //if ($this->employees->check_prefix($mobile_no)) {
                        $result = $this->employees->updateInfo($data);
                        if ($result) {
                            $response = create_response(0, "Success");
                            $this->_getInfo();
                            return;
                        }
                    //} else {
                    //    $response = create_response(1,"Mobile number should be a Globe-issued number");    
                    //}
                } else {
                    $response = create_response(1,"Mobile number should be 11 digits");
                }
            } else {
                $response = create_response(1,"Invalid mobile number format");   
            }
        }
        notify($response);
 	}

    private function _uploadImage() {
        $request = $this->input->get_data(Input::POST);
        $response = create_response(1, "Could not upload image");
        if ($request->isValid()){
            $data = $request->data;
            
            $user_id = $data['user_id'];
            
            $result = false;
            if ($this->upload->is_uploaded("image")) {
                $this->upload->allowed(array("jpg","jpeg","png"));

                $file_path = BASEPATH . "/assets/photo/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_IMAGE,
                    "sizes" => array(64 => 64,128 => 128,256 => 256),
                    "file_prefix" => $user_id
                );

                if (!$this->upload->do_upload("image",$config)) {
                    $response = create_response(1, "Could not upload image");
                } else {
                    $data['img_path'] = BASE_URL . "assets/photo/" . $this->upload->get_uploaded_filename();
                    $result = true;
                }
            }

            if ($result) {
                $result = $this->employees->updateImagePath($data);

                if($result) {
                    $response = create_response(0, "Success");
                    $response->data = $data;
                }
            }
        }
        notify($response);
    }

    private function _get_regions() {
         $result = $this->db->query("SELECT id,region_name FROM region ORDER BY region_name ASC");
         $response = create_response(0,"Success");
         foreach ($result as $index => $data) {
            $result[$index]->region_name = $data->region_name;
         }
         $response->data = $result;
         notify($response);
    }

    private function _get_cities() {
        $request = $this->input->get_data(Input::STREAM);
        $response = create_response(1, "Record not found");

        if ($request->isValid()) {
            $data = $request->get_data();
            if (isset($data->{'region_id'}) && ctype_digit($data->{'region_id'})) {
                $region_id = $data->{'region_id'};

                $cities = $this->db->create("cities");
                $cities->id = 0;
                $cities->city_name = ""; 
                
                $result = $this->db->query($cities,array("region_id" => $region_id));
                foreach ($result as $index => $data) {
                    $result[$index]->city_name = $data->city_name;
                }
                $response  = create_response(0,"Success");
                $response->data = $result;
            }
        }

        notify($response);
    }

    private function _get_places() {
        $result = $this->db->query("SELECT ");
        print_r($result);
    }
}