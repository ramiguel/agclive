<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class ForgotPass extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array('input','db','mail','util','hash','user'));
    }

    public function request_handler($request_method,$request_params) {
        if ($request_method == "update") {
            $this->_change_password();
        } else {
            $this->_forgot_password();
        }
    }

    private function _change_password() {
        $response = create_response(1,"Incorrect Current Password.");
        $response->data = new sdgClass();

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {

            $data = $request->get_data();

            if (isset($data->{'id'}) && ctype_digit($data->{'id'}) && $data->{'id'} > 0) {
                $employee_id = $data->{'id'};

                $oldpass = isset($data->{'oldpass'}) ? trim($data->{'oldpass'}) : "";
                if (is_string($oldpass) && ctype_alnum($oldpass)) {
                    $employee_seek = $this->db->create('employees');
                    $employee_seek->id = 0;
                    $employee_seek->password = "";
                    $employee_seek->is_registered = 0;

                    $result = $this->db->query($employee_seek,array("id"=>$employee_id));
                    if (count($result) > 0 && $result[0]->id) {
                        $result = $result[0];

                        if ($result->is_registered == 1) {
                            if (Hash::CheckPassword($oldpass,$result->password)) {
                                $newpass = isset($data->{'newpass'}) ? trim($data->{'newpass'}) : "";

                                $newpass_length = strlen($newpass);

                                if ($newpass_length >= User::MIN_PASSWORD_LENGTH && $newpass_length <= User::MAX_PASSWORD_LENGTH) {
                                    if (is_string($newpass) && ctype_alnum($newpass)) {
                                        $date_registered = date("Y-m-d H:i:s");

                                        $employee = $this->db->create('employees');
                                        $employee->has_changed_init_pass = 1;
                                        $employee->password = Hash::HashPassword($newpass);

                                        $this->db->update($employee,array("id" => $employee_id));

                                        $response = create_response(0,"Success");
                                        $response->data = new sdgClass();
                                    } else {
                                        $response = create_response(1,"Password should be alpha numeric only");
                                        $response->data = new sdgClass();
                                    }
                                } else {
                                    $response = create_response(1,"Password should be at least greater than " . User::MIN_PASSWORD_LENGTH . " or lower than " . User::MAX_PASSWORD_LENGTH);
                                    $response->data = new sdgClass();
                                }
                            } else {
                                $respone = create_response(1,"Supplied temporary password is incorrect");
                                $response->data = new sdgClass();
                            }
                        }
                    } else {
                        $respone = create_response(1,"Failed to change password. No such user");
                        $response->data = new sdgClass();                        
                    }    
                }
            }
        }

        notify($response);
    }

    private function _forgot_password() {
        $response = create_response(1,"Forgot password error");
        $response->data = new sdgClass();

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {

            $data = $request->get_data();
            if (isset($data->{'username'})) {
                $account_type = 0;
                $seek_field = "";
                if (filter_var($data->{'username'},FILTER_VALIDATE_EMAIL)) {
                    $account_type = 1;
                    $seek_field = "email";
                } else if ($this->is_mobile($data->{'username'})) {
                    $account_type = 2; 
                    $seek_field = "mobile_no";
                }

                if ($account_type > 0) {
                    $employee_seek = $this->db->create('employees');
                    $employee_seek->id = 0;
                    $employee_seek->first_name = "";
                    $employee_seek->last_name = "";
                    $employee_seek->email = "";
                    $employee_seek->is_registered = 0;

                    $result = $this->db->query($employee_seek,array($seek_field => $data->{'username'},"is_deleted" => "0"));
                    if (count($result) > 0  && $result[0]->id) {
                        $employee = $result[0];

                        if ($employee->is_registered == 1) {
                            $current_date = date("Y-m-d H:i:s");

                            $newpass = $this->util->generate_password();

                            $forgot_password_history = $this->db->create("forgot_password");
                            $forgot_password_history->date_requested = $current_date;
                            $forgot_password_history->date_changed = $current_date;
                            $forgot_password_history->email = $data->{'username'};

                            $this->db->insert($forgot_password_history);

                            $change_password_history = $this->db->create("change_password_history");
                            $change_password_history->password = $newpass;
                            $change_password_history->date_created = $current_date;
                            $change_password_history->employee_id = $employee->id;

                            $this->db->insert($change_password_history);

                            $employee_update = $this->db->create("employees");
                            $employee_update->password = Hash::HashPassword($newpass);

                            $this->db->update($employee_update,array("id" => $employee->id));

                            $message = "Hi " . $employee->first_name . " " . $employee->last_name."<br>\n<br>\n";
                            $message.= "Your new password: " . $newpass . "<br>\n";

                            $this->mail->send($employee->email,"AGC Employee Locator - Forgot Password",$message);
                            $response = create_response(0,"Success");
                            $response->data = array(
                                "id" => $employee->id
                            );
                        } else {
                            $response = create_response(1,"Account is invalid");
                            $response->data = new sdgClass();
                        }
                    } else {
                        $response = create_response(1,"Account not found");
                        $response->data = new sdgClass();
                    }
                } else {
                    $response = create_response(1,"Invalid mobile/email address");
                    $response->data = new sdgClass();
                }
            } else if (isset($data->{'mobile'})) {
            }
        }

        notify($response);
    }

    private function is_mobile($mobile_no) {
        if (ctype_digit($mobile_no)) {
            return true;
        } else {
            return false;
        }
    }
}