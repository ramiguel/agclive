<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Reportin extends Controller {
    public function __construct() {
        parent::__construct();

		$this->load->library(array("input"));
        $this->load->model("reportins");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "view") {
            $this->_view_reportin();
		} else {
            method_not_supported();
        }
	}
	
	private function _view_reportin() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");
		
		if ($request->isValid()) {
			$data = $request->data;
			$reportin = $this->reportins->view_reportin($data);

			if ($reportin) {
				$checkin_id = $reportin[0]->{'checkin_id'};
				if(!empty($checkin_id)) {
					$reportin_items = $this->reportins->get_reportin_items($checkin_id);
					$itemsA = "";
					foreach ($reportin_items as $key) {
						$itemsA.=$key->{'item_name'}.",";
					}
					
					$items = rtrim($itemsA, ",");
					$reportin[0]->items = $items;
				}
			}
			
			$result = $reportin;

			if ($result) {

				foreach($result as $key=>$value) {
					$result[$key]->message = $result[$key]->message;
                    $result[$key]->location = $result[$key]->location;
                    $result[$key]->emp_name = $result[$key]->emp_name;
				}

				$response = create_response(0, "Success");
				$response->data = $result;
			}
		}
		notify($response);
	}
}