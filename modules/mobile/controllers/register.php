<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Register extends Controller {
    private $post_data_keys = array();
    private $employee;

    public function __construct() {
        parent::__construct();

        $this->load->library(array("input","util","user","hash","mail","form","smslib"));

        $this->post_data_keys = array(
            "full_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => (128*2)
                )
            ),
            "company_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "email" => array(
                "required" => false,
                "type" => Form::TYPE_EMAIL,
                "extras" => array()
            ),
            "mobile" => array(
                "required" => false,
                "type" => Form::TYPE_MOBILE,
                "extras" => array()
            )
        );
    }

    public function request_handler($request_method,$request_params) {
        if ($request_method == "verify-account") {
            $this->_verify_account();
        } else if ($request_method == "check-user") {
            $this->_check_username();
        } else if ($request_method == "finalize") {
            $this->_finalize_registration();
        } else {
            method_not_supported();
        }
    }

    private function _check_username() {
        $response = create_response(1,"Error checking username");
        $response->data = new sdgClass();
        $request = $this->input->get_data(Input::STREAM);

        if ($request->isValid()) {
            $data = $request->get_data();
            if (isset($data->{'username'}) && ctype_alnum($data->{'username'})) {
                $username_length = strlen($data->{'username'});
                if ($username_length >= User::MIN_USERNAME_LENGTH && $username_length <= User::MAX_USERNAME_LENGTH) {
                    $username_seek = $this->db->create('users');
                    $username_seek->id = 0;
                    $result = $this->db->query($username_seek,array("username" => $data->{'username'}));
                    if (count($result) > 0 && $result[0]->id) {
                        $response = create_response(1,"Name not available");
                        $response->data = new sdgClass();
                    } else {
                        $response = create_response(0,"Success");
                        $response->data = new sdgClass();
                    }
                }
            }
        }

        notify($response);
    }

    private function _verify_account() {
        $response = create_response(1,"Registration error");
        $response->data = new sdgClass();

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $post_data = (array)$request->get_data();

            $employee = null;
            $this->activation_type = 0;
            $this->activate_via_email = false;
            $this->activate_via_mobile = false;

            $this->employee_param = new sdgClass();
            $this->employee_check = new sdgClass();

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                if ($key == "mobile" && !empty($value)) {
                    $this->activate_via_mobile = true;
                    $this->activation_type = 2;
                    $this->employee_param->$key = $value;
                } else if ($key == "email" && !empty($value)) {
                    $this->activate_via_email = true;
                    $this->activation_type = 1;
                    $this->employee_param->$key = $value;
                } else {
                    $this->employee_check->$key = $value;
                }
            },function($error_type,$key) {
            });

            if ($error_count == 0) {
                $statement = "SELECT id,password,email,mobile_no,first_name,last_name,middle_initial,is_registered,company_id,is_deleted FROM employees WHERE is_deleted <> 1 AND ";

                if ($this->activate_via_mobile == true && $this->activate_via_email == true) {
                    $this->activation_type = 1; // high priority, for now
                } else {
                    if ($this->activate_via_mobile == true) {
                        $this->activation_type = 2;
                    } else if ($this->activate_via_email == true) {
                        $this->activation_type = 1;
                    } else {
                        $error_count++;
                    }
                }

                // if ($this->activate_via_mobile == true && $this->activate_via_email == true) {
                // } else {
                //     $error_count++;
                // }                 

                if ($error_count == 0) {
                    $sep = "";
                    $statement .= "(";
                    foreach((array)$this->employee_param AS $key => $data) {
                        //if($key != "email") {
                            $statement .= $sep . ($key == "mobile" ? "mobile_no" : $key) . "=" . $this->db->quote($data);
                            $sep = " OR ";
                        //}
                    }
                    $statement .= ")";

                    $this->employee = $this->db->query($statement);

                    if (count($this->employee) > 0) {
                        $this->employee = $this->employee[0];

                         $this->activation_type = 2;
                         if ($this->activate_via_email == true) {
                             if ($this->employee->email == $this->employee_param->email) {
                                 $this->activation_type = 1;
                             }
                         }
                    } else {
                        $error_count++;
                    }
                }
            }
            
            if ($error_count == 0 && $this->activation_type != 0) {
                //if ($this->is_name_valid($this->employee,$this->employee_check->full_name) || $this->employee->is_deleted != 0) {
                // if ($this->is_last_name_valid($this->employee,$this->employee_check->full_name) || $this->employee->is_deleted != 0) {
                //     $response = create_response(1,"The account you're trying to register is not a valid AGC account (1)");
                //     $response->data = new sdgClass();
                // } else {
                    $company_id = $this->find_company($this->employee_check->company_name);
                    if ($company_id < 1 || $this->employee->company_id != $company_id) {
                        $response = create_response(1,"The account you're trying to register is not a valid AGC account (2)");
                        $response->data = new sdgClass(); 
                    } else {
                        if (!$this->employee->is_registered) {
                            $current_date = date("Y-m-d H:i:s");

                            $this->employee->password = $this->util->generate_password();

                            $employee_update = $this->db->create("employees");
                            $employee_update->password = Hash::HashPassword($this->employee->password);
                            $employee_update->has_changed_init_pass = 0;
                            $employee_update->is_registered = 1;

                            $this->db->update($employee_update,array("id" => $this->employee->id));
                            
                            $remarks = "Your Temporary Password was sent to your email";

                            if ($this->activation_type == 1) {
                                //EMAIL
                                $subject = "AGC Employee Locator - Account Information";

                                $message = "Hi " . ($this->employee->first_name . " " . $this->employee->last_name) . ",<br>\n<br>\n";
                                $message.= "You have successfully registered to ASSIST. Your credentials are shown below.<br>\n<br>\n";
                                $message.= "Username: ".$this->employee->mobile_no." OR ".$this->employee->email."<br>\n";
                                $message.= "Temporary password: " . $this->employee->password . "<br>\n";

                                $email = $this->db->create("emails");
                                $email->email = $this->employee->email;
                                $email->subject = $subject;
                                $email->message = $message;
                                $email->date_sent = $current_date;

                                $this->db->insert($email);

                                $this->mail->send($this->employee->email,$subject,$message);
                            } else {
                                //SMS
                                $remarks = "Your Temporary Password was sent to your sms";

                                $subject = "AGC Employee Locator - Account Information";

                                $message = "Hi " . ($this->employee->first_name . " " . $this->employee->last_name) . ",\n\n";
                                $message.= "You have successfully registered to ASSIST. Your credentials are shown below.\n\n";
                                $message.= "Username: ".$this->employee->mobile_no." OR ".$this->employee->email."\n";
                                $message.= "Temporary password: " . $this->employee->password . "\n";

                                $sms = $this->db->create("sms");
                                $sms->mobile_no = $this->employee->mobile_no;
                                $sms->message = $message;
                                $sms->date_sent = $current_date;

                                $this->db->insert($sms);

                                $mobile_no = $this->employee->mobile_no;
                                $message = $message;

                                $sms_format["mobile_no"] = $mobile_no;
                                $sms_format["message"] = $message;

                                $sms_reg = $this->smslib->sendMessage($sms_format);
                            }

                            $response = create_response(0,"Success");
                            $response->data = array(
                                "id" => $this->employee->id,
                                "message" => $remarks
                            );
                        }
                        else {
                            $response = create_response(1,"Account is already registered");
                            $response->data = new sdgClass();
                        }
                    }
                // }
            } else {
                $response = create_response(1,"The account you're trying to register is not a valid AGC account (3)");
                $response->data = new sdgClass();
            }
        }

        notify($response);
    }

    private function _finalize_registration() {
        $response = create_response(1,"Registration error");
        $response->data = new sdgClass();
        $request = $this->input->get_data(Input::STREAM);

        if ($request->isValid()) {
            $data = $request->get_data();

            if (isset($data->{'id'}) && is_int($data->{'id'}) && $data->{'id'} > 0) {
                $registration_id = $data->{'id'};

                //$oldpass = isset($data->{'oldpass'}) ? trim($data->{'oldpass'}) : "";
                //if (is_string($oldpass) && ctype_alnum($oldpass)) {
                    $employee_seek = $this->db->create('employees');
                    $employee_seek->id = 0;
                    $employee_seek->password = "";
                    $employee_seek->is_registered = 0;
                    $employee_seek->is_deleted = 0;

                    $result = $this->db->query($employee_seek,array("id"=>$registration_id));

                    if (count($result) > 0 && $result[0]->id) {
                        $result = $result[0];
                        if ($result->is_registered == 1 && $result->is_deleted == 0) {
                            //if (Hash::CheckPassword($oldpass,$result->password)) {
                                $newpass = isset($data->{'newpass'}) ? trim($data->{'newpass'}) : "";

                                $newpass_length = strlen($newpass);

                                if ($newpass_length >= User::MIN_PASSWORD_LENGTH && $newpass_length <= User::MAX_PASSWORD_LENGTH) {
                                    if (is_string($newpass) && ctype_alnum($newpass)) {
                                        $date_registered = date("Y-m-d H:i:s");

                                        $employee = $this->db->create('employees');
                                        $employee->has_changed_init_pass = 1;
                                        $employee->date_registered = $date_registered;
                                        $employee->password = Hash::HashPassword($newpass);
                                        $employee->status = 1;

                                        $this->db->update($employee,array("id" => $registration_id));

                                        $response = create_response(0,"Success");
                                        $response->data = new sdgClass();
                                    } else {
                                        $response = create_response(1,"Password should be alpha numeric only");
                                        $response->data = new sdgClass();
                                    }
                                } else {
                                    $response = create_response(1,"Password should be at least greater than " . User::MIN_PASSWORD_LENGTH . " or lower than " . User::MAX_PASSWORD_LENGTH);
                                    $response->data = new sdgClass();
                                }
                            //} else {
                            //    $response = create_response(1,"Provided temporary password and current temporary password doesn't match");
                            //    $response->data = new sdgClass();
                            //}
                        } else {
                            $response = create_response(1,"Account is already registered");
                            $response->data = new sdgClass();
                        }
                    } else {
                        $response = create_response(1,"Error registering account");
                        $response->data = new sdgClass();
                    }
                //} else {
                //    $response = create_response(1,"Password is not valid");
                //    $response->data = new sdgClass();
                //}
            } else {
                $response = create_response(1,"Invalid user id provided");
                $response->data = new sdgClass();
            }
        }

        notify($response);
    }

    private function is_available($username) {
        $user = $this->db->create(null);
        $user->username = $username;
        $result = $this->db->pquery("SELECT id FROM users WHERE username= ? ",$user);
        if (count($result) > 0 && $result[0]->id) {
            return false;
        } else {
            return true;
        }
    }

    private function find_company($company_name) {
        $company_seek = $this->db->create("company_dictionary"); 
        $company_seek->company_id = 0;
        $result = $this->db->query($company_seek,array("company_name" => $company_name));
        if (count($result) > 0) {
            $company_seek->company_id = $result[0]->company_id;
        }
        return $company_seek->company_id;
    }

    private function is_name_valid($employee,$full_name) {
        $full_name_check_1 = $this->sanitize_name($employee->first_name . " " . $employee->middle_initial . " " . $employee->last_name);
        $full_name_check_2 = $this->sanitize_name($employee->last_name . " " . $employee->first_name . " " . $employee->middle_initial);

        $full_name_sanitized = $this->sanitize_name($full_name);

        if (strcasecmp($full_name_check_1,$full_name_sanitized) == 0) {
            return false;
        } else {
            if (strcasecmp($full_name_check_2,$full_name_sanitized) == 0) {
                return false;
            }
        } 
        return true;
    }

    private function is_last_name_valid($employee,$last_name) {
        $last_name_check_1 = $this->sanitize_name($employee->last_name);

        $last_name_sanitized = $this->sanitize_name($last_name);

        if (strcasecmp($last_name_check_1,$last_name_sanitized) == 0) {
            return false;
        }
        return true;
    }

    private function sanitize_name($full_name) {
        $sanitized_name = str_replace(str_split(",~`!@#\$%^&*()-+[{]}\\|:;\"'<>./?."),'',$full_name);
        $sanitized_name = preg_replace('/\s+/',' ',$sanitized_name);
        return $sanitized_name;
    }
}