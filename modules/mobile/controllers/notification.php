<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Notification extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("input","timeago"));
		$this->load->model("notifications");
    }

    public function request_handler($request_method,$request_params) {
        if ($request_method == "get_notif") {
            $this->_get_notif();
        } else if ($request_method == "get_count") {
			$this->_get_count();
		} else if ($request_method == "read") {
			$this->_mark_as_read();
		} else {
            method_not_supported();
        }
    }
	
    private function _mark_as_read() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Data not recognized");

		if ($request->isValid()) {
			$data = $request->data;

			$result = $this->notifications->markAsRead($data);

			if ($result) {
				$response = create_response(0, "Success");
			}
		}
		
        notify($response);
    }

    // command center + reportin status 3/RED
	private function _get_count() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Data not recognized");

		if ($request->isValid()) {
			$input = $request->data;

			$notif = $this->notifications->getNotifCount($input);

			// var_dump($result);
			$notifArray = array();
			$count = 0;
			foreach ($notif as $key => $value) {
				$notifArray['cmd_ctr'][] = $value->{'notification_id'};
				$count++;
			}

			$disaster = $this->notifications->getDisasterCount($input);

			$disaster_count = 0;
			foreach ($disaster as $key => $value) {
				$notifArray['disaster'][$disaster_count]['id'] = $value->{'disaster_id'};
				$notifArray['disaster'][$disaster_count]['name'] = $value->{'disaster_name'};
				$notifArray['disaster'][$disaster_count]['type'] = $value->{'type'};
				$notifArray['disaster'][$disaster_count]['details'] = $value->{'disaster_details'};
				$disaster_count++;
			}

			$incident = $this->notifications->getReportIncidentCount($input);

			foreach ($incident as $key => $value) {
				$notifArray['report_incident'][] = $value->{'report_incident_id'};
				$count++;
			}

			if (isset($notifArray['cmd_ctr']))	{
				$notifArray['cmd_ctr'] = $notifArray['cmd_ctr'];
			} else {
				$notifArray['cmd_ctr'] = [];	
			} 

			if (isset($notifArray['disaster']))	{
				$notifArray['disaster'] = $notifArray['disaster'];
			} else {
				$notifArray['disaster'] = [];	
			} 

			if (isset($notifArray['report_incident']))	{
				$notifArray['report_incident'] = $notifArray['report_incident'];
			} else {
				$notifArray['report_incident'] = [];	
			} 

			$disaster_mode = $this->notifications->isDisasterMode($input);

			$notifArray['disaster_mode'] = $disaster_mode;
			$notifArray['count'] = $count;

			$is_user_deleted = $this->notifications->checkUser($input);

			$notifArray['is_user_deleted'] = $is_user_deleted;

			if ($notifArray) {
				$response = create_response(0, "Success");
				$response->data = $notifArray;
			}
		}
		
        notify($response);
	}
	
	private function _get_notif() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");

		if ($request->isValid()) {
			$data = $request->data;
		
			$result = $this->notifications->getNotif($data);

			//if ($result) {
				foreach($result['message'] as $key=>$value) {
					$date = $value->{'date_created'}; //date from db
					$days = $this->timeago->input_date($date);
					$result['message'][$key]->date_diff = $days;
				}
				$result['has_reached_last_row'] = $result['has_reached_last_row'];

				$response = create_response(0, "Success");
				$response->data = $result;
			//}
		}
        notify($response);
	}
}