<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Company extends Controller {
    public function __construct() {
        parent::__construct();

		$this->load->library(array("input"));
        $this->load->model("companies");
    }

	public function request_handler($request_method,$request_params) {
        if ($request_method == "get_company") {
            $this->_get_company();
        } else if ($request_method == "get_employees") {
			$this->_get_employees();
		} else if ($request_method == "get_supervisor") {
			$this->_get_supervisor();
		} else if ($request_method == "filter_by_status") {
			$this->_filter_by_status();
		} else if ($request_method == "get-direct-reports") {
            $this->_get_supervisor_direct_reports(); 
		} else if ($request_method == "check-1downs") { 
			$this->_check_supervisor_1downs();
		} else {
            method_not_supported();
        }
	}
	
	private function _filter_by_status() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");
		
		if($request->isValid()) {
			$data = $request->data;
			$result = $this->companies->filterByStatus($data);
			
			if($result) {
				$response = create_response(0, "Success");
				$response->data = $result;
			}
		}
		notify($response);
	}
	
	private function _get_supervisor() {
		$request = $this->input->get_data(Input::STREAM);
		$response = create_response(1,"Could not get data");
		
		if ($request->isValid()) {
			$data = $request->data;
			$result = $this->companies->getSupervisor($data);
			
			if ($result) {
				$response = create_response(0, "Success");
				$response->data = $result;
			}
		}
		notify($response);
	}
	
	private function _get_employees() {
		$request = $this->input->get_data(Input::STREAM);
        $response = create_response(1,"Could not get data");
		
        if ($request->isValid()) {
            $data = $request->data;
            $result = $this->companies->getEmployees($data);
			
            if($result){
            	$response = create_response(0, "Success");
            	$response->data = $result;
            }
        }
        notify($response);
	}
	
	private function _get_company() {
        $response = create_response(1,"Could not get data");		
		$result = $this->companies->getCompanyList();
		
		if($result){
			$response = create_response(0, "Success");
			$response->data = $result;
		}
        notify($response);
	}

	/******************************************************************************************************************************************************************
	 * List Supervisor's or 1 Down's Direct Reports
	 *
	 * required params:
	 *             supervisor_id
     *             level                                     // 0 = all (direct reports and 1 down), 2 = direct reports, 3 = 1 downs
     *             status                                    // 0 = all, 1 = none, 2 = safe, 3 = threat, 4 = urgent/critical
     *
     * returns
     *             employee_id 
     *             emp_name
     *             img_path
     *             status
     *
	 ******************************************************************************************************************************************************************/

	private function _get_supervisor_direct_reports() {
		$response = create_response(1,"Record not found");
		$request = $this->input->get_data(Input::STREAM);
		if ($request->isValid()) {
			$data = $request->get_data();

			$supervisor_id = isset($data->{'supervisor_id'}) ? intval($data->{'supervisor_id'}) : 0;
			$level = isset($data->{'level'}) ? intval($data->{'level'}) : 0;
			$status = isset($data->{'status'}) ? intval($data->{'status'}) : 0;
			$company_id = 0;
			$site_id = 0;

			$statement = "";
			$status_filter = "";
			$sort_by = "status";
			$direction = "DESC";

			$get_company_site = "SELECT company_id,site_id FROM employees WHERE id = $supervisor_id LIMIT 1";
			$result = $this->db->query($get_company_site);

			if (count($result) > 0) {
				$company_id = $result[0]->company_id;
				$site_id = $result[0]->site_id;
			}

			$has_disaster = $this->db->query("SELECT c.id FROM cmdctr c
											LEFT JOIN cmdctr_sites cs ON cs.disaster_id = c.id
											WHERE c.disaster_mode = 1 AND cs.company_id = $company_id AND cs.site_id = $site_id");

			if(count($has_disaster) > 0){
				if ($supervisor_id > 0) {
			        if ($level > 0) {
	                    if ($level > 1) {
	                        if ($status > 0) {
		                        $status_filter = "AND e2.status = " . ($status-1);
		                        $sort_by = "e2.first_name,e2.last_name";
		                        $direction = "ASC";
			        	    }

				        	$statement = "SELECT e2.id AS employee_id,CONCAT(e2.first_name,' ',e2.last_name) AS emp_name,e2.img_path,e2.status,e2.mobile_no FROM employees AS e
	                                      INNER JOIN employees AS e2 ON e2.supervisor_id = e.id 
	                                      WHERE e.supervisor_id = $supervisor_id AND e.id <> e.supervisor_id AND e.is_supervisor = 1 AND e.is_deleted <> 1 $status_filter 
	                                      ORDER BY $sort_by $direction";
	                    } else {
	                    	if ($status > 0) {
		                        $status_filter = "AND status = " . ($status-1);
		                        $sort_by = "first_name,last_name";
		                        $direction = "ASC";
	    		        	}
		
				        	$statement = "SELECT id AS employee_id,CONCAT(first_name,' ',last_name) AS emp_name,img_path,status,mobile_no
					        		      FROM employees WHERE is_deleted <> 1 AND supervisor_id = $supervisor_id $status_filter ORDER BY $sort_by $direction";
	                    }		        		          
			        } else {
			        	$status_filter_1 = "";
			        	$status_filter_2 = "";
	                   	if ($status > 0) {
			        		$status_filter_1 = "AND status = " . ($status-1);
			        		$sort_by= "emp_name";
			        		$direction = "ASC";

			        		$status_filter_2 = "AND e2.status = " . ($status-1);
			        	}
			            $statement = "SELECT id AS employee_id,CONCAT(first_name,' ',last_name) AS emp_name,img_path,status,mobile_no
	                                  FROM employees 
	                                  WHERE is_deleted <> 1 AND supervisor_id = $supervisor_id $status_filter_1
	                                  UNION  
	                                  SELECT e2.id AS employee_id,CONCAT(e2.first_name,' ',e2.last_name) AS emp_name,e2.img_path,e2.status,e2.mobile_no FROM employees AS e
	                                  INNER JOIN employees AS e2 ON e2.supervisor_id = e.id 
	                                  WHERE e.supervisor_id = $supervisor_id AND e.id <> e.supervisor_id AND e.is_supervisor = 1 AND e.is_deleted <> 1 $status_filter_2 
	                                  ORDER BY $sort_by $direction";
			        }

			        $result = $this->db->query($statement);
			        if (count($result) > 0) {
			            $response = create_response(0,"Success");
			            $response->data = $result;
			        }
				}
			}
		}

		notify($response);
	}

	/******************************************************************************************************************************************************************
	 * Check if supervisor has 1 down
	 *
	 * required params:
	 *             supervisor_id
     *
     * returns
     *             total
     *             
     *
	 ******************************************************************************************************************************************************************/	

	private function _check_supervisor_1downs() {
		$response = create_response(1,"Record not found");
		$request = $this->input->get_data(Input::STREAM);
		if ($request->isValid()) {

			$data = $request->get_data();
			$supervisor_id = isset($data->{'supervisor_id'}) ? intval($data->{'supervisor_id'}) : 0;
			
			if ($supervisor_id > 0) {
                $result = $this->db->query("SELECT COUNT(id) AS total 
                	                        FROM employees 
                	                        WHERE supervisor_id = " . $supervisor_id . " AND is_deleted <> 1 AND is_supervisor = 1");
                if (count($result) > 0) {
                	$response = create_response(0,"Success");
                	$response->data = $result;
                }
			}
        }
        notify($response);
	}
}