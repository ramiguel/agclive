<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Contact extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("input"));
        $this->load->model("contacts");
    }

    public function request_handler($request_method,$request_params) {
        if ($request_method == "list") {
            $this->_getList();
        }
    }

	//
    private function _getList(){
        $response = create_response(1,"Could not get list");
        $result = $this->contacts->getList();
        if ($result){
            foreach ($result as $key => $value) {
                $result[$key]->contact_no = array();
                $result[$key]->contact_no['mobile_nos'] = array();
                $result[$key]->contact_no['landline_nos'] = array();
                $hotline_id = $result[$key]->id;
                $contact_nos = $this->contacts->get_contact_nos($hotline_id);
                if(count($contact_nos) > 0) {
                    foreach ($contact_nos as $xkey => $xvalue) {
                        if(ctype_digit($contact_nos[$xkey]->contact_no) && strlen($contact_nos[$xkey]->contact_no) == 11) {
                            array_push($result[$key]->contact_no['mobile_nos'], $contact_nos[$xkey]->contact_no);
                        } else {
                            array_push($result[$key]->contact_no['landline_nos'], $contact_nos[$xkey]->contact_no);
                        }
                    }
                }
            }

            $response = create_response(0, "Success");
            $response->data = $result;
        }
        notify($response);
    }
}