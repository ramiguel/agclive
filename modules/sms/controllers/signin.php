<?php

class SignIn extends Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array("smslib"));
        $this->load->model("checkins");
        $this->error = -1;
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "sign-in") {
            $this->_sign_in();
        } else {
            method_not_supported();
        }
    }

    private function _send_message($data) {
        if($this->error > -1) {
            if($this->error == 0) {
                $data["user_id"] = $this->_check_if_mobile_exists($data["mobile_no"]);
                if($data["status"] == SMS_SAFE) {
                    $data["location"] = "Not specified";
                    $data["no_of_persons"] = 0;
                    $data["need_help"] = 0;
                } else {
                    $data["location"] = ucwords(strtolower($data["location"]));
                    $data["no_of_persons"] = $data["number"];
                    $data["need_help"] = ($data["yes_no"] == "YES")? 1: 0;
                }
                $data["status"] = $this->_get_status_id($data["status"]);
                $data["message"] = ucwords(strtolower($data["message"]));
                $data["longitude"] = 0;
                $data["latitude"] = 0;
                $data["disaster_id"] = $this->_get_current_disaster(); //get current disaster

                if($this->_save_checkin($data)){
                    $data["message"] = MSG_SIGNIN_SUCCESS;
                    $sms_res = $this->smslib->sendMessage($data);
                } else {
                    echo "this3";
                    $data["message"] = MSG_SIGNIN_ERROR;
                    $sms_res = $this->smslib->sendMessage($data);
                }

            } else if ($this->error == 1) {
                $data["message"] = MSG_SIGNIN_DISASTER_OFF;
                $sms_res = $this->smslib->sendMessage($data);
            } else if ($this->error == 2) {
                $data["message"] = MSG_SIGNIN_WRONG_FORMAT;
                $sms_res = $this->smslib->sendMessage($data);
            } else {
                echo "this2";
                $data["message"] = MSG_SIGNIN_ERROR;
                $sms_res = $this->smslib->sendMessage($data);
            }
        } else {
            echo "this1";
            $data["message"] = MSG_SIGNIN_ERROR;
            $sms_res = $this->smslib->sendMessage($data);
        }
    }

    private function _sign_in() {
        $data["format"] = "SIGNIN THREAT LOC:Makati City MSG:Holaaa! Test Message! YES 1"; //test data
        $data["mobile_no"] = "09159236376"; //test data

        $this->_check_signin_format($data);
    }

    private function _check_signin_format($data) {
        if($this->_check_disaster_mode()) {
            $signin_data = $this->_split_data($data["format"]);
            if($signin_data != false || $signin_data != null) {
                if($signin_data["status"] == SMS_SAFE) {
                    if($signin_data["keyword"] == SMS_KEYWORD_SIGNIN) {
                        $this->error = 0;
                    } else {
                        $this->error = 2; // invalid format
                    }
                } else if($signin_data["status"] == SMS_THREAT
                        || $signin_data["status"] == SMS_URGENT) {
                    if($signin_data["keyword"] == SMS_KEYWORD_SIGNIN) {
                        if(($signin_data["yes_no"] == SMS_YES)
                            || ($signin_data["yes_no"] == SMS_NO)) {
                            if(is_numeric($signin_data["number"]) && !(strpos($signin_data["number"], "."))) {
                                $this->error = 0;
                            } else {
                                $this->error = 2; // invalid format
                            }
                        } else {
                            $this->error = 2; //invalid format
                        }
                    } else {
                        $this->error = 2; // invalid format
                    }
                } else {
                    $this->error = 2; // invalid format
                }
            } else {
                $this->error = 2; // invalid format
            }
        } else {
            $this->error = 1; // disaster mode off
        }

        $info = array();
        $info["mobile_no"] = $data["mobile_no"];
        $info = array_merge($info, $signin_data);

        $this->_send_message($info);
    }

    private function _split_data($data) {
        $signin_data = array();
        $values = explode(" ", $data);
        $signin_data['message'] = "";
        $signin_data['location'] = "";

        if(count($values) >= 3) {
            $status = $this->_signin_check_type($values);
            if($status == SMS_SAFE) {
                return $this->_signin_split_safe($values);
            } else if (($status == SMS_THREAT) || ($status == SMS_URGENT)) {
                return $this->_signin_split_threat_urgent($values);
            } else {
                return false; // invalid status keyword
            }
        } else {
            return false; // invalid format
        }

        $signin_data['message'] = trim($signin_data['message']);
        $signin_data['location'] = trim($signin_data['location']);

        return $signin_data;
    }

    private function _signin_check_type($data) {
        $status = null;

        foreach($data as $key=>$val):
            if($key == '1') {
                $status = $val;
            }
        endforeach;

        return $status;
    }

    private function _signin_split_threat_urgent($data)
    {
        $signin_data = array();
        $signin_data['message'] = '';
        $signin_data['location'] = '';
        $msg = false;

        foreach ($data as $key => $val):
            if ($key == '0') {
                $signin_data['keyword'] = $val;
            } else if($key == '1') {
                $signin_data['status'] = $val;
            } else if ($key == (count($data) - 2)) {
                $signin_data['yes_no'] = $val;
            } else if ($key == (count($data) -1)) {
                $signin_data['number'] = $val;
            } else if($key != '0'
                && $key != '1'
                && $key != (count($data) -2)
                && $key != (count($data) -1)) {

                if(strpos($val,"MSG:") === 0) {
                    $msg = true;
                    $new_val = str_replace("MSG:", "", $val);
                    $val = $new_val;
                }

                if($msg == false) {
                    if(strpos($val, "LOC:") === 0) {
                        $new_val = str_replace("LOC:", "", $val);
                        $val = $new_val;
                    }

                    if($signin_data["location"] == '') {
                        $signin_data["location"] = $val;
                    } else {
                        $signin_data["location"] .= " " . $val;
                    }
                } else {
                    if($signin_data["message"] == '') {
                        $signin_data["message"] = $val;
                    } else {
                        $signin_data["message"] .= " " . $val;
                    }
                }
            }
        endforeach;

        return $signin_data;
    }

    private function _check_disaster_mode() {
        return $this->db->query("SELECT disaster_mode FROM cmdctr ORDER BY id DESC LIMIT 1");
    }

    public function _check_if_mobile_exists($mobile) {
        $result = $this->db->query(
            "SELECT id FROM employees WHERE
            mobile_no = '".$mobile."'"
        );

        return $result[0]->{'id'};
    }

    private function _get_status_id($status) {
        $status_seek = $this->db->create("status");
        $status_seek->id = 0;
        $result = $this->db->query($status_seek, array("description" => strtolower($status)));

        return $result[0]->{"id"};
    }

    private function _get_current_disaster() {
        $result = $this->db->query("SELECT id FROM cmdctr ORDER BY id DESC LIMIT 1");
        return $result[0]->{'id'};
    }

    public function _save_checkin($data) {
        $date = date('Y-m-d');
        $time = date('H:i:s');

        $checkins = $this->db->create("checkins");
        $checkins->user_id = $data['user_id'];
        $checkins->status = $data['status'];
        $checkins->date_checkedin = $date;
        $checkins->time_checkedin = $time;
        $checkins->location = $data['location'];
        $checkins->message = $data['message'];
        $checkins->no_of_persons = $data['no_of_persons'];
        $checkins->longitude = $data['longitude'];
        $checkins->latitude = isset($data['latitude']) ? $data['latitude'] : 0;
        $checkins->disaster_id = count($data['disaster_id']) > 0 ? $data['disaster_id'][0] : 0;

        $checkins->need_help = $data['need_help'];

        $checkin_id = 0;

        $result = false;

        if($checkins->disaster_id != 0) {
            //select first element disaster_id[0] assume 1 is to 1
            $statement = "SELECT id FROM checkins WHERE user_id = " . $checkins->user_id . " AND disaster_id = " . $checkins->disaster_id;
            $result = $this->db->query($statement);

            if(count($result) > 0) {
                $checkin_id = $result[0]->{'id'};
                $result = $this->db->update($checkins,array("id"=>$checkin_id));
            } else {
                $result = $this->db->insert($checkins);
                $checkin_id = $this->db->getLastInsertId();
            }
        }
        else {
            $statement = "SELECT id, disaster_id FROM checkins WHERE user_id = " . $checkins->user_id . " ORDER BY id DESC LIMIT 1";
            $result = $this->db->query($statement);

            if(count($result) > 0) {
                $checkin_id = $result[0]->{'id'};
                $checkins->disaster_id = $result[0]->{'disaster_id'};
                $result = $this->db->update($checkins,array("id"=>$checkin_id));
            }
        }

        if ($result && $checkin_id != 0) {
            if (isset($data->{'item_list'})) {
                $items = $data->{'item_list'};

                $get_checkin = $this->db->query("SELECT COUNT(checkin_id) AS count FROM checkin_items WHERE checkin_id = " . $checkin_id);
                if(count($get_checkin) > 0) {
                    $this->db->update("DELETE FROM checkin_items WHERE checkin_id = " . $checkin_id);
                }

                foreach ($items as $item_id) {
                    $checkin_items = $this->db->create("checkin_items");

                    $checkin_items->checkin_id = $checkin_id;
                    $checkin_items->item_id = $item_id;

                    $result = $this->db->insert($checkin_items);
                }
            }
        }

        if ($result) {
            $status = $data['status'];

            //FOR SUPERVISOR UPDATE
            $employee = $this->db->create("employees");
            $employee->status = $status;
            $result = $this->db->update($employee,array("id" => $data['user_id']));
        }

        return $result;
    }

}