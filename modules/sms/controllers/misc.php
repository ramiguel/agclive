<?php

class Misc extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("smslib", "helper"));
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "register-success") {
            $this->_register_success($request_params);
        }
    }

    private function _register_success() {
        $mobile_no = $_POST["mobile_no"];
        $message = MSG_REG_SUCCESS;

        $data["mobile_no"] = $mobile_no;
        $data["message"] = $message;

        $sms_reg = $this->smslib->sendMessage($data);
    }
}