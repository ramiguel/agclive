<?php

class Register extends Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array("smslib"));
        $this->load->model("employees");

        $this->reg_error = -1;
        $this->prefixes = null;
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "register") {
            $this->_register();
        } else {
            method_not_supported();
        }
    }

    private function _register() {
        $data["format"] = "REG Pineda YonduInc. cpineda@yondu.com 09199236376";
        $data["mobile_no"] = "09159236376";

        $this->_check_reg_format($data);
    }

    private function _reg_send_message($data) {
        if($this->reg_error > -1) {
            if($this->reg_error == 0) {
                if($this->employees->saveRegistration($data)) {
                    $data["message"] = MSG_REG_SUCCESS;
                    $sms_res = $this->smslib->sendMessage($data);
                } else {
                    $data["message"] = MSG_REG_ERROR;
                    $sms_res = $this->smslib->sendMessage($data);
                }
            } else if($this->reg_error == 1 || $this->reg_error == 2) {
                $data["message"] = MSG_REG_WRONG_FORMAT;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 3) {
                $data["message"] = MSG_REG_NON_GLOBE;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 4) {
                $data["message"] = MSG_REG_NOT_WHITELISTED;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 5) {
                $data["message"] = MSG_REG_ALREADY;
                $sms_res = $this->smslib->sendMessage($data);
            } else {
                $data["message"] = MSG_REG_ERROR;
                $sms_res = $this->smslib->sendMessage($data);
            }
        } else {
            $data["message"] = MSG_REG_ALREADY;
            $sms_res = $this->smslib->sendMessage($data);
        }
    }

    private function _check_reg_format($data) {
        $reg_data = $this->_split_data($data["format"]);
        $is_checked = null;

        if(count($reg_data) == 5) {
            if($reg_data["keyword"] == SMS_KEYWORD_REG) {
                if($this->_check_prefix($reg_data["mobile_no"])) {
                    $is_checked = $this->employees->checkRegisteredData($reg_data);
                    if(!empty($is_checked)) {
                        if($is_checked[0]->is_registered == 0) {
                            $this->reg_error = 0;
                        } else {
                            $this->reg_error = 5; //already registered
                        }
                    } else {
                        $this->reg_error = 4; //not whitelisted
                    }
                } else {
                    $this->reg_error = 3; //non-globe
                }
            } else {
                $this->reg_error = 2; //incorrect format: keyword
            }
        } else {
            $this->reg_error = 1; //incorrect format
        }

        $info = array();

        $info["id"] = ($is_checked != NULL) ? $is_checked[0]->{'id'} : 0;
        $info["date_registered"] = date("Y-m-d H:i:s");
        $info["status"] = 1;

        $info = array_merge($info, $reg_data);
        $this->_reg_send_message($info);
    }

    private function _split_string($str) {
        return preg_split('/(?=[A-Z])/',$str);
    }

    private function _split_data($data) {
        $reg_data = array();
        $values = explode(" ", $data);

        foreach($values as $key=>$val):
            if ($key == '0') {
                $reg_data['keyword'] = $val;
            } else if ($key == '1') {
                $reg_data['last_name'] = implode(" ", $this->_split_string(lcfirst($val)));
            } else if ($key == '2') {
                $reg_data['company'] = implode(" ", $this->_split_string(lcfirst($val)));
            } else if ($key == '3') {
                $reg_data['email'] = $val;
            }else if ($key == '4') {
                $reg_data['mobile_no'] = $val;
            }
        endforeach;

        return $reg_data;
    }

    private function _check_prefix($mobile_no) {
        $result = $this->checking($mobile_no);

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    private function checking($passed_data) {
        $this->prefixes = $this->_get_all_prefixes();

        $mobile_no = substr($passed_data, 1);

        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;

        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }

    private function _get_all_prefixes() {
        return $this->db->query("SELECT number_range, start_msisdn, end_msisdn, brand FROM prefixes");
    }

}