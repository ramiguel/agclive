<?php

class Prefixes extends Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model("prefix");
        $this->prefixes = null;
    }

    public function request_handler($request_method, $request_params) {
        if ($request_method == "check-prefix") {
            $this->_check_prefix($request_params);
        }
    }

    private function _check_prefix() {
        $mobile_no = $_POST["data"];
        $result = $this->checking($mobile_no);

        if($result) {
            echo "ok";
        } else {
            echo "none";
        }
    }

    private function checking($passed_data) {
        $this->prefixes = $this->prefix->getAllPrefixes();

        $mobile_no = substr($passed_data, 1);

        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;

        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }

}