<?php

class CommandCenter extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library("smslib");

        //$this->load->model("employees");
    }

    public function request_handler($request_method, $request_params)
    {
        if ($request_method == "broadcast") {
            $this->_broadcast_cc($request_params);
        } else if($request_method == "toggle-on") {
            $this->_toggle_on();
        } else if($request_method == "toggle-off") {
            $this->_toggle_off();
        } else if($request_method == "disaster-declared") {
            $this->_disaster_declared();
        } else {
            method_not_supported();
        }
    }

    private function _toggle_on() {
        $result = $this->employees->getAllRegistered();
        $sms_res = null;

        if(!empty($result)) {
            foreach($result as $key => $value):
                $data["mobile_no"] = $value->mobile_no;
                $data["message"] = MSG_DISASTER_TOGGLED_ON;
                $sms_res = $this->smslib->sendMessage($data);
            endforeach;
            echo "ok";
        } else {
            echo "none";
        }
    }

    private function _toggle_off() {
        $result = $this->employees->getAllRegistered();
        $sms_res = null;

        if(!empty($result)) {
            foreach($result as $key => $value):
                $data["mobile_no"] = $value->mobile_no;
                $data["message"] = MSG_DISASTER_TOGGLED_OFF;
                $sms_res = $this->smslib->sendMessage($data);
            endforeach;
            echo "ok";
        } else {
            echo "none";
        }
    }

    private function _broadcast() {
        $message = $_POST["data"];
        $company_id = $_POST["company"];

        $sms_res = null;

        if($company_id == 0) {
            $result = $this->employees->getAllRegistered();
        } else {
            $result = $this->employees->getAllRegisteredByCompany($company_id);
        }

        if(!empty($result)) {
            foreach($result as $key => $value):
                $data["mobile_no"] = $value->mobile_no;
                $data["message"] = $message;
                $sms_res = $this->smslib->sendMessage($data);
            endforeach;
            echo "ok";
        } else {
            echo "Messages can only be sent to whitelisted employees of AGC";
        }
    }

    private function _broadcast_cc() {
        $message = $_POST["data"];
        $sites = $_POST["sites"];

//        $message = "Test Broadcast";
//        $sites["sites"][0]["company_id"] = 0;

        $sms_res = null;
        $data["message"] = $message;

        if($sites["sites"][0]["company_id"] == 0) {
            $result = $this->get_company_site_all_employees();

            foreach ($result as $res_key => $res_val):
                foreach ($res_val as $res_k => $res_v):
                    if ($res_k == "mobile_no") {
                        $data["mobile_no"] = $res_v;
                        $sms_res = $this->smslib->sendMessage($data);
                        echo "<pre>";
                        print_r($sms_res);
                    }
                endforeach;
            endforeach;

        } else {
            foreach($sites as $key => $val):

                foreach($val as $k => $v):
                    if($k == "site_id") {
                        $data["site_id"] = $v;
                    }

                    if($k == "company_id") {
                        $data["company_id"] = $v;
                    }
                endforeach;

                $result = $this->get_company_site_employees($data);

                foreach ($result as $res_key => $res_val):
                    foreach ($res_val as $res_k => $res_v):
                        if ($res_k == "mobile_no") {
                            $data["mobile_no"] = $res_v;
                            $sms_res = $this->smslib->sendMessage($data);
                        }
                    endforeach;
                endforeach;


            endforeach;
        }





    }

    private function _disaster_declared() {
        $message = MSG_DISASTER_DECLARED . " " . $_POST["data"];
        $sites = $_POST["sites"];

//        $message = "Test Broadcast";
//        $sites["sites"][0]["company_id"] = 0;

        $sms_res = null;
        $data["message"] = $message;

        if($sites["sites"][0]["company_id"] == 0) {
            $result = $this->get_company_site_all_employees();

            foreach ($result as $res_key => $res_val):
                foreach ($res_val as $res_k => $res_v):
                    if ($res_k == "mobile_no") {
                        $data["mobile_no"] = $res_v;
                        $sms_res = $this->smslib->sendMessage($data);
                        echo "<pre>";
                        print_r($sms_res);
                    }
                endforeach;
            endforeach;

        } else {
            foreach($sites as $key => $val):

                foreach($val as $k => $v):
                    if($k == "site_id") {
                        $data["site_id"] = $v;
                    }

                    if($k == "company_id") {
                        $data["company_id"] = $v;
                    }
                endforeach;

                $result = $this->get_company_site_employees($data);

                foreach ($result as $res_key => $res_val):
                    foreach ($res_val as $res_k => $res_v):
                        if ($res_k == "mobile_no") {
                            $data["mobile_no"] = $res_v;
                            $sms_res = $this->smslib->sendMessage($data);
                        }
                    endforeach;
                endforeach;


            endforeach;
        }
        echo "ok";
    }

    private function _get_disaster_details($id) {
        $result = $this->db->query("SELECT cmd.disaster_name as disaster_name,
                        dt.type as disaster_type
                        FROM cmdctr as cmd
                        INNER JOIN disaster_type as dt
                        ON dt.id = cmd.disaster_type
                        WHERE cmd.id = $id");

        return $result[0];
    }

    private function get_company_site_employees($data) {
        $company_id = $data['company_id'];
        $site_id = $data['site_id'];



        $result = $this->db->query("SELECT e.id as id, e.mobile_no as mobile_no
                            FROM employees as e
                            INNER JOIN companies as c
                            ON e.company_id = c.id
                            INNER JOIN company_sites as cs
                            ON e.site_id = cs.id
                            WHERE cs.id = '" . $site_id . "'
                            AND c.id = '" . $company_id. "'
                            AND e.is_registered = 1
                            AND e.is_deleted <> 1");


        return $result;
    }

    private function get_company_site_all_employees() {
        $result = $this->db->query("SELECT id, mobile_no FROM employees WHERE is_deleted <> 1 AND is_registered = 1");

        return $result;
    }
}