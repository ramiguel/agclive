<?php

class Incoming extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array("smslib", "helper","hash","util"));

        $this->error = -1;
        $this->reg_error = -1;
        $this->help_error = -1;
        $this->prefixes = null;
        $this->open_disasters = null;
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "incoming") {
            $this->_incoming();
        } else {
            method_not_supported();
        }
    }

    private function _incoming() {

        $json = file_get_contents('php://input');
        $json = stripslashes($json);
        $values = json_decode($json, true);

        $inbound = $values["inboundSMSMessageList"]["inboundSMSMessage"][0];
        $data["format"] = $inbound["message"];
        $data["format"] = $inbound["message"];
        $data["mobile_no"] = str_replace("tel:+63", "0", $inbound["senderAddress"]);

        //$data["format"] = "REG Pineda";
        //$data["format"] = "SIGNIN URGENT LOC:BGC MSG:test";
        //$data["format"] = "GETPASSWORD"; 

        //$data["format"] = "SIGNIN THREAT/Makati City"; //test data
        //$data["mobile_no"] = str_replace("tel:+63", "0", "tel:+639063894114"); ///test data
	 //	print_r($data["format"]);
        $data_split = explode(" ", $data["format"]);

        if($data_split[0] == SMS_KEYWORD_SIGNIN || strtolower($data_split[0]) == strtolower(SMS_KEYWORD_SIGNIN)) {
            $this->_sign_in($data);
        } else if($data_split[0] == SMS_KEYWORD_REG || strtolower($data_split[0]) == strtolower(SMS_KEYWORD_REG)) {
            $this->_register($data);
        } else if($data_split[0] == SMS_KEYWORD_HELP || strtolower($data_split[0]) == strtolower(SMS_KEYWORD_HELP)) {
            $this->_help($data);
        } else if(($data_split[0] == SMS_KEYWORD_GETPASS || strtolower($data_split[0]) == strtolower(SMS_KEYWORD_GETPASS)) && count($data_split) == 1) {
            $this->_get_password($data);
        } else {
            $data["message"] = MSG_WRONG_FORMAT;
            $sms_res = $this->smslib->sendMessage($data);
        }
    }

    /* -- REGISTRATION -- */
    private function _register($data) {
        $this->_check_registration_format($data);
    }

    private function _reg_send_message($data) {
        if($this->reg_error > -1) {
            if($this->reg_error == 0) {

                $success_msg = MSG_REG_SUCCESS;

                if($this->_save_registration($data)) {
                    $data["message"] = MSG_REG_SUCCESS;
                    $sms_res = $this->smslib->sendMessage($data);
                } else {
                    $data["message"] = MSG_REG_ERROR;
                    $sms_res = $this->smslib->sendMessage($data);
                }
            } else if($this->reg_error == 1 || $this->reg_error == 2) {
                $data["message"] = MSG_REG_WRONG_FORMAT;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 3) {
                $data["message"] = MSG_REG_NON_GLOBE;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 4) {
                $data["message"] = MSG_REG_NOT_WHITELISTED;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 5) {
                $data["message"] = MSG_REG_ALREADY;
                $sms_res = $this->smslib->sendMessage($data);
            } else if($this->reg_error == 6) {
                $data["message"] = MSG_REG_NOT_WHITELISTED;
                $sms_res = $this->smslib->sendMessage($data);
            } else {
                $data["message"] = MSG_REG_ERROR;
                $sms_res = $this->smslib->sendMessage($data);
            }
        } else {
            $data["message"] = MSG_REG_ALREADY;
            $sms_res = $this->smslib->sendMessage($data);
        }
    }

    private function _check_registration_format($data) {
        $reg_data = $this->_split_registration_data($data["format"]);
        $reg_data["mobile_no"] = $data["mobile_no"];
        $is_checked = null;

        if(count($reg_data) == 3) {
            if($reg_data["keyword"] == SMS_KEYWORD_REG) {
                if($this->_check_prefix($reg_data["mobile_no"])) {
                    if($this->_check_if_valid($reg_data)) {
                        $is_checked = $this->_check_registered_data($reg_data);
                        if(!empty($is_checked)) {
                            if($is_checked[0]->is_registered == 0) {
                                $this->reg_error = 0;
                            } else {
                                $this->reg_error = 5; //already registered
                            }
                        } else {
                            $this->reg_error = 5;  //already registered
                        }
                    } else {
                        $this->reg_error = 6; // invalid credentials
                    }
                } else {
                    $this->reg_error = 3;
                }
            } else {
                $this->reg_error = 2;
            }
        } else {
            $this->reg_error = 1;
        }

        $info = array();

        $info["id"] = ($is_checked != NULL) ? $is_checked[0]->{'id'} : 0;
        $info["date_registered"] = date("Y-m-d H:i:s");
        $info["status"] = 1;
        $info["mobile_no"] = $data["mobile_no"];

        if($reg_data != false) {
            $info = array_merge($info, $reg_data);
        }

        $this->_reg_send_message($info);
    }

    private function _split_registration_data($data) {
        $reg_data = array();
        $values = explode(" ", $data);

        $reg_data["last_name"] = "";

        foreach($values as $key => $val):
            if($key == '0') {
                $reg_data["keyword"] = $val;
            } else {
                if($reg_data["last_name"] == "") {
                    $reg_data["last_name"] = $val;
                } else {
                    $reg_data["last_name"] = $reg_data["last_name"] . " " . $val;
                }
            }
        endforeach;

        return $reg_data;
    }

    private function _check_if_valid($data) {
        $is_valid = false;

        $result = $this->db->query("
                    SELECT id FROM employees
                    WHERE mobile_no = '" . $data['mobile_no'] . "'
                    AND last_name = '". $data['last_name'] ."'
                    AND is_deleted <> 1
                    ");

        if(count($result) > 0) {
            $is_valid = true;
        }
        return $is_valid;
    }

    private function _split_string($str) {
        return preg_split('/(?=[A-Z])/',$str);
    }

    private function _check_prefix($mobile_no) {
        $result = $this->checking($mobile_no);

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    private function checking($passed_data) {
        $this->prefixes = $this->_get_all_prefixes();

        $mobile_no = substr($passed_data, 1);

        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;



        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }

    private function _get_all_prefixes() {
        return $this->db->query("SELECT number_range, start_msisdn, end_msisdn, brand FROM prefixes");
    }

    public function _check_registered_data($data) {
        return $this->db->query("
                SELECT * FROM employees WHERE
                last_name = '" . $data['last_name'] . "' AND
                mobile_no = '" . $data['mobile_no'] . "' AND
                is_registered <> 1
                ");
    }

    private function findCompany($company_name) {
        $company_seek = $this->db->create("company_dictionary");
        $company_seek->company_id = 0;
        $result = $this->db->query($company_seek,array("company_name" => $company_name));
        if (count($result) > 0) {
            $company_seek->company_id = $result[0]->company_id;
        }
        return $company_seek->company_id;
    }

    private function _save_registration($data) {
        $employee = $this->db->create("employees");
        $employee->is_registered = 1;
        $employee->date_registered = $data["date_registered"];
        $employee->status = 1;
        $employee->is_sms_registered = 1;

        $result = $this->db->update($employee, array("mobile_no" => $data["mobile_no"]));

        return $result;
    }

    private function _get_temp_password($data) {
        $password = $this->util->generate_password();

        $save_pass = $this->db->create("employees");
        $save_pass->password = Hash::HashPassword($password);
        $result = $this->db->update("UPDATE employees SET password = '". $save_pass->password ."' WHERE mobile_no = '". $data["mobile_no"] ."'");

        return $password;
    }

    /* -- SIGN IN -- */
    private function _sign_in_send_message($data) {
        if($this->error > -1) {
            if($this->error == 0) {
                $data["user_id"] = $this->_check_if_mobile_exists($data["mobile_no"]);
                if($data["user_id"] != 0) {
                    if($data["status"] == SMS_SAFE) {
                        $data["location"] = ucwords(strtolower($data["location"]));
                        $data["no_of_persons"] = 0;
                        $data["need_help"] = 0;
                    } else {
                        $data["location"] = ucwords(strtolower($data["location"]));
                        $data["no_of_persons"] = $data["number"];
                        $data["need_help"] = ($data["yes_no"] == "YES")? 0: 0;
                    }
                    $data["status"] = $this->_get_status_id($data["status"]);
                    $data["message"] = ucwords(strtolower($data["message"]));
                    $data["longitude"] = 0;
                    $data["latitude"] = 0;

                    $disasters = array();

                    foreach($this->open_disasters as $od):
                        array_push($disasters, $od->disaster_id);
                    endforeach;

                    $data["disaster_id"] = $disasters; //get current disaster

                    if($this->_save_checkin($data)){
                        $data["message"] = MSG_SIGNIN_SUCCESS;
                        $sms_res = $this->smslib->sendMessage($data);
                    } else {
                        $data["message"] = MSG_SIGNIN_ERROR;
                        $sms_res = $this->smslib->sendMessage($data);
                    }

                } else {
                    $data["message"] = MSG_SIGNIN_NOT_WHITELISTED;
                    $sms_res = $this->smslib->sendMessage($data);
                }
            } else if ($this->error == 1) {
                $data["message"] = MSG_SIGNIN_DISASTER_OFF;
                $sms_res = $this->smslib->sendMessage($data);
            } else if ($this->error == 2) {
                $data["message"] = MSG_SIGNIN_WRONG_FORMAT;
                $sms_res = $this->smslib->sendMessage($data);
            } else if ($this->error == 3) {
                $data["message"] = MSG_LOCATION_MISSING;
                $sms_res = $this->smslib->sendMessage($data);
            } else {
                $data["message"] = MSG_SIGNIN_ERROR;
                $sms_res = $this->smslib->sendMessage($data);
            }
        } else {
            $data["message"] = MSG_SIGNIN_ERROR;
            $sms_res = $this->smslib->sendMessage($data);
        }
    }

    private function _sign_in($data) {
        $this->_check_signin_format($data);
    }

    private function _check_signin_format($data) {
        $signin_data = array();

        if($this->_check_disaster_mode($data) == 1) {
            $signin_data = $this->_signin_split_data($data["format"]);

            if($signin_data != false || $signin_data != null) {
                if($signin_data["status"] == SMS_SAFE) {
                    if($signin_data["keyword"] == SMS_KEYWORD_SIGNIN) {
                        if($signin_data["location"] != ""){
                            $this->error = 0;
                        } else {
                            $this->error = 3;
                        }
                    } else {
                        $this->error = 2; // invalid format
                    }
                }
                else if($signin_data["status"] == SMS_THREAT
                    || $signin_data["status"] == SMS_URGENT) {
                    if($signin_data["keyword"] == SMS_KEYWORD_SIGNIN) {
                        if($signin_data["location"] != ""){
                            $this->error = 0;
                        } else {
                            $this->error = 3; // location missing
                        }
                    } else {
                        $this->error = 2; // invalid format
                    }
                }
                else {
                    $this->error = 2; // invalid format
                }
            } else {
                $this->error = 2; // invalid format
            }
        } else {
            $this->error = 1; // disaster mode off
        }

        print_r($signin_data);

        $info = array();
        $info["mobile_no"] = $data["mobile_no"];
        $info["yes_no"] = SMS_YES;
        $info["number"] = 0;

        if($signin_data != false) {
            $info = array_merge($info, $signin_data);
        }

        $this->_sign_in_send_message($info);
    }

    private function _signin_split_data($data) {
        $incoming_details = array();
        $data_details_array = array();
        $data_details = "";
        $status = null;
        $data_vals = str_replace(" ","_",$data);
        $values = explode("_", $data_vals);

        if($values > 2) {
            $incoming_details["keyword"] = $values[0];


            foreach($values as $key=>$val):
                if($key != '0') :
                    if($data_details == "") {
                        $data_details = ucwords($val);
                    } else {
                        $data_details .= " " . ucwords($val);
                    }
                endif;
            endforeach;

        }

        $data_details_array = explode("/", $data_details);

        if(count($data_details_array)== 2 || count($data_details_array) == 3) {

            foreach($data_details_array as $key => $val):
                if(0 == $key) {
                    $incoming_details["status"] = $val;
                } else if(1 == $key) {
                    $incoming_details["location"] = $val;
                } else if(2 == $key) {
                    $incoming_details["message"] = $val;
                }
            endforeach;

            return $incoming_details;

        } else {
            return false;
        }



    }

    private function _signin_check_type($data) {
        $incoming_details = array();
        $data_details_array = array();
        $data_details = "";
        $status = null;

        $incoming_details["keyword"] = $data[0];

        foreach($data as $key=>$val):
            if($key != '0') :
                $data_details .= $val;
            endif;
        endforeach;

        $data_details_array = explode($data_details,"/");

        //return $status;
    }

    private function _signin_split_safe($data) {
        $signin_data = array();
        $signin_data['message'] = '';
        $signin_data['location'] = '';
        $msg = false;

        foreach($data as $key=>$val):
            if($key == '0') {
                $signin_data['keyword'] = $val;
            } else if($key == '1') {
                $signin_data['status'] = $val;
            } else if($key != '0'
                && $key != '1') {

                if(strpos($val,"MSG:") === 0) {
                    $msg = true;
                    $new_val = str_replace("MSG:", "", $val);
                    $val = $new_val;
                }

                if($msg == false) {
                    if(strpos($val, "LOC:") === 0) {
                        $new_val = str_replace("LOC:", "", $val);
                        $val = $new_val;
                    }

                    if($signin_data["location"] == '') {
                        $signin_data["location"] = $val;
                    } else {
                        $signin_data["location"] .= " " . $val;
                    }
                } else {
                    if($signin_data["message"] == '') {
                        $signin_data["message"] = $val;
                    } else {
                        $signin_data["message"] .= " " . $val;
                    }
                }
            }
        endforeach;

        return $signin_data; 
    }

    private function _signin_split_threat_urgent($data)
    {
        $signin_data = array();
        $signin_data['message'] = '';
        $signin_data['location'] = '';
        $msg = false;

        foreach ($data as $key => $val):
            if ($key == '0') {
                $signin_data['keyword'] = $val;
            } else if($key == '1') {
                $signin_data['status'] = $val;
            } else if($key != '0'
                && $key != '1') {

                if(strpos($val,"MSG:") === 0) {
                    $msg = true;
                    $new_val = str_replace("MSG:", "", $val);
                    $val = $new_val;
                }

                if($msg == false) {
                    if(strpos($val, "LOC:") === 0) {
                        $new_val = str_replace("LOC:", "", $val);
                        $val = $new_val;
                    }

                    if($signin_data["location"] == '') {
                        $signin_data["location"] = $val;
                    } else {
                        $signin_data["location"] .= " " . $val;
                    }
                } else {
                    if($signin_data["message"] == '') {
                        $signin_data["message"] = $val;
                    } else {
                        $signin_data["message"] .= " " . $val;
                    }
                }
            }
        endforeach;

        return $signin_data;
    }

    private function _split_by_uppercase($data) {
        return preg_replace('/([a-z0-9])([A-Z])/','$1 $2',$data);
    }

    private function _check_disaster_mode($data) {
        $disaster_mode = 0;

        $employee_result = $this->db->query("SELECT id, mobile_no, site_id, company_id FROM employees WHERE mobile_no = '". $data["mobile_no"] ."' and is_deleted = '" . 0 . "'");
        $employee_details = $employee_result[0];

        $disasters_result = $this->db->query("SELECT disaster_id, company_id, site_id FROM cmdctr_sites WHERE company_id = " . $employee_details->{'company_id'} ." AND " . $employee_details->{'site_id'} ."");

        if(count($disasters_result) > 0) {
            $this->open_disasters = $disasters_result;
            $disaster_mode = 1;
        } else {
            $this->open_disasters = array();
        }

        return $disaster_mode;
    }

    private function _check_if_mobile_exists($mobile) {
        $result = $this->db->query(
            "SELECT id FROM employees WHERE
            mobile_no = '".$mobile."' AND is_deleted = 0"
        );

        return ($result != null)?$result[0]->{'id'} : 0; 
    }

    private function _get_status_id($status) {
        $status_seek = $this->db->create("status");
        $status_seek->id = 0;
        $result = $this->db->query($status_seek, array("description" => strtolower($status)));

        return $result[0]->{"id"};
    }

    private function _get_current_disaster() {
        $result = $this->db->query("SELECT id FROM cmdctr ORDER BY id DESC LIMIT 1");
        return $result[0]->{'id'};
    }

    private function _save_checkin($data) {
        $date = date('Y-m-d');
        $time = date('H:i:s');

        $checkins = $this->db->create("checkins");
        $checkins->user_id = $data['user_id'];
        $checkins->status = $data['status'];
        $checkins->date_checkedin = $date;
        $checkins->time_checkedin = $time;
        $checkins->location = $data['location'];
        $checkins->message = $data['message'];
        $checkins->no_of_persons = $data['no_of_persons'];
        $checkins->longitude = $data['longitude'];
        $checkins->latitude = isset($data['latitude']) ? $data['latitude'] : 0;
        $checkins->disaster_id = count($data['disaster_id']) > 0 ? $data['disaster_id'][0] : 0;
        $checkins->city = !empty($data['location']) ? $data['location'] : 'Others';

        $checkins->need_help = $data['need_help'];
        $checkins->is_sms = 1;

        $checkin_id = 0;

        $result = false;

        if($data['disaster_id'] > 0) {
            foreach ($data['disaster_id'] as $value) {
                $signin_seek = $this->db->create(Table::CHECKINS);
                $signin_seek->id = 0;
                $previous_signin = $this->db->query($signin_seek,array("user_id"=>$checkins->user_id,"disaster_id"=>$value));

                if (count($previous_signin) > 0) {
                    $checkins->disaster_id = $value;
                    $result = $this->db->update($checkins,array("id"=>$previous_signin[0]->{'id'}));
                    $checkin_id = $previous_signin[0]->{'id'};
                } else {
                    $checkins->disaster_id = $value;
                    $result = $this->db->insert($checkins);
                    $checkin_id = $this->db->getLastInsertId();
                }

                $checkin_history_id = 0;
                //History
                $checkin_history = $this->db->create("checkin_history");
                $checkin_history->checkin_id = $checkin_id;
                $checkin_history->status = $checkins->status;
                $checkin_history->date_updated = date("Y-m-d H:i:s");
                $checkin_history->location = $checkins->location;
                $checkin_history->message = $checkins->message;
                $checkin_history->no_of_persons = $checkins->no_of_persons;
                $checkin_history->longitude = $checkins->longitude;
                $checkin_history->latitude = $checkins->latitude;
                $checkin_history->need_help = $checkins->need_help;
                $checkin_history->city = $checkins->city;
                $this->db->insert($checkin_history);

                $checkin_history_id = $this->db->getLastInsertId();

            }
        }

        if ($result) {
            $disasterList ="";
            foreach ($data['disaster_id'] as $key => $value) {
                $disasterList.= $value. ",";
            }

            $disasterList = rtrim($disasterList, ",");

            if($disasterList == "") {
                $disasterList = "NULL";
            }

            $result = $this->db->update("UPDATE cmdctr_status SET is_read = 1
                                            WHERE notification_id IN (".$disasterList.") AND user_id = ".$data['user_id']);

            $status = $data['status'];

            //FOR SUPERVISOR UPDATE
            $employee = $this->db->create("employees");
            $employee->status = $status;
            $result = $this->db->update($employee,array("id" => $data['user_id']));

        }

        return $result;
    }

    /* -- HELP -- */
    private function _help($data) {
        $this->_check_help_format($data);
    }

    private function _help_send_message($data) {
        if($this->error > -1) {
            if($this->error == 0) {
                $data["user_id"] = $this->_check_if_mobile_exists($data["mobile_no"]);
                if($data["user_id"] != 0) {
                    $data["location"] = ucwords(strtolower($data["location"]));
                    $data["no_of_persons"] = $data["number"];
                    $data["need_help"] = ($data["yes_no"] == "YES")? 1: 0;

                    $data["status"] = $this->_get_status_id($data["status"]);
                    $data["message"] = ucwords(strtolower($data["message"]));
                    $data["longitude"] = 0;
                    $data["latitude"] = 0;
						print_r($data);
                    $disasters = array();

                    foreach($this->open_disasters as $od):
                        array_push($disasters, $od->disaster_id);
                    endforeach;

                    $data["disaster_id"] = $disasters; //get current disaster

                    if($this->_save_checkin($data)){
                        $data["message"] = MSG_HELP_SUCCESS;
                        $sms_res = $this->smslib->sendMessage($data);
                    } else {
                        $data["message"] = MSG_SIGNIN_ERROR;
                        $sms_res = $this->smslib->sendMessage($data); 
                    }

                } else {
                    $data["message"] = MSG_HELP_NOT_REGISTERED;
                    $sms_res = $this->smslib->sendMessage($data);
                }
            } else if ($this->error == 1) {
                $data["message"] = MSG_HELP_DISASTER_OFF;
                $sms_res = $this->smslib->sendMessage($data);
            } else if ($this->error == 2) {
                $data["message"] = MSG_HELP_WRONG_FORMAT;
                $sms_res = $this->smslib->sendMessage($data);
            } else if ($this->error == 3) {
                $data["message"] = MSG_LOCATION_MISSING;
                $sms_res = $this->smslib->sendMessage($data);
            } else {
                $data["message"] = MSG_SIGNIN_ERROR;
                $sms_res = $this->smslib->sendMessage($data);
            }
        } else {
            $data["message"] = MSG_SIGNIN_ERROR;
            $sms_res = $this->smslib->sendMessage($data);
        }
    }

    private function _check_help_format($data) {
        if($this->_check_disaster_mode($data) == 1) {
            $help_data = $this->_help_split_format($data["format"]);
            if(count($help_data) == 3) {
                $this->error = 0;
            } else {
                $this->error = 2; //invalid format
            }
        } else {
            $this->help_error = 1;
        }

        $info = array();
        $info["mobile_no"] = $data["mobile_no"];
        $info["location"] = $help_data["location"];
        $info["yes_no"] = SMS_YES;
        $info["number"] = 0;
        $info["status"] = SMS_URGENT;
        $info["message"] = $help_data["message"];

        if($info["location"] == "") {
            $this->error = 3;
        }

        $this->_help_send_message($info);

    }

    private function _help_split_format($data) {
        $help_data = array();
        $data_split = explode(" ", $data);
        $data_details = "";
        $help_data["location"] = "";
        $help_data["keyword"] = "";
        $help_data["message"] = "";
        $msg = false;

        $help_data["keyword"] = $data_split[0];

        if(count($data_split) > 2) {
            foreach($data_split as $key => $val):
                if($key != 0):
                    if($data_details == "") {
                        $data_details = ucwords($val);
                    } else {
                        $data_details .= " " . ucwords($val);
                    }
                endif;
            endforeach;
        }
 	
if(count($data_split) > 2) {

        $data_details_array = explode("/", $data_details);
} else {
$data_details_array = explode("/", $data_split[1]);
}
        if(count($data_details_array)== 0 || count($data_details_array) > 0) {

            foreach($data_details_array as $key => $val):
                if(0 == $key) {
                    $help_data["location"] = $val;
                } else if(1 == $key) {
                    $help_data["message"] = $val;
                }
            endforeach;
//        print_r($data_details_array);
            return $help_data;

        } else {
            return false;
        }

    }

    /* -- GETPASSWORD -- */
    private function _get_password($data) {
       /* $data["message"] = MSG_GET_PASS . $this->_get_temp_password($data);
        $sms_res = $this->smslib->sendMessage($data); */
		
		$data["user_id"] = $this->_check_if_mobile_exists($data["mobile_no"]);
		if($data["user_id"] != 0) {
		$data["message"] = MSG_GET_PASS . $this->_get_temp_password($data);
		$sms_res = $this->smslib->sendMessage($data);
		} else {
		$data["message"] = MSG_SIGNIN_NOT_WHITELISTED;
		$sms_res = $this->smslib->sendMessage($data);
		}
    } 
}