<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Checkpoint extends Controller {
    public function __construct() {
        parent::__construct();
    }

    public function request_handler($request_method,$request_params) {
        redirect(base_url() . "/checkpoint/login");
    }
}