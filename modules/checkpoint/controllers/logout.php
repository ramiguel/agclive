<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Logout extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("security","session","db"));
    }

    public function request_handler($request_method,$request_params) {
        $this->security->logout($this);
    }
}