<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Auth extends Controller {
    const USERNAME_FIELD = "username";
    const PASSWORD_FIELD = "password";

    const MIN_PASSWORD_LENGTH = 6;
    const MAX_PASSWORD_LENGTH = 32;
    const MIN_USERNAME_LENGTH = 3;
    const MAX_USERNAME_LENGTH = 32;

    public function __construct() {
        parent::__construct();

        $this->load->library(array("db","input","session","hash","alert","server"));
    }

    public function index() {        
        $authenticated = false;
        $request = $this->input->get_data();

        $url_base = "/checkpoint";

        $error_message = "Invalid username/password";

        if ($this->input->request_method('POST')) {
            $post_data = $request->get_data();

            if (isset($post_data[Auth::USERNAME_FIELD]) && isset($post_data[Auth::PASSWORD_FIELD])) {
                $username = trim($post_data[Auth::USERNAME_FIELD]);
                $password = trim($post_data[Auth::PASSWORD_FIELD]);

                $username_len = strlen($username);
                $password_len = strlen($password);

                if ($username_len >= Auth::MIN_USERNAME_LENGTH && $username_len <= Auth::MAX_USERNAME_LENGTH) {
                    if ($password_len >= Auth::MIN_PASSWORD_LENGTH && $password_len <= Auth::MAX_PASSWORD_LENGTH) {

                        $users = $this->db->create(Table::USERS);
                        $this->db->add_fields($users,"id,role,password,first_name,last_name,username,session_id,is_deleted");

                        $result = $this->db->query($users,array("username" => $username,"is_deleted" => 0));

                        if (count($result) > 0 && Hash::CheckPassword($password,$result[0]->password) && $result[0]->is_deleted == 0) {

                            $result = $result[0];
                            if ($result->role == 1 || $result->role == 2) {
                                $authenticated = true;

                                $url_base = "/backoffice";

                                $this->closePreviousSession($result->session_id);

                                $session_id = $this->createSession($result->id,$result->username,$this->session->get_session_id(),$this->input->get_remote_addr());

                                $this->session->set_userdata("url_base",$url_base);
                                $this->session->set_userdata("user_id",$result->id);
                                $this->session->set_userdata("role_id",$result->role);
                                $this->session->set_userdata("full_name",$result->first_name . " " . $result->last_name);

                                $user_session = $this->db->create("users");
                                $user_session->session_id = $session_id;

                                $this->db->update($user_session,array("id" => $result->id));

                                if ($result->role == 2) {
                                    $user_id = $result->id;

                                    $result = $this->db->select(Table::COMPANY_ADMINS,"company_id,department_id",array("user_id" => $user_id));

                                    if (count($result) > 0) {
                                        $this->session->set_userdata("company_id",$result[0]->company_id);
                                        $this->session->set_userdata("department_id",$result[0]->department_id);
                                    } else {
                                        $authenticated = false;
                                        $error_message = "Error authenticating user";
                                    }
                                }
                            } else {
                                $error_message = "You are not authorized to access this page";
                            }
                        }
                    }
                }
            }
        }

        if ($authenticated) {
            print json_encode(array("response_code" => 0,
                                    "response_msg" => "Success!",
                                    "redirect_url" => base_url() . $url_base . "/home"));
        } else {            
            print json_encode(array("response_code" => 1,"response_msg" => $error_message));
        }
    }

    private function createSession($user_id,$username,$session_id,$remote_addr) {
        $current_date = date('Y-m-d H:i:s');

        $session = $this->db->create(Table::SESSION);
        $session->session_id = $session_id;
        $session->user_id = $user_id;
        $session->date_started = $current_date;
        $session->username = $username;
        $session->state = 0;
        $session->logged_in_via = 1;
        $session->last_update = $current_date;
        $session->remote_addr = $remote_addr;

        $this->db->insert($session);

        return $this->db->getLastInsertId();
    }

    private function closePreviousSession($session_id) {
        $session = $this->db->create(Table::SESSION);
        $session->state = 1;
        $this->db->update($session,array("id" => $session_id));
    }
}