 <?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * User: Cris del Rosario
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/


define ("DB_HOST","localhost");
define ("DB_USER","root");
define ("DB_PASS","");
define ("DB_NAME","agcdb");

/* end of database configuration */

define ("BASE_URL","http://localhost/agcweb/");

define ("FILE_PATH", $_SERVER['DOCUMENT_ROOT']."agcweb/assets/files/");

/***********************************************************************************************************
 *
 * This can be our first page/redirect page/first controller
 *
 **********************************************************************************************************/

define ("ENTRY_POINT", "/checkpoint/index");

/***********************************************************************************************************
 * Email/SMTP Configuration
 *
 ***********************************************************************************************************/

define ("MAIL_HOST","smtp.gmail.com");
define ("MAIL_PORT",587);

define ("MAIL_USER","smtp.dev.net@gmail.com");
define ("MAIL_PASS","m0nd0f00");

define ("MAIL_FROM","cdelrosario@yondu.com");
define ("MAIL_FROM_NAME","AGC Employee Locator Admin");

define ("MAIL_REPLY_TO","cdelrosario@yondu.com");
define ("MAIL_REPLY_TO_NAME","AGC Employee Locator Admin");

/***********************************************************************************************************
 * IOS Push Notification Certificate URL
 *
 ***********************************************************************************************************/
 
define ("IOS_PUSH_URL", BASEPATH . "cert/AGC_APNS_certificates/pem/AGCAssist_prod_apns.pem");
define ("IOS_PUSH_PASSWORD", "agclocator");

/***********************************************************************************************************
 * Google API KEY for GCM push notifications
 *
 ***********************************************************************************************************/

define("GOOGLE_API_KEY", "AIzaSyCtCUlgY3lD76nsfH7bZMZFeGZ2Y-NP3Nc"); // Place your Google API Key

/***********************************************************************************************************
 * Twitter
 *
 ***********************************************************************************************************/

define ("CONSUMER_KEY","0K7bPQv0Q41zbvvuA1xLH1XUa");
define ("CONSUMER_SECRET","ialBPK6QYs0T9MfocR3BizlIbUV6GQb0zcHJ5E8vpMLRzz75fc");
define ("ACCESS_TOKEN","3273942054-VOj8vBVB0KevJA9KC4nLOaZEfiEfBt3Syft5oo2");
define ("ACCESS_TOKEN_SECRET","whBB2M40SyMVX1Un9Tr90FJxOwuRzIkvpw6Fk6DPx8HuJ");

 /***********************************************************************************************************
  * Globelabs
  *
  ***********************************************************************************************************/
 define ("SMS_KEYWORD_REG", "REG");
 define ("SMS_KEYWORD_SIGNIN", "SIGNIN");
 define ("SMS_KEYWORD_HELP", "HELP");
 define ("SMS_KEYWORD_GETPASS", "GETPASSWORD");
 define ("SMS_SAFE", "SAFE");
 define ("SMS_THREAT", "THREAT");
 define ("SMS_URGENT", "URGENT");
 define ("SMS_YES", "YES");
 define ("SMS_NO", "NO");

 /* --- Test Account 1 --- */
 define ("SHORT_CODE", "0515");
 define ("SHORT_CODE_CROSS", "0580515");
 define ("APP_ID", "oorgSgXxGEu67Ten78ixxzu8ooRaS6Rz");
 define ("APP_SECRET", "4878f4839657529be3f07fd489f33c1a6bce6d0d4dcada862689bfc491b153b3");
 define ("GL_VERSION", "v1");
 define ("PASSPHRASE", "AyalaASSIST2015");
 define ("CORRELATOR", "12345");

// /* --- Test Account 2 --- */
// define ("SHORT_CODE", "2363");
// define ("SHORT_CODE_CROSS", "0582363");
// define ("APP_ID", "98q7soLXAzhMoi6dEecXMKh758MXs7jA");
// define ("APP_SECRET", "7086d3f4722f83c728048a294676195a0e4c4a9d4a7c41c2cf14142c4c89a355");
// define ("GL_VERSION", "v1");
// define ("PASSPHRASE", "AyalaASSIST2015");
// define ("CORRELATOR", "12345");

 define ("MSG_CCB_PREFIX", "Command Center Broadcast: ");
 define ("MSG_DISASTER_TOGGLED_ON", "Disaster Mode is toggled on");
 define ("MSG_DISASTER_TOGGLED_OFF", "Disaster Mode is toggled off");

 define ("MSG_REG_SUCCESS", "Congratulations! You have successfully registered to ASSIST!");
 define ("MSG_REG_WRONG_FORMAT", "You have used the wrong format. Please follow the format below: \nREG\<sp\>\<LastName\>");
 define ("MSG_REG_NOT_WHITELISTED", "The credentials that you have provided are not whitelisted. Please try again.");
 define ("MSG_REG_ALREADY", "The credentials that you have provided are already registered.");
 define ("MSG_REG_NON_GLOBE", "The mobile number that you have provided is a non-globe number. Please try again.");
 define ("MSG_REG_ERROR", "There was a problem encountered. Please try again.");
 define ("MSG_REG_INVALID", "The credentials that you have provided is invalid. Please try again.");

 define ("MSG_SIGNIN_SUCCESS", "You have successfully sent your Sign-In entry for ASSIST. Always be safe!");
 define ("MSG_SIGNIN_UNSUCCESS", "You have failed to send your Sign-In entry for ASSIST. Please try again later. Always be safe!");
 define ("MSG_SIGNIN_WRONG_FORMAT", "You have used the wrong format. Please follow the format below: \nSIGNIN\<sp\>\<SAFE\/THREAT\/URGENT\>/\<Location\>/\<Message\>");
 define ("MSG_SIGNIN_DISASTER_OFF", "You may not Sign-In while Disaster Mode is off. Please wait for further announcements.");
 define ("MSG_SIGNIN_ERROR", "There was a problem encountered. Please try again.");
 define ("MSG_SIGNIN_NOT_WHITELISTED", "The mobile number you are using is not registered in ASSIST. Please contact your HR Administrator.");

 define ("MSG_HELP_SUCCESS", "Your distress call has been received by ASSIST. You will be contacted ASAP.");
 define ("MSG_HELP_WRONG_FORMAT", "You have used the wrong format. Please follow the format below: \nHELP\<sp\>\<Location\>/\<Message\>");
 define ("MSG_HELP_DISASTER_OFF", "You may not Ask for Help while Disaster Mode is off. Please contact concerned authorities directy.");
 define ("MSG_HELP_NOT_REGISTERED", "The mobile number you are using is not registered in ASSIST. Please contact your HR Administrator.");

 define ("MSG_GET_PASS", "Here's your current password: ");

 define ("MSG_LOCATION_MISSING", "You have to indicate your location. Please try again.");
 define ("MSG_WRONG_FORMAT", "You have used the wrong format. Please follow the format below:  \nREG\<sp\>\<LastName\> \nOR SIGNIN\<sp\>\<SAFE\/THREAT\/URGENT\>/\<Location\>/\<Message\> \nOR HELP\<sp\>\<Location\>/\<Message\>");

 define ("MSG_DISASTER_DECLARED", "A disaster has been declared!");