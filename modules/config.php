 <?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * User: Cris del Rosario
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

require_once CONFIG_PATH . "config.production.php";

define ("DEFAULT_EMPLOYEE_PHOTO",BASE_URL . "/assets/photo/default.jpg");
define ("DEFAULT_CONTACT_PHOTO",BASE_URL . "/assets/photo/default.jpg");

define ("AGC_EMPLOYEE_LOCATOR_CMS_VERSION","1.0 r20150622");
 