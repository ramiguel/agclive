<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Event extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/event","Volunteer Maintenance");

        $this->load->library(array("uri","input","util","hash","alert","html","form","upload","page","helper"));

        $this->gender = array(
            "M" => "Male",
            "F" => "Female"
        );

        $this->post_data_keys = array(
            "event_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 256
                )
            ),
            "address" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 256
                )
            ),
            "details" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 21844
                )
            ),
            "event_date_from" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 19,
                    "max" => 19
                )
            ),
            "event_date_to" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 19,
                    "max" => 19
                )
            )
        );
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/event/" . $request_method);

        if ($request_method == "edit") {
            $this->_edit($request_params[1]);
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "add") {
            $this->_add();
        } else if ($request_method == "view") {
            $this->_view($request_params[1]);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "get-department") {
            $this->_get_company_department();
        } else if ($request_method == "get-supervisors") {
            $this->_get_supervisors();
        } else if ($request_method == "export") {
            $this->_export_data(isset($request_params[1]) ? $request_params[1] : 0);
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list(isset($request_params[1]) ? $request_params[1] : 1);
            }
        }
    }

    private function _export_data($id) {
        $headers = array("id",
                         "first_name",
                         "last_name",
                         "middle_initial",
                         "gender",
                         "mobile_no",
                         "email",
                         "position",
                         "employee_no",
                         "blood_type",
                         "company",
                         "department",
                         "address_street",
                         "address_line_2",
                         "address_city",
                         "address_state",
                         "address_country",
                         "address_zip_code",
                         "is_supervisor",
                         "is_rescuer", 
                         "supervisor_id");

        $company_filter = "";
        if ($id > 0) {
            $this->export_data("employee",$headers,"SELECT e.id,
                                                           e.first_name,
                                                           e.last_name,
                                                           e.middle_initial,
                                                           e.gender,
                                                           e.mobile_no,
                                                           e.email,
                                                           e.position,
                                                           e.employee_no,
                                                           e.blood_type,
                                                           c.company_name AS company,
                                                           d.department_name AS department,
                                                           e.address_street,
                                                           e.address_line_2,
                                                           e.address_city,
                                                           e.address_state,
                                                           e.address_country,
                                                           e.address_zip_code,
                                                           e.is_supervisor,
                                                           e.is_rescuer,
                                                           e.supervisor_id
                                                           FROM employees AS e 
                                                           INNER JOIN companies AS c ON c.id = e.company_id
                                                           INNER JOIN department AS d ON d.id = e.department_id
                                                           WHERE e.is_deleted <> 1 AND e.company_id = $id
                                                           ORDER BY e.first_name,e.last_name ASC");
        } else {
            $this->export_data("employee",$headers,"SELECT e.id,
                                                           e.first_name,
                                                           e.last_name,
                                                           e.middle_initial,
                                                           e.gender,
                                                           e.mobile_no,
                                                           e.email,
                                                           e.position,
                                                           e.employee_no,
                                                           e.blood_type,
                                                           c.company_name AS company,
                                                           d.department_name AS department,
                                                           e.address_street,
                                                           e.address_line_2,
                                                           e.address_city,
                                                           e.address_state,
                                                           e.address_country,
                                                           e.address_zip_code,
                                                           e.is_supervisor,
                                                           e.is_rescuer,
                                                           e.supervisor_id
                                                           FROM employees AS e
                                                           INNER JOIN companies AS c ON c.id = e.company_id
                                                           INNER JOIN department AS d ON d.id = e.department_id
                                                           WHERE e.is_deleted <> 1 
                                                           ORDER BY e.first_name,e.last_name ASC");
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR id = " . $text;
            }
            $search_by = "AND (event_name LIKE '%$text%' OR details LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT COUNT(id) AS total FROM events
                                   WHERE is_closed <> 1 $search_by");
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT id,
                                    CONCAT(
                                        DATE_FORMAT(event_date_from, '%b %d, %Y %h:%i %p'),
                                        ' - ',
                                        DATE_FORMAT(event_date_to, '%b %d, %Y %h:%i %p')
                                    ) AS event_date,
                                    event_name,details,
                                    DATE_FORMAT(date_created, '%b %d, %Y %h:%i %p') AS date_created FROM events WHERE is_closed <> 1
                                    $search_by LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _list($page) {
        $company_list = "";
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
            $company_id = $this->session->userdata("company_id");
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 AND id = $company_id");
            foreach ($result as $data) {
                $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
            }
        } else {
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
            $company_list = "<option value=\"0\">All</option>";
            foreach ($result as $data) {
                $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
            }
        }

        //$this->set("company_list",$company_list);

        $this->render("index","event/list","AGC Employee Locator | CMS");
    }

    private function _edit($id) {
        $error = true;
        
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            // $event_date = explode(" - ", $post_data['event_date']);
            // $post_data['event_date_from'] = $event_date[0].":00";
            // $post_data['event_date_to'] = $event_date[1].":00";

            $this->event = $this->db->create("events");
            $this->event->date_modified = $this->get_current_date();
            $this->event->modified_by = $this->session->userdata("user_id");

            $error_message = "";

            $this->original_event = $this->db->create("events");
            foreach ($this->post_data_keys as $key => $data) {
                $this->original_event->$key = "";
            }

            $result = $this->db->query($this->original_event,array("id" => $id));
            if (count($result) > 0) {
                $this->original_event = $result[0];
            }

            $this->log_open(1,"update","employees");

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->event->$key = trim($value);
                if ($this->event->$key != $this->original_event->$key) {
                    $this->log_changes($key,$this->original_event->$key,$this->event->$key);
                }
            },function($error_type,$key) {
            });

            if (strtotime($post_data["event_date_from"]) > strtotime($post_data["event_date_to"])) {
                $error_count++;
                $error_message .= " Invalid date range.";
            }

            if ($error_count == 0) {
                if (!empty($this->event->event_name)) {
                    $event_seek = $this->db->create("events");
                    $event_seek->id = 0;

                    $result = $this->db->query($event_seek,array("event_name" => $this->event->event_name,"is_closed" => 0));
                    if (count($result) > 0 && $result[0]->id) {
                        if ($result[0]->id != $id) {
                            $error_type = 1;
                            $error_count++;
                            $error_message.= "Event name already exists";
                        }
                    }
                }
            }

            if ($error_count == 0) {
              $result = $this->db->update("DELETE FROM event_skills WHERE event_id =".$id);

              if($result) {
                foreach ($post_data['skill_id'] as $key => $value) {
                    $skill_id = 0;
                    if ($value === '') {
                        $skill_desc = $post_data['skill'][$key];
                        $skill = $this->db->create("skills");
                        $skill->description = $skill_desc;
                        $this->db->insert($skill);
                        $skill_id = $this->db->getLastInsertId();
                    } else {
                        $skill = $this->db->create("skills");
                        $skill->description = $post_data['skill'][$key];
                        $this->db->update($skill,array("id"=>$value));
                        $skill_id = $value;
                    }

                    $event_skill = $this->db->create("event_skills");
                    $event_skill->event_id = $id;
                    $event_skill->skill_id = $skill_id;
                    $this->db->insert($event_skill);
                }
              } else {
                $error_count++;
              }
            }

            if ($error_count == 0) {
                $this->log_close();

                $this->db->update($this->event, array("id" => $id));

                print json_encode(array("response_code" => 0,"response_msg" => "Data updated successfully!"));
            } else {
                print json_encode(array("response_code" => 1, "response_msg" => $error_message));
            }
        } else {
            if (ctype_digit($id)) {
                $event = $this->db->create("events");
                $post_keys = array("id",
                                    "event_name",
                                    "event_date_from",
                                    "event_date_to",
                                    "details",
                                    "address");

                foreach ($post_keys as $key) {
                    $event->$key = "";
                }

                $result = $this->db->query($event,array(
                    "id" => $id
                ));

                if (count($result) > 0 && $result[0]->id) {
                    $employee = $result[0];

                    foreach ($post_keys as $key) {
                        $this->set($key,$employee->$key);
                    }

                    $result = $this->db->query("SELECT skills.id,skills.description FROM event_skills 
                                                LEFT JOIN skills ON skills.id = event_skills.skill_id
                                                WHERE event_skills.event_id = ".$id);
                    $skill_list = "";
                    $skill_data = "";
                    if (count($result) > 0) {
                        //foreach ($result[0] as $key) {
                            $skill_id = $result[0]->{'id'};
                            $description = $result[0]->{'description'};
                            $skill_list.= "<input type=\"text\" class=\"form-control\" name=\"skill[]\" placeholder=\"Skill\" value=\"$description\" required>";
                            $skill_list.= "<input type=\"hidden\" class=\"form-control\" name=\"skill_id[]\" placeholder=\"Skill\" value=\"$skill_id\" required>";
                        //}

                        array_shift($result);
                        $skill_sep = "";
                        foreach ($result as $data) {
                            $skill_data.=$skill_sep.json_encode($data);
                            $skill_sep = ",";
                        }
                    }

                    $this->set("skill_list",$skill_list);
                    $this->set("skill_data",$skill_data);

                    $this->render("index","event/edit","AGC Employee Locator | CMS");
                    $error = false;
                }
            }
        }
    }

    private function _view($id) {
        $error = true;
        if (ctype_digit($id)) {
            $event = $this->db->create("events");
            $post_keys = array("id",
                                "event_name",
                                "event_date_from",
                                "event_date_to",
                                "details",
                                "address");

            foreach ($post_keys as $key) {
                $event->$key = "";
            }

            $result = $this->db->query($event,array(
                "id" => $id
            ));

            if (count($result) > 0 && $result[0]->id) {
                $employee = $result[0];

                foreach ($post_keys as $key) {
                    $this->set($key,$employee->$key);
                }

                $result = $this->db->query("SELECT skills.id,skills.description FROM event_skills 
                                            LEFT JOIN skills ON skills.id = event_skills.skill_id
                                            WHERE event_skills.event_id = ".$id);
                $skill_list = "";
                $skill_data = "";
                if (count($result) > 0) {
                    $skill_id = $result[0]->{'id'};
                    $description = $result[0]->{'description'};
                    $skill_list.= "<input type=\"text\" class=\"form-control\" name=\"skill[]\" placeholder=\"Skill\" value=\"$description\" required readonly>";
                    $skill_list.= "<input type=\"hidden\" class=\"form-control\" name=\"skill_id[]\" placeholder=\"Skill\" value=\"$skill_id\" required readonly>";

                    array_shift($result);
                    $skill_sep = "";
                    foreach ($result as $data) {
                        $skill_data.=$skill_sep.json_encode($data);
                        $skill_sep = ",";
                    }
                }

                $this->set("skill_list",$skill_list);
                $this->set("skill_data",$skill_data);

                $employee_list = "";
                $skill_set = $this->db->query("SELECT es.skill_id,s.description 
                                                FROM event_skills es LEFT JOIN skills s ON s.id = es.skill_id 
                                                WHERE es.event_id=".$id);

                $employee_list .= "<div class=\"form-group\">";
                $employee_list.=  "  <label for=\"date_created\" class=\"control-label col-xs-2\">Volunteers</label>";
                $employee_list.=  "  <div class=\"col-xs-10\">";

                if(count($skill_set) > 0) {
                    $employee_list.= "<table class=\"table table-striped table-responsive\">";
                    $employee_list.= "   <thead>";
                    $employee_list.= "       <tr>";
                    $employee_list.= "          <th>Full name</th>";
                    foreach ($skill_set as $data) {
                        $employee_list.= "      <th>$data->description</th>";
                    }
                    $employee_list.= "       </tr>";
                    $employee_list.= "   </thead>";

                    $result = $this->db->query("SELECT ev.id AS event_volunteer_id, ev.user_id,
                                        CONCAT(emp.first_name,' ',emp.last_name) AS full_name,
                                        date_volunteered,es.skill_id AS event_skills, 
                                        IF(SUM(IF(evs.skill_id=es.skill_id,1,0))>=1,'Yes','No') AS is_checked
                                        FROM event_volunteers ev 
                                        LEFT JOIN event_volunteer_skills evs ON ev.id = evs.event_volunteer_id
                                        LEFT JOIN event_skills es ON es.event_id = ev.event_id
                                        LEFT JOIN employees emp ON emp.id = ev.user_id
                                        WHERE ev.event_id = $id GROUP BY ev.id,es.skill_id
                                        ORDER BY ev.id,es.skill_id");

                    $employee_list.= "  <tbody>";

                    if (count($result) > 0) {
                        //var_dump("first row");
                        $link = base_url() . "/backoffice/whitelist/view/" . $result[0]->{'user_id'};
                        $employee_list.= "<tr>";
                        $employee_list.= "<td><a href=\"$link\">".$result[0]->{'full_name'}."</td>";
                        $employee_list.= "<td>".$result[0]->{'is_checked'}."</td>";

                        array_shift($result);

                        $rowCount = 1;
                        foreach ($result as $data) {
                            if ($rowCount % count($skill_set) == 0) {
                                //var_dump("new row");
                                $link = base_url() . "/backoffice/whitelist/view/" . $data->user_id;
                                $employee_list.= "</tr>";
                                $employee_list.= "<tr>";
                                $employee_list.= "<td><a href=\"$link\">".$data->full_name."</td>";
                                $employee_list.= "<td>$data->is_checked</td>";
                            } else {
                                //var_dump("new col");
                                $employee_list.= "<td>$data->is_checked</td>";
                            }
                            $rowCount++;
                        }
                        $employee_list.= "</tr>";
                    } else {
                        $employee_list.= "<tr>";
                        $employee_list.= "<td colspan=".(count($skill_set)+1)." align=\"center\">No volunteers yet</td>";
                        $employee_list.= "</tr>";
                    }
                    $employee_list.= "</tbody>";
                    $employee_list.= "</table>";
                }

                $employee_list.=  "  </div>";
                $employee_list.=  "</div>";

                $this->set('employee_list',$employee_list);
                $this->render("index","event/view","AGC Employee Locator | CMS");
            
                $error = false;
            }
        }

        if ($error) {
            $this->alert->set_message(1,"");
            redirect(base_url() . "/backoffice/whitelist");
        }
    }

    private function _delete($id) {
        $error = true;
        if (ctype_digit($id)) {
            $employee = $this->db->create("events");
            $employee->date_closed = date('Y-m-d H:i:s');
            $employee->is_closed = 1;

            $this->log_open(1,"delete","events");

            $this->db->update($employee,array("id" => $id));
            $error = false;
        }

        if ($error) {
            $this->log_close();
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function _add() {
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            // $event_date = explode(" - ", $post_data['event_date']);
            // $post_data['event_date_from'] = $event_date[0].":00";
            // $post_data['event_date_to'] = $event_date[1].":00";
            
            $this->event = $this->db->create("events");
            $this->event->date_created = $this->get_current_date();
            $this->event->created_by = $this->session->userdata("user_id");

            $error_message = "";

            $this->log_open(1,"insert","event");

            /* Start validating forms based on the rule provided */

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->event->$key = trim($value);
                $this->log_insert($key,$this->event->$key);
            },function($error_type,$key) {
            });

            $error = true;


            if (strtotime($post_data["event_date_from"]) > strtotime($post_data["event_date_to"])) {
                $error_count++;
                $error_message .= " Invalid date range.";
            }

            if ($error_count == 0) {
                if (!empty($this->event->event_name)) {
                    $event_seek = $this->db->create("events");
                    $event_seek->id = 0;

                    $result = $this->db->query($event_seek,array("event_name" => $this->event->event_name,"is_closed" => 0));
                    if (count($result) > 0 && $result[0]->id) {
                        $error_count++;
                        $error_message.= "Event name already exists";
                    }
                }
            }

            if ($error_count == 0) {
                $this->log_close();

                $this->db->insert($this->event);
                $event_id = $this->db->getLastInsertId();

                foreach ($post_data['skill'] as $value) {
                    $skill = $this->db->create("skills");
                    $skill->description = $value;
                    $this->db->insert($skill);
                    $skill_id = $this->db->getLastInsertId();

                    $event_skill = $this->db->create("event_skills");
                    $event_skill->event_id = $event_id;
                    $event_skill->skill_id = $skill_id;
                    $this->db->insert($event_skill);
                }

                $error = false;
            }

            if ($error) {
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            }
        } else {
            $this->set("date_now",date("Y-m-d H:i:s"));
            $this->render("index", "event/add", "AGC Employee Locator | CMS");
        }
    }

    private function _bulk_upload() {
        
    }
}