<?php

class Gcash extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/gcash", "GCash");

        $this->load->library(array("uri","input","util","hash","alert","html","form","upload","page","helper"));

        $this->post_data_keys = array(
            "mobile_no" => array(
                "required" => true,
                "type" => Form::TYPE_MOBILE,
                "extras" => array(
                    "min" => 11,
                    "max" => 11
                )
            ),
            "employee_id" => array(
                "required" => true,
                "type" => Form::TYPE_INT,
                "extras" => array()
            )
        );

        $this->prefixes = null;
    }

    public function request_handler($request_method, $request_params) {
        //$this->checkpoint("/backoffice/gcash/" . $request_method);

        if($request_method == "add") {
            $this->_add();
        } else if ($request_method == "edit") {
            $this->_edit($request_params[1]);
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "get-department") {
            $this->_get_company_department();
        } else if ($request_method == "get-employees") {
            $this->_get_employees();
        } else if ($request_method == "get-mobile") {
            $this->_get_mobile();
        } else if ($request_method == "check-prefix") {
            $this->_check_prefix();
        } else if ($request_method == "check-if-existing") {
            $this->_check_if_existing();
        } else if ($request_method == "check-if-existing-edit") {
            $this->_check_if_existing_edit();
        } else if ($request_method == "check-if-already-added") {
            $this->_check_has_account();
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list(isset($request_params[1]) ? $request_params[1] : 1);
            }
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());
        $search_by = "";

        if(isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if(ctype_digit($text)) {
                $search_id = "OR e.id = $text";
            }
            $search_by = "AND (c.company_name LIKE '%$text%' $search_id OR CONCAT(TRIM(e.first_name),' ',TRIM(e.last_name)) LIKE '%$text%' OR g.mobile_no LIKE '%$text%')";
        }

        $total = $this->get_total("SELECT COUNT(g.id) as total
                                FROM gcash as g INNER JOIN employees as e
                                ON g.employee_id = e.id
                                INNER JOIN companies as c
                                ON e.company_id = c.id
                                WHERE g.is_deleted <> 1 $search_by ");

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT g.id as id, CONCAT(e.first_name, ' ', e.last_name) as full_name, c.company_name as company, g.mobile_no as mobile_no, g.date_created as created
                                FROM gcash as g INNER JOIN employees as e
                                ON g.employee_id = e.id
                                INNER JOIN companies as c
                                ON e.company_id = c.id
                                WHERE g.is_deleted <> 1 $search_by
                                ORDER BY e.first_name, e.last_name ASC LIMIT $offset, $limit");

        $this->send($total, $result);
    }

    private function _list($page) {

        $this->render("index","donate/gcash/list","AGC Employee Locator | CMS");
    }

    private function _add() {

        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $error_message = "";

            $this->gcash = $this->db->create("gcash");
            $this->gcash->date_created = $this->get_current_date();
            $this->gcash->created_by = $this->session->userdata("user_id");

            $this->log_open(1, "insert", "gcash");
            $error_count = 0;

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value){

                if (in_array($key,array("employee_id", "mobile_no"))) {
                    $this->gcash->$key = $value;
                } else {
                    $this->gcash->$key = $value;
                    $this->log_insert($key,$value);
                }

            },function($error_type,$key) {
            });

            $error = true;

            if ($error_count == 0) {
                $this->log_close();
                $this->db->insert($this->gcash);
                $error = false;
            } else {
                $error = true;
                $error_message = "Error in adding gcash account.";
            }

            if ($error) {
                $this->log_close();
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            }

        }
        else {
            $company_list = "";
            $department_list = "";
            $company = $this->db->create("companies");
            $company->id = 0;
            $company->company_name = "";
            $company->setOrderBy("company_name","ASC");

            $company_id = 0;

            $company_filter = array("is_deleted" => "0");
            if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
                $company_id = $this->session->userdata("company_id");
                $company_filter=array("id" => $company_id,"is_deleted" => "0");
            } else {
                $company_list = "<option value=\"0\">Ayala Group of Companies</option>";
                $department_list = "<option value=\"0\">All Department</option>";
            }

            $result = $this->db->query($company,$company_filter);
            if (count($result) > 0) {
                foreach ($result as $company):
                    $company_list .= $this->html->option($company->company_name,$company->id);
                endforeach;
                $company_id = $result[0]->id;
            }

            $this->set("company_list",$company_list);

            if ($company_id > 0) {
                $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id . " AND c.is_deleted <> 1 ORDER BY d.department_name ASC");

                if (count($result) > 0) {
                    foreach ($result as $item):
                        $department_list .= $this->html->option($item->department_name,$item->id);
                    endforeach;
                }
            }

            $this->set("department_list",$department_list);

            $employee_list = "";
            $result = $this->db->query("SELECT id, CONCAT(first_name, ' ', last_name) as employee_name FROM employees WHERE company_id = $company_id AND is_deleted <> 1 ORDER BY first_name ASC");

            if(count($result) > 0) {
                foreach ($result as $data) :
                    $employee_list .= $this->html->option($item->employee_name, $item->id);
                endforeach;
            }


            $this->render("index","donate/gcash/add","AGC Employee Locator | CMS");
        }

    }

    private function _edit($id) {
        $error = true;
        if($this->input->request_method("POST")) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $this->gcash = $this->db->create("gcash");
            $this->gcash->date_modified = $this->get_current_date();
            $this->gcash->modified_by = $this->session->userdata("user_id");

            $this->original_gcash = $this->db->create("gcash");
            foreach($this->post_data_keys as $key => $data):
                $this->original_gcash->$key = "";
            endforeach;

            $result = $this->db->query($this->original_gcash, array("id" => $id));
            if(count($result) > 0) {
                $this->original_gcash = $result[0];
            }

            $this->log_open(1, "update", "gcash");

            $error_count = $this->form->validate($this->post_data_keys, $post_data, function($key, $value) {
                $this->gcash->$key = trim($value);
                if($this->gcash->$key != $this->original_gcash->$key) {
                    $this->log_changes($key, $this->original_gcash->$key, $this->gcash->$key);
                }
            },function($error_type,$key) {
            });

            if($error_count == 0) {
                $this->log_close();

                $this->db->update($this->gcash, array("id" => $id));
                print json_encode(array("response_code" => 0,"response_msg" => "Data updated successfully!"));
            } else {
                print json_encode(array("response_code" => 1, "response_msg" => "Error updating data!"));
            }

        } else {
            if(ctype_digit($id)) {
                $gcash = $this->db->create("gcash");
                $post_keys = array("id",
                            "company_id",
                            "employee_id",
                            "department_id",
                            "mobile_no");

                foreach ($post_keys as $key):
                    $gcash->$key = "";
                endforeach;

                $result = $this->db->query("SELECT distinct g.id AS id, c.id as company_id, e.id as employee_id, g.mobile_no as mobile_no, d.department_id as department_id
                            from gcash as g Inner join employees as e
                            ON g.employee_id = e.id
                            inner join companies as c
                            on e.company_id = c.id
                            INNER JOIN company_department as d
                            ON d.company_id	= c.id
                            where g.is_deleted <> 1 ");

                if(count($result) > 0 && $result[0]->id) {
                    $gcash = $result[0];

                    foreach($post_keys as $key):
                        $this->set($key,$gcash->$key);
                    endforeach;

                    $company_list = "";
                    $department_list = "";
                    $company_filter = array("is_deleted" => "0");

                    $company_list .= "<option value=\"\">Select Company</option>";
                    $department_list = "<option value=\"\">Select Department</option>";

                    $company = $this->db->create("companies");
                    $company->id = 0;
                    $company->company_name = "";
                    $company->setOrderBy("company_name","ASC");

                    $result = $this->db->query($company,$company_filter);
                    if (count($result) > 0) {
                        $company_list .= $this->html->option("Ayala Group of Companies",0,true);
                        foreach ($result as $company) {
                            if ($company->id == $gcash->company_id) {
                                $company_list .= $this->html->option($company->company_name,$company->id,true);
                            } else {
                                $company_list .= $this->html->option($company->company_name,$company->id);
                            }
                        }
                    }

                    $this->set("company_list", $company_list);

                    if($company->id > 0) {
                        $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                                    LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                                    LEFT JOIN companies AS c ON c.id = cd.company_id
                                                    WHERE c.id = " . $gcash->company_id . " AND d.is_deleted <> 1 ORDER BY d.department_name ASC");

                        if (count($result) > 0) {
                            foreach ($result as $item) {
                                if ($item->id == $gcash->department_id) {
                                    $department_list .= $this->html->option($item->department_name,$item->id,true);
                                } else {
                                    $department_list .= $this->html->option($item->department_name,$item->id);
                                }
                            }
                        }
                    }

                    $this->set("department_list", $department_list);

                    $employee_list = "";
                    $result = $this->db->query("SELECT id, CONCAT(first_name, ' ', last_name) as employee_name FROM employees WHERE company_id = $gcash->company_id AND is_deleted <> 1 ORDER BY first_name ASC");

                    if(count($result) > 0) {
                        foreach ($result as $item) :
                            if($gcash->employee_id == $item->id) {
                                $employee_list .= $this->html->option($item->employee_name, $item->id, true);
                            } else {
                                $employee_list .= $this->html->option($item->employee_name, $item->id);
                            }
                        endforeach;
                    }

                    $this->set("employee_list", $employee_list);
                    $this->set("mobile_no", $gcash->mobile_no);

                    $this->render("index","donate/gcash/edit","AGC Employee Locator | CMS");
                    $error = false;
                }
            }
        }
    }

    private function _delete($id) {
        $error = true;
        if(ctype_digit($id)) {
            $gcash = $this->db->create("gcash");
            $gcash->date_deleted = $this->get_current_date();
            $gcash->deleted_by = $this->session->userdata("user_id");
            $gcash->is_deleted = 1;

            $this->log_open(1, "delete", "gcash");

            $this->db->update($gcash, array("id" => $id));
            $error = false;
        }

        if ($error) {
            $this->log_close();
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function _get_company_department() {
        $response = create_response(1,"Error getting department data");

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            $company_id = 0;
            if (isset($data->{'company_id'})) {
                $company_id = $data->{'company_id'};
            }

            if ($company_id > 0) {
                $result = $this->db->query("SELECT d.id,
                                                   d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id);

                if (count($result) > 0) {
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);
    }

    private function _get_employees() {
        $response = create_response(1,"Error getting employee data");

        $company_id = 0;

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            if (isset($data->{'company_id'})) {
                $company_id = $data->{'company_id'};
            }

            if ($company_id > 0) {
                $result = $this->db->query("SELECT id,CONCAT(first_name,' ',last_name) AS employee_name
                                            FROM employees
                                            WHERE company_id = $company_id AND is_deleted <> 1");
                if (count($result) > 0) {
                    foreach ($result as $key=>$val):
                        $result[$key]->full_name = utf8_encode($result[$key]->employee_name);
                    endforeach;
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);
    }

    private function _get_mobile() {
        $response = create_response(1,"Error getting employee data");

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            $employee_id = 0;
            if (isset($data->{'employee_id'})) {
                $employee_id = $data->{'employee_id'};
            }

            if ($employee_id > 0) {
                $result = $this->db->query("SELECT id, mobile_no FROM employees
                                            WHERE id = " . $employee_id);

                if (count($result) > 0) {
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);
    }

    private function find_company($company) {
        $companies = $this->db->create("companies");
        $companies->id = 0;
        $company_id = 0;
        $result = $this->db->query($companies,array("company_name" => $company));
        if (count($result) > 0) {
            $company_id = $result[0]->id;
        }
        return $company_id;
    }

    private function find_department($department) {
        $departments = $this->db->create("department");
        $departments->id = 0;
        $department_id = 0;
        $result = $this->db->query($departments,array("department_name" => $department));
        if (count($result) > 0) {
            $department_id = $result[0]->id;
        }

        return $department_id;
    }

    private function _check_if_existing() {
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            $mobile_no = 0;
            if (isset($data->{'data'})) {
                $mobile_no = $data->{'data'};
            }

            $result = $this->check_all_accounts($mobile_no);

            if($result > 0) {
                echo "none";
            } else {
                echo "ok";
            }
        }
    }

    private function _check_if_existing_edit() {
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            $mobile_no = 0;
            $employee_id = 0;
            if (isset($data->{'data'})) {
                $mobile_no = $data->{'data'};
                $employee_id = $data->{'employee_id'};
            }

            $result = $this->check_all_accounts($mobile_no);
            if($result > 0) {
                if($result != $employee_id) {
                    echo "none";
                } else {
                    echo "ok";
                }
            } else {
                if(!$this->check_mobile($mobile_no, $employee_id)) {
                    echo "ok";
                } else {
                    echo "none";
                }
            }
        }
    }

    private function _check_has_account() {
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            $employee_id = 0;
            if (isset($data->{'data'})) {
                $employee_id = $data->{'employee_id'};
            }

            $result = $this->check_if_added_already($employee_id);

            if($result > 0) {
                echo "none";
            } else {
                echo "ok";
            }
        }
    }

    private function check_all_accounts($mobile_no) {
        $employees = $this->db->create("gcash");
        $employees->id = 0;
        $employees->employee_id = 0;
        $employee_id = 0;
        $result = $this->db->query($employees,array("mobile_no" => $mobile_no));
        if (count($result) > 0) {
            $employee_id = $result[0]->employee_id;
        }

        return $employee_id;
    }

    private function check_mobile($mobile_no, $employee_id) {
        $gcash = $this->db->create("gcash");
        $gcash->id = 0;
        $gcash->mobile_no = 0;
        $is_not = false;
        $result = $this->db->query($gcash, array("mobile_no" => $mobile_no));

        if (count($result) > 0) {
            if($result[0]->employee_id != $employee_id) {
                $is_not = true;
            }
        }

        return $is_not;
    }

    private function check_if_added_already($employee_id)  {
        $gcash = $this->db->create("gcash");
        $gcash->id = 0;
        $gcash_id = 0;
        $result = $this->db->query($gcash,array("employee_id" => $employee_id));
        if (count($result) > 0) {
            $gcash_id = $result[0]->id;
        }

        return $gcash_id;
    }

    private function _check_prefix() {
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            $mobile_no = 0;
            if (isset($data->{'data'})) {
                $mobile_no = $data->{'data'};
            }

            $result = $this->checking($mobile_no);

            if($result) {
                echo "ok";
            } else {
                echo "none";
            }
        }
    }

    private function checking($passed_data) {
        $this->prefixes = $this->db->query("
                    SELECT number_range, start_msisdn,
                        end_msisdn, brand
                        FROM prefixes
                    ");

        $mobile_no = substr($passed_data, 1);

        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;

        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }
}