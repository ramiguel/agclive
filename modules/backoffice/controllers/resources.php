<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Resources extends CMS_Controller {
    private $post_data_keys = array();
    private $error_message;
    private $resource = null;

    public function __construct() {
        parent::__construct("/backoffice/resources","Official Links Maintenance");

        $this->load->library(array("uri","input","form","alert","upload"));

        $this->post_data_keys = array(
            "description" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 64
                )
            ),
            "url" => array(
                "required" => true,
                "type" => Form::TYPE_URL,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            )
        );
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/resources/" . $request_method);
        if ($request_method == "add") {
            $this->_add();
        } else if ($request_method == "edit") {
            $this->_edit($request_params[1]);
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "export") {
            $this->_export_data();
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list();
            }
        }
    }

    private function _export_data() {
        $headers = array("id",
                         "description",
                         "url");

        $this->export_data("official_links-",$headers,"SELECT " . implode(",",$headers) . " FROM resources WHERE is_deleted <> 1 ORDER BY id ASC");
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR id = " . $text;
            }
            $search_by = "AND (description LIKE '%$text%' OR url LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT COUNT(id) AS total FROM resources
                                   WHERE is_deleted <> 1 $search_by");
        
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT id,description,url,date_created FROM resources
                                    WHERE is_deleted <> 1 $search_by ORDER BY description ASC LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _list() {
        $this->render("index","resources/list","AGC Employee Locator | CMS");
    }

    private function _add() {
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $this->resource = $this->db->create("resources");
            $this->resource->date_created = $this->get_current_date();
            $this->resource->created_by = $this->session->userdata("user_id");

            $this->log_open(2,"insert","resources");

            $this->error_message = "Error adding record! ";

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->resource->$key = $value;
                $this->log_insert($key,$this->resource->$key);
            },function($error_type,$key) {
                if ($error_type == Form::ERROR_VALIDATION_FAILED) {
                } else if ($error_type == Form::ERROR_ID_NOT_SET) {
                }
            });

            if ($error_count == 0) {
                $resource_seek = $this->db->create("resources");
                $resource_seek->url = "";
                $result = $this->db->query($resource_seek,array("url" => $this->resource->url, "is_deleted" => 0));

                if(count($result) > 0) {
                    $error_count++;
                    $this->error_message.= "Link already exists";
                }
            }

            if ($error_count == 0) {
                $this->log_close();
                $this->db->insert($this->resource);
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_message));
            }
        } else {
            $this->alert->display_message_on_error($this);
            $this->render("index","resources/add","AGC Employee Locator | CMS");
        }
    }

    private function _edit($id) {
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);
            $this->resource = $this->db->create("resources");
            $this->resource->date_modified = $this->get_current_date();
            $this->resource->modified_by = $this->session->userdata("user_id");

            $original_resource = $this->db->create("resources");
            $original_resource->url = "";
            $original_resource->description = "";

            $this->error_message = "Error updating record";

            $result = $this->db->query($original_resource,array("id" => $id));
            if (count($result) > 0) {
                $original_resource = $result[0];
            }

            $post_data = $request->get_data();

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->resource->$key = $value;
            },function($error_type,$key) {
                if ($error_type == Form::ERROR_VALIDATION_FAILED) {
                } else if ($error_type == Form::ERROR_ID_NOT_SET) {
                }
            });

            //check if url is existing
            if ($error_count == 0) {
                $existing = $this->db->query("SELECT url FROM resources WHERE url=".$this->db->quote($this->resource->url)." AND
                    is_deleted = 0 AND id != ". $id);

                if(count($existing) > 0) {
                    $error_count++;
                    $this->error_message.= "Url is already in use";
                }  
            }

            if ($error_count == 0) {
                $this->log_close();
                $this->db->update($this->resource,array("id" => $id));
                print json_encode(array("response_code" => 0,"response_msg" => "Record updated successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_message));
            }
        } else {
            if (ctype_digit($id)) {
                $resource = $this->db->create('resources');
                $resource->id = 0;
                $resource->description = "";
                $resource->url = "";
                $result = $this->db->query($resource,array("id" => $id));
                if (count($result) > 0 && $result[0]->id) {
                    $this->set("url",$result[0]->url);
                    $this->set("description",$result[0]->description);
                    $this->set("id",$result[0]->id);

                    $this->alert->display_message_on_error($this);
                    $this->render("index", "resources/edit", "AGC Employee Locator | CMS");
                }
            }
        }
    }

    private function _delete($id) {
        $error = true;
        if (ctype_digit($id)) {
            $resource = $this->db->create('resources');
            $resource->is_deleted = 1;
            $resource->deleted_by = $this->session->userdata("user_id");
            $resource->date_deleted = $this->get_current_date();

            $this->db->update($resource,array("id" => $id));
            $error = false;
        }
        if ($error) {
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function _bulk_upload() {
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $this->error_data = "";
            if ($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));

                //$file_path = BASEPATH . "assets/uploads/bulk/";
                $file_path = BASEPATH . "bulk/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if ($this->upload->do_upload("file",$config)) {
                    $file_name = $this->upload->get_uploaded_filename();

                    $csv_data = $this->helper->get_csv($file_path.$file_name,false);

                    $action  = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "resources";

                    $this->db->insert($file);

                    $this->row = 0;

                    $this->post_data_keys = array(
                        "url" => array(
                            "required" => false,
                            "type" => Form::TYPE_URL,
                            "extras" => array(
                            )
                        ),
                        "description" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 64
                            )
                        )
                    );

                    foreach ($csv_data as $data) {
                        ++$this->row;
                        $this->resource = $this->db->create("resources");
                        
                        if ($action == "add") {
                            $this->log_open(1,"insert","bulk upload");
                            $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                if ($key != "id") {
                                    $this->resource->$key = utf8_encode($value);
                                    $this->log_insert($key,$this->resource->$key);
                                }
                            }, function ($error_type,$key) {
                                $this->error_data .= "insert at " . $this->row . " " . $error_type . " " . $key . "<br>\n";
                            });

                            if ($error_count == 0) {
                                $result = $this->db->query("SELECT id FROM resources WHERE url = " . $this->db->quote($this->resource->url) . " AND is_deleted <> 1 LIMIT 0,1");
                                if (count($result) > 0) {
                                    $this->error_data.= "cannot insert data at " . $this->row . " record already exists<br>\n";
                                } else {
                                    $this->resource->date_created = $this->get_current_date();
                                    $this->resource->created_by = $this->get_current_user();
                                    $this->db->insert($this->resource);
                                }
                            }
                        } else if ($action == "update") {
                             if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"update","bulk upload");
                                $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                    if ($key != "id") {
                                        $this->resource->$key = utf8_encode($value);
                                        $this->log_changes($key,$this->resource->$key,$this->employee->$key);                                    
                                    }
                                }, function ($error_type,$key) {
                                    $this->error_data .= "update at ". $this->row . " " . $error_type . " " . $key . "<br>\n";
                                });

                                if ($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM resources WHERE id = " . intval($data["id"]));
                                    if (count($result) > 0) {
                                        $this->resource->date_modified = $this->get_current_date();
                                        $this->resource->modified_by = $this->get_current_user();
                                        $this->db->update($this->resource,array("id" => $data["id"]));
                                    } else {
                                        $this->error_data .= "update at " . $this->row . " record not found<br>\n";                                        
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"delete","bulk upload");
                                $this->resource->is_deleted = 1;
                                $this->resource->date_deleted = $this->get_current_date();
                                $this->db->update($this->resource,array("id" => $data["id"]));
                                $this->log_close();
                            } else {
                                $this->error_data.= "delete at " . $this->row . " id not defined<br>\n";
                            }
                        }
                    }
                }
            }

            if (!empty($this->error_data)) {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            }
        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by                                         
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list.= "<tbody>";
                foreach ($result as $data) {
                    $upload_list.= "<tr>";
                    $upload_list.= "<td>$data->file_name</td>";
                    $upload_list.= "<td>$data->date_uploaded</td>";
                    $upload_list.= "<td>$data->section</td>";
                    $upload_list.= "<td>$data->action</td>";
                    $upload_list.= "<td>$data->uploaded_by</td>";
                    $upload_list.= "<td>$data->hash_value</td>";
                    $upload_list.= "</tr>";
                }
                $upload_list.= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by                                        
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list.= "<tr>";
                    $download_list.= "<td>$data->file_name</td>";
                    $download_list.= "<td>$data->date_created</td>";
                    $download_list.= "<td>$data->created_by</td>";
                    $download_list.= "<td>$data->hash_value</td>";
                    $download_list.= "</tr>";
                }
                $download_list.= "</tbody>";
            }

            $this->set("upload_list",$upload_list);
            $this->set("download_list",$download_list);

            $this->render("index", "resources/upload", "AGC Employee Locator | CMS");
        }
    }    
}