<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Accounts extends CMS_Controller {
    private $post_data_keys = array();
    private $user, $admin;
    private $error_message;

    public function __construct() {
        parent::__construct("/backoffice/accounts","Account Maintenance");

        $this->load->library(array("html","form","input","util","hash","alert","mail"));

        $this->post_data_keys = array(
            "username" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 3,
                    "max" => 22
                )
            ),
            "first_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "last_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "role" => array(
                "required" => true,
                "type" => Form::TYPE_INT,
                "extras" => array()
            ),
            "email" => array(
                "required" => true,
                "type" => Form::TYPE_EMAIL,
                "extras" => array()
            ),
            "company_id" => array(
                "required" => false,
                "type" => Form::TYPE_INT,
                "extras" => array()
            ),
            "department_id" => array(
                "required" => false,
                "type" => Form::TYPE_INT,
                "extras" => array()
            )
        );
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/accounts/" . $request_method);

        if ($request_method == "admin") {
            if (isset($request_params) && count($request_params) > 0 && in_array(isset($request_params[1]) ? $request_params[1] : "",array("add","view","edit","delete","list","changepass","get-department","repair"))) {
                $request = $request_params[1];
                if ($request == "add") {
                    $this->_admin_add();
                } else if ($request == "view") {
                    $this->_admin_view($request_params[2]);
                } else if ($request == "edit") {
                    $this->_admin_edit($request_params[2]);
                } else if ($request == "delete") {
                    $this->_admin_delete();
                } else if ($request == "list" && $request_params[2] == "data") {
                    $this->_admin_list_data(1);
                } else if ($request == "changepass") {
                    $this->_resend_password(isset($request_params[2]) ? $request_params[2] : 0);
                } else if ($request == "repair") {
                    $this->_repair_permissions();
                } else if ($request == "get-department") {
                    $this->_get_department();
                }
            } else {
                if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                    $this->_admin_list_data(1);
                } else {
                    $this->_admin_list();
                }
            }
        } else if ($request_method == "supervisor") {
            $request = isset($request_params[1]) ? $request_params[1] : "";
            if ($request == "list" && (isset($request_params[2]) && $request_params[2] == "data")) {
                $this->_supervisor_list_data(1);
            } else {
                $this->_list_supervisors();
            }
        } else if ($request_method == "rescuer") {
            $request = isset($request_params[1]) ? $request_params[1] : "";
            if ($request == "list" && (isset($request_params[2]) && $request_params[2] == "data")) {
                $this->_rescuer_list_data(1);
            } else {
                $this->_list_rescuers();
            }            
        }
    }

    private function _admin_list_data($page) {
        $request = $this->input->get_data(Input::STREAM);
        $limit = 10;
        $offset = 0;
        $search_by = "";

        if ($request->isValid()) {
            $data = json_decode($request->get_data());

            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'};
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR u.id = " . $text;
                }
                $search_by = "AND (u.username LIKE '%$text%' OR r.description LIKE '%$text%' OR u.first_name LIKE '%$text%' OR u.last_name LIKE '%$text%' $search_id)";
            }

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;
        }

        $total = $this->get_total("SELECT COUNT(u.id) AS total FROM users AS u
                                   LEFT JOIN roles AS r ON r.id = u.role
                                   WHERE u.is_deleted <> 1 $search_by");

        $result = $this->db->query("SELECT u.id,u.username,CONCAT(u.last_name,', ',u.first_name) AS full_name,u.email,u.date_created,u.password_expiration_date,r.description AS role 
                                    FROM users AS u
                                    LEFT JOIN roles AS r ON r.id = u.role
                                    WHERE u.is_deleted <> 1 $search_by 
                                    ORDER BY u.username ASC 
                                    LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _repair_permissions() {
        $response = $this->create_client_response(1,"Account deletion failed!");
        $response->error("Error repairing user permissions"); 
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $user = $this->db->create("users");
            $user->id = 0;
            $user->role = 0;
            $result = $this->db->query($user,array("id" => $post_data["id"]));

            if (count($result) > 0) {
                $user = $result[0];

                $filter_by = "WHERE ";
                if ($user->role == 2) {
                    $filter_by.= " a = 1";
                } else {
                    $filter_by.= " s = 1";
                }

                $this->db->update("DELETE FROM admin_menu WHERE user_id = $user->id");
                $this->db->update("DELETE FROM user_module WHERE user_id = $user->id");

                $result = $this->db->query("SELECT id FROM cms_menu $filter_by");
                foreach ($result as $data) {
                    $admin_menu = $this->db->create("admin_menu");
                    $admin_menu->user_id = $user->id;
                    $admin_menu->menu_id = $data->id;
                    $admin_menu->can_view = 1;
                    $admin_menu->can_add = 1;
                    $admin_menu->can_edit = 1;
                    $admin_menu->can_delete = 1;
                    $this->db->insert($admin_menu);
                }

                $result = $this->db->query("SELECT id FROM modules");
                foreach ($result as $data) {
                    $module = $this->db->create("user_module");
                    $module->user_id = $user->id;
                    $module->module_id = $data->id;
                    $module->can_view = 1;
                    $module->can_update = 1;
                    $module->can_delete = 1;
                    $module->can_add = 1;
                    $this->db->insert($module);
                }

                $response->success("Permissons repaired successfully");
            }
        }

        $response->notify();
    }

    private function _get_department() {
        $response = create_response(1,"Error getting department data");

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            $company_id = 0;
            if (isset($data->{'company_id'})) {
                $company_id = $data->{'company_id'};
            }

            if ($company_id > 0) {                
                $result = $this->db->query("SELECT d.id,
                                                   d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id);

                if (count($result) > 0) {
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);
    }

    private function _supervisor_list_data($page) {
        $request = $this->input->get_data(Input::STREAM);
        $limit = 10;
        $offset = 0;
        $search_by = "";
        $company_filter = "";

        if ($request->isValid()) {
            $data = json_decode($request->get_data());

            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'};
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR e.id = " . $text;
                } 
                $search_by = "AND (e.first_name LIKE '%$text%' OR e.last_name LIKE '%$text%' OR e.email LIKE '%$text%' $search_id)";
            }

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;

            if (isset($data->{'company_id'}) && ctype_digit($data->{'company_id'}) && intval($data->{'company_id'}) > 0) {
                $company_filter = "AND e.company_id = " . $data->{'company_id'};
            }
        }

        $department_id = "e.department_id";
        $company_id = "e.company_id";

        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
            $company_id = $this->session->userdata("company_id");
            if ($this->session->userdata("department_id") > 0) {
                $department_id = $this->session->userdata("department_id");
            }
        }
 
        $total = $this->get_total("SELECT COUNT(e.id) AS total FROM employees AS e 
                                   INNER JOIN companies AS c ON c.id = $company_id
                                   INNER JOIN department AS d ON d.id = $department_id 
                                   WHERE e.is_supervisor = 1 AND e.is_deleted <> 1 $company_filter $search_by");
        
        $result = $this->db->query("SELECT e.id,CONCAT(e.first_name,' ',e.last_name) AS full_name,c.company_name AS company,d.department_name AS department,e.email,e.date_created 
                                    FROM employees AS e
                                    INNER JOIN companies AS c ON c.id = $company_id
                                    INNER JOIN department AS d ON d.id = $department_id
                                    WHERE e.is_supervisor = 1 AND e.is_deleted <> 1 $company_filter $search_by LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _rescuer_list_data($page) {
        $request = $this->input->get_data(Input::STREAM);
        $limit = 10;
        $offset = 0;
        $search_by = "";
        $company_filter = "";

        if ($request->isValid()) {
            $data = json_decode($request->get_data());

            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'};
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR e.id = " . $text;
                }     
                $search_by = "AND (e.first_name LIKE '%$text%' OR e.last_name LIKE '%$text%' OR e.email LIKE '%$text%' $search_id)";
            }

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;

            if (isset($data->{'company_id'}) && ctype_digit($data->{'company_id'}) && intval($data->{'company_id'}) > 0) {
                $company_filter = "AND e.company_id = " . $data->{'company_id'};
            }            
        }

        $total = $this->get_total("SELECT COUNT(e.id) AS total FROM employees AS e WHERE e.is_rescuer = 1 AND e.is_deleted <> 1 $company_filter $search_by");

        $result = $this->db->query("SELECT e.id,CONCAT(e.first_name,' ',e.last_name) AS full_name,e.email,c.company_name AS company,d.department_name AS department,e.date_created
                                    FROM employees AS e
                                    INNER JOIN companies AS c ON c.id = e.company_id
                                    INNER JOIN department AS d ON d.id = e.department_id
                                    WHERE e.is_rescuer = 1 AND e.is_deleted <> 1 $company_filter $search_by LIMIT $offset,$limit");
        $this->send($total,$result);
    }

    private function _admin_add() {
        if ($this->input->request_method('POST')) {
            $response = $this->create_client_response(1,"Error adding account");
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $password = $this->util->generate_password();

            $this->user = $this->db->create("users");
            $this->user->created_by = $this->session->userdata("user_id");
            $this->user->date_created = $this->get_current_date();
            $this->user->password = Hash::HashPassword($password);

            $this->admin = $this->db->create("company_admins");

            $this->log_open(1,"insert","users");

            $this->error_message = "";

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                if (in_array($key,array("company_id","department_id"))) {
                    $this->admin->$key = $value;
                } else {
                    $this->user->$key = $value;
                    $this->log_insert($key,$value);    
                }
            },function($error_type,$key) {
                if ($error_type == Form::ERROR_VALIDATION_FAILED) {
                    $this->error_message.= "'$key' is required<br>\n";
                } else if ($error_type == Form::ERROR_ID_NOT_SET) {
                    $this->error_message.= "'$key' is required<br>\n";
                }
            });

            if ($error_count == 0) {
                $user_seek = $this->db->create("users");
                $user_seek->username = "";
                $user_seek->is_deleted = 0;
                $result = $this->db->query($user_seek,array("username" => $this->user->username));
                if (count($result) > 0 && $result[0]->is_deleted != 1) {
                    $error_count++;
                    $this->error_message="Username is already taken";
                }
            }

            if ($error_count == 0) {
                $this->log_close();

                $filter_by = "WHERE ";
                if ($this->user->role == 2) {
                    $filter_by.= " a = 1";
                } else {
                    $filter_by.= " s = 1";
                }

                $this->db->insert($this->user);
                $id = $this->db->getLastInsertId();

                $this->admin->user_id = $id;

                // $this->db->insert("INSERT INTO admin_menu(user_id,menu_id,can_view,can_add,can_edit,can_delete) S
                //                    SELECT $id,id,1,1,1,1 FROM cms_menu $filter_by");
                // $this->db->insert("INSERT INTO user_module(user_id,module_id,can_view,can_update,can_delete,can_add) 
                //                    SELECT $id,id,1,1,1,1 FROM modules");

                $result = $this->db->query("SELECT id FROM cms_menu $filter_by");
                foreach ($result as $data) {
                    $admin_menu = $this->db->create("admin_menu");
                    $admin_menu->user_id = $id;
                    $admin_menu->menu_id = $data->id;
                    $admin_menu->can_view = 1;
                    $admin_menu->can_add = 1;
                    $admin_menu->can_edit = 1;
                    $admin_menu->can_delete = 1;
                    $this->db->insert($admin_menu);
                }

                $result = $this->db->query("SELECT id FROM modules");
                foreach ($result as $data) {
                    $module = $this->db->create("user_module");
                    $module->user_id = $id;
                    $module->module_id = $data->id;
                    $module->can_view = 1;
                    $module->can_update = 1;
                    $module->can_delete = 1;
                    $module->can_add = 1;
                    $this->db->insert($module);
                }


                if ($this->user->role == 2) {
                    $this->db->insert($this->admin);
                }

                $message = "Hi " . ($this->user->first_name . " " . $this->user->last_name) . "<br>\n<br>\n";
                $message.= "Welcome to ASSIST<br>\n";
                $message.= "<br>\n";
                $message.= "Your username is: ". $this->user->username ."<br>\n";
                $message.= "This is your temporary password: " . $password . "<br>\n";

                $subject = "AGC Employee Locator - Account Information";

                $email = $this->db->create("emails");
                $email->email = $this->user->email;
                $email->subject = $subject;
                $email->message = $message;
                $email->date_sent = $this->get_current_date();

                $this->db->insert($email);

                $this->mail->send($this->user->email,$subject,$message);

                $this->activity_insert("New account has been created. Welcome, " . $this->user->username . "!");
                $response->success("Account added successfully",base_url() . "/backoffice/accounts/admin");
            } else {
                $response->error($this->error_message);
            }

            $response->notify();
        } else {
            $result = $this->db->query("SELECT id,description 
                                        FROM roles 
                                        WHERE id = 1 OR id = 2 ORDER BY id ASC");
            
            $roles = "";
            
            foreach ($result as $data) {
                $roles .= $this->html->option($data->description,$data->id);
            }

            $this->set("roles",$roles);

            $company_list = "";
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
            if (count($result) > 0) {
                foreach ($result as $company) {
                    $company_list .= $this->html->option($company->company_name,$company->id);
                }
                $company_id = $result[0]->id;
            }

            $this->set("company_list",$company_list);

            $department_list = "";

            if ($company_id > 0) {
                $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id . " AND c.is_deleted <> 1");

                if (count($result) > 0) {
                    foreach ($result as $item) {
                        $department_list .= $this->html->option($item->department_name,$item->id);
                    }
                }
            }

            $this->set("department_list",$department_list);


            $this->render("index","accounts/admin_add","AGC Employee Locator | CMS");
        }
    }

    private function _admin_view($id) {
        if (ctype_digit($id)) {
            $result = $this->db->query("SELECT u.username,u.first_name,u.last_name,r.description as role,u.email,u.date_created,u2.username AS created_by,IF(u.date_modified IS NULL,'',u.date_modified) AS date_modified,u3.username AS modified_by FROM users AS u
                                        LEFT JOIN roles AS r ON r.id = u.role 
                                        LEFT JOIN users AS u2 ON u2.id = u.created_by 
                                        LEFT JOIN users AS u3 ON u3.id = u.modified_by
                                        WHERE u.id=".$id);
            $post_keys = array("username","first_name","last_name","email","role","date_created","created_by","date_modified","modified_by");

            if (count($result) > 0) {
                foreach ($post_keys as $key) {
                    $this->set($key,$result[0]->$key);
                }
            } else {
                foreach ($post_keys as $key) {
                    $this->set($key,"");
                }
            }

            $result = $this->db->query("SELECT cm.id,cm.label,IF(am.can_view = 1,'Yes','No') AS can_view,IF(am.can_add = 1,'Yes','No') AS can_add,IF(am.can_edit = 1,'Yes','No') AS can_edit,IF(am.can_delete = 1,'Yes','No') AS can_delete FROM cms_menu AS cm 
                                        RIGHT JOIN admin_menu AS am ON am.menu_id = cm.id
                                        WHERE am.user_id = $id
                                        ORDER BY cm.label");
            $menus = "";
            if (count($result) > 0) {
                foreach ($result as $data) {
                             // <td class=\"text-center\">
                             //     <button type=\"button\" class=\"btn btn-default btn-tool-square btn-sm cmd_menu_enable\" rel=\"$data->id\"><i class=\"fa fa-minus-circle icon-red\"></i></button>
                             // </td>
                    $menus.="<tr>
                             <td>$data->label</td>
                             <td class=\"text-center\">$data->can_view</td>
                             <td class=\"text-center\">$data->can_add</td>
                             <td class=\"text-center\">$data->can_edit</td>
                             <td class=\"text-center\">$data->can_delete</td>
                         </tr>";
                }
            }

            $this->set("menus",$menus);

            $result = $this->db->query("SELECT um.id,m.module_uri,um.can_view,um.can_update,um.can_delete,um.can_add FROM user_module AS um
                                        LEFT JOIN modules AS m ON m.id = um.module_id
                                        WHERE um.user_id = " . $id);
            $modules = "";
            foreach ($result as $data) {
                $can_view = $data->can_view ? " checked" : "";
                $can_update = $data->can_update ? " checked" : "";
                $can_delete = $data->can_delete ? " checked" : "";
                $can_add = $data->can_add ? " checked" : "";
                $modules .= "<tr>
                                 <td>$data->module_uri</td>
                                 <td class=\"text-center\"><input type=\"checkbox\" name=\"can_view\"$can_view></td>
                                 <td class=\"text-center\"><input type=\"checkbox\" name=\"can_update\"$can_update></td>
                                 <td class=\"text-center\"><input type=\"checkbox\" name=\"can_delete\"$can_delete></td>
                                 <td class=\"text-center\"><input type=\"checkbox\" name=\"can_add\"$can_add></td>
                                 <td class=\"text-center\">
                                     <button type=\"button\" class=\"btn btn-default btn-tool-square btn-sm cmd_module_enable\" rel=\"$data->id\"><i class=\"fa fa-minus-circle icon-red\"></i></button>
                                 </td>
                             </tr>";
            }

            $this->set("modules",$modules);

            $this->set("id",$id);

            $this->render("index","accounts/admin_view","AGC Employee Locator | CMS");
        }
    }

    private function _admin_edit($id) {
        if (ctype_digit($id)) {
            if ($this->input->request_method('POST')) {
                $post_data_keys = array("first_name","last_name","email","role","can_view","can_read","can_add");
                $post_data = $this->input->get_data(Input::POST)->get_data();
                $this->user = $this->db->create("users");
                $this->user->modified_by = $this->get_current_user();
                $this->user->date_modified = $this->get_current_date();

                $this->admin = $this->db->create("company_admins");

                $this->cms_menu = $this->db->create("cms_menu");

                foreach ($post_data as $key => $data) {
                    if($key == "company_id"
                        || $key == "department_id") {
                        $this->admin->$key = $data;
                    } else if($key == "module"
                        || $key == "can_view"
                        || $key == "can_add"
                        || $key == "can_edit"
                        || $key == "can_delete") {
                        $this->cms_menu->$key = $data;
                    } else {
                        $this->user->$key = $data;
                    }
                }
                //update user table
                $this->db->update($this->user,array("id" => $id));
                $this->db->update("DELETE FROM company_admins WHERE user_id = $id");

                if ($this->user->role == 2) {
                    $this->admin->user_id = $id;
                    $this->db->insert($this->admin);
                }

                $error_count = 0;
                //update cms_menu table
                foreach($this->cms_menu->module as $key => $data):
                    $user_module = $this->db->create("admin_menu");
                    $user_module->can_view = 0;
                    $user_module->can_add = 0;
                    $user_module->can_edit = 0;
                    $user_module->can_delete = 0;

                    foreach($this->cms_menu->can_view as $k => $v):
                        if($k == $data) :
                            $user_module->can_view = $v;
                        endif;
                    endforeach;

                    foreach($this->cms_menu->can_add as $k => $v):
                        if($k == $data) :
                            $user_module->can_add = $v;
                        endif;
                    endforeach;

                    foreach($this->cms_menu->can_edit as $k => $v):
                        if($k == $data) :
                            $user_module->can_edit = $v;
                        endif;
                    endforeach;

                    foreach($this->cms_menu->can_delete as $k => $v):
                        if($k == $data) :
                            $user_module->can_delete = $v;
                        endif;
                    endforeach;

                    $result = $this->db->update($user_module,
                        array("user_id" => $id, "menu_id" => $data));

                    if(!$result){
                        $error_count += 1;
                    }
                endforeach;

                if ($error_count == 0) {
                    print json_encode(array("response_code" => 0,"response_msg" => "Data updated successfully!"));
                } else {
                    print json_encode(array("response_code" => 1, "response_msg" => "Error updating data!"));
                }

               //redirect(base_url() . "/backoffice/accounts/admin");
            }
            else {
                $result = $this->db->query("SELECT u.username,u.first_name,u.last_name,u.role,u.email,u.date_created,u2.username AS created_by,IF(u.date_modified IS NULL,'',u.date_modified) AS date_modified,u3.username AS modified_by FROM users AS u
                                            LEFT JOIN users AS u2 ON u2.id = u.created_by 
                                            LEFT JOIN users AS u3 ON u3.id = u.modified_by
                                            WHERE u.id=".$id);
                $post_keys = array("username","first_name","last_name","email","role","date_created","created_by","date_modified","modified_by");

                $role_id = 0;

                if (count($result) > 0) {
                    foreach ($post_keys as $key) {
                        if ($key == "role") {
                             $role_id = $result[0]->$key;
                        } else {
                            $this->set($key,$result[0]->$key);                            
                        }
                    }
                } else {
                    foreach ($post_keys as $key) {
                        $this->set($key,"");
                    }
                }

                $result = $this->db->query("SELECT id,label FROM cms_menu ORDER BY label");
                $menu_data = $this->db->query("SELECT id,menu_id,can_view,can_add,can_edit,can_delete 
                                               FROM admin_menu
                                               WHERE user_id = $id");
                $menus = "";
                if (count($result) > 0) {
                    foreach ($result as $data) {
                        $check_view = "";
                        $check_add = "";
                        $check_edit = "";
                        $check_delete = "";
                        $toggle = "";
                        foreach ($menu_data as $md) {
                            if ($data->id == $md->menu_id) {
                                $toggle = "icon-red";
                                if ($md->can_view == 1) {
                                    $check_view = "checked";
                                }

                                if ($md->can_add == 1) {
                                    $check_add = "checked";
                                }

                                if ($md->can_edit == 1) {
                                    $check_edit = "checked";
                                }
                                if ($md->can_delete == 1) {
                                    $check_delete = "checked";
                                }
                                break;
                            }
                        }
                        $menus.="<tr>
                                 <td>$data->label</td>
                                 <td class=\"text-center\">
                                     <input type=\"hidden\" checked data-toggle\"toggle\" name=\"module[]\" value=\"$data->id\">
                                 </td>
                                 <td><input type=\"checkbox\" name=\"can_view[$data->id]\" $check_view id=\"can_view\"></td>
                                 <td><input type=\"checkbox\" name=\"can_add[$data->id]\" $check_add id=\"can_add\"></td>
                                 <td><input type=\"checkbox\" name=\"can_edit[$data->id]\" $check_edit id=\"can_edit\"></td>
                                 <td><input type=\"checkbox\" name=\"can_delete[$data->id]\" $check_delete id=\"can_delete\"></td>
                             </tr>";
                    }
                }

                $this->set("menus",$menus);

                $result = $this->db->query("SELECT id,description 
                                            FROM roles 
                                            WHERE id = 1 OR id = 2 ORDER BY id ASC");
            
                $roles = "";
            
                foreach ($result as $data) {
                    if ($role_id == $data->id) {
                        $roles .= $this->html->option($data->description,$data->id,true);
                    } else {
                        $roles .= $this->html->option($data->description,$data->id);
                    }
                }

                $company_id = 0;
                $department_id = 0;

                if ($role_id == 2) {
                    $result = $this->db->query("SELECT company_id,department_id FROM company_admins WHERE user_id = $id");
                    if(count($result) > 0) {
                        $company_id = $result[0]->{'company_id'};
                        $department_id = $result[0]->{'department_id'};
                    }
                }
                
                $this->set("roles",$roles);

                $this->set("id",$id);

                $company_list = "";
                $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
                if (count($result) > 0) {
                    foreach ($result as $company) {
                        if ($company_id == $company->id) {
                            $company_list .= $this->html->option($company->company_name,$company->id,true);
                        } else {
                            $company_list .= $this->html->option($company->company_name,$company->id);
                        }
                    }
                }

                $this->set("company_list",$company_list);

                $department_list = "";

                if ($company_id > 0) {
                    $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                                LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                                LEFT JOIN companies AS c ON c.id = cd.company_id
                                                WHERE c.id = " . $company_id . " AND c.is_deleted <> 1");

                    if (count($result) > 0) {
                        foreach ($result as $item) {
                            if ($department_id == $item->id) {
                                $department_list .= $this->html->option($item->department_name,$item->id,true);
                            } else {
                                $department_list .= $this->html->option($item->department_name,$item->id);
                            }
                        }
                    }
                }

                $this->set("department_list",$department_list);                

                $this->render("index","accounts/admin_edit","AGC Employee Locator | CMS");
            }
        }
    }

    private function _admin_delete() {
        $response = $this->create_client_response(1,"Account deletion failed!");
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $response->error(json_encode($post_data));

            if ($post_data["id"] !== null && (is_int($post_data["id"]) || ctype_digit($post_data["id"]))) {
                if ($post_data["id"] != $this->get_current_user()) {
                    $user = $this->db->create("users");
                    $user->is_deleted = 1;
                    $user->date_deleted = $this->get_current_date();
                    $user->deleted_by = $this->session->userdata("user_id");
                    $this->db->update($user,array("id" => $post_data['id']));

                    $response->success("Account deleted successfully");
                } else {
                    $response->error("Could not delete own account");
                }
            } else {
                $response->error("Supplied id is invalid"); 
            }            
        }

        $response->notify();
    }

    private function _admin_list() {        
        $this->render("index","accounts/admins","AGC Employee Locator | CMS");
    }

    private function _list_supervisors() {
        $company_list = "";

        $result = array();
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
            $company_id = $this->session->userdata("company_id");
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE id = $company_id ORDER BY company_name ASC");
        } else {
            $company_list.= "<option value='0'>All</option>";
            $result = $this->db->query("SELECT id,company_name FROM companies ORDER BY company_name ASC");
        }
        
        foreach ($result as $data) {
            $company_list.= $this->html->option($data->company_name,$data->id);
        }

        $this->set("company_list",$company_list);

        $this->render("index","accounts/supervisor","AGC Employee Locator | CMS");
    }

    private function _list_rescuers() {
        $company_list = "";
        $result = array();
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
            $company_id = $this->session->userdata("company_id");
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE id = $company_id ORDER BY company_name ASC");
        } else {
            $company_list.= "<option value='0'>All</option>";
            $result = $this->db->query("SELECT id,company_name FROM companies ORDER BY company_name ASC");
        }

        foreach ($result as $data) {
            $company_list.= $this->html->option($data->company_name,$data->id);
        }

        $this->set("company_list",$company_list);        
        $this->render("index","accounts/rescuer","AGC Employee Locator | CMS");
    }

    private function _resend_password($id) {
        if (ctype_digit($id)) {
            $this->user = $this->db->create("users");
            $this->user->password = "";
            $this->user->first_name = "";
            $this->user->last_name = "";
            $this->user->email = "";
            $this->user->username = "";

            $result = $this->db->query($this->user,array("id" => $id));
            if (count($result) > 0) {
                $this->user = $result[0];
                $password = $this->util->generate_password();

                $this->user->modified_by = $this->session->userdata("user_id");
                $this->user->date_modified = $this->get_current_date();
                $this->user->password = Hash::HashPassword($password);
                
                $message = "Hi " . ($this->user->first_name . " " . $this->user->last_name) . "<br>\n<br>\n";
                $message.= "Your username is: ". $this->user->username ."<br>\n";
                $message.= "This is your temporary password: " . $password . "<br>\n";

                $subject = "AGC Employee Locator - Resend password request";

                $email = $this->db->create("emails");
                $email->email = $this->user->email;
                $email->subject = $subject;
                $email->message = $message;
                $email->date_sent = $this->get_current_date();

                $this->db->insert($email);

                $this->mail->send($this->user->email,$subject,$message);


                $changepass = $this->db->create("users");
                $changepass->password = Hash::HashPassword($password);
                $this->db->update($changepass,array("id"=>$id));

                redirect(base_url() . "/backoffice/accounts/admin");
            }
        }
    }
}