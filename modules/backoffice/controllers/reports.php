<?php

class Reports extends CMS_Controller {

    public function __construct() {
        parent::__construct("/backoffice/reports", "Generate Reports");

        $this->load->library(array("uri","input","form","alert","html","twitterapi", "upload"));

        $this->headers_array = array();
        $this->sql_statement = "";
    }

    public function request_handler($request_method,$request_params) {
        if($request_method == "dynamic") {
            $this->_dynamic_reports();
        } else if($request_method == "get-list") {
            $this->_get_list_of_reports();
        } else if($request_method == "get-searched") {
            $this->_get_searched_reports($request_params);
        } else if($request_method == "get-selected") {
            $this->_get_selected_report($request_params);
        } else if($request_method == "get-items") {
            $this->_get_selected_report_items($request_params);
        } else if($request_method == "export") {
            $this->_export(isset($request_params[1]) ? $request_params[1] : 0);
        } else if($request_method == "export-xls") {
            $this->_export_xls(isset($request_params[1]) ? $request_params[1] : 0);
        } else if($request_method == "canned") {
            $this->_canned();
        } else if($request_method == "get-disaster-details") {
            $this->_get_disaster_details($request_params);
        } else if($request_method == "get-employees") {
            $this->_get_employees($request_params);
        } else if($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
        } else {
            $this->_dynamic_reports();
        }
    }

    private function _list_data($page) {
        $data = $this->input->get_data(Input::STREAM)->get_data();

        $fcol_id = $data->{'fcol_id'};
        $disaster_id = $data->{'disaster_id'};
        $start_date = $data->{'start_date'};
        $end_date = $data->{'end_date'};
        $status = $data->{'status'};

        $fcol_name = "site_id";
        if ($this->session->userdata("role_id") == parent::ROLE_SUPER_ADMINISTRATOR) {
            $fcol_name = "company_id";
        }


        $search_by = "";
        $having_by = "";

        $having_by = " HAVING status = $status ";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};

            $having_by .= " AND (employee_name LIKE '%$text%' OR message LIKE '%$text%' OR items LIKE '%$text%')";
        }

        $result = $this->db->query("SELECT CONCAT(e.last_name,', ',e.first_name) AS employee_name,e.company_id, e.site_id,
                                            co.company_name,cs.site, t1.checkin_id,IF(c.is_rescued=1,1,t1.status) AS status,
                                            t1.date_updated,t1.message,
                                            GROUP_CONCAT(DISTINCT i.item_name SEPARATOR ',') AS items
                                            FROM 
                                                ( 
                                                    SELECT ch.checkin_id,ch.status,ch.date_updated,ch.message FROM checkin_history ch 
                                                    WHERE DATE(date_updated) >= '$start_date' AND DATE(date_updated) <= '$end_date'
                                                    ORDER BY ch.checkin_id,ch.date_updated DESC 
                                                )t1 
                                            LEFT JOIN checkins c ON c.id = t1.checkin_id 
                                            LEFT JOIN employees e ON e.id = c.user_id 
                                            LEFT JOIN companies co ON co.id = e.company_id 
                                            LEFT JOIN company_sites cs ON cs.id = e.site_id 
                                            LEFT JOIN checkin_items ci ON c.id = ci.checkin_id
                                            LEFT JOIN items i ON i.id = ci.item_id
                                            WHERE c.disaster_id = $disaster_id
                                            AND e.$fcol_name = $fcol_id 
                                            GROUP BY checkin_id $having_by");

        $total = count($result);

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;


        $result = $this->db->query("SELECT CONCAT(e.last_name,', ',e.first_name) AS employee_name,e.company_id, e.site_id,
                                            co.company_name,cs.site, t1.checkin_id,IF(c.is_rescued=1,1,t1.status) AS status,
                                            t1.date_updated,t1.message,
                                            GROUP_CONCAT(DISTINCT i.item_name SEPARATOR ',') AS items 
                                            FROM 
                                                ( 
                                                    SELECT ch.checkin_id,ch.status,ch.date_updated,ch.message FROM checkin_history ch 
                                                    WHERE DATE(date_updated) >= '$start_date' AND DATE(date_updated) <= '$end_date'
                                                    ORDER BY ch.checkin_id,ch.date_updated DESC 
                                                )t1 
                                            LEFT JOIN checkins c ON c.id = t1.checkin_id 
                                            LEFT JOIN employees e ON e.id = c.user_id 
                                            LEFT JOIN companies co ON co.id = e.company_id 
                                            LEFT JOIN company_sites cs ON cs.id = e.site_id 
                                            LEFT JOIN checkin_items ci ON c.id = ci.checkin_id
                                            LEFT JOIN items i ON i.id = ci.item_id
                                            WHERE c.disaster_id = $disaster_id
                                            AND e.$fcol_name = $fcol_id 
                                            $search_by
                                            GROUP BY checkin_id $having_by
                                            ORDER BY e.last_name ASC LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _dynamic_reports() {

        if($this->input->request_method('POST')) {

        } else {
            $this->render("index","reports/dynamic","AGC Employee Locator | CMS");
        }
    }

    private function _canned() {
        $disaster_list = "";
        $report_table = "";
        $start_date = $end_date = $min_date = $max_date = date("Y-m-d");
        $disaster_id = 0;

        if($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $disaster_date = explode(" - ", $post_data['daterange']);
            $start_datex = $disaster_date[0];
            $end_datex = $disaster_date[1];

            $start_date = $start_datex;
            $end_date = $end_datex;

            $disaster_id = $post_data['disaster_id'];
            $company_id = $this->session->userdata("company_id");

            $result = $this->db->query("SELECT DATE(date_created) AS date_created,
                                            is_closed,DATE(date_closed) AS date_closed 
                                            FROM cmdctr
                                            WHERE id = $disaster_id");

            if(count($result) > 0) {
                $min_date = $result[0]->date_created;
                $max_date = $result[0]->date_closed;
                if ($result[0]->is_closed == 0) {
                    $max_date = date("Y-m-d");
                }
                if ($start_datex == $end_datex) {
                    $start_datex = $min_date;
                }
            }

            $report_table .= "<div class=\"form-group\">";
            $report_table.=  "  <label for=\"date_created\" class=\"control-label col-xs-2\">Sign-In Status Report</label>";
            $report_table.=  "  <div class=\"col-xs-10\">";

                $report_table.= "<table class=\"table table-striped table-responsive\">";
                $report_table.= "   <thead>";
                $report_table.= "       <tr>";
                $report_table.= "          <th>Company name</th>";
                $report_table.= "          <th>Safe</th>";
                $report_table.= "          <th>Threat</th>";
                $report_table.= "          <th>Urgent</th>";
                $report_table.= "          <th>No status</th>";
                $report_table.= "          <th>Total</th>";
                $report_table.= "       </tr>";
                $report_table.= "   </thead>";

                $report_table.= "  <tbody>";

                $colname = "site";
                $colid = "site_id";
                $colidt = "id";
                $coltab = "company_sites";
                $where_company = " AND e.company_id = $company_id ";
                $where_companyt = " AND e4.company_id = $company_id ";
                $group_by = "cs4.site_id";

                if ($this->session->userdata("role_id") == parent::ROLE_SUPER_ADMINISTRATOR) {
                    $colname = "company_name";
                    $colid = "company_id";
                    $colidt = "company_id";
                    $coltab = "companies";
                    $where_company = "";
                    $where_companyt = "";
                    $group_by = "e4.company_id";
                }

                $result = $this->db->query("SELECT t4.$colid AS fcolid,
                                            IFNULL(fcol,t4.$colname) AS fcol, 
                                            IFNULL(safe,0) AS safe, 
                                            IFNULL(threat,0) AS threat, 
                                            IFNULL(urgent,0) AS urgent, 
                                            t4.total4 AS total 
                                            FROM 
                                                (
                                                    SELECT DISTINCT e4.$colid, c4.$colname, 
                                                    COUNT(cst4.id) AS total4 FROM cmdctr_status cst4
                                                    LEFT JOIN employees e4 ON e4.id = cst4.user_id
                                                    LEFT JOIN $coltab c4 ON c4.id = e4.$colid
                                                    WHERE cst4.notification_id = $disaster_id AND e4.is_deleted = 0
                                                    $where_companyt
                                                    GROUP BY e4.$colid 
                                                )t4 
                                            LEFT JOIN 
                                                ( 
                                                SELECT t2.$colid,t2.$colname AS fcol, 
                                                SUM(IF(STATUS=1,1,0)) AS safe,
                                                SUM(IF(STATUS=2,1,0)) AS threat, 
                                                SUM(IF(STATUS=3,1,0)) AS urgent, 
                                                COUNT(t2.$colid) AS total
                                                FROM
                                                    (
                                                    SELECT e.id,e.first_name,e.last_name,e.company_id, e.site_id, 
                                                    co.company_name,cs.site, IF(c.is_rescued=1,1,t1.status) AS STATUS, t1.date_updated
                                                    FROM
                                                        (
                                                            SELECT ch.checkin_id,ch.status,ch.date_updated FROM checkin_history ch 
                                                            WHERE DATE(date_updated) >= '$start_datex' 
                                                            AND DATE(date_updated) <= '$end_datex'
                                                            ORDER BY ch.checkin_id,ch.date_updated DESC
                                                        )t1
                                                    LEFT JOIN checkins c ON c.id = t1.checkin_id 
                                                    LEFT JOIN employees e ON e.id = c.user_id 
                                                    LEFT JOIN companies co ON co.id = e.company_id 
                                                    LEFT JOIN company_sites cs ON cs.id = e.site_id 
                                                    LEFT JOIN cmdctr_status cst ON cst.notification_id = c.disaster_id
                                                    WHERE c.disaster_id = $disaster_id AND e.is_deleted = 0 $where_company
                                                    GROUP BY checkin_id
                                                    )t2
                                                WHERE id IN (SELECT user_id FROM cmdctr_status WHERE notification_id = $disaster_id)
                                                GROUP BY t2.$colname 
                                            )t3 
                                            ON t3.$colid = t4.$colid");

                if (count($result) > 0) {
                    foreach ($result as $key => $value) {
                        $no_status = $value->total-$value->safe-$value->threat-$value->urgent;
                        $report_table.= "<tr>";
                        $report_table.= "<td id=".$value->fcolid.">".$value->fcol."</td>";
                        $report_table.= "<td>".$value->safe."</td>";
                        $report_table.= ($value->threat == 0) ? "<td>0</td>" : "<td><a href='#'>".$value->threat."</a></td>";
                        $report_table.= ($value->urgent == 0) ? "<td>0</td>" : "<td><a href='#'>".$value->urgent."</a></td>";
                        $report_table.= "<td>".$no_status."</td>";
                        $report_table.= "<td>".$value->total."</td>";
                        $report_table.= "</tr>";   
                    }
                } else {
                    $report_table.= "<tr>";
                    $report_table.= "<td colspan=6 align=\"center\">No records found</td>";
                    $report_table.= "</tr>";
                }
                $report_table.= "</tbody>";
                $report_table.= "</table>";

            $report_table.=  "  </div>";
            $report_table.=  "</div>";

            if ($this->session->userdata("role_id") == parent::ROLE_SUPER_ADMINISTRATOR) {
                $companies = $this->db->create("cmdctr");
                $companies->id = 0;
                $companies->disaster_name = "";
                $result = $this->db->query($companies);
                $disaster_list = "";
                foreach ($result as $data) {
                    if ($data->id == $post_data['disaster_id']) {
                        $disaster_list .= $this->html->option($data->disaster_name,$data->id,true);
                    } else {
                        $disaster_list .= $this->html->option($data->disaster_name,$data->id);
                    }
                }
            } else {
                $result = $this->db->query("SELECT DISTINCT cs.disaster_id AS id,c.disaster_name
                                            FROM cmdctr_sites cs LEFT JOIN cmdctr c ON c.id = cs.disaster_id");
                $disaster_list = "";
                foreach ($result as $data) {
                    if ($data->id == $post_data['disaster_id']) {
                        $disaster_list .= $this->html->option($data->disaster_name,$data->id,true);
                    } else {
                        $disaster_list .= $this->html->option($data->disaster_name,$data->id);
                    }
                }
            }

            $disaster_id = $post_data['disaster_id'];
        } else {
            if ($this->session->userdata("role_id") == parent::ROLE_SUPER_ADMINISTRATOR) {
                $companies = $this->db->create("cmdctr");
                $companies->id = 0;
                $companies->disaster_name = "";
                $result = $this->db->query($companies);
                $disaster_list = "";
                foreach ($result as $data) {
                    $disaster_list .= $this->html->option($data->disaster_name,$data->id);
                }
            } else {
                $result = $this->db->query("SELECT DISTINCT cs.disaster_id AS id,c.disaster_name
                                            FROM cmdctr_sites cs LEFT JOIN cmdctr c ON c.id = cs.disaster_id");
                $disaster_list = "";
                foreach ($result as $data) {
                    $disaster_list .= $this->html->option($data->disaster_name,$data->id);
                }
            }
        }

        $this->set("disaster_id",$disaster_id);
        $this->set("start_date",$start_date);
        $this->set("end_date",$end_date);
        $this->set("min_date",$min_date);
        $this->set("max_date",$max_date);
        $this->set("disaster_list",$disaster_list);
        $this->set("report_table",$report_table);
        $this->render("index","reports/canned","AGC Employee Locator | CMS");
    }

    private function _get_disaster_details() {
        $response = create_response(1, "Error getting details");

        $disaster_id = 0;

        $request = $this->input->get_data(Input::STREAM);

        if($request->isValid()) {
            $data = $request->get_data();
            if(isset($data->{'disaster_id'})) {
                $disaster_id = $data->{'disaster_id'};
            }

            if($disaster_id > 0) {
                $result = $this->db->query("SELECT id,DATE(date_created) AS date_created,
                                            is_closed,DATE(date_closed) AS date_closed 
                                            FROM cmdctr
                                            WHERE id = $disaster_id");

                if(count($result) > 0) {
                    if ($result[0]->is_closed == 0) {
                        $result[0]->date_closed = date("Y-m-d");
                    }

                    $response = create_response(0, "Success");
                    $response->data = $result[0];
                } 
            }
        }

        notify($response);
    }

    private function _get_employees() {
        $response = create_response(1, "Error getting details");
        $request = $this->input->get_data(Input::STREAM);

        if($request->isValid()) {
            $data = $request->get_data();
            $fcol_id = $data->{'fcol_id'};
            $disaster_id = $data->{'disaster_id'};
            $start_date = $data->{'start_date'};
            $end_date = $data->{'end_date'};
            $status = $data->{'status'};

            $fcol_name = "site_id";
            if ($this->session->userdata("role_id") == parent::ROLE_SUPER_ADMINISTRATOR) {
                $fcol_name = "company_id";
            }

            if($disaster_id > 0) {
                $result = $this->db->query("SELECT e.first_name,e.last_name,e.company_id, e.site_id,
                                            co.company_name,cs.site, t1.checkin_id,
                                            IF(c.is_rescued=1,1,t1.status) AS status,t1.date_updated,t1.message,
                                            GROUP_CONCAT(i.item_name SEPARATOR ',') AS items 
                                            FROM 
                                                ( 
                                                    SELECT ch.checkin_id,ch.status,ch.date_updated,ch.message FROM checkin_history ch 
                                                    WHERE DATE(date_updated) >= '$start_date' AND DATE(date_updated) <= '$end_date'
                                                    ORDER BY ch.checkin_id,ch.date_updated DESC 
                                                )t1 
                                            LEFT JOIN checkins c ON c.id = t1.checkin_id 
                                            LEFT JOIN employees e ON e.id = c.user_id 
                                            LEFT JOIN companies co ON co.id = e.company_id 
                                            LEFT JOIN company_sites cs ON cs.id = e.site_id 
                                            LEFT JOIN checkin_items ci ON c.id = ci.checkin_id
                                            LEFT JOIN items i ON i.id = ci.item_id
                                            WHERE c.disaster_id = $disaster_id
                                            AND e.$fcol_name = $fcol_id 
                                            AND t1.status = $status
                                            AND c.user_id IN (SELECT user_id FROM cmdctr_status WHERE notification_id = $disaster_id)
                                            GROUP BY checkin_id
                                            ORDER BY e.last_name ASC");

                if(count($result) > 0) {
                    $response = create_response(0, "Success");
                    $response->data = $result;
                } 
            }
        }
        notify($response);
    }

    private function _dynamic_list($page) {

    }

    private function _get_list_of_reports() {
        $result = $this->db->query("SELECT name, headers
                            FROM reports
                            WHERE is_active = 1");

        $list = array();
        foreach($result as $key => $val):
            foreach($val as $k => $v):
                if($k == "headers"):
                    $v_array = explode(",", $v);
                    foreach($v_array as $v_key => $v_val):
                        if(!in_array(strtolower($v_val), $list)) :
                            array_push($list, strtolower($v_val));
                        endif;
                    endforeach;
                endif;
            endforeach;
        endforeach;

        print json_encode($list);
    }

    private function _get_searched_reports($data) {
        $filter = str_replace('"', '',$_POST["data"]);

        $result = $this->db->query("SELECT
                            id, name, headers
                            FROM reports
                            WHERE is_active = 1");

        $report_list = "";
        $result_list = [];

        foreach ($result AS $data) {
            $headers = explode(",", $data->headers);
            foreach($headers as $key => $val):
                if(strtolower($filter) == strtolower($val)) {
                    $report_list .= $this->html->option($data->name, $data->id);
                    array_push($result_list, $data);
                }
            endforeach;

        }

        if(count($report_list) > 0) {
            print json_encode(array("response_code" => "0", "report_list" => $result_list));
        } else {
            print json_encode(array("response_code" => "1", "report_list" => $result_list));
        }
    }

    private function _get_selected_report($data) {
        $sql_statement = "";
        $headers = "";

        if($this->input->request_method("POST")) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $id = str_replace('"', '', $post_data["data"]);

            $result = $this->db->query("SELECT
                            id, name, sql_statement, headers
                            FROM reports
                            WHERE id = $id");

            if(count($result) > 0) {
                $sql_statement = $result[0]->sql_statement;
                $headers = $result[0]->headers;
            }

            if($sql_statement != "" && $headers != "") {
                print json_encode(array("response_code" => "0", "sql_statement" => $sql_statement, "headers" => $headers));
            } else {
                print json_encode(array("response_code" => "1", "sql_statement" => $sql_statement, "headers" => $headers));
            }

        } else {
            print json_encode(array("response_code" => "1", "sql_statement" => $sql_statement, "headers" => $headers));
        }
    }

    private function _get_selected_report_items($data) {
        $result = null;
        if($this->input->request_method("POST")) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $sql_statement = str_replace('"', '', $post_data["data"]);

            $result = $this->db->query($sql_statement);

            if(count($result) > 0) {
                print json_encode(array("response_code" => "0", "result" => $result));
            } else {
                print json_encode(array("response_code" => "1", "result" => $result));
            }

        } else {
            print json_encode(array("response_code" => "1", "result" => $result));
        }
    }

    private function _report_maintenance() {
        if($this->input->request_method('POST')) {
            $post_data_keys = array("name", "sql_statement", "headers");
        } else {
            $result = $this->db->query("SELECT r.id, r.name, r.sql_statement, r.headers,
                                        IF(r.is_active = 1, 'Yes', 'No') AS active
                                        FROM reports as r");
            $reports_list = "";
            if(count($result) > 0) {
                $reports_list .= "<tbody>";
                foreach($result as $data):
                    $reports_list .= "<tr>";
                    $reports_list .= "<td>$data->id</td>";
                    $reports_list .= "<td>$data->name</td>";
                    $reports_list .= "<td>$data->sql_statement</td>";
                    $reports_list .= "<td>$data->headers</td>";
                    $reports_list .= "<td>$data->active</td>";
                    $reports_list .= "</tr>";
                endforeach;
                $reports_list .= "</tbody>";
            }

            $this->set("report_list", $reports_list);
            $this->render("index","reports/maintenance","AGC Employee Locator | CMS");
        }
    }

    private function _get_report_list() {

    }

    private function _export($id) {
        $headers = "";
        $sql_statement = "";

        $report = $this->db->create("reports");
        $report->id = 0;
        $report->sql_statement = "";
        $report->headers = "";
        $result = $this->db->query($report, array("id" => $id));

        foreach($result as $data):
            $headers = $data->headers;
            $sql_statement = $data->sql_statement;
        endforeach;
        $headers_array = explode(",", $headers);

        $this->export_data("report", $headers_array, $sql_statement);
    }

    private function _export_xls($data) {
        if($this->input->request_method("POST")) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $headers = str_replace('"', '', $post_data["headers"]);
            $sql_statement = str_replace('"', '', $post_data["sql_statement"]);

            $headers_array = explode(",", $headers);

           $this->export_data_xls("report", $headers_array, $sql_statement);
        } else {
            print_r($data);
        }
    }

    private function _export_data($headers_array, $sql_statement) {
        $this->export_data("report", $headers_array, $sql_statement);
    }


}