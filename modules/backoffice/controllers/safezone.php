<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Safezone extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/safezone","Safe Zone Maintenance");

        $this->load->library(array("uri","input","form","alert","html","twitterapi", "upload"));

        $this->post_data_keys = array(
                "safezone_name" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 256
                    )
                ),
                "street" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 128
                    )
                ),
                "barangay" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 48
                    )
                ),"city" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 64
                    )
                ),"province_id" => array(
                    "required" => true,
                    "type" => Form::TYPE_NUMBER,
                    "extras" => array(
                        "min" => 1,
                        "max" => 3
                    )
                ),"category_id" => array(
                    "required" => true,
                    "type" => Form::TYPE_NUMBER,
                    "extras" => array(
                        "min" => 1,
                        "max" => 3
                    )
                ),"zip_code" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 16
                    )
                ),"information" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 512
                    )
                ),"person_in_charge" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 256
                    )
                ),"contact_no" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 16
                    )
                )                
            );

    }

    public function request_handler($request_method,$request_params) {
        //$this->checkpoint("/backoffice/safezone/" . $request_method);

        if ($request_method == "add") {
            $this->_add_safezone();
        } else if ($request_method == "edit") {
            $this->_edit_safezone($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "delete") {
            $this->_delete_safezone($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "approve") {
            $this->_approve($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "disapprove") {
            $this->_disapprove($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "view") {
            $this->_view($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "export") {
            $this->_export_data(isset($request_params[1]) ? $request_params[1] : 0);
        } else if ($request_method == "nominated") {
            if(isset($request_params[1]) && $request_params[1] == "list") {
                $this->_list_nominated_safezone(1);
            } else {
                $this->_list_nominated();
            }
        } else {
            if ($request_method == "list" && $request_params[1] !== null && $request_params[1] == "data") {
                $this->_list_safezone_data();
            } else {
                $this->_list_safezone();
            }            
        }
    }

    private function _list_nominated() {
        $this->render("index","safezone/nominated","AGC Employee Locator | CMS");
    }

    private function _list_nominated_safezone($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR s.id = " . $text;
            }
            $search_by = "AND (safezone_name LIKE '%$text%' OR CONCAT(street,', ',barangay,', ',city,', ',province_name) LIKE '%$text%' OR contact_no LIKE '%$text%' OR person_in_charge LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT count(s.id) AS total FROM safezones s
                                    LEFT JOIN provinces p ON p.id = s.province_id 
                                    WHERE s.is_deleted <> 1 AND is_nominated = 1 $search_by");
        
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT s.id, safezone_name,
                                    CONCAT(street,', ',barangay,', ',city,', ',province_name) AS address,
                                    contact_no,
                                    person_in_charge,
                                    CASE
                                      when is_approved = 1 then 'Approved'
                                      when is_approved = 2 then 'Disapproved'
                                      when is_approved = 0 then 'No Status'
                                    END
                                      AS status
                                    FROM safezones s
                                    LEFT JOIN provinces p ON p.id = s.province_id WHERE s.is_deleted <> 1 AND is_nominated = 1
                                    $search_by
                                    LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _view($id) {
        if (ctype_digit($id)) {
            $result = $this->db->query("SELECT 	s.id as id,
                                        s.safezone_name as safezone,
                                        s.street as street,
                                        s.barangay as barangay,
                                        s.city as city,
                                        p.province_name as province,
                                        s.zip_code as zip_code,
                                        s.information as information,
                                        s.person_in_charge as person_in_charge,
                                        s.contact_no as contact_no,
                                        c.description AS category
                                        FROM safezones as s
                                        INNER JOIN provinces as p
                                        ON s.province_id = p.id
                                        LEFT JOIN safezone_categories c
                                        ON c.id = s.category_id
                                        WHERE s.id=" . $id);


            if (count($result) > 0) {
                foreach ((array)$result[0] as $key => $data) {
                    $this->set($key,$data);
                }
            }

            $safezone_supplies = $this->db->query("SELECT item_id FROM safezone_supplies WHERE safezone_id=".$id);

            $supplies = $this->db->query("SELECT id,item_name FROM items");
            $supply_list = "";

            $safezone_supplies_array = array();
            foreach ($safezone_supplies as $data) {
                array_push($safezone_supplies_array, $data->item_id);
            }

            foreach ($supplies as $data) {
                $checked = "";
                if(in_array($data->id, $safezone_supplies_array)) {
                    $checked = "checked";
                }
                $supply_list.="<label><input type=\"checkbox\" value=$data->id $checked name=\"supplies[]\" disabled>$data->item_name</label><br>";
            }

            $this->set("supply_list",$supply_list);


            $this->render("index","safezone/view","AGC Employee Locator | CMS");
        }
    }

    private function _bulk_upload() {
        $this->debug_message = "";
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $this->error_data = "<strong><h3>Bulk upload error.</h3></strong> <br/><br/>";
            $rows_updated = 0;
            $rows_toupdate = 0;

            if($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));
                $file_path = BASEPATH . "assets/uploads/bulk/";
                //$file_path = BASEPATH . "bulk/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if($this->upload->do_upload("file", $config)) {
                    $file_name = $this->upload->get_uploaded_filename();
                    $csv_data = $this->helper->get_csv($file_path.$file_name, false);

                    $action = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "safezone";

                    $this->db->insert($file);
                    $this->row = 0;

                    $this->post_data_keys = array(
                        "id" => array(
                            "required" => false,
                            "type" => Form::TYPE_INT,
                            "extras" => array(
                            )
                        ),
                        "safezone_name" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 256
                            )
                        ),
                        "street" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "barangay" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 48
                            )
                        ),
                        "city" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 64
                            )
                        ),
                        "province" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "zip_code" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 16
                            )
                        ),
                        "person_in_charge" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 256
                            )
                        ),
                        "contact_no" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 16
                            )
                        ),"information" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 512
                            )
                        ),
                        "category" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 24
                            )
                        )
                    );

                    $rows_toupdate = count($csv_data);

                    foreach($csv_data as $data):
                        ++$this->row;
                        $this->safezone = $this->db->create("safezones");
                        $this->safezone_supplies = $this->db->create("safezone_supplies");
                        $this->supplies = "";
                        $this->supplies_list = array();

                        if($action == "add") {
                            $this->log_open(1, "insert", "bulk upload");
                            $error_count = $this->form->validate($this->post_data_keys, $data, function($key, $value) {
                                if($key != "id") {
                                    if($key == "province") {
                                        $value = $this->find_province($value);
                                        $key = "province_id";
                                    } else if($key == "information") {
                                        $value = $value;
                                        $key = "information";
                                    } else if($key == "category") {
                                        $value = $this->find_category($value);
                                        $key = "category_id";
                                    }

                                    $this->safezone->$key = utf8_encode(trim($value));
                                    $this->log_insert($key, $this->safezone->$key);
                                }
                            }, function($error_type, $key){
                                $this->error_data .= "Unable to add record at row #$this->row: ";
                                if ($error_type == Form::ERROR_ID_NOT_SET) {
                                    $this->error_data .= " missing required field '$key'<br>\n";
                                } else {
                                    $this->error_data .= " could not validate '$key' field<br>\n";
                                }
                            });

                            if($this->find_category($data["category"]) == "0") {
                                $error_count += 1;
                                $this->error_data .= " Category field is invalid.<br/>";
                            }

                            if($this->find_province($data["province"]) == "0") {
                                $error_count += 1;
                                $this->error_data .= " Province field is invalid.<br/>";
                            }

                            $this->supplies = $data["safezone_supplies"];
                            $this->supplies_list = explode("-",$this->supplies);

                            foreach($this->supplies_list as $key => $val):
                                if(is_numeric($val)) {
                                    if($val > 0 && $val < 5) {
                                    } else {
                                        $error_count += 1;
                                        $this->error_data .= "Invalid data for safezone supplies. ";
                                    }
                                } else {
                                    $error_count += 1;
                                    $this->error_data .= "Invalid data for safezone supplies. ";
                                }
                            endforeach;

                            if($error_count == 0) {
                                $this->safezone->date_created = $this->get_current_date();
                                $this->safezone->created_by = $this->get_current_user();
                                $this->db->insert($this->safezone);

                                $last_id = $this->db->getLastInsertId();

                                foreach($this->supplies_list as $key => $val):
                                    $this->safezone_supplies = $this->db->create("safezone_supplies");
                                    $this->safezone_supplies->safezone_id = $last_id;
                                    $this->safezone_supplies->item_id = $val;
                                    $this->db->insert($this->safezone_supplies);
                                endforeach;

                                $rows_updated++;
                            } else {
                                $this->error_data .= "Unable to add record at row #$this->row ";
                            }
                        } else if ($action == "update") {
                            if(isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1, "update", "bulk upload");
                                $error_count = $this->form->validate($this->post_data_keys, $data, function($key, $value){
                                    if($key != "id") {
                                        if ($key == "province") {
                                            $value = $this->find_province($value);
                                            $key = "province_id";
                                        } else if ($key == "information") {
                                            $value = $value;
                                            $key = "information";
                                        } else if($key == "category") {
                                            $value = $this->find_category($value);
                                            $key = "category_id";
                                        }

                                        $this->safezone->$key = utf8_encode(trim($value));
                                        $this->log_changes($key, $this->safezone->$key, $this->safezone->$key);
                                    }
                                }, function($error_type, $key) {
                                    $this->error_data .= "Unable to add record at row #$this->row: ";
                                    if ($error_type == Form::ERROR_ID_NOT_SET) {
                                        $this->error_data .= " missing required field '$key'<br>\n";
                                    } else {
                                        $this->error_data .= " could not validate '$key' field<br>\n";
                                    }
                                });

                                $this->supplies = $data["safezone_supplies"];
                                $this->supplies_list = explode("-",$this->supplies);

                                foreach($this->supplies_list as $key => $val):
                                    if(is_numeric($val)) {
                                        if($val > 0 && $val < 5) {
                                        } else {
                                            $error_count += 1;
                                            $this->error_data .= "Invalid data for safezone supplies. ";
                                        }
                                    } else {
                                        $error_count += 1;
                                        $this->error_data .= "Invalid data for safezone supplies. ";
                                    }
                                endforeach;

                                if($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM safezones WHERE id = " . intval($data["id"]));
                                    if(count($result) > 0) {
                                        $this->safezone->date_modified = $this->get_current_date();
                                        $this->safezone->modified_by = $this->get_current_user();
                                        $this->db->update($this->safezone, array("id" => $data["id"]));

                                        $sql = "DELETE FROM safezone_supplies WHERE safezone_id = " . $data["id"];
                                        $this->db->update($sql);

                                        foreach($this->supplies_list as $key => $val):
                                            $this->safezone_supplies = $this->db->create("safezone_supplies");
                                            $this->safezone_supplies->safezone_id = $data["id"];
                                            $this->safezone_supplies->item_id = $val;
                                            $this->db->insert($this->safezone_supplies);
                                        endforeach;

                                        $rows_updated++;
                                    } else {
                                        $this->error_data .= "Unable to update row #" . $this->row . ": no matching record found for '$this->safezone->id'<br>\n";
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if(isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1, "delete", "bulk upload");
                                $this->safezone->is_deleted = 1;
                                $this->safezone->date_deleted = $this->get_current_date();
                                $this->safezone->deleted_by = $this->get_current_user();
                                $this->db->update($this->safezone, array("id" => $data["id"]));
                                $this->log_close();
                                $rows_updated++;
                            } else {
                                $this->error_data .= "Unable to delete record at row #" . $this->row . " missing required field id<br>\n";
                            }
                        }

                    endforeach;

                }
            }

            if ($rows_updated > 0 && ($rows_updated === $rows_toupdate)) {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            }

        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list .= "<tbody>";
                foreach ($result as $data) {
                    $upload_list .= "<tr>";
                    $upload_list .= "<td>$data->file_name</td>";
                    $upload_list .= "<td>$data->date_uploaded</td>";
                    $upload_list .= "<td>$data->section</td>";
                    $upload_list .= "<td>$data->action</td>";
                    $upload_list .= "<td>$data->uploaded_by</td>";
                    $upload_list .= "<td>$data->hash_value</td>";
                    $upload_list .= "</tr>";
                }
                $upload_list .= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list .= "<tr>";
                    $download_list .= "<td>$data->file_name</td>";
                    $download_list .= "<td>$data->date_created</td>";
                    $download_list .= "<td>$data->created_by</td>";
                    $download_list .= "<td>$data->hash_value</td>";
                    $download_list .= "</tr>";
                }
                $download_list .= "</tbody>";
            }

            $this->set("upload_list", $upload_list);
            $this->set("download_list", $download_list);

            $this->render("index", "safezone/upload", "AGC Employee Locator | CMS");
        }
    }

    private function _export_data($id) {
        $headers = array("id",
                        "safezone_name",
                        "street",
                        "barangay",
                        "city",
                        "province",
                        "zip_code",
                        "person_in_charge",
                        "contact_no",
                        "information",
                        "safezone_supplies",
                        "category");

        $this->export_data("safezone", $headers, "SELECT
                            s.id as id,
                            s.safezone_name as safezone_name,
                            s.street as street,
                            s.barangay as barangay,
                            s.city as city,
                            p.province_name as province,
                            s.zip_code as zip_code,
                            s.information as information,
                            s.person_in_charge as person_in_charge,
                            s.contact_no as contact_no,
                            (SELECT GROUP_CONCAT(item_id SEPARATOR '-')
                                FROM safezone_supplies
                                WHERE safezone_id = s.id) as safezone_supplies,
                            c.description as category
                            FROM safezones as s
                            INNER JOIN provinces as p
                            ON s.province_id = p.id
                            LEFT JOIN safezone_categories c
                            ON c.id = s.category_id
                            WHERE s.is_deleted <> 1
                            ORDER BY s.safezone_name");

    }

    private function _edit_safezone($id) {
        if (ctype_digit($id)) {
            if ($this->input->request_method('POST')) {
                $post_data = $this->input->get_data(Input::POST)->get_data();

                $this->safezone = $this->db->create("safezones");
                $this->safezone->date_modified = $this->get_current_date();
                $this->safezone->modified_by = $this->get_current_user();

                $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                    $this->safezone->$key = $value;
                },function($error_type,$key) {
                });

                if(!isset($post_data['supplies']) || count($post_data['supplies']) == 0) {
                    $error_count = 1;
                }

                $result = false;

                if ($error_count == 0) {
                    $result = $this->db->update($this->safezone,array("id" => $id));

                    if($result) {
                        $sql = "DELETE FROM safezone_supplies WHERE safezone_id = " . $id;
                        $this->db->update($sql);

                        foreach ($post_data['supplies'] as $value) {
                            $safezone_supplies = $this->db->create("safezone_supplies");
                            $safezone_supplies->safezone_id = $id;
                            $safezone_supplies->item_id = $value;
                            $result = $this->db->insert($safezone_supplies);
                        }
                    }
                }

                if ($result) {
                    print json_encode(array("response_code" => 0,"response_msg" => "Record updated successfully!"));
                } else {
                    print json_encode(array("response_code" => 1,"response_msg" => "Error updating record"));
                }
            } else {
                $safezone = $this->db->create("safezones");
                $safezone->id = 0;
                $safezone->safezone_name = 0;
                $safezone->street = "";
                $safezone->barangay = "";
                $safezone->city = "";
                $safezone->province_id = "";
                $safezone->zip_code = "";
                $safezone->person_in_charge = "";
                $safezone->contact_no = "";
                $safezone->information = "";
                $safezone->category_id = "";

                $result = $this->db->query($safezone,array("id" => $id));
                if (count($result) > 0) {
                    $safezone = $result[0];            
                }

                $result = $this->db->query("SELECT id,description FROM safezone_categories ORDER BY description ASC");
                $categories = "";
                foreach ($result as $data) {
                    if ($data->id == $safezone->category_id) {
                        $categories.= $this->html->option($data->description,$data->id,true);
                    } else {
                        $categories.= $this->html->option($data->description,$data->id);
                    }
                }
                $this->set("categories",$categories);


                $result = $this->db->query("SELECT id,province_name FROM provinces ORDER BY province_name ASC");
                $province_list = "";
                foreach ($result AS $data) {
                    if ($data->id == $safezone->province_id) {
                        $province_list.= $this->html->option($data->province_name,$data->id,true);
                    } else {
                        $province_list.= $this->html->option($data->province_name,$data->id);
                    }
                }
                $this->set("province_list",$province_list); 

                $safezone_supplies = $this->db->query("SELECT item_id FROM safezone_supplies WHERE safezone_id=".$id);

                $supplies = $this->db->query("SELECT id,item_name FROM items");
                $supply_list = "";

                $safezone_supplies_array = array();
                foreach ($safezone_supplies as $data) {
                    array_push($safezone_supplies_array, $data->item_id);
                }

                foreach ($supplies as $data) {
                    $checked = "";
                    if(in_array($data->id, $safezone_supplies_array)) {
                        $checked = "checked";
                    }
                    $supply_list.="<label><input type=\"checkbox\" value=$data->id $checked name=\"supplies[]\">$data->item_name</label><br>";
                }

                $this->set("supply_list",$supply_list);

                foreach ((array)$safezone as $key => $data) {
                    $this->set($key,$data);
                }
                $this->render("index","safezone/edit","AGC Employee Locator | CMS");
            }
        }
    }

    private function _add_safezone() {
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $result = false;

            $this->safezone = $this->db->create("safezones");
            $this->safezone->date_created = $this->get_current_date();
            $this->safezone->created_by = $this->get_current_user();
            $this->safezone->is_nominated = 0;

            $error_message = "Error adding record";

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->safezone->$key = $value;
            },function($error_type,$key) {
            });

            if ($error_count == 0) {
                $result = $this->db->insert($this->safezone);
                if($result) {
                    if (!isset($post_data['supplies']) || count($post_data['supplies']) > 0) {
                        $supplies = $post_data['supplies'];
                        $safezone_id = $this->db->getLastInsertId();

                        foreach ($supplies as $supply_id) {
                            $safezone_supplies = $this->db->create("safezone_supplies");
                            $safezone_supplies->safezone_id = $safezone_id;
                            $safezone_supplies->item_id = $supply_id;
                            $result = $this->db->insert($safezone_supplies);
                        }
                    }
                }
            }

            if($result) {
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            }
        } else {
            $supplies = $this->db->query("SELECT id,item_name FROM items");
            $supply_list = "";
            foreach ($supplies as $data) {
                $supply_list.="<label><input type=\"checkbox\" value=$data->id name=\"supplies[]\">$data->item_name</label><br>";
            }

            $this->set("supply_list",$supply_list);

            $result = $this->db->query("SELECT id,province_name FROM provinces ORDER BY province_name ASC");
            $province_list = "";
            foreach ($result AS $data) {
                $province_list.= $this->html->option($data->province_name,$data->id);
            }
            $this->set("provinces",$province_list);

            $result = $this->db->query("SELECT id,description FROM safezone_categories ORDER BY description ASC");
            $categories = "";
            foreach ($result AS $data) {
                $categories.= $this->html->option($data->description,$data->id);
            }
            $this->set("categories",$categories);

            $this->render("index","safezone/add","AGC Employee Locator | CMS");
        }
    }

    private function _delete_safezone($id) {
        $error = true;
        if (ctype_digit($id)) {
            if ($id != $this->get_current_user()) {
                $safezone = $this->db->create("safezones");
                $safezone->date_deleted = $this->get_current_date();
                $safezone->deleted_by = $this->get_current_user();
                $safezone->is_deleted = 1;

                $this->db->update($safezone,array("id" => $id));
                $error = false;
            }
        }

        if ($error == true) {
            print json_encode(array("response_code" => 1,"response_msg" => "Error deleting record!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function _approve($id) {
        $error = true;
        if(ctype_digit($id)) {
            if($id != $this->get_current_user()) {
                $safezone = $this->db->create("safezones");
                $safezone->date_approved = $this->get_current_date();
                $safezone->approved_by = $this->get_current_user();
                $safezone->is_approved = 1;

                $this->db->update($safezone, array("id" => $id));
                $error = false;
            }
        }

        if($error == true) {
            print json_encode(array("response_code" => 1, "response_msg" => "Error approving safezone!"));
        } else {
            print json_encode(array("response_code" => 0, "response_msg" => "Record approved successfully!"));
        }
    }

    private function _disapprove($id) {
        $error = true;
        if(ctype_digit($id)) {
            if($id != $this->get_current_user()) {
                $safezone = $this->db->create("safezones");
                $safezone->date_disapproved = $this->get_current_date();
                $safezone->disapproved_by = $this->get_current_user();
                $safezone->is_approved = 2;

                $this->db->update($safezone, array("id" => $id));
                $error = false;
            }
        }

        if($error == true) {
            print json_encode(array("response_code" => 1, "response_msg" => "Error disapproving safezone!"));
        } else {
            print json_encode(array("response_code" => 0, "response_msg" => "Record disapproved successfully!"));
        }
    }

    private function _list_safezone_data() {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR s.id = " . $text;
            }
            $search_by = "AND (safezone_name LIKE '%$text%' OR CONCAT(street,', ',barangay,', ',city,', ',province_name) LIKE '%$text%' OR contact_no LIKE '%$text%' OR person_in_charge LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT count(s.id) AS total FROM safezones s
                                    LEFT JOIN provinces p ON p.id = s.province_id 
                                    WHERE s.is_deleted <> 1 AND is_nominated <> 1 $search_by");
        
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT s.id, safezone_name, CONCAT(street,', ',barangay,', ',city,', ',province_name) 
                                    AS address,
                                    contact_no,
                                    person_in_charge
                                    FROM safezones s
                                    LEFT JOIN provinces p ON p.id = s.province_id WHERE s.is_deleted <> 1 AND is_nominated <> 1
                                    $search_by
                                    LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _list_safezone() {
        $this->render("index","safezone/list","AGC Employee Locator | CMS");
    }

    private function find_province($prov) {
        $province = $this->db->create("provinces");
        $province->id = 0;
        $province_id = 0;
        $result = $this->db->query($province, array("province_name" => $prov));
        if(count($result) > 0) {
            $province_id = $result[0]->id;
        }
        return $province_id;
    }

    private function find_category($cat) {
        $category = $this->db->create("safezone_categories");
        $category->id = 0;
        $category_id = 0;
        $result = $this->db->query($category, array("description" => $cat));
        if(count($result) > 0) {
            $category_id =$result[0]->id;
        }
        return $category_id;
    }
}