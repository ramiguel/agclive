<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Department extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/department","Company Maintenance");

        $this->load->library(array("uri","input","alert","form","upload"));
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/department/" . $request_method);

        if ($request_method == "add") {
            $this->_add_department();
        } else if ($request_method == "edit") {
            $this->_edit_department($request_params[1]);
        } else if ($request_method == "list-by-department") {
            $this->_list_by_company_department($request_params[1]);
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "export") {
            $this->_export_data(isset($request_params[1]) ? $request_params[1] : 0);
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list_department(isset($request_params[1]) ? $request_params[1] : 1);
            }            
        }
    }

    private function _export_data($id) {
        $headers = array("id",
                         "department_name",
                         "description");

        $company_filter = "";
        if ($id > 0) {
            $company_filter = "AND cd.company_id = $id";
        }

        $this->export_data("department-",$headers, "SELECT d.id,d.department_name,d.description FROM department AS d
                                                        INNER JOIN company_department AS cd ON cd.department_id = d.id
                                                        WHERE d.is_deleted <> 1 $company_filter
                                                        ORDER BY d.department_name ASC");
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        $total = 0;
        $result = null;

        if (isset($data->{'company_id'}) && $data->{'company_id'} > 0) {
            $company_id = $data->{'company_id'};
            $search_by = "";

            $department_id = "d.id";

            if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
                if ($company_id > 0) {
                    $company_id = $this->session->userdata("company_id");
                }
                if ($this->session->userdata("department_id") > 0) {
                    $department_id = $this->session->userdata("department_id");
                }
            }

            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'}; 
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR d.id = " . $text;
                }
                $search_by = "AND (d.department_name LIKE '%$text%' OR d.description LIKE '%$text%' $search_id)";
            }

            $total = $this->get_total("SELECT COUNT(d.id) AS total FROM department AS d 
                                       LEFT JOIN company_department AS c ON c.department_id = $department_id WHERE d.is_deleted <> 1 AND c.company_id = $company_id $search_by");

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;
            $result = $this->db->query("SELECT d.id,d.department_name,d.description,d.date_created FROM department AS d
                                        LEFT JOIN company_department AS c ON c.department_id = $department_id
                                        WHERE d.is_deleted <> 1 AND c.company_id = $company_id $search_by ORDER BY d.department_name ASC LIMIT $offset,$limit");

        } else {
            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'}; 
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR id = " . $text;
                }
                $search_by = "AND (department_name LIKE '%$text%' OR description LIKE '%$text%' $search_id)";
            }

            $total = $this->get_total("SELECT COUNT(id) AS total FROM department WHERE is_deleted <> 1 $search_by");

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;
            $result = $this->db->query("SELECT id,department_name,description,date_created FROM department
                                        WHERE is_deleted <> 1 $search_by ORDER BY department_name ASC LIMIT $offset,$limit");

        }

        $this->send($total,$result);
    }

    private function _list_by_company_department($company_id) {
        print $company_id;
    }

    private function _delete($id) {
        $error = true;
        if (ctype_digit($id)) {
            $current_date = date('Y-m-d H:i:s');
            $department = $this->db->create("department");
            $department->is_deleted = 1;
            $department->deleted_by = $this->session->userdata("user_id");
            $department->date_deleted = $current_date;
            $this->db->update($department,array("id" => $id));
            $error = false;
        }
        if ($error) {
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function _list_department($page) {
        $result = array();
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
            $company_id = $this->session->userdata("company_id");
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 AND id = $company_id ORDER BY company_name ASC");
        } else {
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
        }
        $company_list = "";
        foreach ($result as $data) {
            $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
        }

        $this->set("company_list",$company_list);
        $this->render("index","department/list","AGC Employee Locator | CMS");
    }

    private function _add_department() {
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);

            $post_data_keys = array("department_name","description");
            $this->department = $this->db->create("department");
            $this->department->date_created = $this->get_current_date();
            $this->department->created_by = $this->get_current_user();

            $post_data = $request->get_data();
            
            $this->post_data_keys = array(
                "department_name" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 128
                    )
                ),
                "description" => array(
                    "required" => false,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 0,
                        "max" => 128
                    )
                )
            );

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->department->$key = $value;
            },function($error_type,$key) {
            });

            $error_message = "Error adding department record";
 
            if ($error_count == 0) {
                //$result = $this->db->query("SELECT id FROM department WHERE department_name = '" . $this->department->department_name . "'");
                $department_seek = $this->db->create("department");
                $department_seek->id = 0;
                $result = $this->db->query($department_seek,array("department_name"=>$this->department->department_name));

                if (count($result) > 0) {
                    $error_message = "Department already exists";
                    $error_count++;
                } else {
                    $result = $this->db->insert($this->department);
                }
            }

            if ($error_count != 0) {
               print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "Department added successfully!"));
            }      
        } else {
            $this->render("index","department/add","AGC Employee Locator | CMS");
        }
    }

    private function _edit_department($id) {
//        if ($this->input->request_method('POST')) {
//            $post_data = $this->input->get_data(Input::POST)->get_data();
//
//            $this->department = $this->db->create("department");
//
//            $this->post_data_keys = array(
//                "department_name" => array(
//                    "required" => true,
//                    "type" => Form::TYPE_TEXT,
//                    "extras" => array(
//                        "min" => 1,
//                        "max" => 128
//                    )
//                ),
//                "description" => array(
//                    "required" => false,
//                    "type" => Form::TYPE_TEXT,
//                    "extras" => array(
//                        "min" => 0,
//                        "max" => 128
//                    )
//                )
//            );
//
//            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
//                $this->department->$key = $value;
//            },function($error_type,$key) {
//            });
//
//            $error_message = "Error updating department record";
//
//            if ($error_count == 0) {
//                $result = $this->db->query("SELECT id FROM department WHERE department_name = '" . $this->department->department_name . "' AND id <> " . $id);
//
//                if (count($result) > 0) {
//                    $error_message = "Department already exists";
//                    $error_count++;
//                } else {
//                    $this->db->update($this->department,array("id" => $id));
//                }
//            }
//
//            if ($error_count != 0) {
//               print json_encode(array("response_code" => 1,"response_msg" => $error_message));
//            } else {
//                print json_encode(array("response_code" => 0,"response_msg" => "Department updated successfully!"));
//            }
//        } else {
//            $post_data_keys = array("department_name","description");
//            if (ctype_digit($id)) {
//                $result = $this->db->query("SELECT department_name,description FROM department WHERE id=" . $id);
//
//                foreach ($post_data_keys as $field) {
//                    $this->set($field, isset($result[0]->$field) ? $result[0]->$field : "");
//                }
//
//                $this->set("department_id",$id);
//
//                $this->render("index","department/edit","AGC Employee Locator | CMS");
//            } else {
//                redirect(base_url() . "/backoffice/department/list");
//            }
//        }
        $company_id = $this->_get_company_id($id);
        redirect(base_url() . "/backoffice/company/edit/".$company_id);
    }

    private function _bulk_upload() {
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $company_selected = $post_data["company_filter"];

            $this->error_data = "";
            if ($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));

                $file_path = BASEPATH . "assets/uploads/bulk/";
                //$file_path = BASEPATH . "bulk/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if ($this->upload->do_upload("file",$config)) {
                    $file_name = $this->upload->get_uploaded_filename();

                    $csv_data = $this->helper->get_csv($file_path.$file_name,false);

                    $action  = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "department";

                    $this->db->insert($file);

                    $this->row = 0;

                    $this->post_data_keys = array(
                        "department_name" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "description" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 0,
                                "max" => 128
                            )
                        )
                    );

                    foreach ($csv_data as $data) {
                        ++$this->row;
                        $this->department = $this->db->create("department");
                        
                        if ($action == "add") {
                            $this->log_open(1,"insert","bulk upload");
                            $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                if ($key != "id") {
                                    $this->department->$key = utf8_encode($value);
                                    $this->log_insert($key,$this->department->$key);
                                }
                            }, function ($error_type,$key) {
                                $this->error_data .= "insert at " . $this->row . " " . $error_type . " " . $key . "<br>\n";
                            });

                            if ($error_count == 0) {
                                //$result = $this->db->query("SELECT id FROM department WHERE department_name = " . $this->db->quote($this->department->department_name) . " LIMIT 0,1");
                                $department_seek = $this->db->create("department");
                                $department_seek->id = 0;
                                $result = $this->db->query("SELECT d.id, cd.company_id FROM department as d
                                    INNER JOIN company_department as cd
                                    ON d.id = cd.department_id
                                    WHERE cd.company_id = ". $company_selected ." AND d.department_name = '" . $this->department->department_name ."'");

                                if (count($result) > 0) {
                                    $this->error_data.= "cannot insert data at " . $this->row . " record already exists<br>\n";
                                } else {
                                    $this->department->date_created = $this->get_current_date();
                                    $this->department->created_by = $this->get_current_user();
                                    $this->db->insert($this->department);
                                    $last_id = $this->db->getLastInsertId();

                                    $this->company_department = $this->db->create("company_department");
                                    $this->company_department->company_id = $company_selected;
                                    $this->company_department->department_id = $last_id;
                                    $this->db->insert($this->company_department);
                                }
                            }
                        } else if ($action == "update") {
                             if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"update","bulk upload");
                                $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                    if ($key != "id") {
                                        $this->department->$key = utf8_encode($value);
                                        $this->log_changes($key,$this->department->$key,$this->department->$key);                                    
                                    }
                                }, function ($error_type,$key) {
                                    $this->error_data .= "update at ". $this->row . " " . $error_type . " " . $key . "<br>\n";
                                });

                                if ($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM department WHERE id = " . intval($data["id"]));
                                    if (count($result) > 0) {
                                        $this->department->date_modified = $this->get_current_date();
                                        $this->department->modified_by = $this->get_current_user();
                                        $this->db->update($this->department,array("id" => $data["id"]));
                                    } else {
                                        $this->error_data .= "Update at row #" . $this->row . ": Record not found<br>\n";
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {

                                $result = $this->db->query("SELECT id FROM department WHERE id = " . intval($data["id"]));
                                if (count($result) > 0) {
                                    $this->log_open(1, "delete", "bulk upload");
                                    $this->department->is_deleted = 1;
                                    $this->department->date_deleted = $this->get_current_date();
                                    $this->db->update($this->department, array("id" => $data["id"]));

                                    //delete company_departments
                                    $sql = "DELETE FROM company_department WHERE department_id = " . $data["id"];
                                    $this->db->update($sql);
                                    $this->log_close();
                                } else {
                                    $this->error_data.= "Delete at row #" . $this->row . ": ID not defined<br>\n";
                                }

                            } else {
                                $this->error_data.= "Delete at row #" . $this->row . ": ID not defined<br>\n";
                            }
                        }
                    }
                }
            }

            if (!empty($this->error_data)) {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            }
        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by                                         
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list.= "<tbody>";
                foreach ($result as $data) {
                    $upload_list.= "<tr>";
                    $upload_list.= "<td>$data->file_name</td>";
                    $upload_list.= "<td>$data->date_uploaded</td>";
                    $upload_list.= "<td>$data->section</td>";
                    $upload_list.= "<td>$data->action</td>";
                    $upload_list.= "<td>$data->uploaded_by</td>";
                    $upload_list.= "<td>$data->hash_value</td>";
                    $upload_list.= "</tr>";
                }
                $upload_list.= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by                                        
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list.= "<tr>";
                    $download_list.= "<td>$data->file_name</td>";
                    $download_list.= "<td>$data->date_created</td>";
                    $download_list.= "<td>$data->created_by</td>";
                    $download_list.= "<td>$data->hash_value</td>";
                    $download_list.= "</tr>";
                }
                $download_list.= "</tbody>";
            }

            $this->set("upload_list",$upload_list);
            $this->set("download_list",$download_list);

            $result = array();
            if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
                $company_id = $this->session->userdata("company_id");
                $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 AND id = $company_id ORDER BY company_name ASC");
            } else {
                $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
            }
            $company_list = "";
            foreach ($result as $data) {
                $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
            }

            $this->set("company_list",$company_list);

            $this->render("index", "department/upload", "AGC Employee Locator | CMS");
        }
    }

    private function _get_company_id($id) {
        $result = $this->db->query("SELECT company_id FROM company_department WHERE department_id = $id");

        return $result[0]->company_id;
    }
}