<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Whitelist extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/whitelist","Whitelist Maintenance");

        $this->load->library(array("uri","input","util","hash","alert","html","form","upload","page","helper","androidpushnotif"));

        $this->bulk_company_id = 0;

        $this->gender = array(
            "M" => "Male",
            "F" => "Female"
        );

        $this->post_data_keys = array(
            "first_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "last_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "middle_initial" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 3
                )
            ),
            "position" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 32
                )
            ),
            "email" => array(
                "required" => true,
                "type" => Form::TYPE_EMAIL,
                "extras" => array()
            ),
            "mobile_no" => array(
                "required" => true,
                "type" => Form::TYPE_MOBILE,
                "extras" => array(
                    "min" => 7,
                    "max" => 15
                )
            ),
            "employee_no" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 32
                )
            ),
            "gender" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 2
                )
            ),            
            "blood_type" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 0,
                    "max" => 8
                )
            ),
            "address_street" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "address_line_2" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 128
                )
            ),
            "address_city" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 64
                )
            ),
            "address_state" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 64
                )
            ),
            "address_zip_code" => array(
                "required" => false,
                "type" => Form::TYPE_NUMBER,
                "extras" => array(
                    "min" => 1,
                    "max" => 16
                )
            ),
            "address_country" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 42
                )
            ),
            "supervisor_id" => array(
                "required" => false,
                "type" => Form::TYPE_INT,
                "extras" => array()
            ),            
            "is_supervisor" => array(
                "required" => false,
                "type" => Form::TYPE_BOOL,
                "extras" => array()
            ),            
            "is_rescuer" => array(
                "required" => false,
                "type" => Form::TYPE_BOOL,
                "extras" => array()
            ),
            "company_id" => array(
                "required" => true,
                "type" => Form::TYPE_INT,
                "extras" => array()
            ),
            "department_id" => array(
                "required" => true,
                "type" => Form::TYPE_INT,
                "extras" => array()
            ),
            "site_id" => array(
                "required" => true,
                "type" => Form::TYPE_INT,
                "extras" => array()
            )
        );

        $this->prefixes = null;
    }

    public function request_handler($request_method,$request_params) {
        //$this->checkpoint("/backoffice/whitelist/" . $request_method);

        if ($request_method == "edit") {
            $this->_edit($request_params[1]);
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "delete_view") {
            $this->_delete_view($request_params[1]);
        } else if ($request_method == "add") {
            $this->_add();
        } else if ($request_method == "view") {
            $this->_view($request_params[1]);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "get-department") {
            $this->_get_company_department();
        } else if ($request_method == "get-supervisors") {
            $this->_get_supervisors();
        } else if ($request_method == "get-sites") {
            $this->_get_sites();
        } else if ($request_method == "export") {
            $this->_export_data(isset($request_params[1]) ? $request_params[1] : 0);
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list(isset($request_params[1]) ? $request_params[1] : 1);
            }
        }
    }

    private function _export_data($id) {
        $headers = array("id",
                         "first_name",
                         "last_name",
                         "middle_initial",
                         "gender",
                         "mobile_no",
                         "email",
                         "position",
                         "employee_no",
                         "blood_type",
                         "company",
                         "department",
                         "site",
                         "address_street",
                         "address_line_2",
                         "address_city",
                         "address_state",
                         "address_country",
                         "address_zip_code",
                         "is_supervisor",
                         "is_rescuer", 
                         "supervisor_id",
                         "date_registered");

        $company_filter = "";
        if ($id > 0) {
            $this->export_data("employee",$headers,"SELECT e.id,
                                                           e.first_name,
                                                           e.last_name,
                                                           e.middle_initial,
                                                           e.gender,
                                                           e.mobile_no,
                                                           e.email,
                                                           e.position,
                                                           e.employee_no,
                                                           e.blood_type,
                                                           c.company_name AS company,
                                                           d.department_name AS department,
                                                           cs.site as site,
                                                           e.address_street,
                                                           e.address_line_2,
                                                           e.address_city,
                                                           e.address_state,
                                                           e.address_country,
                                                           e.address_zip_code,
                                                           e.is_supervisor,
                                                           e.is_rescuer,
                                                           e.supervisor_id,
                                                           e.date_registered
                                                           FROM employees AS e 
                                                           LEFT JOIN companies AS c ON c.id = e.company_id
                                                           LEFT JOIN department AS d ON d.id = e.department_id
                                                           LEFT JOIN company_sites AS cs ON e.site_id = cs.id
                                                           WHERE e.is_deleted <> 1 AND e.company_id = $id
                                                           ORDER BY e.first_name,e.last_name ASC");
        } else {
            $this->export_data("employee",$headers,"SELECT e.id,
                                               e.first_name,
                                               e.last_name,
                                               e.middle_initial,
                                               e.gender,
                                               e.mobile_no,
                                               e.email,
                                               e.position,
                                               e.employee_no,
                                               e.blood_type,
                                               c.company_name AS company,
                                               d.department_name AS department,
                                               cs.site as site,
                                               e.address_street,
                                               e.address_line_2,
                                               e.address_city,
                                               e.address_state,
                                               e.address_country,
                                               e.address_zip_code,
                                               e.is_supervisor,
                                               e.is_rescuer,
                                               e.supervisor_id,
                                               e.date_registered
                                               FROM employees AS e
                                               LEFT JOIN companies AS c ON c.id = e.company_id
                                               LEFT JOIN department AS d ON d.id = e.department_id
                                               LEFT JOIN company_sites AS cs ON e.site_id = cs.id
                                               WHERE e.is_deleted <> 1
                                               ORDER BY e.first_name,e.last_name ASC");
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = "OR e.id = $text"; 
            }
            $search_by = "AND (c.company_name LIKE '%$text%' $search_id OR d.department_name LIKE '%$text%' OR e.email LIKE '%$text%' OR e.mobile_no LIKE '%$text%' OR CONCAT(TRIM(e.first_name),' ',TRIM(e.last_name)) LIKE '%$text%')";
        }

        $company_filter = "";

        $filter = "";
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
            $filter = "";
            if ($this->session->userdata("company_id") > 0) {
                $filter.= " AND e.company_id = " . $this->session->userdata("company_id");
            }

            if ($this->session->userdata("department_id")) {
                //$filter.= " AND e.department_id = " . $this->session->userdata("department_id");
            }
            $filter.= " ";
        } else {
            if (isset($data->{'company_id'}) && $data->{'company_id'} > 0) {
                $company_filter = " AND e.company_id = " . $data->{'company_id'}; 
            }
        }

        $total = $this->get_total("SELECT COUNT(e.id) AS total FROM employees AS e
                                   LEFT JOIN companies AS c ON c.id = e.company_id
                                   LEFT JOIN department AS d ON d.id = e.department_id
                                   WHERE e.is_deleted <> 1 $search_by $company_filter ".$filter);

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT e.id,
                                           CONCAT(e.last_name,', ',e.first_name) AS full_name,
                                           e.email,
                                           e.mobile_no,
                                           c.company_name,
                                           d.department_name,
                                           e.date_created,
                                           IF(e.is_supervisor <> 1,'No','Yes') AS is_supervisor,
                                           IF(e.is_rescuer <> 1,'No','Yes') AS is_rescuer,
                                           IF(e.is_registered <> 1,'No','Yes') AS is_registered
                                    FROM employees AS e
                                    LEFT JOIN companies AS c ON c.id = e.company_id
                                    LEFT JOIN department AS d ON d.id = e.department_id
                                    WHERE e.is_deleted <> 1 $search_by $company_filter $filter
                                    ORDER BY e.first_name,e.last_name ASC LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _list($page) {
        $company_list = "";
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
            $company_id = $this->session->userdata("company_id");
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 AND id = $company_id");
            foreach ($result as $data) {
                $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
            }
        } else {
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
            $company_list = "<option value=\"0\">All</option>";
            foreach ($result as $data) {
                $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
            }
        }

        $this->set("company_list",$company_list);

        $this->render("index","whitelist/list","AGC Employee Locator | CMS");
    }

    private function _edit($id) {
        $error = true;
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $this->employee = $this->db->create("employees");
            $this->employee->date_modified = $this->get_current_date();
            $this->employee->modified_by = $this->session->userdata("user_id");

            $this->original_employee = $this->db->create("employees");
            foreach ($this->post_data_keys as $key => $data) {
                $this->original_employee->$key = "";
            }

            $result = $this->db->query($this->original_employee,array("id" => $id));
            if (count($result) > 0) {
                $this->original_employee = $result[0];
            }

            $this->log_open(1,"update","employees");
            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {

                $this->employee->$key = trim($value);

                if ($this->employee->$key != $this->original_employee->$key) {
                    if($key == "supervisor_id"){
                        if($this->employee->$key != "") {
                            $this->log_changes($key,$this->original_employee->$key,$this->employee->$key);
                        }
                    } else if($key == "is_supervisor" || $key == "is_rescuer") {
                            
                    } else {
                        $this->log_changes($key,$this->original_employee->$key,$this->employee->$key);
                    }
                }
            },function($error_type,$key) {
            });

            $error_type = 0;
            $error_msg = "";
            if ($error_count == 0) {

                $this->employee->is_rescuer = ($post_data["is_rescuer"] == "true" ? 1 : 0);
                $this->employee->is_supervisor = ($post_data["is_supervisor"]  == "true" ? 1 : 0);

                if (!empty($this->employee->email)) {
                    $email_seek = $this->db->create("employees");
                    $email_seek->id = 0;

                    $result = $this->db->query($email_seek,array("email" => $this->employee->email,"is_deleted" => 0));
                    if (count($result) > 0 && $result[0]->id) {
                        if ($result[0]->id != $id) {
                            $error_type = 1;
                        }
                    }
                }


                if (!empty($this->employee->mobile_no)) {
                    $mobile_seek = $this->db->create("employees");
                    $mobile_seek->id = 0;

                    $result = $this->db->query($mobile_seek,array("mobile_no" => $this->employee->mobile_no,"is_deleted" => 0));
                    if (count($result) > 0 && $result[0]->id) {
                        if ($result[0]->id != $id) {
                            $error_type = 2;
                        }
                    }
                }

                if ($error_type == 1) {
                    $error_msg = "The e-mail you are trying to register is already in use";
                } else if ($error_type == 2) {
                    $error_msg = "The mobile you are trying to register is already in use";
                }

                if($error_type == 0) {
                    $this->log_close();
                    $this->db->update($this->employee, array("id" => $id));

                    print json_encode(array("response_code" => 0,"response_msg" => "Data updated successfully!"));
                } else {
                    print json_encode(array("response_code" => 1, "response_msg" => $error_msg));
                }

            } else {
                print json_encode(array("response_code" => 1, "response_msg" => "Error updating data!"));
            }

        }
        else {
            if (ctype_digit($id)) {
                $employee = $this->db->create("employees");
                $post_keys = array("id",
                                    "first_name",
                                    "last_name",
                                    "middle_initial",
                                    "email",
                                    "mobile_no",
                                    "is_supervisor",
                                    "is_rescuer",
                                    "supervisor_id",
                                    "position",
                                    "employee_no",
                                    "gender",
                                    "blood_type",
                                    "company_id",
                                    "department_id",
                                    "site_id",
                                    "address_street",
                                    "address_line_2",
                                    "address_city",
                                    "address_zip_code",
                                    "address_state",
                                    "address_country");

                foreach ($post_keys as $key) {
                    $employee->$key = "";
                }

                $result = $this->db->query($employee,array(
                    "id" => $id
                ));

                if (count($result) > 0 && $result[0]->id) {
                    $employee = $result[0];

                    foreach ($post_keys as $key) {
                        if ($key == "is_supervisor" || $key == "is_rescuer") {
                            $this->set($key,$employee->$key == 1 ? "checked" : "");
                        } else {
                            $this->set($key,$employee->$key);    
                        }
                    }

                    $company_list = "";
                    $department_list = "";
                    $company_filter = array("is_deleted" => "0");
                    if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
                        $company_filter=array("id" => $employee->company_id,"is_deleted" => "0");
                    } else {
                        $company_list .= "<option value=\"\">Select Company</option>";
                        $department_list = "<option value=\"\">Select Department</option>";
                        $site_list = "<option value=\"\">Select Site</option>";
                    }

                    $company = $this->db->create("companies");
                    $company->id = 0;
                    $company->company_name = "";
                    $company->setOrderBy("company_name","ASC");

                    $result = $this->db->query($company,$company_filter);
                    if (count($result) > 0) {
                        $company_list .= $this->html->option("Ayala Group of Companies",0,true);
                        foreach ($result as $company) {
                            if ($company->id == $employee->company_id) {
                                $company_list .= $this->html->option($company->company_name,$company->id,true);
                            } else {
                                $company_list .= $this->html->option($company->company_name,$company->id);
                            }
                        }
                    }

                    $this->set("company_list",$company_list);

                    if ($company->id > 0) {
                        $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                                    LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                                    LEFT JOIN companies AS c ON c.id = cd.company_id
                                                    WHERE c.id = " . $employee->company_id . " AND d.is_deleted <> 1 ORDER BY d.department_name ASC");

                        if (count($result) > 0) {
                            foreach ($result as $item) {
                                if ($item->id == $employee->department_id) {
                                    $department_list .= $this->html->option($item->department_name,$item->id,true);
                                } else {
                                    $department_list .= $this->html->option($item->department_name,$item->id);
                                }
                            }
                        }
                    }

                    $this->set("department_list",$department_list);

                    $supervisor_list = "";
                    $result = $this->db->query("SELECT id,CONCAT(first_name,' ',last_name) AS full_name,department_id FROM employees 
                                                WHERE is_deleted <> 1 AND is_supervisor = 1 AND id <> ". $id ." AND company_id = " . $employee->company_id. " ORDER BY first_name ASC");
                    if (count($result) > 0) {
                        foreach ($result as $item) {
                            if ($employee->supervisor_id == $item->id) {
                                $supervisor_list .= $this->html->option($item->full_name,$item->id,true);
                            } else {
                                $supervisor_list .= $this->html->option($item->full_name,$item->id);                                
                            }
                        }
                    }

                    $this->set("supervisor_list",$supervisor_list);

                    $result = $this->db->query("SELECT description FROM blood_types ORDER BY description");
                    $blood_types = "";
                    foreach ($result as $data) {
                        if ($data->description == $employee->blood_type) {
                            $blood_types .= $this->html->option($data->description,$data->description,true);
                        } else {
                            $blood_types .= $this->html->option($data->description,$data->description);
                        }
                    }

                    $this->set("blood_types",$blood_types);
                    $gender_list = "";
                    foreach ($this->gender AS $key => $value) {
                        if ($employee->gender == $key) {
                            $gender_list .= $this->html->option($value,$key,true);
                        } else {
                            $gender_list .= $this->html->option($value,$key);
                        }
                    }


                    $this->set("gender_list",$gender_list);

                    $result = $this->db->query("SELECT id, company_id, site FROM company_sites WHERE company_id = " . $employee->company_id. " ORDER BY site");

                    if (count($result) > 0) {
                        foreach ($result as $item) {
                            if($item->id == $employee->site_id) {
                                $site_list .= $this->html->option($item->site,$item->id, true);
                            } else {
                                $site_list .= $this->html->option($item->site,$item->id);
                            }
                        }
                    }

                    $this->set("site_list", $site_list);

                    $this->render("index","whitelist/edit","AGC Employee Locator | CMS");
                    $error = false;
                }
            }
        }
    }

    private function _view($id) {
        $error = true;
        if (ctype_digit($id)) {
            $employee = null;
            $result = $this->db->query("SELECT e.id,
                                               e.first_name,
                                               e.last_name,
                                               e.middle_initial,
                                               e.email,
                                               e.mobile_no,
                                               e.is_supervisor,
                                               e.is_rescuer,
                                               e.date_created,
                                               u.username AS created_by,
                                               e.position,
                                               c.company_name,
                                               d.department_name,
                                               cs.site as site_name,
                                               e.employee_no, 
                                               e.gender,
                                               e.blood_type,
                                               e.address_street,
                                               e.address_line_2,
                                               e.address_city,
                                               e.address_zip_code,
                                               e.address_state,
                                               e.address_country,
                                               e.date_modified,
                                               u2.username AS modified_by,
                                               e.date_registered,
                                               e.is_registered,
                                               e.img_path,
                                               CONCAT(s.first_name,' ',s.middle_initial,' ',s.last_name) AS supervisor FROM employees AS e
                                        LEFT JOIN companies AS c ON c.id = e.company_id
                                        LEFT JOIN department AS d ON d.id = e.department_id
                                        LEFT JOIN users AS u ON u.id = e.created_by
                                        LEFT JOIN employees AS s ON s.id = e.supervisor_id 
                                        LEFT JOIN users AS u2 ON u2.id = e.modified_by
                                        LEFT JOIN company_sites AS cs ON e.site_id = cs.id
                                        WHERE e.id = " . $id . " AND e.is_deleted <> 1 LIMIT 0,1");

            if (count($result) > 0 && $result[0]->id) {
                $employee = $result[0];

                foreach (get_object_vars($employee) as $key => $value) {
                    if (in_array($key,array("is_supervisor","is_rescuer","is_registered"))) {
                        $this->set($key,$employee->$key == 1 ? "checked" : "");
                    } else {
                        if ($key == "img_path") {
                            if ($value == "") {
                                //$value = BASE_URL . "assets/photo/avatar_male_light_on_gray_200x200.png";
                                $value = BASE_URL . "assets/photo/default-profile.png";
                            }
                        }
                        $this->set($key,$value);
                    }
                }

                $supervisor_list = "";
                $employee_list = "";
                $rescuer_list = "";
                if ($employee->is_supervisor == 1) {
                    $result = $this->db->query("SELECT id,CONCAT(first_name,' ',middle_initial,' ',last_name) AS full_name 
                                                FROM employees 
                                                WHERE supervisor_id = " . $employee->id . " AND is_supervisor = 1");

                    if (count($result) > 0) {
                        $supervisor_list .= "<div class=\"form-group\">";
                        $supervisor_list.=  "  <label for=\"date_created\" class=\"control-label col-xs-2\">Supervisors</label>";
                        $supervisor_list.=  "  <div class=\"col-xs-4\">";

                        $supervisor_list.= "<table class=\"table table-striped table-responsive\">";
                        $supervisor_list.= "   <thead>";
                        $supervisor_list.= "       <tr>";
                        $supervisor_list.= "           <th>ID</th>";
                        $supervisor_list.= "           <th>Name</th>";
                        $supervisor_list.= "       </tr>";
                        $supervisor_list.= "   </thead>";

                        $supervisor_list.= "   <tbody>";

                        foreach ($result as $data) {
                            $link = base_url() . "/backoffice/whitelist/view/" . $data->id;
                            $supervisor_list.= "       <tr>";
                            $supervisor_list.= "           <td>$data->id</td>";
                            $supervisor_list.= "           <td><a href=\"$link\">$data->full_name</a></td>";
                            $supervisor_list.= "       </tr>";
                        }

                        $supervisor_list.= "   </tbody>";
                        $supervisor_list.= "</table>";

                        $supervisor_list.=  "  </div>";
                        $supervisor_list.=  "</div>";                
                    }

                    $result = $this->db->query("SELECT id,CONCAT(first_name,' ',middle_initial,' ',last_name) AS full_name 
                                                FROM employees 
                                                WHERE supervisor_id = " . $employee->id . " AND is_rescuer = 1");

                    if (count($result) > 0) {
                        $rescuer_list .= "<div class=\"form-group\">";
                        $rescuer_list.=  "  <label for=\"date_created\" class=\"control-label col-xs-2\">Rescuers</label>";
                        $rescuer_list.=  "  <div class=\"col-xs-4\">";

                        $rescuer_list.= "<table class=\"table table-striped table-responsive\">";
                        $rescuer_list.= "   <thead>";
                        $rescuer_list.= "       <tr>";
                        $rescuer_list.= "           <th>ID</th>";
                        $rescuer_list.= "           <th>Name</th>";
                        $rescuer_list.= "       </tr>";
                        $rescuer_list.= "   </thead>";

                        $rescuer_list.= "   <tbody>";
                        foreach ($result as $data) {
                            $link = base_url() . "/backoffice/whitelist/view/" . $data->id;
                            $rescuer_list.= "       <tr>";
                            $rescuer_list.= "           <td>$data->id</td>";
                            $rescuer_list.= "           <td><a href=\"$link\">$data->full_name</a></td>";
                            $rescuer_list.= "       </tr>";
                        }

                        $rescuer_list.= "   </tbody>";
                        $rescuer_list.= "</table>";       

                        $rescuer_list.=  "  </div>";
                        $rescuer_list.=  "</div>";                
                    }

                    $result = $this->db->query("SELECT id,CONCAT(first_name,' ',middle_initial,' ',last_name) AS full_name FROM employees WHERE supervisor_id = " . $employee->id . " AND is_supervisor <> 1 AND is_rescuer <> 1");

                    if (count($result) > 0) {
                        $employee_list .= "<div class=\"form-group\">";
                        $employee_list.=  "  <label for=\"date_created\" class=\"control-label col-xs-2\">Employees</label>";
                        $employee_list.=  "  <div class=\"col-xs-4\">";

                        $employee_list.= "<table class=\"table table-striped table-responsive\">";
                        $employee_list.= "   <thead>";
                        $employee_list.= "       <tr>";
                        $employee_list.= "           <th>ID</th>";
                        $employee_list.= "           <th>Name</th>";
                        $employee_list.= "       </tr>";
                        $employee_list.= "   </thead>";

                        $employee_list.= "   <tbody>";
                        foreach ($result as $data) {
                            $link = base_url() . "/backoffice/whitelist/view/" . $data->id;
                            $employee_list.= "       <tr>";
                            $employee_list.= "           <td>$data->id</td>";
                            $employee_list.= "           <td><a href=\"$link\">$data->full_name</a></td>";
                            $employee_list.= "       </tr>";
                        }

                        $employee_list.= "   </tbody>";
                        $employee_list.= "</table>";       

                        $employee_list.=  "  </div>";
                        $employee_list.=  "</div>";                
                    }
                }

                $this->set("supervisor_list",$supervisor_list);
                $this->set("rescuer_list",$rescuer_list);
                $this->set("employee_list",$employee_list);

                $this->render("index","whitelist/view","AGC Employee Locator | CMS");

                $error = false;
            }
        }

        if ($error) {
            $this->alert->set_message(1,"");
            redirect(base_url() . "/backoffice/whitelist");
        }
    }

    private function _delete($id) {
        $error = true;
        if (ctype_digit($id)) {
            $employee = $this->db->create("employees");
            $employee->date_deleted = date('Y-m-d H:i:s');
            $employee->is_deleted = 1;

            $this->log_open(1,"delete","employees");

            $this->db->update($employee,array("id" => $id));
            $error = false;

            $result = $this->db->query("SELECT os_type,device_key FROM devices WHERE owner=".$id." LIMIT 1");
            if (count($result) > 0 && $result[0]->os_type == "2") {
                $date = date_create();
                $message = array(
                        "is_user_deleted" => 1,
                        "timestamp" => date_format($date, 'U')
                );
                $this->androidpushnotif->push(array($result[0]->device_key),$message);
            }
        }

        if ($error) {
            $this->log_close();
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function _delete_view($id) {
        $error = true;
        if (ctype_digit($id)) {
            $employee = $this->db->create("employees");
            $employee->date_deleted = date('Y-m-d H:i:s');
            $employee->is_deleted = 1;

            $this->log_open(1,"delete","employees");

            $this->db->update($employee,array("id" => $id));
            $error = false;
        }

        redirect(base_url() . "/backoffice/whitelist");
    }

    private function _add() {
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $error_message = "";

            $this->employee = $this->db->create("employees");
            $this->employee->password = base64_encode($this->util->generate_password());
            $this->employee->date_created = $this->get_current_date();
            $this->employee->created_by = $this->session->userdata("user_id");
            $this->employee->img_path = BASE_URL . "/assets/photo/avatar_male_light_on_gray_200x200.jpg";

            $this->log_open(1,"insert","employees");

            /* Start validating forms based on the rule provided */

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->employee->$key = trim($value);
                $this->log_insert($key,$this->employee->$key);
            },function($error_type,$key) {
            });

            $error = true;

            if ($error_count == 0) {
                $error_type = 0;

                if (!empty($this->employee->email)) {
                    $email_seek = $this->db->create("employees");
                    $email_seek->id = 0;

                    $result = $this->db->query($email_seek,array("email" => $this->employee->email,"is_deleted" => 0));
                    if (count($result) > 0 && $result[0]->id) {
                        $error_type = 1;
                    }                    
                }

                if (!empty($this->employee->mobile_no)) {
                    $mobile_seek = $this->db->create("employees");
                    $mobile_seek->id = 0;

                    $result = $this->db->query($mobile_seek,array("mobile_no" => $this->employee->mobile_no,"is_deleted" => 0));
                    if (count($result) > 0 && $result[0]->id) {
                        $error_type = 2;
                    }                    
                }

                if ($error_type != 0) {
                    if ($error_type == 1) {
                        $error_message = "The e-mail you are trying to register is already in use";
                    } else if ($error_type == 2) {
                        $error_message = "The mobile you are trying to register is already in use";
                    }
                } else {
                    $this->log_close();

                    $this->db->insert($this->employee);
                    $error = false;
                }
            }

            if ($error) {
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            }
        }
        else {
            $company_list = "";
            $department_list = "";
            $company = $this->db->create("companies");
            $company->id = 0;
            $company->company_name = "";
            $company->setOrderBy("company_name","ASC");

            $company_id = 0;

            $company_filter = array("is_deleted" => "0");
            if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
                $company_id = $this->session->userdata("company_id");
                $company_filter=array("id" => $company_id,"is_deleted" => "0");
            } else {
                $company_list = "<option value=\"\">Select Company</option>";
                $department_list = "<option value=\"\">Select Department</option>";
                $site_list = "<option value=\"\">Select Site</option>";
            }

            $result = $this->db->query($company,$company_filter);
            if (count($result) > 0) {
                foreach ($result as $company) {
                    $company_list .= $this->html->option($company->company_name,$company->id);
                }
                $company_id = $result[0]->id;
            }

            $this->set("company_list",$company_list);

            if ($company_id > 0) {
                $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id . " AND c.is_deleted <> 1 ORDER BY d.department_name ASC");

                if (count($result) > 0) {
                    foreach ($result as $item) {
                        $department_list .= $this->html->option($item->department_name,$item->id);
                    }
                }
            }

            $this->set("department_list",$department_list);            

            $supervisor_list = "";
            $result = $this->db->query("SELECT id,CONCAT(first_name,' ',last_name) AS full_name FROM employees WHERE is_supervisor = 1 AND company_id = $company_id AND is_deleted <> 1 ORDER BY first_name ASC");
            
            //echo $asd = "SELECT id,CONCAT(first_name,' ',last_name) AS full_name FROM employees WHERE is_supervisor = 1 AND company_id = $company_id AND is_deleted <> 1";
            
            if (count($result) > 0) {
                foreach ($result as $item) {
                    $supervisor_list .= $this->html->option($item->full_name,$item->id);
                }
            }

            $this->set("supervisor_list",$supervisor_list);

            $result = $this->db->query("SELECT description FROM blood_types ORDER BY description");
            $blood_types = "";
            foreach ($result as $data) {
                $blood_types .= $this->html->option($data->description,$data->description);
            }

            $result = $this->db->query("SELECT id, company_id, site FROM company_sites ORDER BY site");

            if (count($result) > 0) {
                foreach ($result as $item) {
                    $site_list .= $this->html->option($item->site,$item->id);
                }
            }

            $this->set("site_list", $site_list);

            $this->set("blood_types",$blood_types);

            $this->render("index", "whitelist/add", "AGC Employee Locator | CMS");
        }
    }

    private function _bulk_upload() {
        $this->debug_message = "";
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $this->error_data = "<strong>Bulk upload error.</strong><br/><br/>";
            $rows_updated = 0;
            $rows_toupdate = 0;
            if ($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));

                $file_path = BASEPATH . "assets/uploads/bulk/";
                //$file_path = BASEPATH . "bulk/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if ($this->upload->do_upload("file",$config)) {
                    $file_name = $this->upload->get_uploaded_filename();

                    $csv_data = $this->helper->get_csv($file_path.$file_name,false);

                    $action  = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "whitelist";

                    $this->db->insert($file);

                    $this->row = 0;

                    $this->post_data_keys = array(
                        "id" => array(
                            "required" => false,
                            "type" => Form::TYPE_INT,
                            "extras" => array(
                            )
                        ),
                        "first_name" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "last_name" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "middle_initial" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 3
                            )
                        ),
                        "position" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 64
                            )
                        ),
                        "email" => array(
                            "required" => false, //Phase 2: FRD pg. 25
                            "type" => Form::TYPE_EMAIL,
                            "extras" => array()
                        ),
                        "mobile_no" => array(
                            "required" => true,
                            "type" => Form::TYPE_MOBILE,
                            "extras" => array(
                                "min" => 10,
                                "max" => 15
                            )
                        ),
                        "employee_no" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 32
                            )
                        ),
                         "gender" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 2
                            )
                        ),            
                        "blood_type" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 0,
                                "max" => 8
                            )
                        ),   
                        "address_street" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "address_line_2" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "address_city" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 64
                            )
                        ),
                        "address_state" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 64
                            )
                        ),
                        "address_zip_code" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 16
                            )
                        ),
                        "address_country" => array(
                            "required" => false,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 42
                            )
                        ),
                        "supervisor_id" => array(
                            "required" => false,
                            "type" => Form::TYPE_INT,
                            "extras" => array()
                        ),            
                        "is_supervisor" => array(
                            "required" => false,
                            "type" => Form::TYPE_BOOL,
                            "extras" => array()
                        ),            
                        "is_rescuer" => array(
                            "required" => false,
                            "type" => Form::TYPE_BOOL,
                            "extras" => array()
                        ),
                        "company" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "department" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        ),
                        "site" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        )
                    );

                    $rows_toupdate = count($csv_data);

                    $device_keys = array();

                    foreach ($csv_data as $data) {
                        ++$this->row;
                        $this->employee = $this->db->create("employees");
                        
                        if ($action == "add") {
                            $this->log_open(1,"insert","bulk upload");
                            if (isset($data["company"])) {
                                $this->bulk_company_id = $this->find_company($data["company"]);
                            }
                            $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) { 
                                if ($key != "id") {
                                    if ($key == "company") {
                                        $value = $this->find_company($value);
                                        $key = "company_id";
                                    } else if ($key == "department") {
                                        $value = $this->find_department($value, $this->bulk_company_id);
                                        $key = "department_id";
                                    } else if ($key == "site") {
                                        $value = $this->find_site($value, $this->bulk_company_id);
                                        $key = "site_id";
                                    }
                                    $this->employee->$key = utf8_encode(trim($value));
                                    $this->log_insert($key,$this->employee->$key);
                                }
                            }, function ($error_type,$key) {
                                $this->error_data .= "Unable to add record at row #$this->row: ";

                                if ($error_type == Form::ERROR_ID_NOT_SET) {
                                    $this->error_data .= " missing required field '$key'<br>\n";
                                } else {
                                    $this->error_data .= " could not validate '$key' field<br>\n";
                                }
                            });

                            if(!$this->_check_prefix($data["mobile_no"])) {
                                $error_count += 1;
                                $this->error_data .= "Unable to add row #" . $this->row . ": mobile number should be a Globe-issued number.<br/>";
                            }

                            if($data["gender"] == "M" || $data["gender"] == "F") {
                            } else {
                                $error_count +=1;
                                $this->error_data .= " Unable to add row # ". $this->row . " Gender value is invalid.<br/>";
                            }

                            if($this->check_blood_type($data["blood_type"]) == false) {
                                $error_count += 1;
                                $this->error_data .= " Unable to add row # ". $this->row . " Blood type value is invalid.<br/>";
                            }

                            if($this->find_company($data["company"]) == 0) {
                                $error_count += 1;
                                $this->error_data .= " Unable to add row # " . $this->row . " Company does not exists.<br/>";
                            }

                            if($this->find_department($data["department"],$this->bulk_company_id) == 0) {
                                $error_count += 1;
                                $this->error_data .= " Unable to add row # " . $this->row . " Department does not exists.<br/>";
                            }

                            if($this->find_site($data["site"],$this->bulk_company_id) == 0) {
                                $error_count += 1;
                                $this->error_data .= " Unable to add row # " . $this->row . " Site does not exists.<br/>";
                            }

                            if ($error_count == 0) {
                                $result = $this->db->query("SELECT id FROM employees WHERE is_deleted = 0 AND (email = " . $this->db->quote($this->employee->email) . " OR mobile_no = " . $this->db->quote($this->employee->mobile_no) . ") LIMIT 0,1");
                                if (count($result) > 0) {
                                    $this->error_data.= " Unable to add row #" . $this->row . ": record already exists<br>\n";
                                } else {
                                    $this->employee->date_created = $this->get_current_date();
                                    $this->employee->created_by = $this->get_current_user();
                                    $this->db->insert($this->employee);
                                    $rows_updated++;
                                }
                            } else {
                                $this->error_data .= " Unable to add record at row #$this->row. ";
                            }
                        } else if ($action == "update") {
                             if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"update","bulk upload");
                                if (isset($data["company"])) {
                                    $this->bulk_company_id = $this->find_company($data["company"]);
                                }
                                $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                    if ($key != "id") {
                                        if ($key == "company") {
                                            $value = $this->find_company($value);
                                            $key = "company_id";
                                        } else if ($key == "department") {
                                            $value = $this->find_department($value, $this->bulk_company_id);
                                            $key = "department_id";
                                        } else if ($key == "site") {
                                            $value = $this->find_site($value, $this->bulk_company_id);
                                            $key = "site_id";
                                        }
                                        $this->employee->$key = utf8_encode(trim($value));
                                        $this->log_changes($key,$this->employee->$key,$this->employee->$key);                                    
                                    }
                                }, function ($error_type,$key) {
                                    $this->error_data .= "Unable to update record at row #$this->row: ";
                                    if ($error_type == Form::ERROR_ID_NOT_SET) {
                                        $this->error_data .= " missing required field '$key'<br>\n";
                                    } else {
                                        $this->error_data .= " could not validate '$key' field<br>\n";
                                    }
                                });

                                if ($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM employees WHERE id = " . intval($data["id"]));
                                    if (count($result) > 0) {
                                        $this->employee->date_modified = $this->get_current_date();
                                        $this->employee->modified_by = $this->get_current_user();
                                        $this->db->update($this->employee,array("id" => $data["id"]));
                                        $rows_updated++;
                                    } else {
                                        $this->error_data.= "Unable to update row #" . $this->row . ": no matching record found for '$this->employee->id'<br>\n";                                      
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"delete","bulk upload");
                                $this->employee->is_deleted = 1;
                                $this->employee->date_deleted = $this->get_current_date();
                                $this->db->update($this->employee,array("id" => $data["id"]));
                                $this->log_close();
                                $rows_updated++;

                                $result = $this->db->query("SELECT os_type,device_key FROM devices WHERE owner=".$data["id"]." LIMIT 1");
                                if (count($result) > 0 && $result[0]->os_type == "2") {
                                    $device_keys[] = $result[0]->device_key;
                                }
                            } else {
                                $this->error_data.= "Unable to delete record at row #" . $this->row . " missing required field id<br>\n";
                            }
                        }
                    }

                    if ($action == "delete") {
                        $datetimestamp = date_create();
                        $message = array(
                                "is_user_deleted" => 1,
                                "timestamp" => date_format($datetimestamp, 'U')
                        );
                        $this->androidpushnotif->push($device_keys,$message);
                    }
                }
            }

            if ($rows_updated > 0 && ($rows_updated === $rows_toupdate)) {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            }
        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by                                         
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list.= "<tbody>";
                foreach ($result as $data) {
                    $upload_list.= "<tr>";
                    $upload_list.= "<td>$data->file_name</td>";
                    $upload_list.= "<td>$data->date_uploaded</td>";
                    $upload_list.= "<td>$data->section</td>";
                    $upload_list.= "<td>$data->action</td>";
                    $upload_list.= "<td>$data->uploaded_by</td>";
                    $upload_list.= "<td>$data->hash_value</td>";
                    $upload_list.= "</tr>";
                }
                $upload_list.= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by                                        
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list.= "<tr>";
                    $download_list.= "<td>$data->file_name</td>";
                    $download_list.= "<td>$data->date_created</td>";
                    $download_list.= "<td>$data->created_by</td>";
                    $download_list.= "<td>$data->hash_value</td>";
                    $download_list.= "</tr>";
                }
                $download_list.= "</tbody>";
            }

            $this->set("upload_list",$upload_list);
            $this->set("download_list",$download_list);

            $this->render("index", "whitelist/upload", "AGC Employee Locator | CMS");
        }
    }

    private function _get_company_department() {
        $response = create_response(1,"Error getting department data");

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            $company_id = 0;
            if (isset($data->{'company_id'})) {
                $company_id = $data->{'company_id'};
            }

            if ($company_id > 0) {                
                $result = $this->db->query("SELECT d.id,
                                                   d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id);

                if (count($result) > 0) {
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);
    }

    private function _get_supervisors() {
        $response = create_response(1,"Error getting supervisor data");

        $company_id = 0;

        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();
            if (isset($data->{'company_id'})) {
                $company_id = $data->{'company_id'};
            }

            if ($company_id > 0) {
                $result = $this->db->query("SELECT id,CONCAT(first_name,' ',last_name) AS full_name 
                                            FROM employees 
                                            WHERE is_supervisor = 1 AND company_id = $company_id AND is_deleted <> 1");                
                if (count($result) > 0) {
                    foreach ($result as $key=>$val) {
                        $result[$key]->full_name = utf8_encode($result[$key]->full_name);
                    }
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);        
    }

    private function _get_sites() {
        $response = create_response(1, "Error getting sites");

        $company_id = 0;

        $request = $this->input->get_data(Input::STREAM);

        if($request->isValid()) {
            $data = $request->get_data();
            if(isset($data->{'company_id'})) {
                $company_id = $data->{'company_id'};
            }

            if($company_id > 0) {
                $result = $this->db->query("SELECT id, company_id, site FROM company_sites
                              WHERE company_id = $company_id");

                if(count($result) > 0) {
                    $response = create_response(0, "Success");
                    $response->data = $result;
                }
            }
        }

        notify($response);
    }

    private function find_company($company) {
        $companies = $this->db->create("companies");
        $companies->id = 0;
        $company_id = 0;
        $result = $this->db->query($companies,array("company_name" => $company));
        if (count($result) > 0) {
            $company_id = $result[0]->id;
        }
        return $company_id;
    }

    private function find_department($department,$company_id) {
        $department_id = 0;
        $result = $this->db->query("SELECT d.id FROM company_department cd 
                                            LEFT JOIN department d ON cd.department_id = d.id 
                                            WHERE department_name = '$department' AND company_id = $company_id");
        if (count($result) > 0) {
            $department_id = $result[0]->id;
        }

        return $department_id;
    }

    private function find_site($site,$company_id) {
        $sites = $this->db->create("company_sites");
        $sites->id = 0;
        $site_id = 0;
        $result = $this->db->query($sites,array("site" => $site,"company_id" => $company_id));
        if (count($result) > 0) {
            $site_id = $result[0]->id;
        }

        return $site_id;
    }

    private function _check_prefix($data) {
        $mobile_no = $data;
        $result = $this->checking($mobile_no);

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    private function checking($passed_data) {

        $this->prefixes = $this->db->query("
                    SELECT number_range, start_msisdn,
                        end_msisdn, brand
                        FROM prefixes
                    ");
        $mobile_no = $passed_data;
        if($passed_data[0] == 0) {
            $mobile_no = substr($passed_data, 1);
        }
        
        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;

        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }

    private function check_blood_type($type) {
        $valid = false;
        $result = $this->db->query("SELECT description FROM blood_types ORDER BY description");

        foreach($result as $data):
            if($data->description == $type):
                $valid = true;
            endif;
        endforeach;

        return $valid;
    }


}