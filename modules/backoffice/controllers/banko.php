<?php

class Banko extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/banko", "BanKo");

        $this->load->library(array("uri","input","util","hash","alert","html","form","upload","page","helper"));

        $this->post_data_keys = array(
            "account_no" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 10,
                    "max" => 25
                )
            ),
            "employee_id" => array(
                "required" => true,
                "type" => Form::TYPE_INT,
                "extras" => array()
            )
        );
    }

    public function request_handler($request_method, $request_params) {
        //$this->checkpoint("/backoffice/banko/" . $request_method);

        if($request_method == "add") {
            $this->_add();
        } else if($request_method == "edit") {
            $this->_edit($request_params[1]);
        } else if($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if($request_method == "check-if-existing") {
            $this->_check_if_existing();
        } else if($request_method == "check-if-existing-edit") {
            $this->_check_if_existing_edit();
        } else if($request_method == "check-if-already-added") {
            $this->_check_if_already_added();
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list(isset($request_params[1]) ? $request_params[1] : 1);
            }
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());
        $search_by = "";

        if(isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if(ctype_digit($text)) {
                $search_id = "OR e.id = $text";
            }
            $search_by = "AND (c.company_name LIKE '%$text%' $search_id OR CONCAT(TRIM(e.first_name),' ',TRIM(e.last_name)) LIKE '%$text%' OR b.account_no LIKE '%$text%')";
        }

        $total = $this->get_total("SELECT COUNT(b.id) as total
                                FROM banko as b INNER JOIN employees as e
                                ON b.employee_id = e.id
                                INNER JOIN companies as c
                                ON e.company_id = c.id
                                WHERE b.is_deleted <> 1 $search_by");

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT b.id as id, CONCAT(e.first_name, ' ', e.last_name) as full_name, c.company_name as company, b.account_no as account_no, b.date_created as created
                                FROM banko as b INNER JOIN employees as e
                                ON b.employee_id = e.id
                                INNER JOIN companies as c
                                ON e.company_id = c.id
                                WHERE b.is_deleted <> 1 $search_by
                                ORDER BY e.first_name, e.last_name ASC LIMIT $offset, $limit");

        $this->send($total, $result);
    }

    private function _list($page) {
        $this->render("index","donate/banko/list","AGC Employee Locator | CMS");
    }

    private function _add() {
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $error_message = "";

            $this->banko = $this->db->create("banko");
            $this->banko->date_created = $this->get_current_date();
            $this->banko->created_by = $this->session->userdata("user_id");

            $this->log_open(1, "insert", "banko");
            $error_count = 0;

            $error_count = $this->form->validate($this->post_data_keys, $post_data, function ($key, $value){
                if (in_array($key,array("employee_id"))) {
                    $this->banko->$key = $value;
                } else {
                    $this->banko->$key = $value;
                    $this->log_insert($key, $value);
                }
            }, function($error_type, $key) {
            });

            $error = true;

            if($error_count == 0) {
                $this->log_close();
                $this->db->insert($this->banko);
                $error = false;
            } else {
                $error = true;
                $error_message = "Error in adding banko account";
            }

            if ($error) {
                $this->log_close();
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            }

        } else {
            $company_list = "";
            $department_list = "";
            $company = $this->db->create("companies");
            $company->id = 0;
            $company->company_name = "";
            $company->setOrderBy("company_name","ASC");

            $company_id = 0;

            $company_filter = array("is_deleted" => "0");
            if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
                $company_id = $this->session->userdata("company_id");
                $company_filter=array("id" => $company_id,"is_deleted" => "0");
            } else {
                $company_list = "<option value=\"0\">Ayala Group of Companies</option>";
                $department_list = "<option value=\"0\">All Department</option>";
            }

            $result = $this->db->query($company,$company_filter);
            if (count($result) > 0) {
                foreach ($result as $company):
                    $company_list .= $this->html->option($company->company_name,$company->id);
                endforeach;
                $company_id = $result[0]->id;
            }

            $this->set("company_list",$company_list);

            if ($company_id > 0) {
                $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                            LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                            LEFT JOIN companies AS c ON c.id = cd.company_id
                                            WHERE c.id = " . $company_id . " AND c.is_deleted <> 1 ORDER BY d.department_name ASC");

                if (count($result) > 0) {
                    foreach ($result as $item):
                        $department_list .= $this->html->option($item->department_name,$item->id);
                    endforeach;
                }
            }

            $this->set("department_list",$department_list);

            $employee_list = "";
            $result = $this->db->query("SELECT id, CONCAT(first_name, ' ', last_name) as employee_name FROM employees WHERE company_id = $company_id AND is_deleted <> 1 ORDER BY first_name ASC");

            if(count($result) > 0) {
                foreach ($result as $data) :
                    $employee_list .= $this->html->option($item->employee_name, $item->id);
                endforeach;
            }

            $this->render("index","donate/banko/add","AGC Employee Locator | CMS");
        }
    }

    private function _edit($id) {
        $error = true;
        if($this->input->request_method("POST")) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $this->banko = $this->db->create("banko");
            $this->banko->date_modified = $this->get_current_date();
            $this->banko->modified_by = $this->session->userdata("user_id");

            $this->original_banko = $this->db->create("banko");
            foreach($this->post_data_keys as $key => $data):
                $this->original_banko->$key = "";
            endforeach;

            $result = $this->db->query($this->original_banko, array("id" => $id));
            if(count($result) > 0) {
                $this->original_banko = $result[0];
            }

            $this->log_open(1, "update", "banko");

            $error_count = $this->form->validate($this->post_data_keys, $post_data, function($key, $value) {
                $this->banko->$key = trim($value);
                if($this->banko->$key != $this->original_banko->$key) {
                    $this->log_changes($key, $this->original_banko->$key, $this->banko->$key);
                }
            },function($error_type,$key) {
            });

            if($error_count == 0) {
                $this->log_close();

                $this->db->update($this->banko, array("id" => $id));
                print json_encode(array("response_code" => 0,"response_msg" => "Data updated successfully!"));
            } else {
                print json_encode(array("response_code" => 1, "response_msg" => "Error updating data!"));
            }

        } else {
            if(ctype_digit($id)) {
                $banko = $this->db->create("banko");
                $post_keys = array("id",
                            "company_id",
                            "employee_id",
                            "department_id",
                            "account_no");

                foreach($post_keys as $key):
                    $banko->$key = "";
                endforeach;

                $result = $this->db->query("SELECT distinct b.id AS id, c.id as company_id, e.id as employee_id, b.account_no as account_no, d.department_id as department_id
                            from banko as b Inner join employees as e
                            ON b.employee_id = e.id
                            inner join companies as c
                            on e.company_id = c.id
                            INNER JOIN company_department as d
                            ON d.company_id	= c.id
                            where b.is_deleted <> 1");

                if(count($result) > 0 && $result[0]->id) {
                    $banko = $result[0];

                    foreach($post_keys as $key):
                        $this->set($key, $banko->$key);
                    endforeach;

                    $company_list = "";
                    $department_list = "";
                    $company_filter = array("is_deleted" => "0");

                    $company_list .= "<option value=\"\">Select Company</option>";
                    $department_list = "<option value=\"\">Select Department</option>";

                    $company = $this->db->create("companies");
                    $company->id = 0;
                    $company->company_name = "";
                    $company->setOrderBy("company_name", "ASC");

                    $result = $this->db->query($company, $company_filter);
                    if(count($result) > 0) {
                        $company_list .= $this->html->option("Ayala Group of Companies",0,true);
                        foreach ($result as $company) {
                            if ($company->id == $banko->company_id) {
                                $company_list .= $this->html->option($company->company_name,$company->id,true);
                            } else {
                                $company_list .= $this->html->option($company->company_name,$company->id);
                            }
                        }
                    }

                    $this->set("company_list", $company_list);

                    if($company->id > 0) {
                        $result = $this->db->query("SELECT d.id,d.department_name FROM department AS d
                                                    LEFT JOIN company_department AS cd ON cd.department_id = d.id
                                                    LEFT JOIN companies AS c ON c.id = cd.company_id
                                                    WHERE c.id = " . $banko->company_id . " AND d.is_deleted <> 1 ORDER BY d.department_name ASC");
                        if (count($result) > 0) {
                            foreach ($result as $item) {
                                if ($item->id == $banko->department_id) {
                                    $department_list .= $this->html->option($item->department_name,$item->id,true);
                                } else {
                                    $department_list .= $this->html->option($item->department_name,$item->id);
                                }
                            }
                        }
                    }

                    $this->set("department_list", $department_list);

                    $employee_list = "";
                    $result = $this->db->query("SELECT id, CONCAT(first_name, ' ', last_name) as employee_name FROM employees WHERE company_id = $banko->company_id AND is_deleted <> 1 ORDER BY first_name ASC");

                    if(count($result) > 0) {
                        foreach($result as $item):
                            if($banko->employee_id == $item->id) {
                                $employee_list .= $this->html->option($item->employee_name, $item->id, true);
                            } else {
                                $employee_list .= $this->html->option($item->employee_name, $item->id);
                            }
                        endforeach;
                    }

                    $this->set("employee_list", $employee_list);
                    $this->set("account_no", $banko->account_no);

                    $this->render("index","donate/banko/edit","AGC Employee Locator | CMS");
                }
            }
        }
    }

    private function _delete($id) {
        $error = true;

        if(ctype_digit($id)) {
            $banko = $this->db->create("banko");
            $banko->date_deleted = $this->get_current_date();
            $banko->deleted_by = $this->session->userdata("user_id");
            $banko->is_deleted = 1;

            $this->log_open(1, "delete", "banko");

            $this->db->update($banko, array("id" => $id));
            $error = false;
        }

        if ($error) {
            $this->log_close();
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
    }

    private function find_company($company) {
        $companies = $this->db->create("companies");
        $companies->id = 0;
        $company_id = 0;
        $result = $this->db->query($companies,array("company_name" => $company));
        if (count($result) > 0) {
            $company_id = $result[0]->id;
        }
        return $company_id;
    }

    private function find_department($department) {
        $departments = $this->db->create("department");
        $departments->id = 0;
        $department_id = 0;
        $result = $this->db->query($departments,array("department_name" => $department));
        if (count($result) > 0) {
            $department_id = $result[0]->id;
        }

        return $department_id;
    }

    private function _check_if_existing() {
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            $account_no = 0;
            if(isset($data->{'data'})) {
                $account_no = $data->{'data'};
            }

            $result = $this->_check_all_accounts($account_no);

            if($result > 0) {
                echo "none";
            } else {
                echo "ok";
            }
        }
    }

    private function _check_if_existing_edit() {
        $request = $this->input->get_data(Input::STREAM);
        if ($request->isValid()) {
            $data = $request->get_data();

            $account_no = 0;
            if(isset($data->{'data'})) {
                $account_no = $data->{'data'};
                $employee_id = $data->{'employee_id'};
            }

            $result = $this->_check_all_accounts($account_no);

            if($result > 0) {
                if($result != $employee_id) {
                    echo "none";
                } else {
                    echo "ok";
                }
            } else {
                if(!$this->_check_all_accounts_edit($account_no, $employee_id)) {
                    echo "ok";
                } else {
                    echo "none";
                }
            }
        }
    }

    private function _check_all_accounts($account_no) {
        $banko = $this->db->create("banko");
        $banko->id = 0;
        $banko->employee_id = 0;
        $id = 0;
        $result = $this->db->query($banko, array("account_no" => $account_no));
        if(count($result) > 0) {
            $id = $result[0]->employee_id;
        }

        return $id;
    }

    private function _check_all_accounts_edit($account_no, $employee_id) {
        $banko = $this->db->create("banko");
        $banko->id = 0;
        $banko->account_no = 0;
        $is_not = false;

        $result = $this->db->query($banko, array("account_no" => $account_no));

        if(count($result) > 0) {
            if($result[0]->employee_id != $employee_id) {
                $is_not = true;
            }
        }

        return $is_not;
    }

    private function _check_if_already_added() {
        $request = $this->input->get_data(Input::STREAM);
        if($request->isValid()) {
            $data = $request->get_data();

            $employee_id = 0;
            if(isset($data->{'data'})){
                $employee_id = $data->{'data'};
            }

            $result = $this->_check_added_accounts($employee_id);

            if($result > 0) {
                echo "none";
            } else {
                echo "ok";
            }
        }
    }

    private function _check_added_accounts($employee_id) {
        $banko = $this->db->create("banko");
        $banko->id = 0;
        $banko_id = 0;
        $result = $this->db->query($banko, array("employee_id" => $employee_id));
        if(count($result) > 0) {
            $banko_id = $result[0]->id;
        }

        return $banko_id;
    }
}