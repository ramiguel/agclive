<?php

class Maintenance extends CMS_Controller {

    public function __construct() {
        parent::__construct("/backoffice/maintenance", "Report Maintenance");

        $this->load->library(array("uri","input","form","html","alert", "upload"));

        $this->post_data_keys = array(
            "name" => array(
                "required" => true,
                "type" => Form:: TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 256
                )
            ),
            "sql_statement" => array(
                "required" => true,
                "type" => Form:: TYPE_TEXT,
                "extras" => array(
                    "min" => 1
                )
            ),
            "headers" => array(
                "required" => true,
                "type" => Form:: TYPE_TEXT,
                "extras" => array(
                    "min" => 1
                )
            )
        );
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "add") {
            $this->_add();
        } else if($request_method == "delete") {
            $this->_delete($request_params[1] !== null ? $request_params[1] : 0);
        } else if($request_method == "edit") {
            $this->_edit($request_params[1] !== null ? $request_params[1] : 0);
        } else if($request_method == "view") {
            $this->_view($request_params[1] !== null ? $request_params[1] : 0);
        } else if($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
            $this->_list_data(1);
        } else {
            $this->_list();
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if(isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR r.id = " . $text;
            }
            $search_by = "AND (r.name LIKE '%$text%' OR r.headers LIKE '%$text%' OR r.sql_statement LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT count(r.id) as total
                            FROM reports as r
                            WHERE r.is_active = 1 AND r.is_deleted <> 1 $search_by");

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT r.id, r.name, r.sql_statement, r.headers,
                                    IF(r.is_active = 1, 'Yes', 'No') AS is_active
                                    FROM reports as r WHERE r.id <> 0 AND r.is_deleted <> 1 $search_by
                                    LIMIT $offset, $limit");

        $this->send($total, $result);

    }

    private function _list() {
        $this->render("index","reports/maintenance","AGC Employee Locator | CMS");
    }

    private function _add() {
        if($this->input->request_method('POST')) {
            $error_count = 0;
            $error_type = 0;
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $result = false;

            $this->report = $this->db->create("reports");
            $this->report->is_active = 1;
            $error_message = "Error adding record";

            $this->report->name = $post_data["name"];
            $this->report->sql_statement = $post_data["sql_statement"];
            $this->report->headers = $post_data["headers"];
            $this->report->created_by = $this->get_current_user();
            $this->report->date_created = $this->get_current_date();

            $exp = explode(" ", $post_data["sql_statement"]);
            if((in_array("SELECT", $exp) || in_array(strtolower("select"), strtolower($exp)))
                && ($exp[0] == "SELECT" || strtolower($exp[0]) == strtolower("select"))) {

                $is_valid = false;

                try {
                    $res = $this->db->query($this->report->sql_statement);
                    if($res) {
                        $is_valid = true;
                    } else {
                        $is_valid = false;
                    }
                } catch(Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                    $is_valid = false;
                }

                if($is_valid) {
                    $result = $this->db->insert($this->report);
                    $error_count += 0;
                    $error_type = 0;
                } else {
                    $error_count += 1;
                    $error_type = 1;
                }

            } else {
                $error_count += 1;
                $error_type = 1;
            }

            if($error_count == 0) {
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully."));
            } else {
                if($error_type = 1) {
                    $error_message = "Please check your SQL statement.";
                }
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            }

        } else {
            $this->render("index","reports/add","AGC Employee Locator | CMS");
        }
    }

    private function _delete($id) {
        $error = true;
        if(ctype_digit($id)) {
            $report = $this->db->create("reports");
            $report->date_deleted = $this->get_current_date();
            $report->deleted_by = $this->get_current_user();
            $report->is_deleted = 1;
            $report->is_active = 0;

            $this->db->update($report, array("id" => $id));
            $error = false;
        }

        if($error == false) {
            print json_encode(array("response_code" => 0, "response_msg" => "Record successfully deleted."));
        } else {
            print json_encode(array("response_code" => 1, "response_msg" => "Error encountered!"));
        }
    }

    private function _edit($id) {
        if(ctype_digit($id)) {

            if($this->input->request_method("POST")) {
                $error_count = 0;
                $error_type = 0;
                $post_data = $this->input->get_data(Input::POST)->get_data();

                $this->report = $this->db->create("reports");
                $this->report->date_modified = $this->get_current_date();
                $this->report->modified_by = $this->get_current_user();
                $error_message = "Error adding record";

                $this->report->name = $post_data["name"];
                $this->report->sql_statement = $post_data["sql_statement"];
                $this->report->headers = $post_data["headers"];

                $result = false;

                $exp = explode(" ", $post_data["sql_statement"]);
                if((in_array("SELECT", $exp) || in_array(strtolower("select"), strtolower($exp)))
                    && ($exp[0] == "SELECT" || strtolower($exp[0]) == strtolower("select"))) {

                    $is_valid = false;

                    try {
                        $res = $this->db->query($this->report->sql_statement);
                        if($res) {
                            $is_valid = true;
                        } else {
                            $is_valid = false;
                        }
                    } catch(Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                        $is_valid = false;
                    }

                    if($is_valid) {
                        $result = $this->db->update($this->report,array("id"=>$id));
                        $error_count += 0;
                        $error_type = 0;
                    } else {
                        $error_count += 1;
                        $error_type = 1;
                    }

                } else {
                    $error_count += 1;
                    $error_type = 1;
                }

                if($error_count == 0) {
                    print json_encode(array("response_code" => 0,"response_msg" => "Record modified successfully."));
                } else {
                    if($error_type = 1) {
                        $error_message = "Please check your SQL statement.";
                    }
                    print json_encode(array("response_code" => 1,"response_msg" => $error_message));
                }


            } else {
                $report = $this->db->create("reports");
                $report->id = 0;
                $report->name = "";
                $report->sql_statement = "";
                $report->headers = "";

                $result = $this->db->query($report, array("id" => $id));
                if(count($result) > 0) {
                    $report = $result[0];
                }

                foreach((array)$report as $key => $val):
                    $this->set($key, $val);
                endforeach;

                $this->render("index","reports/edit","AGC Employee Locator | CMS");
            }

        }
    }

    private function _view($id) {
        $error = true;
        if(ctype_digit($id)) {
            $report = $this->db->create("reports");
            $post_keys = array("id",
                            "name",
                            "sql_statement",
                            "headers");

            foreach($post_keys as $keys):
                $report->$keys = "";
            endforeach;

            $result = $this->db->query($report, array("id" => $id));

            if(count($result) > 0 && $result[0]->id) {
                $report = $result[0];
                foreach((array)$report as $key => $val):
                    $this->set($key, $val);
                endforeach;
            }

            $this->render("index","reports/view","AGC Employee Locator | CMS");
        }
    }
}
