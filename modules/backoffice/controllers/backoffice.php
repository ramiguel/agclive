<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Backoffice extends Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect(base_url() . "/backoffice/home");
    }
}