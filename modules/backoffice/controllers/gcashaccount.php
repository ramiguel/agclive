<?php

class GcashAccount extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/gcashaccount", "GCash Account");

        $this->load->library(array("uri","input","util","hash","alert","html","form","upload","page","helper"));

        $this->post_data_keys = array(
            "gcash_account" => array(
                "required" => true,
                "type" => Form::TYPE_MOBILE,
                "extras" => array(
                    "min" => 11,
                    "max" => 11
                )
            )
        );

        $this->prefixes = null;
    }

    public function request_handler($request_method, $request_params) {
        if($request_method == "edit") {
            $this->_edit();
        } else {
            $this->_view();
        }
    }

    private function _edit() {
        $error = true;
        if($this->input->request_method("POST")) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $result = $this->db->query("SELECT id, gcash_account FROM gcash_account LIMIT 1");

            if(count($result) > 0) {
                $this->gcash = $this->db->create("gcash_account");
                $this->gcash->date_modified = $this->get_current_date();
                $this->gcash->modified_by = $this->get_current_user();

                $this->log_open(1, "update", "gcash");

            } else {
                $this->gcash = $this->db->create("gcash_account");
                $this->gcash->date_created = $this->get_current_date();
                $this->gcash->created_by = $this->get_current_user();

                $this->log_open(1, "create", "gcash");
            }

            $error_count = $this->form->validate($this->post_data_keys, $post_data, function($key, $value) {
                $this->gcash->$key = trim($value);
            },function($error_type,$key) {

            });

            if($error_count == 0) {
                $this->log_close();
                if(count($result) > 0) {
                    $this->db->update($this->gcash);
                } else {
                    $this->db->insert($this->gcash);
                }
                print json_encode(array("response_code" => 0,"response_msg" => "Data updated successfully!"));
            } else {
                print json_encode(array("response_code" => 1, "response_msg" => "Error updating data!"));
            }

        } else {
            $result = $this->db->query("SELECT id, gcash_account FROM gcash_account LIMIT 1");

            if(count($result) > 0) {
                $this->set("gcash_account", $result[0]->{'gcash_account'});
            } else {
                $this->set("gcash_account", "");
            }

            $this->render("index","donate/edit","AGC Employee Locator | CMS");
        }
    }

    private function _view() {
        $result = $this->db->query("SELECT id, gcash_account FROM gcash_account LIMIT 1");

        if(count($result) > 0) {
            $this->set("gcash_account", $result[0]->{'gcash_account'});
        } else {
            $this->set("gcash_account", "");
        }

        $this->render("index","donate/gcash","AGC Employee Locator | CMS");
    }
}