<?php

class Site extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/site", "Site");

        $this->load->library(array("uri","input","util","hash","alert","html","form","upload","page","helper"));
    }

    public function request_handler($request_method, $request_params) {

        if($request_method == "edit") {
            $this->_edit($request_params[1]);
        } else if($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "export") {
            $this->_export_data(isset($request_params[1]) ? $request_params[1] : 0);
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list(isset($request_params[1]) ? $request_params[1] : 1);
            }
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        if(isset($data->{'company_id'}) && $data->{'company_id'} > 0) {
            $company_id = $data->{'company_id'};

            $search_by = "";

            $site_id = "id";

            $search_by = "";

            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'};
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR id = " . $text;
                }
                $search_by = "AND (company_name LIKE '%$text%' $search_id)";
            }

            $total = $this->get_total("SELECT COUNT(id) AS total FROM company_sites
                                   WHERE id <> 0 AND company_id = $company_id $search_by");

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;
            $result = $this->db->query("SELECT cs.id as id, c.company_name as company, cs.site as site
                                    FROM companies as c
                                    INNER JOIN company_sites as cs
                                    ON c.id = cs.company_id
                                    WHERE c.is_deleted <> 1 AND company_id = $company_id
                                    ORDER BY c.company_name ASC LIMIT $offset,$limit");

        } else {
            $search_by = "";

            if (isset($data->{'search'}) && !empty($data->{'search'})) {
                $text = $data->{'search'};
                $search_id = "";
                if (ctype_digit($text)) {
                    $search_id = " OR id = " . $text;
                }
                $search_by = "AND (company_name LIKE '%$text%' $search_id)";
            }

            $total = $this->get_total("SELECT COUNT(id) AS total FROM company_sites
                                   WHERE id <> 0 $search_by");

            $limit = $data->{'limit'};

            $offset = ($limit * $data->{'offset'}) / $limit;
            $result = $this->db->query("SELECT cs.id as id, c.company_name as company, cs.site as site
                                    FROM companies as c
                                    INNER JOIN company_sites as cs
                                    ON c.id = cs.company_id
                                    WHERE c.is_deleted <> 1
                                    ORDER BY c.company_name ASC LIMIT $offset,$limit");

        }


        $this->send($total,$result);
    }

    private function _list($page) {
        $result = array();
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
            $company_id = $this->session->userdata("company_id");
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 AND id = $company_id ORDER BY company_name ASC");
        } else {
            $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
        }
        $company_list = "";
        foreach ($result as $data) {
            $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
        }

        $this->set("company_list",$company_list);

        $this->render("index","site/list","AGC Employee Locator | CMS");
    }

    private function _delete($id) {
        $response = $this->create_client_response(1, "Company site deletion failed!");

        if(ctype_digit($id)) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $response->error(json_encode($post_data));

            if($id !== null &&  (is_int($id) || ctype_digit($id))) {
                $sql = "DELETE FROM company_sites WHERE id = '" . $id ."'";
                $this->db->update($sql);
                $response->success("Company site deleted successfully!");
            } else {
                $response->error("Supplied id is invalid");
            }
        }

        $response->notify();
    }

    private function _edit($id) {
        $company_id = $this->_get_company_id($id);
        redirect(base_url() . "/backoffice/company/edit/".$company_id);
    }

    private function _bulk_upload() {
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $company_selected = $post_data["company_filter"];

            if ($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));

                $file_path = BASEPATH . "assets/uploads/bulk/";
                //$file_path = BASEPATH . "bulk/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if ($this->upload->do_upload("file",$config)) {
                    $file_name = $this->upload->get_uploaded_filename();

                    $csv_data = $this->helper->get_csv($file_path.$file_name,false);

                    $action  = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "company_sites";

                    $this->db->insert($file);

                    $this->row = 0;

                    $this->post_data_keys = array(
                        "site" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 128
                            )
                        )
                    );

                    foreach ($csv_data as $data) {
                        ++$this->row;
                        $this->sites = $this->db->create("company_sites");

                        if ($action == "add") {
                            $this->log_open(1,"insert","bulk upload");
                            $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                if ($key != "id") {
                                    $this->sites->$key = utf8_encode($value);
                                    $this->log_insert($key,$this->sites->$key);
                                }
                            }, function ($error_type,$key) {
                                $this->error_data .= "Insert at row #" . $this->row . ": " . $error_type . " " . $key . "<br>\n";
                            });

                            if ($error_count == 0) {
                                $site_seek = $this->db->create("company_sites");
                                $site_seek->id = 0;
                                $result = $this->db->query($site_seek,array("site"=>$this->sites->site, "company_id" => $company_selected));

                                if (count($result) > 0) {
                                    $this->error_data.= "Cannot insert data at row #" . $this->row . ": Record already exists<br>\n";
                                } else {
                                    $this->sites->date_created = $this->get_current_date();
                                    $this->sites->created_by = $this->get_current_user();
                                    $this->sites->company_id = $company_selected;
                                    $this->db->insert($this->sites);
                                }
                            }
                        } else if ($action == "update") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"update","bulk upload");
                                $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                    if ($key != "id") {
                                        $this->sites->$key = utf8_encode($value);
                                        $this->log_changes($key,$this->sites->$key,$this->sites->$key);
                                    }
                                }, function ($error_type,$key) {
                                    $this->error_data .= "Update at row #". $this->row . ": " . $error_type . " " . $key . "<br>\n";
                                });

                                if ($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM company_sites WHERE id = " . intval($data["id"]));
                                    if (count($result) > 0) {
                                        $this->db->update($this->sites,array("id" => $data["id"]));
                                    } else {
                                        $this->error_data .= "Cannot update at row #" . $this->row . ": Record not found<br>\n";
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $result = $this->db->query("SELECT id FROM company_sites WHERE id = " . intval($data["id"]) . "' and company_id = " . intval($company_selected));
                                if (count($result) > 0) {
                                    $result = $this->db->delete("DELETE FROM company_sites WHERE id = '" . intval($data["id"]) . "' and company_id = " . intval($company_selected));

                                } else {
                                    $this->error_data.= "Cannot delete row #" . $this->row . ": ID not defined<br>\n";
                                }
                            } else {
                                $this->error_data.= "Cannot delete row #" . $this->row . ": ID not defined<br>\n";
                            }
                        }
                    }
                }
            }


            if (!empty($this->error_data)) {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            }
        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list.= "<tbody>";
                foreach ($result as $data) {
                    $upload_list.= "<tr>";
                    $upload_list.= "<td>$data->file_name</td>";
                    $upload_list.= "<td>$data->date_uploaded</td>";
                    $upload_list.= "<td>$data->section</td>";
                    $upload_list.= "<td>$data->action</td>";
                    $upload_list.= "<td>$data->uploaded_by</td>";
                    $upload_list.= "<td>$data->hash_value</td>";
                    $upload_list.= "</tr>";
                }
                $upload_list.= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list.= "<tr>";
                    $download_list.= "<td>$data->file_name</td>";
                    $download_list.= "<td>$data->date_created</td>";
                    $download_list.= "<td>$data->created_by</td>";
                    $download_list.= "<td>$data->hash_value</td>";
                    $download_list.= "</tr>";
                }
                $download_list.= "</tbody>";
            }

            $this->set("upload_list",$upload_list);
            $this->set("download_list",$download_list);

            $result = array();
            if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR && $this->session->userdata("company_id") > 0) {
                $company_id = $this->session->userdata("company_id");
                $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 AND id = $company_id ORDER BY company_name ASC");
            } else {
                $result = $this->db->query("SELECT id,company_name FROM companies WHERE is_deleted <> 1 ORDER BY company_name ASC");
            }
            $company_list = "";
            foreach ($result as $data) {
                $company_list .= "<option value=\"$data->id\">$data->company_name</option>";
            }

            $this->set("company_list",$company_list);

            $this->render("index", "site/upload", "AGC Employee Locator | CMS");
        }
    }

    private function _export_data($id) {

        if($id == 0) {
            $sql = "SELECT id, site FROM company_sites";
        } else {
            $sql = "SELECT id, site FROM company_sites WHERE company_id = $id";
        }

        $headers = array("id",
            "site");

        $this->export_data("site-",$headers, $sql);
    }

    private function _get_company_id($id) {
        $result = $this->db->query("SELECT company_id FROM company_sites WHERE id = '$id'");

        return $result[0]->company_id;
    }
}