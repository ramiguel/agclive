<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Disaster extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/disaster","Disaster Maintenance");

        $this->load->library(array("html","form","input","util","hash","alert"));
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/disaster/" . $request_method);

        if ($request_method == "notifications") {
            $request = isset($request_params[1]) ? $request_params[1] : "";
            if ($request == "list" && isset($request_params[2]) && $request_params[2] == "data") {
                $this->_notifications_list_data(1);
            } else {
                $this->_list_notifications(isset($request_params[1]) ? $request_params[1] : 1);
            }
        } else if ($request_method == "notification-toggle") {
            $this->_notification_toggle($request_params[1]);
        } else if ($request_method == "incidents") {
            $request = isset($request_params[1]) ? $request_params[1] : "";
            if ($request == "list" && isset($request_params[2]) && $request_params[2] == "data") {
                $this->_incidents_list_data(1);
            } else if ($request == "view" && isset($request_params[2]) && ctype_digit($request_params[2])) { 
                $this->_incidents_view($request_params[2]);
            } else {
                $this->_list_incidents(isset($request_params[1]) ? $request_params[1] : 1);
            }
        } else if ($request_method == "reportins") {
            $request = isset($request_params[1]) ? $request_params[1] : "";
            if ($request == "list" && isset($request_params[2]) && $request_params[2] == "data") {
                $this->_report_ins_list_data(1);
            } else if ($request == "view" && isset($request_params[2]) && ctype_digit($request_params[2])) { 
                $this->_reportins_view($request_params[2]);
            } else {
                $this->_list_report_ins(isset($request_params[1]) ? $request_params[1] : 1);
            }            
        }
    }

    private function _notification_toggle($id) {
        $result = $this->db->query("SELECT id,active FROM notification WHERE id = " . $id);
        $notification = $this->db->create("notification");
        $notification->active = 0;
        if (count($result) > 0) {
            $notification->active = $result[0]->active == 1 ? 0 : 1;
        }
        $this->db->update($notification,array("id" => $id));

        redirect(base_url() . "/backoffice/disaster/notifications");
    }

    private function _notifications_list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());
        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR n.id = " . $text;
            } 
            $search_by = "WHERE u.username LIKE '%$text%' OR n.message LIKE '%$text%' $search_id";
        } 

        $total = $this->get_total("SELECT COUNT(n.id) AS total FROM broadcasts AS n
                                   INNER JOIN users AS u ON u.id = n.user_id $search_by");
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT n.id,CONCAT(u.username,' - ',n.name) AS broadcast_by,
                                    n.date_created AS broadcast_date,
                                    n.message,
                                    n.status,
                                    n.active,
                                    IF(n.active = 1 AND DATE(n.date_created) = DATE(CONVERT_TZ(NOW(),@@session.time_zone,'+8:00')),'icon-toggle-red','') AS icon,
                                    comp.company_name,
                                    cs.site
                                    FROM broadcasts AS n
                                    INNER JOIN users AS u ON u.id = n.user_id
                                    LEFT JOIN broadcast_sites as bs ON bs.broadcast_id = n.id
                                    LEFT JOIN companies as comp ON bs.company_id = comp.id
                                    LEFT JOIN company_sites AS cs ON bs.site_id = cs.id $search_by
                                    ORDER BY n.date_created DESC LIMIT $offset,$limit");

        foreach ($result as $index => $data) {
            $result[$index]->message = strip_tags(htmlspecialchars($data->message));
            if($data->company_name == "" || $data->company_name == NULL) {
                $data->company_name = "All";
            }

            if($data->site == "" || $data->site == NULL) {
                $data->site = "All";
            }
        }        

        $this->send($total,$result);
    }

    private function _list_notifications($page) {
        $this->render("index","disaster/notifications","AGC Employee Locator | CMS");
    }

    private function _incidents_list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());
        $search_by = "";
        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR n.id = " . $text;
            } 
            $search_by = "WHERE (e.first_name LIKE '%$text%' OR  e.last_name LIKE '%$text%' OR i.location LIKE '%$text%' OR i.message LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT COUNT(i.id) AS total FROM report_incidents AS i
                                   INNER JOIN employees AS e ON e.id = i.user_id $search_by");
        $limit = $data->{'limit'};
        
        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT i.id,CONCAT(e.first_name,' ',e.last_name) AS full_name,i.latitude,i.longitude,i.location,i.message,i.date_reported 
                                    FROM report_incidents AS i
                                    INNER JOIN employees AS e ON e.id = i.user_id $search_by 
                                    ORDER BY i.date_reported DESC LIMIT $offset,$limit");

        foreach ($result as $index => $data) {
            $result[$index]->location = strip_tags(htmlspecialchars($data->location));
            $result[$index]->message = strip_tags(htmlspecialchars($data->message));
        }

        $this->send($total,$result);
    }

    private function _incidents_view($id) {
        $post_data_keys = array("id","full_name","latitude","longitude","location","message","date_reported");
        $result = $this->db->query("SELECT i.id,CONCAT(e.first_name,' ',e.last_name) AS full_name,i.latitude,i.longitude,i.location,i.message,i.date_reported 
                                    FROM report_incidents AS i
                                    INNER JOIN employees AS e ON e.id = i.user_id
                                    WHERE i.id = " . $id);

        foreach ($post_data_keys as $key) {
            if (isset($result[0]->$key)) {
                $this->set($key,$result[0]->$key);
            }
        }

        $this->render("index","disaster/incidents_view","AGC Employee Locator | CMS");
    }

    private function _list_incidents($page) {
        $this->render("index","disaster/incidents","AGC Employee Locator | CMS");
    }

    private function _report_ins_list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());
        $search_by = "";
        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR c.id = " . $text;
            } 
            $search_by = "WHERE (e.first_name LIKE '%$text%' OR  e.last_name LIKE '%$text%' OR c.location LIKE '%$text%' OR c.message LIKE '%$text%' $search_id)";
        }
        $total = $this->get_total("SELECT COUNT(c.id) AS total FROM checkins AS c
                                   INNER JOIN employees AS e ON e.id = c.user_id
                                   LEFT JOIN cmdctr as cmd ON c.disaster_id = cmd.id
                                   LEFT JOIN disaster_type as dt on dt.id = cmd.disaster_type $search_by");
        $limit = $data->{'limit'};
        
        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT c.id,CONCAT(e.first_name,' ',e.last_name) AS full_name,status.description as status,CONCAT(c.date_checkedin,' ',c.time_checkedin) date_checkedin,c.location,c.message,c.latitude,c.longitude,c.no_of_persons, cmd.disaster_name, dt.type
                                    FROM checkins AS c
                                    INNER JOIN employees AS e ON e.id = c.user_id
                                    LEFT JOIN cmdctr as cmd ON c.disaster_id = cmd.id
                                    LEFT JOIN disaster_type as dt on dt.id = cmd.disaster_type
                                    LEFT JOIN status as status ON status.id = c.status
                                    $search_by
                                    ORDER BY c.date_checkedin DESC LIMIT $offset,$limit");

        foreach ($result as $index => $data) {
            $result[$index]->location = strip_tags(htmlspecialchars($data->location));
            $result[$index]->message = strip_tags(htmlspecialchars($data->message));
        }

        $this->send($total,$result);
    }

    private function _reportins_view($id) {
        $post_data_keys = array("id","full_name","status","date_checkedin","location","location","message","latitude","longitude","no_of_persons","disaster_name", "type");
        $result = $this->db->query("SELECT c.id,CONCAT(e.first_name,' ',e.last_name) AS full_name,c.status,CONCAT(c.date_checkedin,' ',c.time_checkedin) date_checkedin,c.location,c.message,c.latitude,c.longitude,c.no_of_persons,
                                    cmd.disaster_name, dt.type
                                    FROM checkins AS c
                                    INNER JOIN employees AS e ON e.id = c.user_id
                                    LEFT JOIN cmdctr as cmd ON c.disaster_id = cmd.id
                                    LEFT JOIN disaster_type as dt on dt.id = cmd.disaster_type
                                    WHERE c.id = " . $id);

        foreach ($post_data_keys as $key) {
            if (isset($result[0]->$key)) {
                $this->set($key,$result[0]->$key);
            }
        }

        $this->render("index","disaster/reportins_view","AGC Employee Locator | CMS");
    }

    private function _list_report_ins($page) {
      $this->render("index","disaster/reportins","AGC Employee Locator | CMS");
    }
}