<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Settings extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/settings","Settings");

        $this->load->library(array("input","db"));
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/settings/" . $request_method);

        if ($request_method == "menu") {
            $this->_menu_setup();
        } else if ($request_method == "modules") {
            $this->_module_setup();
        } else if ($request_method == "add-module") {
            $this->_module_add_new();
        } else if ($request_method == "maintenance") {
            $this->_maintenance();
        } else {
            $this->_settings();
        }
    }

    private function _settings() {
        $this->render("index","settings/settings","AGC Employee Locator | CMS");
    }

    private function _menu_setup() {
        $menu_list = "";
        $result = $this->db->query("SELECT id,icon,url,label,is_submenu_of,has_submenu,s,a
                                    FROM cms_menu
                                    ORDER BY label DESC");
        foreach ($result as $data) {

        }
        $this->set("menu",$menu_list);
        $this->render("index","settings/menu","AGC Employee Locator | CMS");
    }

    private function _module_setup() {
        $modules = $this->bind("SELECT m.id,m.module_uri,u.username,m.date_created FROM modules AS m
                                LEFT JOIN users AS u on u.id = m.created_by ORDER BY m.module_uri DESC",array("id","module_uri","date_created","username"));
        $this->set("modules",$modules);
        $this->render("index","settings/modules","AGC Employee Locator | CMS");
    }

    private function _module_add_new() {
        $error = true;
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);
            $post_data = $request->get_data();

            if (isset($post_data["url"])) {
                $current_date = date('Y-m-d H:i:s');

                $module = $this->db->create("modules");
                $module->module_uri = $post_data["url"];
                $module->created_by = $this->session->userdata("user_id");
                $module->date_created = $current_date;

                $result = $this->db->query("SELECT id FROM modules WHERE module_uri=".$this->db->quote($module->module_uri));
                if (count($result) < 1) {
                    $this->db->insert($module);
                    $module_id = $this->getLastInsertId();

                    $users = $this->db->create("users");
                    $result = $this->db->query("SELECT id FROM users WHERE role = 1");
                    foreach ($result as $data) {
                        $module = $this->db->create("user_module");
                        $module->user_id = $data->id;
                        $module->module_id = $module_id;
                        $module->can_view = 1;
                        $module->can_update = 1;
                        $module->can_delete = 1;
                        $module->can_add = 1;
                        $this->db->insert($module);
                    }
                    $error = false;
                }
            }
        }
        if ($error == true) {
            print json_encode(array("response_code" => 1,"response_msg" => "Error adding module!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Module added successfully!"));
        }
    }

    private function _maintenance() {
        $result = $this->db->query("SELECT status FROM maintenance");
        $status = count($result) > 0 ? $result[0]->status : 0;

        if ($this->input->request_method('POST')) {
            $status = $status == 1 ? 0 : $status;
            $maintenance = $this->db->create("maintenance");
            $maintenance->status = $status;
            $this->db->update($maintenance);            
        }

        $switch_state = $status == 1 ? "switch-on" : "switch-off";

        $this->set("status",$switch_state);
        $this->render("index","settings/maintenance","AGC Employee Locator | CMS");
    }
}