<?php
class Profile extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/profile","Account Settings");

        $this->load->library(array("html","form","input","util","hash","alert","user"));
	}

	public function request_handler($request_method,$request_params) {
		$this->checkpoint("/backoffice/profile/" . $request_method);

        if ($request_method == "changepass") {
            $this->_profile_changepass();
        } else if ($request_method == "update") {
            $this->_profile_update();
        } else {
		    $this->_profile_settings();        	
        }
	}

	private function _profile_update() {
		$this->post_data_keys = array("username","first_name","last_name","email","role");

		if ($this->input->request_method('POST')) {
			$post_data = $this->input->get_data(Input::POST)->get_data();
			$user = $this->db->create("users");
			foreach ($this->post_data_keys as $key) {
				if (isset($post_data[$key]) && !in_array($key,array("username","role"))) {
					$user->$key = $post_data[$key];
				}
			}
			$this->db->update($user,array("id" => $this->get_current_user()));

			print json_encode(array("response_code" => 0,"response_msg" => "Profile updated successfully!"));
		} else {
		    $result = $this->db->query("SELECT u.username,u.first_name,u.last_name,u.role,u.email FROM users AS u
		                                WHERE u.id=".$this->get_current_user());

		    if (count($result) > 0) {
		        foreach ($this->post_data_keys as $key) {
		            $this->set($key,$result[0]->$key);
		        }
		    } else {
		        foreach ($this->post_data_keys as $key) {
		            $this->set($key,"");
		        }
		    }

			$this->render("index","profile/update","AGC Employee Locator | CMS");
		}
	}

	private function _profile_changepass() {
		if ($this->input->request_method('POST')) {
			$post_data = $this->input->get_data(Input::POST)->get_data();
			$confirm = isset($post_data["confirm"]) ? trim($post_data["confirm"]) : "";
			$newpass = isset($post_data["newpass"]) ? trim($post_data["newpass"]) : "";
			$current = isset($post_data["current"]) ? trim($post_data["current"]) : "";

			$result = $this->db->query("SELECT password FROM users WHERE id = " . $this->get_current_user());

			$has_valid_password = false;
			if (!empty($newpass) && $newpass == $confirm) {
				$newpass_length = strlen($newpass);
				if ($newpass_length >= User::MIN_PASSWORD_LENGTH && $newpass_length <= User::MAX_PASSWORD_LENGTH) {
					$has_valid_password = true;
				}
			}

			if ($has_valid_password && count($result) > 0 && Hash::CheckPassword($current,$result[0]->password)) {
			    $user = $this->db->create("users");
			    $user->password = Hash::HashPassword($newpass);
			    $this->db->update($user,array("id" => $this->get_current_user()));
			    print json_encode(array("response_code" => 0,"response_msg" => "Password changed successfully!"));
			} else {
			    print json_encode(array("response_code" => 1,"response_msg" => "Unable to change password!"));
			}

		} else {
		    $this->render("index","profile/changepass","AGC Employee Locator | CMS");			
		}	
	}

	private function _profile_settings() {
        $result = $this->db->query("SELECT u.username,u.first_name,u.last_name,r.description as role,u.email,u.date_created,u2.username AS created_by,IF(u.date_modified IS NULL,'',u.date_modified) AS date_modified,u3.username AS modified_by FROM users AS u
                                    LEFT JOIN roles AS r ON r.id = u.role 
                                    LEFT JOIN users AS u2 ON u2.id = u.created_by 
                                    LEFT JOIN users AS u3 ON u3.id = u.modified_by
                                    WHERE u.id=".$this->get_current_user());
        $post_keys = array("username","first_name","last_name","email","role","date_created","created_by","date_modified","modified_by");

        if (count($result) > 0) {
            foreach ($post_keys as $key) {
                $this->set($key,$result[0]->$key);
            }
        } else {
            foreach ($post_keys as $key) {
                $this->set($key,"");
            }
        }		
		$this->render("index","profile/profile","AGC Employee Locator | CMS");
	}
}