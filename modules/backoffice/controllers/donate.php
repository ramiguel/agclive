<?php

class Donate extends CMS_Controller {
    private $post_data_keys = array();

    public function __construct() {
        parent::__construct("/backoffice/donate", "Donate Maintenance");

        $this->load->library(array("uri","input","form","alert","html","twitterapi", "upload"));

    }

    public function request_handler($request_method, $request_params) {
        //$this->checkpoint("/backoffice/donate/" . $request_method);
        if($request_method == "export") {
            $this->_export_data(isset($request_params[1]) ? $request_params[1] : 0);
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list(isset($request_params[1]) ? $request_params[1] : 1);
            }
        }

    }

    private function _list_data($data) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());
        $search_by = "";

        if(isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if(ctype_digit($text)) {
                $search_id = "OR d.id = $text";
            }
            $search_by = "AND (c.company_name LIKE '%$text%' $search_id OR CONCAT(TRIM(e.first_name),' ',TRIM(e.last_name)) LIKE '%$text%')";
        }

        $total = $this->get_total("SELECT COUNT(e.id) as total
                                    FROM employees as e
                                    INNER JOIN gcash_donations as gd
                                    ON gd.employee_id = e.id
                                    INNER JOIN companies AS c
                                    ON c.id = e.company_id
                                    WHERE e.id <> 0 $search_by");

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT e.id as id,
                                    c.company_name as company_name,
                                    CONCAT(e.first_name , ' ' , e.last_name) as employee_name,
                                    gd.donate as donate,
                                    gd.date_donated as date_donated
                                    FROM employees as e
                                    INNER JOIN gcash_donations as gd
                                    ON gd.employee_id = e.id
                                    INNER JOIN companies AS c
                                    ON c.id = e.company_id
                                    WHERE e.id <> 0 $search_by
                                    ORDER BY c.company_name ASC LIMIT $offset, $limit");

        $this->send($total, $result);
    }

    private function _list($page) {
        $this->render("index","donate/list","AGC Employee Locator | CMS");
    }

    private function _export_data($id) {
        $header = array("id",
                        "employee",
                        "company",
                        "site",

                        "amount",
                        "date_donated",
                        "time");

        $this->_export_data("donation", $header, "SELECT gc.id as id,
                                CONCAT(e.first_name, ' ', e.last_name) as employee,
                                c.company_name as company,
                                cs.site as site,

                                gc.donate as amount,
                                SUBSTRING_INDEX(SUBSTRING_INDEX(gc.date_donated, ' ', 1), ' ', -1)  as date_donated,
                                SUBSTRING_INDEX(SUBSTRING_INDEX(gc.date_donated, ' ', 2), ' ', -1)  as time_donated
                            FROM gcash_donations as gc
                            INNER JOIN employees as e
                            ON e.id = gc.employee_id
                            INNER JOIN companies as c
                            ON c.id = e.company_id
                            INNER JOIN company_sites as cs
                            ON cs.id = e.site_id");

    }
}