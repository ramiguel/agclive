<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Home extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/home","Dashboard");
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/home");

        $this->_home();
    }

    private function _home() {
        set_time_limit(60);

        $current_date = date('Y-m-d H:i:s');

        $result = $this->db->query("SELECT COUNT(c.id) AS total FROM checkins AS c
                                    LEFT JOIN users AS u ON u.id = c.user_id WHERE CONCAT(c.date_checkedin,' ',c.time_checkedin) > DATE_SUB('$current_date',INTERVAL 1 DAY)");

        $report_ins = 0;
        if (count($result) > 0) {
            $report_ins = $result[0]->total;
        }

        $this->set("reportins",$report_ins);

        $result = $this->db->query("SELECT COUNT(n.id) AS total FROM broadcasts AS n
                                    LEFT JOIN users AS u ON u.id = n.user_id WHERE n.date_created > DATE_SUB('$current_date',INTERVAL 1 DAY)");

        $disaster = 0;
        if (count($result) > 0) {
            $disaster = $result[0]->total;
        }

        $this->set("disaster",$disaster);

        $incidents = 0;
        $result = $this->db->query("SELECT COUNT(id) AS total FROM report_incidents WHERE DATE(date_reported) = DATE(CONVERT_TZ(NOW(),@@session.time_zone,'+8:00'))");
        if (count($result) > 0) {
            $incidents = $result[0]->total;
        }

        $this->set("incidents",$incidents);

        // SELECT DATE(DATE_SUB(NOW(),INTERVAL DAY(NOW())-1 DAY)),LAST_DAY(NOW())

        $result = $this->db->query("SELECT SUM(disasters) AS disasters, SUM(incidents) AS incidents, SUM(reportins) AS reportins, DATE_FORMAT(week,'%d - %W') AS week FROM (
                                        SELECT COUNT(id) AS disasters,0 AS incidents, 0 AS reportins, DATE(date_created) AS `week` 
                                        FROM broadcasts 
                                        WHERE DATE(date_created) >= DATE(DATE_SUB(NOW(),INTERVAL DAY('$current_date')-1 DAY)) AND DATE(date_created) <= DATE(LAST_DAY('$current_date'))
                                        GROUP BY DATE(date_created)
                                    UNION ALL
                                        SELECT 0 AS disasters, COUNT(id) AS incidents, 0 AS reportins,DATE(date_reported) AS `week`
                                        FROM report_incidents 
                                        WHERE DATE(date_reported) >= DATE(DATE_SUB(NOW(),INTERVAL DAY('$current_date')-1 DAY)) AND DATE(date_reported) <= DATE(LAST_DAY('$current_date'))
                                        GROUP BY DATE(date_reported)
                                    UNION ALL 
                                        SELECT 0 AS disasters, 0 AS incidents, COUNT(id) AS reportins,DATE(date_checkedin) AS `week`
                                        FROM checkins 
                                        WHERE DATE(date_checkedin) >= DATE(DATE_SUB(NOW(),INTERVAL DAY('$current_date')-1 DAY)) AND DATE(date_checkedin) <= DATE(LAST_DAY('$current_date'))
                                        GROUP BY DATE(date_checkedin)) AS d
                                    GROUP BY week
                                    ORDER BY week ASC");
        $disaster_per_week = "";
        $disaster_per_week_sep = "";
        foreach ($result as $data) {
            $disaster_per_week .= $disaster_per_week_sep."{period: '$data->week',disasters: $data->disasters,incidents: $data->incidents, reportins: $data->reportins}";
            $disaster_per_week_sep = ",";
        }

        $this->set("disaster_per_week",$disaster_per_week);

        $result = $this->db->query("SELECT COUNT(s.id) AS total FROM session AS s
                                    LEFT JOIN login_types AS t ON t.id = s.logged_in_via
                                    WHERE s.state <> 1 AND DATE(s.last_update) = DATE('$current_date')");

        $num_users_online = 0;
        if (count($result) > 0) {
            $num_users_online = $result[0]->total;
        }

        $this->set("num_users_online",$num_users_online);

        $result = $this->db->query("SELECT s.id,s.session_id,s.username,s.date_started,s.last_update,t.description AS logged_in_via,FLOOR(HOUR(TIMEDIFF(s.last_update,'$current_date'))/24) AS update_days,MOD(HOUR(TIMEDIFF(s.last_update,'$current_date')),24) AS update_hours,MINUTE(TIMEDIFF(s.last_update,'$current_date')) AS update_minutes FROM session AS s
                                    LEFT JOIN login_types AS t ON t.id = s.logged_in_via
                                    WHERE s.state <> 1 AND DATE(s.last_update) = DATE('$current_date') ORDER BY s.last_update DESC LIMIT 0,5");
        $online_users = "";
        foreach ($result as $data) {
            $last_seen = "";
            if ($data->update_days <= 0) {
                if ($data->update_hours <= 0) {
                    if ($data->update_minutes <= 0) {
                        $last_seen = "now";
                    } else {
                        $last_seen = "$data->update_minutes minute(s)";
                    }
                } else {
                    $last_seen = "$data->update_hours hour(s) $data->update_minutes minute(s)";
                }
            } else {
                $last_seen = "$data->update_days day(s) $data->update_hours hour(s) $data->update_minutes minute(s)";
            }
            $online_users .= "<tr>
                                 <td class=\"col-xs-2 text-center\">$data->last_update</td>
                                 <td class=\"col-xs-2 text-center\">$data->date_started</td>
                                 <td class=\"col-xs-1\">$data->username</td>
                                 <td class=\"col-xs-2 text-center\">$last_seen</td>
                              </tr>";
        }

        $this->set("online_users",$online_users);

//        $result = $this->db->query("SELECT l.date_created,c.description AS component,l.description,l.action,l.created_by,u.username FROM admin_actions AS l
//                                    LEFT JOIN users AS u ON u.id = l.created_by
//                                    LEFT JOIN component AS c ON c.id = l.component
//                                    ORDER BY l.date_created DESC LIMIT 0,5");

        //recent activities : new
        $result = $this->db->query("SELECT l.activity,l.created_by,u.username,l.date_created
                                    FROM activities AS l
                                    LEFT JOIN users AS u ON u.id = l.created_by
                                    ORDER BY l.date_created DESC LIMIT 0,5");

        $recent_activity = "";
        foreach ($result as $data) {
            $recent_activity .= "<tr>
                                     <td>". ucwords(strtolower($data->activity)) ."</td>
                                     <td>$data->username</td>
                                     <td class=\"text-center\">$data->date_created</td>
                                 </tr>";
        }

        $this->set("recent_activity",$recent_activity);

        $this->set("month",date("F"));

        $this->render("index","backoffice/home",parent::PAGE_TITLE);
    }
}