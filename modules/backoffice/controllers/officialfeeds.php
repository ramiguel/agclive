<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class OfficialFeeds extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/officialfeeds","Official Links Maintenance");

        $this->load->library(array("uri","input","form","alert","html","twitterapi"));

        $this->post_data_keys = array(
                "feed_type" => array(
                    "required" => true,
                    "type" => Form::TYPE_INT,
                    "extras" => array()
                ),
                "twitter_screen_name" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 128
                    )
                ),
                "description" => array(
                    "required" => false,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 48
                    )
                )                                                
            );

    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/officialfeeds/" . $request_method);

        if ($request_method == "add") {
            $this->_add_official_feeds();
        } else if ($request_method == "edit") {
            $this->_edit_official_feeds($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "delete") {
            $this->_delete_official_feeds($request_params[1] !== null ? $request_params[1] : 0);
        } else if ($request_method == "view") {
            $this->_view_official_feeds($request_params[1] !== null ? $request_params[1] : 0);
        } else {
            if ($request_method == "list" && $request_params[1] !== null && $request_params[1] == "data") {
                $this->_list_official_feeds_data();
            } else {
                $this->_list_official_feeds();
            }            
        }
    }

    private function _view_official_feeds($id) {
        if (ctype_digit($id)) {
            $result = $this->db->query("SELECT f.id,t.description AS feed_type,f.twitter_screen_name,f.description,f.consumer_key,f.consumer_secret,f.access_token,f.access_token_secret,f.date_created,u1.username AS created_by,f.date_modified,u2.username AS modified_by FROM officialfeeds AS f
                                        INNER JOIN feedtype AS t ON t.id = f.feed_type 
                                        INNER JOIN users AS u1 ON u1.id = f.created_by
                                        LEFT JOIN users AS u2 ON u2.id = f.modified_by
                                        WHERE f.id = " . $id); 
            if (count($result) > 0) {
                foreach ((array)$result[0] as $key => $data) {
                    $this->set($key,$data);
                }
            }
           $this->render("index","officialfeeds/view","AGC Employee Locator | CMS");
        }
    }

    private function _edit_official_feeds($id) {
        if (ctype_digit($id)) {
            if ($this->input->request_method('POST')) {
				$query = $this->db->query("SELECT twitter_screen_name FROM officialfeeds WHERE id = ". $id ."");
				
                $post_data = $this->input->get_data(Input::POST)->get_data();

                $this->feeds = $this->db->create("officialfeeds");
                $this->feeds->date_modified = $this->get_current_date();
                $this->feeds->modified_by = $this->get_current_user();

                $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                    $this->feeds->$key = $value;
                },function($error_type,$key) {
                });

                if ($error_count == 0) {
					$old_screen_name = $query[0]->{'twitter_screen_name'};
					$new_screen_name = $this->feeds->twitter_screen_name;
					
                    $this->db->update($this->feeds,array("id" => $id));
                    print json_encode(array("response_code" => 0,"response_msg" => "Record updated successfully!"));
					
					// $this->twitterapi->follow_edited_screen_name($old_screen_name, $new_screen_name);
                } else {
                    print json_encode(array("response_code" => 1,"response_msg" => "Error updating record"));
                }
            } else {
                $feed = $this->db->create("officialfeeds");
                $feed->id = 0;
                $feed->feed_type = 0;
                $feed->twitter_screen_name = "";
                $feed->description = "";
     
                $result = $this->db->query($feed,array("id" => $id));
                if (count($result) > 0) {
                    $feed = $result[0];            
                }

                $result = $this->db->query("SELECT id,description FROM feedtype ORDER BY description DESC");
                $feed_type_list = "";
                foreach ($result AS $data) {
                    if ($data->id == $feed->feed_type) {
                        $feed_type_list.= $this->html->option($data->description,$data->id,true);
                    } else {
                        $feed_type_list.= $this->html->option($data->description,$data->id);
                    }
                }
                $this->set("feed_type_list",$feed_type_list); 

                foreach ((array)$feed as $key => $data) {
                    $this->set($key,$data);
                }
                $this->render("index","officialfeeds/edit","AGC Employee Locator | CMS");
            }
        }
    }

    private function _add_official_feeds() {
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();

            $this->feeds = $this->db->create("officialfeeds");
            $this->feeds->date_created = $this->get_current_date();
            $this->feeds->created_by = $this->get_current_user();

            $error_message = "Error adding record";

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->feeds->$key = $value;
            },function($error_type,$key) {
            });

            if ($error_count == 0) {
                $feed_seek = $this->db->create("officialfeeds");
                $feed_seek->id = 0;
                $result = $this->db->query($feed_seek,array("twitter_screen_name" => $this->feeds->twitter_screen_name, "is_deleted" => 0));
                if (count($result) > 0) {
                    $error_message = "Screen name exists already";
                    $error_count++;
                }
            }

            if ($error_count == 0) {
                $this->db->insert($this->feeds);
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
				
				// $this->twitterapi->follow_added_screen_name($this->feeds->twitter_screen_name);
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $error_message));
            }
        } else {
            $result = $this->db->query("SELECT id,description FROM feedtype ORDER BY description DESC");
            $feed_type_list = "";
            foreach ($result AS $data) {
                $feed_type_list.= $this->html->option($data->description,$data->id);
            }
            $this->set("feed_type_list",$feed_type_list);

            $this->render("index","officialfeeds/add","AGC Employee Locator | CMS");
        }
    }

    private function _delete_official_feeds($id) {
        $error = true;
        if (ctype_digit($id)) {
            if ($id != $this->get_current_user()) {
                $officialfeeds = $this->db->create("officialfeeds");
                $officialfeeds->date_deleted = $this->get_current_date();
                $officialfeeds->deleted_by = $this->get_current_user();
                $officialfeeds->is_deleted = 1;

                $this->db->update($officialfeeds,array("id" => $id));
                $error = false;
            }
        }

        if ($error == true) {
            print json_encode(array("response_code" => 1,"response_msg" => "Error deleting record!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }
		
		$query = $this->db->query("SELECT twitter_screen_name FROM officialfeeds WHERE id = ". $id ."");
		
		$screen_name = $query[0]->{'twitter_screen_name'};
		
		// $this->twitterapi->unfollow_screen_name($screen_name);
    }

    private function _list_official_feeds_data() {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR f.id = " . $text;
            }
            $search_by = "AND (f.description LIKE '%$text%' OR f.twitter_screen_name LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT COUNT(f.id) AS total FROM officialfeeds AS f
                                   INNER JOIN feedtype AS t ON t.id = f.feed_type
                                   WHERE f.is_deleted <> 1 $search_by");
        
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT f.id,t.description AS feed_type,f.twitter_screen_name AS screen_name,f.description,f.date_created FROM officialfeeds AS f
                                    INNER JOIN feedtype AS t ON t.id = f.feed_type
                                    WHERE f.is_deleted <> 1 $search_by ORDER BY f.description ASC LIMIT $offset,$limit");

        $this->send($total,$result);
    }

    private function _list_official_feeds() {
        $this->render("index","officialfeeds/list","AGC Employee Locator | CMS");
    }
}