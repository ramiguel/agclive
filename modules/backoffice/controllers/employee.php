<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Employee extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/employee","Employees");

        $this->load->library(array("uri","input","util","hash","alert","html"));
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/employee/" . $request_method);

        if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "add") {
            $this->_add_employee();
        } else if ($request_method == "view") {
            $this->_view_employee_information($request_params[1]);
        } else if ($request_method == "add-success") {
            $this->_add_employee_success();
        } else if ($request_method == "edit") {
            $this->_edit_employee($request_params[1]);
        } else {
            $this->_list_employees();
        }
    }

    private function _bulk_upload() {

    }

    private function _view_employee_information($id) {
        if (ctype_digit($id)) {
            $result = $this->db->query("SELECT e.id,
                                    e.blood_type,
                                    r.description AS role,
                                    e.first_name,
                                    e.last_name,
                                    e.email,
                                    e.mobile_no,
                                    c.company_name AS company,
                                    e.address,
                                    e.landline_no,
                                    e.gender,d.name AS department,
                                    e.date_created,
                                    e.employee_no,
                                    e.middle_initial,
                                    e.position,
                                    e.emergency_name,
                                    e.emergency_address,
                                    e.emergency_relationship,
                                    e.emergency_contact_no,
                                    e.emergency_name_two,
                                    e.emergency_address_two,
                                    e.emergency_relationship_two,
                                    e.emergency_contact_no_two
                                    FROM employees AS e
                                    LEFT JOIN companies AS c ON c.id = e.company_id
                                    LEFT JOIN department AS d ON d.id = e.department_id
                                    LEFT JOIN users AS u ON c.id = e.user_id
                                    LEFT JOIN roles AS r ON r.id = u.role
                                    WHERE e.is_deleted <> 1 AND e.id=" . $id);

            if (count($result) > 0) {
                foreach (get_object_vars($result[0]) as $key => $value) {
                    $this->set($key,$value);
                }
                $this->set("user_id",$result[0]->id);

                $this->render("index","employee/view",parent::PAGE_TITLE);
            } else {
                redirect(base_url() . "/backoffice/employee/list");
            }
        } else {
            redirect(base_url() . "/backoffice/employee/list");
        }
    }

    private function _list_employees($page=1) {
        $filter = "";
        if ($this->session->userdata("role_id") == parent::ROLE_ADMINISTRATOR) {
            $filter = " AND e.company_id = " . $this->session->userdata("company_id");
            $filter.= " AND e.department_id = " . $this->session->userdata("department_id");
            $filter.= " ";
        }

        $total = $this->get_total("SELECT COUNT(e.id) AS total FROM employees AS e
                                    LEFT JOIN users AS u ON u.id = e.user_id
                                    WHERE u.role = 6 AND u.is_deleted <> 1 $filter");

        $result = $this->db->query("SELECT e.id,e.first_name,e.last_name,u.username,e.date_created,c.company_name,d.name AS department_name,e.mobile_no,e.email,e.is_supervisor,e.is_rescuer FROM employees AS e
                                    LEFT JOIN users AS u ON u.id = e.user_id
                                    LEFT JOIN companies AS c ON c.id = e.company_id
                                    LEFT JOIN department AS d ON d.id = e.department_id
                                    WHERE u.role = 6 AND u.is_deleted <> 1 $filter ORDER BY u.username ASC");
        $employees = "";
        foreach ($result as $data) {
            $link = base_url() . "/backoffice/employee/edit/".$data->id;

            $employees.= "<tr>
                            <td>$data->username</th>
                            <td>$data->first_name $data->last_name</td>
                            <td>$data->email</td>
                            <td>$data->company_name</td>
                            <td>$data->department_name</td>
                            <td class=\"text-center\">".($data->is_supervisor ? "Yes" : "No")."</td>
                            <td class=\"text-center\">".($data->is_rescuer ? "Yes" : "No")."</td>
                            <td class=\"text-center\">
                                <button type=\"button\" class=\"btn btn-default action-button-item cmd_view\" rel=\"$data->id\"><i class=\"fa fa-folder\"></i></button>
                                <button type=\"button\" class=\"btn btn-default action-button-item cmd_edit\" rel=\"$data->id\"><i class=\"fa fa-edit\"></i></button>
                                <button type=\"button\" class=\"btn btn-default action-button-item cmd_delete\" rel=\"$data->id\"><i class=\"fa fa-trash\"></i></button>
                            </td>
                          </tr>";
        }

        $this->set('employees',$employees);

        $this->paginate($page,$total,10,base_url() . "/backoffice/employee/list/");

        $this->render("index","employee/list",parent::PAGE_TITLE);
    }

    private function _add_employee() {
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);
            if (count($request) > 0) {

                // NOTE: we need to filter keys that we need to accept - sql injection prevention
                // we'll accept only known keys

                $post_data_keys = array("first_name","last_name","middle_initial","company_id","department_id","position","mobile_no","email","employee_no");

                if(isset($_FILES["file"])){
                    if($_FILES["file"]["error"] > 0){
                        $this->alert->set_message("Error uploading file");
                        redirect(base_url() . "/backoffice/employee/add");
                    }else{
                        $path = FILE_PATH.$_FILES["file"]["name"];
                        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                        if(strtolower($ext) == "csv" ){
                            if(move_uploaded_file($_FILES["file"]["tmp_name"], $path)){
                                $post_data_keys = array("first_name","last_name","middle_initial","company_id","department_id",
                                "position","mobile_no","email","employee_no", "password");

                                $post_data = "";
                                $post_values = "";
                                $invalid_keys = array();

                                $row = 1;
                                if (($handle = fopen($path, "r")) !== FALSE) {
                                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                        $num = count($data);
                                        
                                        for ($c=0; $c < $num; $c++) {
                                            if($row == 1){
                                                //filter keys
                                                if(in_array($data[$c], $post_data_keys)){
                                                    $post_data.=$data[$c].",";
                                                }else{ //key not exists
                                                    $invalid_keys[$c]['name'] = $data[$c];
                                                }
                                            }else{
                                                //check key in invalid keys
                                                if(!isset($invalid_keys[$c])){
                                                    $post_values.= "'".$data[$c]."',";
                                                }
                                            }
                                        }

                                        $row++;

                                        if($post_values != ""){
                                            $post_data = rtrim($post_data, ",");
                                            $post_values = rtrim($post_values, ",");
                                            $sql = "INSERT INTO employee_registration($post_data) VALUES($post_values)";
                                            $response = $this->db->insert($sql);
                                            $post_values = "";  //clear values
                                        }
                                    }
                                    if($response){
                                        redirect(base_url() . "/backoffice/employee/add-success");
                                    }else{
                                        $this->alert->set_message("Failed saving file");
                                        redirect(base_url() . "/backoffice/employee/add");
                                    }
                                    //var_dump($invalid_keys);
                                    fclose($handle);
                                }else{
                                    $this->alert->set_message("Failed saving file");
                                    redirect(base_url() . "/backoffice/employee/add");
                                }

                            }else{
                                $this->alert->set_message("Failed saving file");
                                redirect(base_url() . "/backoffice/employee/add");
                            }
                        }else{
                            //echo "not csv file";
                            $this->alert->set_message("Invalid file type. Accepting CSV files only");
                            redirect(base_url() . "/backoffice/employee/add");
                        }
                    }
                    //redirect(base_url() . "/backoffice/employee/add");
                }

                $dateToday = date('Y-m-d H:i:s');

                $employee = $this->db->create("employee_registration");
                $employee->date_created = $dateToday;
                $employee->password = base64_encode($this->util->generate_password());
                $employee->created_by = $this->session->userdata("user_id");
                $employee->is_registered = 0;
                $employee->date_registered = $dateToday;

                $post_data = $request->get_data();
                foreach ($post_data_keys as $key) {
                    if (isset($post_data[$key])) {
                        $employee->$key = trim($post_data[$key]);
                    }
                }

                $employee_seek = $this->db->create("employee_registration");
                $employee_seek->id = 0;
                $employee_seek->email = "";
                $employee_seek->mobile_no = "";

                $result = $this->db->query($employee_seek,array("email" => $employee->email));
                if (count($result) > 0 && $result[0]->id) {
                    $this->alert->set_message("This e-mail you are trying to register is already in use");
                    redirect(base_url() . "/backoffice/employee/add");
                } else {
                    $this->db->insert($employee);
                    redirect(base_url() . "/backoffice/employee/add-success");
                }
            }
        } else {
            $this->load->model("companies");

            $company = $this->db->create('companies');
            $company->id = 0;
            $company->company_name = "";

            $result = $this->db->query($company);

            $companies = "";
            foreach ($result as $data) {
                $companies .= $this->html->option($data->id,$data->company_name);
            }

            $department = $this->db->create('department');
            $department->id = 0;
            $department->name = "";

            $result = $this->db->query($department);
            $departments = "";
            foreach ($result as $data) {
                $departments .= $this->html->option($data->id,$data->name);
            }

            $this->set('companies', $companies);
            $this->set('departments', $departments);

            $this->render("index","employee/add",parent::PAGE_TITLE);
        }
    }

    private function _add_employee_success() {
        $this->render("index","employee/add_success",parent::PAGE_TITLE);
    }

    private function _edit_employee($id) {
        if (ctype_digit($id)) {
            $result = $this->db->query("SELECT e.id,e.first_name,e.last_name,e.email,e.mobile_no,e.landline_no,e.address,c.company_name,d.name AS department_name,e.date_created,e.employee_no,e.middle_initial,e.position,e.emergency_name,e.emergency_address,e.emergency_contact_no,e.emergency_relationship,e.emergency_name_two,e.emergency_address_two,e.emergency_contact_no_two,e.emergency_relationship_two FROM employees AS e
                                        LEFT JOIN companies AS c ON c.id = e.company_id
                                        LEFT JOIN department AS d ON d.id = e.department_id
                                        WHERE e.is_deleted <> 1 AND e.id=" . $id);

            if (count($result) > 0) {
                $fields = array(
                    "first_name",
                    "last_name",
                    "email",
                    "mobile_no",
                    "landline_no",
                    "company_name",
                    "department_name",
                    "employee_no",
                    "address",
                    "position",
                    "middle_initial",
                    "emergency_name",
                    "emergency_contact_no",
                    "emergency_address",
                    "emergency_relationship",
                    "emergency_name_two",
                    "emergency_contact_no_two",
                    "emergency_address_two",
                    "emergency_relationship_two"
                );

                foreach ($fields as $field) {
                    $this->set("$field", isset($result[0]->$field) ? $result[0]->$field : "");
                }

                $this->set("user_id",$result[0]->id);

                $this->render("index","employee/edit",parent::PAGE_TITLE);
            } else {
                redirect(base_url() . "/backoffice/employee/list");
            }
        } else {
            redirect(base_url() . "/backoffice/employee/list");
        }
    }
}