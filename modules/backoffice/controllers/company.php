<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Company extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/company","Company Maintenance");

        $this->load->library(array("uri", "input","form","upload"));
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/company/" . $request_method);

        if ($request_method == "add") {
            $this->_add_company();
        } else if ($request_method == "edit") {
            $this->_edit_company($request_params[1]);
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "view") {
            $this->_view_company_details();
        } else if ($request_method == "export") {
            $this->_export_data();
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list_company();
            }
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_id = "";
            if (ctype_digit($text)) {
                $search_id = " OR id = " . $text;
            } 
            $search_by = "AND (company_name LIKE '%$text%' OR company_address LIKE '%$text%' OR company_contact_no LIKE '%$text%' $search_id)";
        }

        $total = $this->get_total("SELECT COUNT(id) AS total FROM companies
                                   WHERE is_deleted <> 1 $search_by");
        
        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;
        $result = $this->db->query("SELECT id,company_name,company_address,company_contact_no FROM companies
                                    WHERE is_deleted <> 1 $search_by ORDER BY company_name ASC LIMIT $offset,$limit");

        $this->send($total,$result); 
    }

    private function _list_company() {
        $this->render("index","company/list","AGC Employee Locator | CMS");
    }

    private function _add_company() {
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);

            $this->post_data_keys = array(
                "company_name" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 80
                    )
                ),
                "company_address" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 256
                    )
                ),
                "company_contact_no" => array(
                    "required" => true,
                    "type" => Form::TYPE_TEXT,
                    "extras" => array(
                        "min" => 1,
                        "max" => 20
                    )
                )
            );
            $post_data = $request->get_data();            

            $this->company = $this->db->create("companies");
            $this->company->date_created = $this->get_current_date();
            $this->company->created_by = $this->get_current_user();

            $this->log_open(2,"insert","company");

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                if ($key != "department") {
                    $this->company->$key = $value;
                    $this->log_insert($key,$this->company->$key);
                }
            },function($error_type,$key) {
                if ($error_type == Form::ERROR_VALIDATION_FAILED) {
                } else if ($error_type == Form::ERROR_ID_NOT_SET) {
                }
            });

            if ($error_count == 0) {
                $company_seek = $this->db->create("companies");
                $company_seek->id = 0;
                $result = $this->db->query($company_seek,array("company_name" => $this->company->company_name,"is_deleted" => 0));
                if (count($result) > 0) {
                    $error_count++;
                }
            }

            if ($error_count == 0) {
                $this->log_close();
                $result = $this->db->insert($this->company);

                $company_id = $this->db->getLastInsertId();
                foreach ($post_data['department'] as $key) {
                    $department = $this->db->create("department");
                    $department->department_name = $key;
                    $department->date_created = $this->get_current_date();
                    $department->created_by = $this->get_current_user();
                    $this->db->insert($department);

                    $department_id = $this->db->getLastInsertId();

                    $company_department = $this->db->create("company_department");
                    $company_department->company_id = $company_id;
                    $company_department->department_id = $department_id;
                    $this->db->insert($company_department);
                }

                $dictionary = $this->db->create("company_dictionary");
                //$dictionary->company_name = $this->sanitize_name($this->company->company_name);
                $dictionary->company_name = $this->company->company_name;
                $dictionary->date_created = $this->get_current_date();
                $dictionary->created_by = $this->get_current_user();
                $dictionary->company_id = $company_id;

                $this->db->insert($dictionary);

                //Phase 2 Additionals
                foreach($post_data['site'] as $key => $value):
                    $company_site = $this->db->create("company_sites");
                    $company_site->company_id = $company_id;
                    $company_site->site = $value;
                    $company_site->date_created = $this->get_current_date();
                    $company_site->created_by = $this->session->userdata("user_id");
                    $result = $this->db->insert($company_site);
                endforeach;
                //End Phase 2

                redirect(base_url() . "/backoffice/company/list");
            } else {
                redirect(base_url() . "/backoffice/company/add");
            }
        }

        $result = $this->db->query("SELECT id,department_name FROM department WHERE is_deleted <> 1 ORDER BY department_name ASC");

        $departments = "";

        foreach ($result as $data) {
            //$departments .= "<option value=$data->id>$data->department_name</option>";
        }

        $this->set('departments', $departments);
        $this->render("index","company/add","AGC Employee Locator | CMS");
    }

    private function sanitize_name($name) {
        $sanitized_name = str_replace(str_split(",~`!@#\$%^&*()-+[{]}\\|:;\"'<>./?."),'',$name);
        $sanitized_name = preg_replace('/\s+/',' ',$sanitized_name);
        return $sanitized_name;
    }    

    private function _edit_company($id) {
        $post_data_keys = array("company_name","company_address","company_contact_no");

        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $company = $this->db->create("companies");

            foreach ($post_data_keys as $key) {
                $company->$key = $post_data[$key];
            }

            $result = $this->db->update($company,array("id" => $id));

            if ($result) {
                $old_deps = $this->db->create("company_department");
                $old_deps->id = 0;
                $old_deps->department_name = "";
                $old_result = $this->db->query("SELECT d.id, d.department_name, cd.company_id
                                FROM department AS d
                                INNER JOIN company_department as cd
                                ON cd.department_id = d.id
                                WHERE cd.company_id = $id");
                $main_new_id = "";

                foreach($post_data["department"] as $key => $value):
                    $old_id = 0;

                    //insert new department
                    $department = $this->db->create("department");
                    $department->department_name = $value;
                    $department->date_created = $this->get_current_date();
                    $department->created_by = $this->session->userdata("user_id");
                    $this->db->insert($department);
                    $last_id = $this->db->getLastInsertId();

                    if($key == 0) {
                        $main_new_id = $last_id;
                    }

                    //insert new company_department
                    $company_department = $this->db->create("company_department");
                    $company_department->company_id = $id;
                    $company_department->department_id = $last_id;
                    $this->db->insert($company_department);

                    foreach($old_result as $old):
                        if(strtolower($old->department_name) == strtolower($value)):
                            $old_id = $old->id;

                            $employees = $this->db->create("employees");
                            $employees->id = 0;
                            $emp_result = $this->db->query($employees, array("company_id" => $id, "department_id" => $old_id));

                            //update employee
                            foreach($emp_result as $emp_data):
                                $employee_update = $this->db->create("employees");
                                $employee_update->department_id = $last_id;
                                $this->db->update($employee_update, array("id" => $emp_data->id));
                            endforeach;

                        endif;
                    endforeach;
                endforeach;

                //delete old department
                foreach($old_result as $old):
                    $employees = $this->db->create("employees");
                    $employees->id = 0;
                    $emp_result = $this->db->query($employees, array("company_id" => $id, "department_id" => $old->id));

                    foreach($emp_result as $e_data):
                        $emp_update = $this->db->create("employees");
                        $emp_update->department_id = $main_new_id;
                        $this->db->update($emp_update, array("id" => $e_data->id));
                    endforeach;

                    $sql = "DELETE FROM company_department WHERE department_id = " . $old->id . " AND company_id = " . $id;
                    $this->db->update($sql);

                    $sql = "DELETE FROM department WHERE id = " . $old->id;
                    $this->db->update($sql);
                endforeach;




                $old_sites = $this->db->create("company_sites");
                $old_sites->id = 0;
                $old_sites->site = "";
                $old_result = $this->db->query($old_sites, array("company_id" => $id));
                $main_new_id = "";

                foreach($post_data["site"] as $key => $value):
                    $old_id = 0;

                    //insert new site
                    $company_site = $this->db->create("company_sites");
                    $company_site->company_id = $id;
                    $company_site->site = $value;
                    $company_site->date_created = $this->get_current_date();
                    $company_site->created_by = $this->session->userdata("user_id");
                    $this->db->insert($company_site);
                    $last_id = $this->db->getLastInsertId();

                    if($key == 0) {
                        $main_new_id = $last_id;
                    }

                    foreach($old_result as $old):
                        if(strtolower($old->site) == strtolower($value)):
                            $old_id = $old->id;

                            $employees = $this->db->create("employees");
                            $employees->id = 0;
                            $emp_result = $this->db->query($employees, array("company_id" => $id, "site_id" => $old_id));

                            //update employee
                            foreach($emp_result as $emp_data):
                                $employee_update = $this->db->create("employees");
                                $employee_update->site_id = $last_id;
                                $this->db->update($employee_update, array("id" => $emp_data->id));
                            endforeach;

                            $broadcast_sites = $this->db->create("broadcast_sites");
                            $broadcast_sites->id = 0;
                            $bs_result = $this->db->query($broadcast_sites, array("company_id" => $id, "site_id" => $old_id));

                            //update broadcast sites
                            foreach($bs_result as $bs_data):
                                $bs_update = $this->db->create("broadcast_sites");
                                $bs_update->site_id = $last_id;
                                $this->db->update($bs_update, array("id" => $bs_data->id));
                            endforeach;

                            $cmdctr_sites = $this->db->create("cmdctr_sites");
                            $cmdctr_sites->id = 0;
                            $cmdctr_result = $this->db->query($cmdctr_sites, array("company_id" => $id, "site_id" => $old_id));

                            //update cmdctr sites
                            foreach($cmdctr_result as $cmdctr_data):
                                $cmdctr_update = $this->db->create("cmdctr_sites");
                                $cmdctr_update->site_id = $last_id;
                                $this->db->update($cmdctr_update, array("id" => $cmdctr_data->id));
                            endforeach;

                        endif;
                    endforeach;
                endforeach;

                //delete old site
                foreach($old_result as $old):
                    //employees
                    $emp = $this->db->create("employees");
                    $emp->id = 0;
                    $emp_result  = $this->db->query($emp, array("company_id" => $id, "site_id" => $old->id));

                    foreach($emp_result as $e_data):
                        $emp_update = $this->db->create("employees");
                        $emp_update->site_id = $main_new_id;
                        $this->db->update($emp_update, array("id" => $e_data->id));
                    endforeach;

                    //broadcasts
                    $broad = $this->db->create("broadcast_sites");
                    $broad->id = 0;
                    $broad_result  = $this->db->query($broad, array("company_id" => $id, "site_id" => $old->id));

                    foreach($broad_result as $b_data):
                        $broad_update = $this->db->create("broadcast_sites");
                        $broad_update->site_id = $main_new_id;
                        $this->db->update($broad_update, array("id" => $b_data->id));
                    endforeach;

                    //cmdctr
                    $cmd = $this->db->create("cmdctr_sites");
                    $cmd->id = 0;
                    $cmd_result  = $this->db->query($cmd, array("company_id" => $id, "site_id" => $old->id));

                    foreach($cmd_result as $c_data):
                        $cmd_update = $this->db->create("cmdctr_sites");
                        $cmd_update->site_id = $main_new_id;
                        $this->db->update($cmd_update, array("id" => $c_data->id));
                    endforeach;


                    $sql = "DELETE FROM company_sites WHERE id = " . $old->id;
                    $this->db->update($sql);
                endforeach;

            }

            $dictionary = $this->db->create("company_dictionary");
            $dictionary->company_name = $company->company_name;

            $this->db->update($dictionary,array("company_id" => $id));

            if ($result) {
                redirect(base_url() . "/backoffice/company/list");
            } else {
                redirect(base_url() . "/backoffice/company/edit/".$id);
            }
        } else {
            $company = $this->db->create("companies");
            foreach ($post_data_keys as $key) {
                $company->$key = "";
            }

            $result = $this->db->query($company,array("id" => $id));

            foreach ($post_data_keys as $key) {
                $this->set("$key", isset($result[0]->$key) ? $result[0]->$key : "");
            }

            //$departments = $this->db->query("SELECT id,department_name FROM department WHERE is_deleted <> 1 ORDER BY department_name ASC");

            $html = array();

            $company_department = $this->db->create("company_department");
            $company_department->id = 0;
            $company_department->company_id = 0;
            $company_department->department_id = 0;

            $result = $this->db->query($company_department,array("company_id" => $id));

            $result = $this->db->query("SELECT company_id,department_id,department_name FROM company_department cd 
                                        LEFT JOIN department d ON d.id = cd.department_id
                                        WHERE cd.company_id = $id AND d.is_deleted <> 1 ORDER BY department_name ASC");

            $department_data = "";
            $department_sep = "";
            if (count($result) > 0) {
                foreach ($result as $data) {
                    $department_data.=$department_sep.json_encode($data);
                    $department_sep = ",";
                }
            } 

            $result = $this->db->query("SELECT id, company_id, site FROM company_sites WHERE company_id = $id");
            $site_data = "";
            $site_sep = "";
            if(count($result) > 0) {
                foreach($result as $data):
                    $site_data.=$site_sep.json_encode($data);
                    $site_sep = ",";
                endforeach;
            }

            $this->set("department_data", $department_data);
            $this->set("site_data", $site_data);

            $this->render("index","company/edit","AGC Employee Locator | CMS");
        }
    }

    private function _delete($id) {
        $response = $this->create_client_response(1,"Company deletion failed!");
        if ($this->input->request_method('POST')) {
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $response->error(json_encode($post_data));

            if ($post_data["id"] !== null && (is_int($post_data["id"]) || ctype_digit($post_data["id"]))) {
                $company = $this->db->create("companies");
                $company->date_deleted = $this->get_current_date();
                $company->deleted_by = $this->get_current_user();
                $company->is_deleted = 1;
                $this->db->update($company,array("id" => $post_data["id"]));

                $response->success("Company deleted successfully");
            } else {
                $response->error("Supplied id is invalid"); 
            }            
        }

        $response->notify();
    }

    private function _view_company_details() {

    }

    private function _export_data() {
        $headers = array("id",
                         "company_name",
                         "company_address",
                         "company_contact_no");

        $this->export_data("company-", $headers, "SELECT " . implode(",",$headers) . " FROM companies 
                                                            WHERE is_deleted <> 1 ORDER BY id ASC");
    }

    private function _bulk_upload() {
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $this->error_data = "";
            if ($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));

                $file_path = BASEPATH . "assets/uploads/bulk/";
                //$file_path = BASEPATH . "bulk/";

                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if ($this->upload->do_upload("file",$config)) {
                    $file_name = $this->upload->get_uploaded_filename();

                    $csv_data = $this->helper->get_csv($file_path.$file_name,false);

                    $action  = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "company";

                    $this->db->insert($file);

                    $this->row = 0;

                    $this->post_data_keys = array(
                        "company_name" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 80
                            )
                        ),
                        "company_address" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 256
                            )
                        ),
                        "company_contact_no" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 20
                            )
                        ),
                        "department" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 256
                            )
                        ),
                        "site" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 256
                            )
                        )
                    );

                    foreach ($csv_data as $data) {
                        ++$this->row;
                        $this->company = $this->db->create("companies");
                        
                        if ($action == "add") {
                            $this->log_open(1,"insert","bulk upload");
                            $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                if ($key != "id" && $key != "department" && $key != "site") {
                                    $this->company->$key = utf8_encode($value);
                                    $this->log_insert($key,$this->company->$key);
                                }
                            }, function ($error_type,$key) {
                                $this->error_data .= "Unable to add row #" . $this->row . ": " . $error_type . " :" . $key . " does not exists.<br>\n";
                            });

                            if ($error_count == 0) {
                                $result = $this->db->query("SELECT id FROM companies WHERE company_name = " . $this->db->quote($this->company->company_name) . " LIMIT 0,1");
                                if (count($result) > 0) {
                                    $this->error_data.= "Unable to add row #" . $this->row . ": Record already exists<br>\n";
                                } else {
                                    $this->company->date_created = $this->get_current_date();
                                    $this->company->created_by = $this->get_current_user();
                                    $this->db->insert($this->company);

                                    $company_id = $this->db->getLastInsertId();

                                    $dictionary = $this->db->create("company_dictionary");
                                    //$dictionary->company_name = $this->sanitize_name($this->company->company_name);
                                    $dictionary->company_name = $this->company->company_name;
                                    $dictionary->date_created = $this->get_current_date();
                                    $dictionary->created_by = $this->get_current_user();
                                    $dictionary->company_id = $company_id;

                                    $this->db->insert($dictionary);

                                    //insert department and site
                                    $department = $this->db->create("department");
                                    $department->department_name = $data["department"];
                                    $department->date_created = $this->get_current_date();
                                    $department->created_by = $this->session->userdata("user_id");
                                    $this->db->insert($department);
                                    $last_id = $this->db->getLastInsertId();

                                    //insert new company_department
                                    $company_department = $this->db->create("company_department");
                                    $company_department->company_id = $company_id;
                                    $company_department->department_id = $last_id;
                                    $this->db->insert($company_department);

                                    //insert new site
                                    $company_site = $this->db->create("company_sites");
                                    $company_site->company_id = $company_id;
                                    $company_site->site = $data["site"];
                                    $company_site->date_created = $this->get_current_date();
                                    $company_site->created_by = $this->session->userdata("user_id");
                                    $this->db->insert($company_site);
                                }
                            }


                        } else if ($action == "update") {
                             if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"update","bulk upload");
                                $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                    if ($key != "id") {
                                        $this->company->$key = utf8_encode($value);
                                        $this->log_changes($key,$this->company->$key,$this->company->$key);                                    
                                    }
                                }, function ($error_type,$key) {
                                    $this->error_data .= "Unable to edit row #". $this->row . " " . $error_type . " " . $key . "<br>\n";
                                });

                                if ($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM companies WHERE id = " . intval($data["id"]));
                                    if (count($result) > 0) {
                                        $this->company->date_modified = $this->get_current_date();
                                        $this->company->modified_by = $this->get_current_user();
                                        $this->db->update($this->company,array("id" => $data["id"]));

                                        $dictionary = $this->db->create("company_dictionary");
                                        $dictionary->company_name = $this->company->company_name;

                                        $this->db->update($dictionary,array("company_id" => $data["id"]));
                                    } else {
                                        $this->error_data .= "Unable to edit row #" . $this->row . ": Record not found<br>\n";
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"delete","bulk upload");
                                $this->company->is_deleted = 1;
                                $this->company->date_deleted = $this->get_current_date();
                                $this->db->update($this->company,array("id" => $data["id"]));
                                $this->log_close();
                            } else {
                                $this->error_data.= "Unable to delete row #" . $this->row . ": ID is not defined<br>\n";
                            }
                        }
                    }
                }
            }

            if (!empty($this->error_data)) {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            }
        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by                                         
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list.= "<tbody>";
                foreach ($result as $data) {
                    $upload_list.= "<tr>";
                    $upload_list.= "<td>$data->file_name</td>";
                    $upload_list.= "<td>$data->date_uploaded</td>";
                    $upload_list.= "<td>$data->section</td>";
                    $upload_list.= "<td>$data->action</td>";
                    $upload_list.= "<td>$data->uploaded_by</td>";
                    $upload_list.= "<td>$data->hash_value</td>";
                    $upload_list.= "</tr>";
                }
                $upload_list.= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by                                        
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list.= "<tr>";
                    $download_list.= "<td>$data->file_name</td>";
                    $download_list.= "<td>$data->date_created</td>";
                    $download_list.= "<td>$data->created_by</td>";
                    $download_list.= "<td>$data->hash_value</td>";
                    $download_list.= "</tr>";
                }
                $download_list.= "</tbody>";
            }

            $this->set("upload_list",$upload_list);
            $this->set("download_list",$download_list);

            $this->render("index", "company/upload", "AGC Employee Locator | CMS");
        }
    }    
}