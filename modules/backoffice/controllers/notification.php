<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Notification extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/notification","Notifications");

        $this->load->library(array("uri","iospushnotif"));
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/notification/" . $request_method);

        if ($request_method == "ios_push") {
            $this->_ios_push();
        }
    }
	
	private function _ios_push() {
		$result = $this->db->query("SELECT id, user_id, name, date, time, message, status FROM notification");
		
		$data = '';
		
		if($result) {
			foreach($result as $key=>$value) {
				$message[] = $value->{'message'};
			}
			
			// $this->iospushnotif->push($message, $result);
			$this->iospushnotif->push($message, $data);
		}
	}
	
}