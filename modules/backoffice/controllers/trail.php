<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Trail extends CMS_Controller {
    public function __construct() {
        parent::__construct("/backoffice/trail","Audit Trail");

        $this->load->library(array("uri","input","form","alert","html","twitterapi", "upload"));

    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/trail/" . $request_method);

        if($request_method == "list") {
            $this->_list_admin_actions(1);
        } else {
            $this->_list_audit_trail();
        }
        //$this->_list_admin_actions(isset($request_params[1]) ? $request_params[1] : 1);
    }

    private function _list_admin_actions($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $total = $this->get_total("SELECT COUNT(t.id) total FROM admin_actions AS t
                                    LEFT JOIN users AS u ON u.id = t.created_by
                                    LEFT JOIN component AS c ON c.id = t.component");

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT t.id as id,
                                        t.action as action,
                                        t.description as description,
                                        t.date_created as date_created,
                                        u.username AS created_by
                                    FROM admin_actions AS t
                                    LEFT JOIN users AS u ON u.id = t.created_by
                                    LEFT JOIN component AS c ON c.id = t.component
                                    ORDER BY t.date_created DESC LIMIT $offset, $limit");

        $this->send($total, $result);
    }

    private function _list_audit_trail() {
        $this->render("index","trail/list","AGC Employee Locator | CMS");
    }
}
