<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Hotline extends CMS_Controller {
    private $hotline = null;
    private $error_message;

    public function __construct() {
        parent::__construct("/backoffice/hotline","Contacts Maintenance");

        $this->load->library(array("uri","input","form","alert","upload"));

        $this->post_data_keys = array(
            "contact_name" => array(
                "required" => true,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 64
                )
            ),
            /*"contact_no" => array(
                "required" => true,
                "type" => Form::TYPE_NUMBER,
                "extras" => array()
            ),*/
            "logo" => array(
                "required" => false,
                "type" => Form::TYPE_TEXT,
                "extras" => array(
                    "min" => 1,
                    "max" => 256
                )
            )
        );
    }

    public function request_handler($request_method,$request_params) {
        $this->checkpoint("/backoffice/hotline/" . $request_method);

        if ($request_method == "add") {
            $this->_add_hotline();
        } else if ($request_method == "edit") {
            $this->_edit_hotline($request_params[1]);
        } else if ($request_method == "add-success") {
            $this->_add_hotline_success();
        } else if ($request_method == "delete") {
            $this->_delete($request_params[1]);
        } else if ($request_method == "bulk") {
            $this->_bulk_upload();
        } else if ($request_method == "export") {
            $this->_export_data();
        } else {
            if ($request_method == "list" && (isset($request_params[1]) && $request_params[1] == "data")) {
                $this->_list_data(1);
            } else {
                $this->_list_hotline();
            }
        }
    }

    private function _list_data($page) {
        $data = json_decode($this->input->get_data(Input::STREAM)->get_data());

        $search_by = "";
        $having_by = "";

        if (isset($data->{'search'}) && !empty($data->{'search'})) {
            $text = $data->{'search'};
            $search_contact = "";
            if (ctype_digit($text)) {
                $search_contact = " OR contact_no LIKE '%$text%'";
            }
            $having_by = "HAVING (eh.contact_name LIKE '%$text%' $search_contact)";
        }

        $total = $this->db->query("SELECT eh.id,eh.contact_name,GROUP_CONCAT(ehn.contact_no SEPARATOR ', ') AS contact_no,eh.logo,
                                    eh.date_created 
                                    FROM emergency_hotline eh 
                                    LEFT JOIN emergency_hotline_num ehn ON eh.id = ehn.hotline_id
                                    WHERE eh.is_deleted <> 1
                                    GROUP BY eh.id $having_by");

        $total = count($total);

        $limit = $data->{'limit'};

        $offset = ($limit * $data->{'offset'}) / $limit;

        $result = $this->db->query("SELECT eh.id,eh.contact_name,GROUP_CONCAT(ehn.contact_no SEPARATOR ', ') AS contact_no,eh.logo,
                                    eh.date_created 
                                    FROM emergency_hotline eh 
                                    LEFT JOIN emergency_hotline_num ehn ON eh.id = ehn.hotline_id
                                    WHERE eh.is_deleted <> 1
                                    GROUP BY eh.id $having_by ORDER BY eh.contact_name ASC LIMIT $offset,$limit");
        $this->send($total,$result);
    }

    private function _list_hotline() {
        $this->render("index","hotline/list","AGC Employee Locator | CMS");
    }

    private function _add_hotline() {
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);
            $post_data = $request->get_data();
            $post_data['logo'] = DEFAULT_CONTACT_PHOTO;

            $this->hotline = $this->db->create("emergency_hotline");
            $this->hotline->date_created = $this->get_current_date();
            $this->hotline->created_by = $this->get_current_user();
            $this->error_message = "";

            $landline = array();
            $mobile = array();

            $error_count = 0;

            if(isset($post_data['contact_no'])) {
                foreach ($post_data['contact_no'] as $value) {
                    if(!ctype_digit($value)){
                        $error_count++;
                    }
                }
            } else {
                $error_count++;
            }

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->hotline->$key = $value;
                $this->log_insert($key,$this->hotline->$key);
            },function($error_type,$key) {
                if ($error_type == Form::ERROR_VALIDATION_FAILED) {
                    $this->error_message.= "'$key' is required<br>\n";
                } else if ($error_type == Form::ERROR_ID_NOT_SET) {
                    $this->error_message.= "'$key' is required<br>\n";
                }
            });

            if ($error_count == 0) {
                $contact_seek = $this->db->create("emergency_hotline");
                $contact_seek->id = 0;

                $result = $this->db->query($contact_seek,array("contact_name" => $this->hotline->contact_name));
                if (count($result) > 0) {
                    $this->error_message = "Contact already exists";
                    $error_count++;
                }
            }

            if ($error_count == 0) {
                if ($this->upload->is_uploaded("image")) {
                    $this->upload->allowed(array("jpg","jpeg","png"));

                    $config = array(
                        "upload_dir" => BASEPATH . "assets/images/logo/",
                        "file_type" => Upload::TYPE_IMAGE,
                        "sizes" => array(64 => 64,128 => 128,256 => 256)
                    );

                    if (!$this->upload->do_upload("image",$config)) {
                        $error_count++;
                        $ths->error_message = "Cannot upload logo";
                    } else {
                        $this->hotline->logo = BASE_URL . "assets/images/logo/" . $this->upload->get_uploaded_filename();
                    }
                }
            }

            if ($error_count == 0) {
                foreach ($post_data['contact_no'] as $value) {
                    $result = $this->db->query("SELECT num.contact_no FROM emergency_hotline em 
                                    INNER JOIN emergency_hotline_num num ON num.hotline_id = em.id 
                                    WHERE em.is_deleted <> 1 AND num.contact_no = ". $value);
                    if (count($result) > 0) {
                        $this->error_message.= "Contact already exists: ". $value . "\n";
                        $error_count++;
                    }
                }
            }

            if ($error_count == 0) {
                $this->log_close();
                $result = $this->db->insert($this->hotline);
                $hotline_id = $this->db->getLastInsertId();

                foreach ($post_data['contact_no'] as $value) {
                    $numbers = $this->db->create("emergency_hotline_num");
                    $numbers->hotline_id = $hotline_id;
                    $numbers->contact_no = $value;
                    $this->db->insert($numbers);
                }
                print json_encode(array("response_code" => 0,"response_msg" => "Record added successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_message));
            }
        } else {
            $this->render("index","hotline/add","AGC Employee Locator | CMS");
        }
    }

    private function _edit_hotline($id) {
        if ($this->input->request_method('POST')) {
            $request = $this->input->get_data(Input::POST);
            $post_data = $request->get_data();

            //$post_data['logo'] = DEFAULT_CONTACT_PHOTO;

            $this->original_hotline = $this->db->create("emergency_hotline");
            $this->original_hotline->logo = "";
            //$this->original_hotline->contact_no = "";
            $this->original_hotline->contact_name = "";

            $result = $this->db->query($this->original_hotline,array("id" => $id));
            if (count($result) > 0) {
                $this->original_hotline = $result[0];
            }

            $this->hotline = $this->db->create("emergency_hotline");
            $this->hotline->date_modified = $this->get_current_date();
            $this->hotline->modified_by = $this->get_current_user();

            $this->error_message = "";

            $error_count = $this->form->validate($this->post_data_keys,$post_data,function($key,$value) {
                $this->hotline->$key = $value;
                if ($this->hotline->$key != $this->original_hotline->$key) {
                    $this->log_insert($key,$this->hotline->$key);
                }
            },function($error_type,$key) {
                if ($error_type == Form::ERROR_VALIDATION_FAILED) {
                } else if ($error_type == Form::ERROR_ID_NOT_SET) {
                }
            });

            if(isset($post_data['contact_no'])) {
                foreach ($post_data['contact_no'] as $value) {
                    if(!ctype_digit($value)){
                        $error_count++;
                    }
                }
            } else {
                $error_count++;
            }

            if ($error_count == 0) {
                if ($this->upload->has("image")) {
                    $upload_state = $this->upload->get_error("image");

                    if (!$upload_state) {
                        $this->upload->allowed(array("jpg","jpeg","png"));

                        $config = array(
                            "upload_dir" => BASEPATH . "assets/images/logo/",
                            "file_type" => Upload::TYPE_IMAGE,
                            "sizes" => array(64 => 64,128 => 128,256 => 256)
                        );

                        if (!$this->upload->do_upload("image",$config)) {
                            $this->error_message = "Could not upload photo";
                            $error_count++;
                        } else {
                            $this->hotline->logo = BASE_URL . "assets/images/logo/" . $this->upload->get_uploaded_filename();
                        }
                    }
                }
            }    

            if ($error_count == 0) {
                foreach ($post_data['contact_no'] as $value) {
                    $result = $this->db->query("SELECT num.contact_no FROM emergency_hotline em 
                                    INNER JOIN emergency_hotline_num num ON num.hotline_id = em.id 
                                    WHERE em.is_deleted <> 1 AND em.id <> ".$id." AND num.contact_no = ". $value);
                    if (count($result) > 0) {
                        $this->error_message.= "Contact already exists: ". $value . "\n";
                        $error_count++;
                    }
                }
            }

            if ($error_count == 0) {
                $this->log_close();
                $result = $this->db->update($this->hotline,array("id" => $id));

                $this->db->update("DELETE FROM emergency_hotline_num WHERE hotline_id = " .$id);

                foreach ($post_data['contact_no'] as $value) {
                    $numbers = $this->db->create("emergency_hotline_num");
                    $numbers->hotline_id = $id;
                    $numbers->contact_no = $value;
                    $this->db->insert($numbers);
                }

                print json_encode(array("response_code" => 0,"response_msg" => "Record updated successfully!"));
            } else {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_message));
            }
        } else {
            if (ctype_digit($id)) {
                $contact = $this->db->create("emergency_hotline");
                $contact->contact_name = "";
                $contact->contact_no = "";
                $contact->logo = "";

                $result = $this->db->query($contact,array("id" => $id));

                if (count($result) > 0) {
                    $fields = array("contact_name","contact_no","logo");
                    foreach ($fields as $field) {
                        $this->set("$field", isset($result[0]->$field) ? $result[0]->$field : "");
                    }
                }

                $hotline = $this->db->create("emergency_hotline_num");
                $hotline->contact_no = "";

                $result = $this->db->query($hotline,array("hotline_id" => $id));
                $number_list = "";
                $number_data = "";

                if (count($result) > 0) {
                    foreach ($result[0] as $key) {
                        $number_list.= "<input type=\"number\" class=\"form-control\" name=\"contact_no[]\" placeholder=\"Contact No.\" value=\"$key\" required>";
                    }

                    array_shift($result);
                    $number_sep = "";
                    foreach ($result as $data) {
                        $number_data.=$number_sep.json_encode($data);
                        $number_sep = ",";
                    }
                }

                $this->set("number_list",$number_list);
                $this->set("number_data",$number_data);
                
                $this->set("id",$id);

                $this->render("index","hotline/edit","AGC Employee Locator | CMS");
            }
        }
    }

    private function _delete($id) {
        $error = true;
        if (ctype_digit($id)) {
            $contact = $this->db->create("emergency_hotline");
            $contact->is_deleted = 1;
            $contact->deleted_by = $this->get_current_user();
            $contact->date_deleted = $this->get_current_date();

            $this->db->update($contact,array("id" => $id));

            $log_action = "Deleted from contact id " . $id;

            $this->log_action(3,"insert",$log_action);
            $error = false;
        }
        if ($error) {
            print json_encode(array("response_code" => 1,"response_msg" => "Record could not be deleted!"));
        } else {
            print json_encode(array("response_code" => 0,"response_msg" => "Record deleted successfully!"));
        }    
    }

    private function _export_data() {
        $headers = array("id",
                         "contact_name",
                         "contact_no");

        $this->export_data("contact-", $headers,"SELECT eh.id,eh.contact_name,GROUP_CONCAT(ehn.contact_no SEPARATOR ', ') 
                                                AS contact_no FROM emergency_hotline eh 
                                                LEFT JOIN emergency_hotline_num ehn ON eh.id = ehn.hotline_id
                                                WHERE eh.is_deleted <> 1
                                                GROUP BY eh.id ORDER BY eh.id ASC");
    }

    private function _bulk_upload() {
        if ($this->input->request_method('POST')) {
            set_time_limit(600);
            $post_data = $this->input->get_data(Input::POST)->get_data();
            $this->error_data = "";
            if ($this->upload->is_uploaded("file")) {
                $this->upload->allowed(array("csv"));

                $file_path = BASEPATH . "assets/uploads/bulk/";
                //$file_path = BASEPATH . "bulk/";


                $config = array(
                    "upload_dir" => $file_path,
                    "file_type" => Upload::TYPE_BINARY
                );

                $original_file_name = $this->upload->get_name("file");

                if ($this->upload->do_upload("file",$config)) {
                    $file_name = $this->upload->get_uploaded_filename();

                    $csv_data = $this->helper->get_csv($file_path.$file_name,false);

                    $action  = $post_data["action"];

                    $file = $this->db->create("files");
                    $file->date_uploaded = $this->get_current_date();
                    $file->uploaded_by = $this->get_current_user();
                    $file->flag = 2;
                    $file->action = $action;
                    $file->file_name = $original_file_name;
                    $file->file_path = $file_path . $file_name;
                    $file->file_size = filesize($file->file_path);
                    $file->hash_value = sha1_file($file->file_path);
                    $file->section = "company";

                    $this->db->insert($file);

                    $this->row = 0;

                    $this->post_data_keys = array(
                        "contact_name" => array(
                            "required" => true,
                            "type" => Form::TYPE_TEXT,
                            "extras" => array(
                                "min" => 1,
                                "max" => 64
                            )
                        )
                        /*,
                        "contact_no" => array(
                            "required" => true,
                            "type" => Form::TYPE_NUMBER,
                            "extras" => array()
                        )
                        */
                    );

                    foreach ($csv_data as $data) {
                        ++$this->row;
                        $this->hotline = $this->db->create("emergency_hotline");
                        $this->hotline_numbers = "";
                        $this->hotline_numbers_list = array();
                        
                        if ($action == "add") {
                            $this->log_open(1,"insert","bulk upload");
                            $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                if ($key != "id") {
                                    $this->hotline->$key = utf8_encode($value);
                                    $this->log_insert($key,$this->hotline->$key);
                                }
                            }, function ($error_type,$key) {
                                $this->error_data .= "Unable to add record at row #$this->row: ";
                                if ($error_type == Form::ERROR_ID_NOT_SET) {
                                    $this->error_data .= " missing required field '$key'<br>\n";
                                } else {
                                    $this->error_data .= " could not validate '$key' field<br>\n";
                                }
                            });

                            if ($error_count == 0) {
                                if (!isset($data['contact_no'])){
                                    $error_count++;
                                } else {
                                    $this->hotline_numbers = $data['contact_no'];
                                    $this->hotline_numbers_list = explode(",",$this->hotline_numbers);
                                    foreach ($this->hotline_numbers_list as $key => $value) {
                                        if(empty($value)) {
                                            $error_count++;
                                            $this->error_data .= "Contact no is required row #$this->row";  
                                        } else {
                                            if(!is_numeric($value)) {
                                                $error_count++;
                                                $this->error_data .= "Invalid data for contact_no row #$this->row: $value\n";
                                            }
                                        }
                                    }
                                }
                            }

                            if ($error_count == 0) {
                                //$result = $this->db->query("SELECT id FROM emergency_hotline WHERE (contact_name = " . $this->db->quote($this->hotline->contact_name) . " OR contact_no = " . $this->db->quote($this->hotline->contact_no) . ") AND is_deleted = 0 LIMIT 0,1");
                                $result = $this->db->query("SELECT id FROM emergency_hotline WHERE contact_name = ".$this->db->quote($this->hotline->contact_name)." AND is_deleted = 0 LIMIT 0,1");
                                if (count($result) > 0) {
                                    $this->error_data.= "Unable to add row #" . $this->row . ": record already exists<br>\n";
                                } else {
                                    $this->hotline->date_created = $this->get_current_date();
                                    $this->hotline->created_by = $this->get_current_user();
                                    $this->hotline->logo = DEFAULT_CONTACT_PHOTO;
                                    $this->db->insert($this->hotline);
                                    $last_id = $this->db->getLastInsertId();

                                    foreach ($this->hotline_numbers_list as $key => $value) {
                                        $this->hotline_numbers = $this->db->create("emergency_hotline_num");
                                        $this->hotline_numbers->hotline_id = $last_id;
                                        $this->hotline_numbers->contact_no = trim($value);
                                        $this->db->insert($this->hotline_numbers);
                                    }
                                }
                            }
                        } else if ($action == "update") {
                             if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"update","bulk upload");
                                $error_count = $this->form->validate($this->post_data_keys,$data,function($key,$value) {
                                    if ($key != "id") {
                                        $this->hotline->$key = utf8_encode($value);
                                        $this->log_changes($key,$this->hotline->$key,$this->hotline->$key);                                    
                                    }
                                }, function ($error_type,$key) {
                                    $this->error_data .= "Unable to update record at row #$this->row: ";
                                    if ($error_type == Form::ERROR_ID_NOT_SET) {
                                        $this->error_data .= " missing required field '$key'<br>\n";
                                    } else {
                                        $this->error_data .= " could not validate '$key' field<br>\n";
                                    }
                                });

                                if ($error_count == 0) {
                                    if (!isset($data['contact_no'])){
                                        $error_count++;
                                    } else {
                                        $this->hotline_numbers = $data['contact_no'];
                                        $this->hotline_numbers_list = explode(",",$this->hotline_numbers);
                                        foreach ($this->hotline_numbers_list as $key => $value) {
                                            if(empty($value)) {
                                                $error_count++;
                                                $this->error_data .= "Contact no is required row #$this->row";  
                                            } else {
                                                if(!is_numeric($value)) {
                                                    $error_count++;
                                                    $this->error_data .= "Invalid data for contact_no row #$this->row: $value\n";
                                                }
                                            }
                                        }
                                    }
                                }

                                if ($error_count == 0) {
                                    $result = $this->db->query("SELECT id FROM emergency_hotline WHERE id = " . intval($data["id"]));
                                    if (count($result) > 0) {
                                        $this->db->update($this->hotline,array("id" => $data["id"]));
                                        if ($this->db->update("DELETE FROM emergency_hotline_num WHERE hotline_id=".$data["id"])) {
                                            foreach ($this->hotline_numbers_list as $key => $value) {
                                                $this->hotline_numbers = $this->db->create("emergency_hotline_num");
                                                $this->hotline_numbers->hotline_id = $data["id"];
                                                $this->hotline_numbers->contact_no = trim($value);
                                                $this->db->insert($this->hotline_numbers);
                                            }
                                        }
                                    } else {
                                        $this->error_data.= "Unable to update row #" . $this->row . ": no matching record found for '$this->hotline->id'<br>\n";                                    
                                    }
                                }
                            }
                        } else if ($action == "delete") {
                            if (isset($data["id"]) && ctype_digit($data["id"])) {
                                $this->log_open(1,"delete","bulk upload");
                                $this->hotline->is_deleted = 1;
                                $this->hotline->date_deleted = $this->get_current_date();
                                $this->db->update($this->hotline,array("id" => $data["id"]));
                                $this->log_close();
                            } else {
                                $this->error_data.= "Unable to delete record at row #" . $this->row . " missing required field id<br>\n";
                            }
                        }
                    }
                }
            }

            if (!empty($this->error_data)) {
                print json_encode(array("response_code" => 1,"response_msg" => $this->error_data));
            } else {
                print json_encode(array("response_code" => 0,"response_msg" => "File uploaded successfully!"));
            }
        } else {
            $result = $this->db->query("SELECT f.file_name,f.section,f.action,f.date_uploaded,u.username AS uploaded_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.uploaded_by                                         
                                        WHERE f.flag = 2
                                        ORDER BY f.date_uploaded DESC");
            $download_list = "";
            $upload_list = "";
            if (count($result) > 0) {
                $upload_list.= "<tbody>";
                foreach ($result as $data) {
                    $upload_list.= "<tr>";
                    $upload_list.= "<td>$data->file_name</td>";
                    $upload_list.= "<td>$data->date_uploaded</td>";
                    $upload_list.= "<td>$data->section</td>";
                    $upload_list.= "<td>$data->action</td>";
                    $upload_list.= "<td>$data->uploaded_by</td>";
                    $upload_list.= "<td>$data->hash_value</td>";
                    $upload_list.= "</tr>";
                }
                $upload_list.= "</tbody>";
            }

            $result = $this->db->query("SELECT f.file_name,f.date_created,u.username AS created_by,f.hash_value FROM files AS f
                                        LEFT JOIN users AS u ON u.id = f.created_by                                        
                                        WHERE f.flag = 1
                                        ORDER BY f.date_created DESC");
            if (count($result) > 0) {
                $download_list .= "<tbody>";
                foreach ($result as $data) {
                    $download_list.= "<tr>";
                    $download_list.= "<td>$data->file_name</td>";
                    $download_list.= "<td>$data->date_created</td>";
                    $download_list.= "<td>$data->created_by</td>";
                    $download_list.= "<td>$data->hash_value</td>";
                    $download_list.= "</tr>";
                }
                $download_list.= "</tbody>";
            }

            $this->set("upload_list",$upload_list);
            $this->set("download_list",$download_list);

            $this->render("index", "hotline/upload", "AGC Employee Locator | CMS");
        }
    }
}