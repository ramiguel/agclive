<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Dashboard extends Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library(array("uri","security","themes","iospushnotif","androidpushnotif","input","twitterapi","timeago"));
		$this->load->model("dashboards");
    }

    public function request_handler($request_method,$request_params) {
		$this->security->checkpoint($this,"/dashboard/" . $request_method);
		
        if ($request_method == "home") {
            $this->_home();
        } else if ($request_method == "broadcast") {
			$this->_broadcast();
		} else if ($request_method == "get_last_checkin") {
			$this->_get_last_checkin();
		} else if ($request_method == "get_max_checkin") {
			$this->_get_max_checkin();
		} else if ($request_method == "get_emp_count") {
			$this->_get_emp_count();
		} else if ($request_method == "get_emergency") {
			$this->_get_emergency();
		} else if ($request_method == "search_emp_db") {
			$this->_search_emp_db();
		} else if ($request_method == "change_status") {
			$this->_change_status();
		} else if ($request_method == "get_twitter_feed") {
			$this->_get_twitter_feed();
		} else if ($request_method == "get_incident_report") {
			$this->_get_incident_report();
		} else if ($request_method == "post_filter") {
			$this->_filter();
		} else if ($request_method == "disaster_mode") {
			$this->_disaster_mode();
		} else if ($request_method == "archive") {
			$this->_archive();
		} else if($request_method == "get_latest_disaster") {
			$this->_get_latest_disaster();
		} else if($request_method == "twitter_feed_interval") {
			$this->_twitter_feed_interval();
		} else if($request_method == "post_twitter_feed") {
			$this->_post_twitter_feed();
		} else if ($request_method == "sms-broadcast") {
            $this->_sms_broadcast();
        } else if($request_method == "get_disaster_company") {
			$this->_get_disaster_company();
		} else {
            if ($request_method == "") {
            } else {
                error_page();
            }
        }
    }

    private function _archive() {
    	$response = create_response(1, "Could not archive");

    	$result = $this->dashboards->archive();

    	if($result) {
    		$response = create_response(0, "Success");
    		$response->data = $result;
    	}

    	notify($response);
    }

    private function _filter() {
    	$request = $this->input->get_data(Input::POST);
    	$response = create_response(1, "Could not get data");
		
		//$get_latest_disaster = $this->dashboards->get_last_disaster();
		
		// if($get_latest_disaster[0]->{'disaster_mode'} == '0') {
		// 	$result = 'disaster_mode_off';
			
		// 	$response = create_response(0, "Success");
		// 	$response->data = $result;
		// } else {
			if ($request->isValid()) {					
				$data = $request->data;

				if ($data['filter_type'] == "employee") {
					$result = $this->dashboards->filter_signins($data);
				} else if ($data['filter_type'] == "report") {
					$result = $this->dashboards->filter_incidents($data);
					foreach($result as $key=>$value) {
						$date = $value->{'date_reported'}; //date from db
						$days = $this->timeago->input_date($date);
						$result[$key]->timeago = $days;
					}
				} else if ($data['filter_type'] == "incident") {
					$result = $this->dashboards->filter_broadcasts($data);
				}
				
				
				$response = create_response(0, "Success");
				$response->data = $result;
			}
		//}
		
		notify($response);
    }
	
	private function _post_filter() {
		$input = $this->input->get_data(Input::POST);
		$response = create_response(1, "Could not get data");
		
		if($input->{'data'}['from'] == 'employee') {
			$data = $input->{'data'};

			$company = isset($data['company']) ? $data['company'] : 0;
			$status = isset($data['status']) ? $data['status'] : 0;			
			$datefrom = isset($data['datefrom']) ? $data['datefrom'] : "";
			$dateto = isset($data['dateto']) ? $data['dateto'] : "";
			
			$result = array();
			
			if($company > 0) { // by company
				if($status > 0) { // by status
						$result = '1';
				} else { // all status
						$result = '2';
				}
			} else { // all company
				if($status > 0) { // by status
						$result = '3';
				} else { // all status
						$result = '4';
				}
			}
			var_dump($result);
			
		} else if($input->{'data'}['from'] == 'report') {
			if($input->{'data'}['datefrom'] != '' && $input->{'data'}['company'] != '' && $input->{'data'}['status'] != '') {
				echo '1';
			} else if($input->{'data'}['datefrom'] != '' && $input->{'data'}['company'] != '') {
				echo '2';
			} else if($input->{'data'}['company'] != '' && $input->{'data'}['status'] != '') {
				echo '3';
			} else if($input->{'data'}['datefrom'] != '' && $input->{'data'}['status'] != '') {
				echo '4';
			} else {
				echo '5';
			}
		} else if($input->{'data'}['from'] == 'incident') {
			if($input->{'data'}['datefrom'] != '' && $input->{'data'}['company'] != '') {
				echo '1';
			} else {
				echo '2';
			}
		}
	}
	
	private function _get_incident_report() {
		$response = create_response(1,"Could not get data");
		$result = $this->dashboards->get_incident_report();
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		}
		
		notify($response);
	}
	
	private function _get_twitter_feed() {
		$data = $this->input->get_data(Input::POST)->get_data();
		$screen_names = array();
		if (isset($data["screen_name"])) {
			$tweet = new sdgClass();
			$tweet->screen_name= $data["screen_name"];
            $screen_names[] = $tweet;
		} else {
		    $screen_names = $this->dashboards->get_screen_name();
	    }
		$result = $this->twitterapi->get_timeline_feeds($screen_names);
		
		$this->db->update("TRUNCATE TABLE twitter_feed_cmdctr");
		
		foreach($result as $feeds) {
			foreach($feeds as $value) {
				$twitter = $this->db->create("twitter_feed_cmdctr");
				$twitter->profile_image = $value->user->profile_image_url;
				$twitter->user_name = $value->user->name;
				$twitter->created_at = date('Y-m-d H:i:s', strtotime($value->created_at));
				$twitter->screen_name = $value->user->screen_name;
				$twitter->text = $value->text;
				
				$this->db->insert($twitter);
			}
		}
		
		$query = $this->dashboards->get_twitter_feed_cmdctr($data);
		echo json_encode($query);
	}
	
	private function _change_status() {
		$input = $this->input->get_data(Input::POST);
		$response = create_response(1, "Could not get data");
		$result = $this->dashboards->post_change_status($input);
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		}
		notify($response);
	}
	
	private function _search_emp_db() {
		$input = $this->input->get_data(Input::POST);
		$response = create_response(1, "Could not get data");
		$result = $this->dashboards->search_emp_db($input);
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		}
		
		notify($response);
	}
	
	private function _get_emergency() {
		$data = $this->input->get_data(Input::POST)->get_data();
		$response = create_response(1,"Could not get data");
		$result = $this->dashboards->get_emergency($data);
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		}		
		
		notify($response);
	}
	
	private function _get_emp_count() {
		$response = create_response(1,"Could not get data");
		$result = $this->dashboards->get_emp_count();
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		}
		
		notify($response);
	}
	
	private function _get_last_checkin() {
		$input = $this->input->get_data(Input::POST);
		$result = $this->dashboards->get_last_checkin($input);
		echo json_encode($result);
	}
	
	private function _get_max_checkin() {
		$result = $this->dashboards->get_max_checkin();
		echo json_encode($result);
	}

    private function _home() {
        $this->themes->set('title','AGC Locator | Dashboard');
        $this->themes->set('base_url',base_url());
		
		$user_id = $this->session->userdata("user_id");
		
		//$checkins = $this->dashboards->get_checkin();
		$checkins = array();
		$reportin = $this->dashboards->get_reportin();
		$companies = $this->dashboards->get_company_list();
		$incident = $this->dashboards->get_incident($user_id);
		$status = $this->dashboards->get_status();
		$current_disaster = $this->dashboards->get_current_disaster();
		$screen_names = $this->dashboards->get_screen_name();
		$disaster_type = $this->dashboards->get_disaster_type();
		$disaster = $this->dashboards->get_disaster();
		
		$emp_list = "";
		$reportin_list = "";
		$company_list = "";
		$incident_list = "";
		$status_list = "";
		$screen_name_list = "";
		$current_disaster_list = "";
		$disaster_type_list = "";
		$disaster_list = "";
		
		foreach($disaster as $data) {
			$data->disaster_name = str_replace(array("'", "\""), "&apos;", htmlspecialchars($data->disaster_name));
			$disaster_list .= '<option value="'. $data->id .'">'. $data->disaster_name .'</option>';
		}
		
		foreach($disaster_type as $data) {
			$disaster_type_list .= '<option value="'. $data->id .'">'. $data->type .'</option>';
		}

		foreach($screen_names as $data) {
			$screen_name_list .= '<option value="'. $data->screen_name .'">'. $data->screen_name .'</option>';
		}
		
		foreach($status as $data) {
			if ($data->id == 0) {
				$data->id = "nostatus";
			}
			$status_list .= '<option value="'. $data->id .'">'. $data->description .'</option>';
		}
		
		foreach($incident as $data) {
			$incident_list .= '<tr><td><div class="name">'. $data->name .'</div><div class="incident">'. $data->message .'</div></td></tr>';
		}
		
		foreach($companies as $data) {
			$company_list .= '<option value="'. $data->id .'">'. $data->company_name .'</option>';
		}
		
		foreach($reportin as $data) {
			$reportin_list .= '<tr><td><div class="name"><strong>'. $data->emp_name .'</strong></div><div><small>'. $data->location .'</small></div><div><small>'. $this->timeago->input_date($data->date_reported) .'</small></div><div class="incident">'. $data->message .'</div></td><td class="status-cell"></td></tr>';
		}
		
        foreach ($checkins as $data) {
			$emp_list .= '<tr id="'. $data->id .'"><td class="image-container employee"><div><img src="' . $data->img_path . '" alt></div></td><td>'. $data->emp_name .'</td><td><div class="employee-status status-'. $data->status .'">'. $data->status .'</div></td><td class="status-cell"><div class="dropdown"><button id="dLabel" type="button" class="btn btn-toggledown btn-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button><ul class="dropdown-menu dropdown dropdown-emp-db" role="menu" aria-labelledby="dLabel"><li class="status-safe" value="1">safe</li><li class="status-threat" value="2">threat</li><li class="status-urgent" value="3">urgent</li><li class="status-nostatus" value="0">no status</li></ul></div></td></tr>';
        }
		
		foreach($current_disaster as $data) {
			$data->disaster_name = str_replace(array("'", "\""), "&apos;", htmlspecialchars($data->disaster_name));
			$data->disaster_type = str_replace(array("'", "\""), "&apos;", htmlspecialchars($data->type));
            $data->disaster_details = str_replace(array("'", "\""), "&apos;", htmlspecialchars_decode($data->disaster_details));
            $data->disaster_details = str_replace(array("\n"), " ", htmlspecialchars_decode($data->disaster_details));

			$current_disaster_list .= "<tr id=\"$data->id\">";
				$current_disaster_list .= "<td>". $data->disaster_name ."</td>";
				$current_disaster_list .= "<td>". $data->type ."</td>";
				$current_disaster_list .= "<td>". $data->disaster_details ."</td>";
				$current_disaster_list .= "<td>". $data->date_created ."</td>";
				$current_disaster_list .= "<td>";
					$current_disaster_list .= "<button type=\"button\" class=\"fa fa-search view-current-disaster\"></button>";
					$current_disaster_list .= "<button type=\"button\" class=\"fa fa-times delete-current-disaster\" id=\"$data->id\" data-toggle=\"modal\" data-target=\"#confirmDelete\"></button>";
				$current_disaster_list .= "</td>";
			$current_disaster_list .= "</tr>";
		}

		$this->themes->set("screen_names", $screen_name_list);
		$this->themes->set("company", $company_list);
		$this->themes->set("emp", $emp_list);
		$this->themes->set("reportin", $reportin_list);
		$this->themes->set("incident", $incident_list);
		$this->themes->set("status", $status_list);
		$this->themes->set("date", date("Y-m-d"));
		$this->themes->set("current_disaster", $current_disaster_list);
		$this->themes->set("disaster_type", $disaster_type_list);
		$this->themes->set("disaster", $disaster_list);

        $this->themes->layout("dashboard");
        $this->themes->template("dashboard/home");
        $this->themes->render();
    }
	
	const DEVICE_IOS = 1;
	const DEVICE_ANDROID = 2;
	
	private function _broadcast() {
		$input = $this->input->get_data(Input::POST);
		$result = $this->dashboards->post_broadcast($input);
		
		if(isset($input->data['sites']) || $input->data['company_id'] == 0) {
			$emp_ids_array = array();
			$notifId = $this->dashboards->get_last_broadcast_id();

			foreach ($result as $key => $value) {
				$emp_ids_array['emp_id'][] = $value->{'id'};
				$this->dashboards->save_to_notif_is_read($notifId, $value->{'id'});
			}
			
			$deviceKeys = $this->dashboards->get_device_keys($emp_ids_array["emp_id"]);

			if(!empty($deviceKeys)) {
				
				$payload = array(
					1 => array(),
					2 => array()
				);
				
				$message_template = array(
					'message' => $input->data['msg'],
					'flag' => 'Command Center',
					'notif_id' => $notifId,
					'user_id' => 0
				);
				
				// $count = $this->dashboards->get_notif_count();
				// $count = 0;
				
				foreach ($deviceKeys as $device) {
					$payload_message = $message_template;
					$payload_message['user_id'] = $device->{'owner'};
				
					if (in_array($device->{'os_type'},array(1,2))) {
						$payload_data = new stdClass();
						$payload_data->message = $payload_message;
						$payload_data->device_key = $device->{'device_key'};
						
						$payload[$device->{'os_type'}][] = $payload_data;
						
						//$result = $this->dashboards->save_to_notif_is_read($notifId, $device->{'owner'});
					}
				}
				
				if (count($payload[1]) > 0) {
					$this->iospushnotif->pushBroadcast_New($payload[1],$count[0]->{'count'});
				}
					
			    if (count($payload[2]) > 0) {
					$this->androidpushnotif->pushBroadcast($payload[2]);
			    }
				
				echo "Success";
				
			} else {
				echo "Empty";
			}
		} else {
			echo 'Failed';
		}
	}
	
	private function _disaster_mode() {
		$request = $this->input->get_data(Input::POST);
		$response = create_response(1, "Could not get data");
		
		if($request->data['type'] == 'view') {
			if($request->data['mode'] == 'filter' || $request->data['mode'] == 'search') {
				$result = $this->dashboards->view_current_disaster($request);
			
				if($result) {
					$response = create_response(0, "Success");
					$response->data = $result;
				}
				
				foreach($response->data as $data) {
					$data->disaster_details = str_replace(array("\n"), " ", htmlspecialchars_decode($data->disaster_details));
				}
				
				notify($response);
			} else {
				$result = $this->dashboards->view_current_disaster($request);

				$array = array();

				//print_r($result);

				if(count($result) > 0) {
					$array["id"] = $result[0]->{"id"};
					$array["disaster_name"] = $result[0]->{"disaster_name"};
					$array["disaster_details"] = str_replace(array("\n"), " ", htmlspecialchars_decode($result[0]->{"disaster_details"}));
					$array["disaster_type"] = $result[0]->{"disaster_type"};
					$array["type"] = $result[0]->{"type"};
					$array["date_created"] = $result[0]->{"date_created"};
					$array["date_closed"] = $result[0]->{"date_closed"};
				}

				$company_array = array();
				$count = 0;
				foreach ($result as $key => $value) {
					$company_id = $result[$key]->{"company_id"};
					$site_id = $result[$key]->{"site_id"};
					$site = $result[$key]->{"site"};

					if (!in_array($company_id, $company_array)) {
						$company_array[] = $company_id;

						$array["companies"][$count]["company_id"] = $company_id;
						$array["companies"][$count]["company_name"] = $result[$key]->{"company_name"};

						$site_array = array();
						$site_array["site_id"] = $site_id;
						$site_array["site"] = $site;

						$array["companies"][$count]["sites"][] = $site_array;
						
						$count++;
					} else {
						$site_array = array();
						$site_array["site_id"] = $site_id;
						$site_array["site"] = $site;

						foreach ($array["companies"] as $keyx => $valuex) {
							if ($valuex["company_id"] == $company_id) {
								$array["companies"][$keyx]["sites"][] = $site_array;
							}
						}
					}
				}
				//print_r($company_array);
				//print_r($array);
				//return;

				if($result) {
					$response = create_response(0, "Success");
					$response->data = $array;
				}
				
				// foreach($response->data as $data) {
				// 		$data->disaster_details = str_replace(array("\n"), " ", htmlspecialchars_decode($data->disaster_details));
				// }
				
				notify($response);
			}
		} else if($request->data['type'] == 'edit') {
			$result = $this->dashboards->edit_current_disaster($request);
			
			if($result) {
				$emp_ids_array = array();
				$notifId = $request->data['disaster_id'];

				foreach ($result as $key => $value) {
					$emp_ids_array['emp_id'][] = $value->{'id'};
					$this->dashboards->save_to_notif_is_read_disaster($notifId, $value->{'id'});
				}
				
				$deviceKeys = $this->dashboards->get_device_keys($emp_ids_array["emp_id"]);
				//$notifId = $this->dashboards->db->getLastInsertId();
					
				if(!empty($deviceKeys)) {
					$payload = array(
						1 => array(),
						2 => array()
					);
					
					$message_template = array(
						'message' => $request->data['name'],
						'flag' => 'Disaster',
						'notif_id' => $notifId,
						'user_id' => 0
					);
					
					// $count = $this->dashboards->get_notif_count($message_template);
					
					foreach($deviceKeys as $device) {
						$payload_message = $message_template;
						$payload_message['user_id'] = $device->{'owner'};
						
						if(in_array($device->{'os_type'},array(1,2))) {
							$payload_data = new stdClass();
							$payload_data->message = $payload_message;
							$payload_data->device_key = $device->{'device_key'};
							
							$payload[$device->{'os_type'}][] = $payload_data;
							
							// $result = $this->dashboards->save_to_notif_is_read_disaster($notifId, $device->{'owner'});
						}
					}
					
					if(count($payload[1] > 0)) {
						$this->iospushnotif->pushDeclareDisaster($payload[1], $count[0]->{'count'});
					}
					
					if(count($payload[2] > 0)) {
						$this->androidpushnotif->pushBroadcast($payload[2]);
					}
					echo "Success";
				} else {
					echo "Empty";
				}
			} else {
				echo 'Failed';
			}
		} else if($request->data['type'] == 'delete') {
			$result = $this->dashboards->delete_current_disaster($request);
			
			if($result) {
				$emp_ids_array = array();
				$notifId = $this->dashboards->db->getLastInsertId();

				foreach ($result as $key => $value) {
					$emp_ids_array['emp_id'][] = $value->{'id'};
					$this->dashboards->save_to_notif_is_read($notifId, $device->{'owner'});
				}
				
				$deviceKeys = $this->dashboards->get_device_keys($emp_ids_array["emp_id"]);
					
				if(!empty($deviceKeys)) {
					$payload = array(
						1 => array(),
						2 => array()
					);
					
					$message_template = array(
						'message' => $result[0]->{'disaster_name'},
						'flag' => 'Disaster',
						'notif_id' => $notifId,
						'user_id' => 0
					);
					
					// $count = $this->dashboards->get_notif_count($message_template);
					
					foreach($deviceKeys as $device) {
						$payload_message = $message_template;
						$payload_message['user_id'] = $device->{'owner'};
						
						if(in_array($device->{'os_type'},array(1,2))) {
							$payload_data = new stdClass();
							$payload_data->message = $payload_message;
							$payload_data->device_key = $device->{'device_key'};
							
							$payload[$device->{'os_type'}][] = $payload_data;
							
							//$result = $this->dashboards->save_to_notif_is_read($notifId, $device->{'owner'});
						}
					}
					
					if(count($payload[1] > 0)) {
						$this->iospushnotif->pushDeclareDisaster($payload[1], $count[0]->{'count'});
					}
					
					if(count($payload[2] > 0)) {
						$this->androidpushnotif->pushBroadcast($payload[2]);
					}
					echo "Success";
				} else {
					echo "Empty";
				}
			} else {
				echo 'Failed';
			}
		} else {
			$result = $this->dashboards->post_declare_disaster($request);
			$notifId = $this->dashboards->get_notifid_declare_disaster($request);
			
			if($result) {
				$emp_ids_array = array();
				foreach ($result as $key => $value) {
					$emp_ids_array['emp_id'][] = $value->{'id'};
					$this->dashboards->reset_emp_status($value->{'id'});
					$this->dashboards->save_to_notif_is_read_disaster($notifId[0]->{'disaster_id'}, $value->{'id'});
				}
				
				$deviceKeys = $this->dashboards->get_device_keys($emp_ids_array["emp_id"]);
					
				if(!empty($deviceKeys)) {
					$payload = array(
						1 => array(),
						2 => array()
					);
					
					$message_template = array(
						'message' => 'A disaster has been declared! Disaster Name: '. $request->data['name']. ' Disaster Type: '. $notifId[0]->{'type'},
						'flag' => 'Disaster',
						'notif_id' => $notifId[0]->{'disaster_id'},
						'user_id' => 0
					);
					
					// $count = $this->dashboards->get_notif_count($message_template);
					
					foreach($deviceKeys as $device) {
						$payload_message = $message_template;
						$payload_message['user_id'] = $device->{'owner'};
						
						if(in_array($device->{'os_type'},array(1,2))) {
							$payload_data = new stdClass();
							$payload_data->message = $payload_message;
							$payload_data->device_key = $device->{'device_key'};
							
							$payload[$device->{'os_type'}][] = $payload_data;
							
							//$result = $this->dashboards->save_to_notif_is_read_disaster($notifId[0]->{'disaster_id'}, $device->{'owner'});
						}
					}
					
					if(count($payload[1] > 0)) {
						$this->iospushnotif->pushDeclareDisaster($payload[1], $count[0]->{'count'});
					}
					
					if(count($payload[2] > 0)) {
						$this->androidpushnotif->pushBroadcast($payload[2]);
					}
					
					echo "Success";
				} else {
					echo "Empty";
				}
			} else {
				echo "Failed";
			}
		}
	}
	
	private function _get_latest_disaster() {
		$result = $this->dashboards->get_last_disaster();
		
		if($result) {
			$response = create_response(0, "Success");
			$response->data = $result;
		} else if(empty($result)) {
			$response = create_response(0, "Success");
			$response->data[0]['disaster_mode'] = 0;
		}
		
		notify($response);
	}
	
	private function _twitter_feed_interval() {
		$screen_names = $this->dashboards->get_twitter_interval();
		$this->twitterapi->post_twitter_interval($screen_names);
	}
	
	private function _post_twitter_feed() {
		$request = $this->input->get_data(Input::POST);
		$this->dashboards->insert_twitter_feed($request);
		$timenow = date("Y-m-d H:i");
		$query = $this->dashboards->delete_twitter_feed($timenow);
	}

    private function _sms_broadcast() {
        $input = $this->input->get_data(Input::POST);
        $result = $this->dashboards->post_broadcast($input);

        if($result) {
            echo "Success";
        } else {
            echo "Failed";
        }

    }
	
	private function _get_disaster_company() {
		$request = $this->input->get_data(Input::POST);
		$response = create_response(1, "Error");
    	$result = $this->dashboards->get_disaster_company($request);

    	if($result) {
    		$response = create_response(0, "Success");
    		$response->data = $result;
    	}

    	notify($response);
	}

    private function activity_insert($msg) {
        $this->activity = $this->db->create("activities");
        $this->activity->activity = $msg;
        $this->date_created = date('Y-m-d H:i:s');
        $this->created_by = $this->session->userdata("user_id");

        $this->db->insert($this->activity);

        $this->activity = null;
    }


}
