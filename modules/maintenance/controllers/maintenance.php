<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Maintenance extends Controller {
        public function __construct() {
            parent::__construct();

            $this->load->library(array("themes","server","db","session"));
        }

        public function index() {
                $this->server->maintenance_check($this,true);

        	$this->themes->set('title','AGC Locator | Welcome');
                $this->themes->set('base_url',base_url());

                $this->themes->layout("index");
                $this->themes->template("maintenance/home");
                $this->themes->render();
        }
}