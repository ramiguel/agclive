<?php

class GcashAccounts extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function getGcashAccountList() {
        return $this->db-query("SELECT id, gcash_account, employee_id, mobile_no
                    FROM gcash
                    WHERE is_deleted <> 1");
    }

    public function save_gcash($data) {
        $gcash = $this->db->create("gcash_donations");
        $gcash->employee_id = $data->{'employee_id'};
        $gcash->donate = $data->{'donate'};
        $gcash->date_donated = date('Y-m-d H:i:s');

        $result = $this->db->insert($gcash);

        return $result;
    }
}