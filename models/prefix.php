<?php

class Prefix extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function getAllPrefixes() {
        return $this->db->query("
                    SELECT number_range, start_msisdn,
                        end_msisdn, brand
                        FROM prefixes
                    ");
    }
}