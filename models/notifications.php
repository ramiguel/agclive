<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Notifications extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function checkUser($input) {
        $result = $this->db->query("SELECT is_deleted FROM employees WHERE id = ".$input->{'user_id'});
        $is_deleted = $result[0]->{'is_deleted'};
        return $is_deleted;
    }

    public function getNotif($input) {
        // need to edit queries must be administrator

        $company_site_id = $input->{'company_site_id'};
        $company_id = $input->{'company_id'};
        $broadcast_id = $input->{'broadcast_id'};
        $disaster_id = $input->{'disaster_id'};
        $offset_broadcast = "";
        $offset_disaster = "";
        $limit = 10;
        $has_reached_last_row = 0;
        $date_three_days_ago = date('Y-m-d', strtotime('-3 days', strtotime(date("Y-m-d"))));

        if ($broadcast_id > 0) {
            $offset_broadcast = " AND n.id < $broadcast_id ";
        }
        if ($disaster_id > 0) {
            $offset_disaster = " AND d.id < $disaster_id ";
        }

        $result = $this->db->query("SELECT 'broadcast' AS message_type, n.id AS notif_id, n.user_id, n.date_created, 
                                    n.message, n.status, CONCAT(u.first_name,' ',u.last_name) AS user_name,
                                    n.company_id,'' AS disaster_name,'' AS disaster_details,'' AS disaster_type,'' AS img_path
                                    FROM broadcasts AS n 
                                    LEFT JOIN users AS u ON u.id = n.user_id 
                                    WHERE n.id IN 
                                    (
                                        SELECT b.id FROM broadcasts b LEFT JOIN broadcast_sites bs ON b.id = bs.broadcast_id 
                                        WHERE bs.company_id = $company_id AND bs.site_id = $company_site_id
                                    ) $offset_broadcast 
                                    AND DATE(n.date_created) >= '$date_three_days_ago'
                                        UNION ALL
                                    SELECT 'disaster' AS message_type, d.id,d.created_by,d.date_created,'','',
                                    CONCAT(us.first_name,' ',us.last_name) AS user_name,'',
                                    d.disaster_name,d.disaster_details,dt.type,'' AS img_path FROM cmdctr d
                                    LEFT JOIN disaster_type dt ON dt.id = d.disaster_type
                                    LEFT JOIN users AS us ON us.id = d.created_by
                                    WHERE d.id IN
                                    (
                                        SELECT cc.id FROM cmdctr cc LEFT JOIN cmdctr_sites cs ON cc.id = cs.disaster_id 
                                        WHERE cs.company_id = $company_id AND cs.site_id = $company_site_id
                                    ) $offset_disaster
                                    AND DATE(d.date_created) >= '$date_three_days_ago'
                                    ORDER BY date_created DESC LIMIT $limit");



        $result_array['message'] = $result;


        $limit += 1;
        $result = $this->db->query("SELECT COUNT(message_type) AS count FROM (
                                    SELECT 'broadcast' AS message_type, n.id AS notif_id, n.user_id, n.date_created, 
                                    n.message, n.status, CONCAT(u.first_name,' ',u.last_name) AS user_name,
                                    n.company_id,'' AS disaster_name,'' AS disaster_details,'' AS disaster_type,'' AS img_path
                                    FROM broadcasts AS n 
                                    LEFT JOIN users AS u ON u.id = n.user_id 
                                    WHERE n.id IN 
                                    (
                                        SELECT b.id FROM broadcasts b LEFT JOIN broadcast_sites bs ON b.id = bs.broadcast_id 
                                        WHERE bs.company_id = $company_id AND bs.site_id = $company_site_id
                                    ) $offset_broadcast
                                    AND DATE(n.date_created) >= '$date_three_days_ago'
                                        UNION ALL
                                    SELECT 'disaster' AS message_type, d.id,d.created_by,d.date_created,'','',
                                    CONCAT(us.first_name,' ',us.last_name) AS user_name,'',
                                    d.disaster_name,d.disaster_details,dt.type,'' AS img_path FROM cmdctr d
                                    LEFT JOIN disaster_type dt ON dt.id = d.disaster_type
                                    LEFT JOIN users AS us ON us.id = d.created_by
                                    WHERE d.id IN
                                    (
                                        SELECT cc.id FROM cmdctr cc LEFT JOIN cmdctr_sites cs ON cc.id = cs.disaster_id 
                                        WHERE cs.company_id = $company_id AND cs.site_id = $company_site_id
                                    ) $offset_disaster
                                    AND DATE(d.date_created) >= '$date_three_days_ago'
                                    ORDER BY date_created DESC LIMIT $limit)test");

        if ($result[0]->count < $limit) {
            $has_reached_last_row = 1;
        }

        $result_array['has_reached_last_row'] = $has_reached_last_row;

        return $result_array;
    }
    
    public function getNotifCount($input) {
        //select only notifs which are not read yet
        $user_id = $input->{'user_id'};
        $result = $this->db->query("SELECT nis.notification_id FROM broadcast_status nis
                                    LEFT JOIN broadcasts n ON n.id = nis.notification_id
                                    WHERE nis.user_id = ".$user_id." AND is_read = 0");
        return $result;
    }

    public function getDisasterCount($data) {
        $user_id = $data->{'user_id'};
        $result = $this->db->query("SELECT notification_id AS disaster_id, disaster_name, type,disaster_details 
                                    FROM cmdctr_status 
                                    LEFT JOIN cmdctr ON cmdctr_status.notification_id = cmdctr.id
                                    LEFT JOIN disaster_type ON disaster_type.id = cmdctr.disaster_type
                                    WHERE user_id = ".$user_id." AND is_read = 0 AND cmdctr.disaster_mode = 1 
                                    ORDER BY cmdctr.id DESC");
        return $result;
    }

    public function getReportinCount($data) {
        //select only reportins which are not read yet
        $user_id = $data->{'user_id'};
        $result = $this->db->query("SELECT reportin_id FROM reportin_status WHERE user_id = ".$user_id." AND is_read = 0");
        return $result;
    }

    public function getReportIncidentCount($data) {
        //select only reportins which are not read yet
        $user_id = $data->{'user_id'};
        $result = $this->db->query("SELECT report_incident_id FROM report_incident_status WHERE user_id = ".$user_id." AND is_read = 0");
        return $result;
    }    

    public function isDisasterMode($data) {
        $user_id = $data->{'user_id'};
        $isDisaster = 0;

        $result = $this->db->query("SELECT company_id,site_id FROM employees WHERE id = ".$user_id);

        if (count($result) > 0) {
            $company_id = $result[0]->company_id;
            $site_id = $result[0]->site_id;

            $result = $this->db->query("SELECT c.id FROM cmdctr c
                                        LEFT JOIN cmdctr_sites cs ON cs.disaster_id = c.id
                                        WHERE c.disaster_mode = 1 AND cs.company_id = $company_id
                                        AND cs.site_id = $site_id");

            if (count($result) > 0) {
                $isDisaster = 1;
            }
        }
        return $isDisaster;
    }

    public function markAsRead($data) {
        $user_id = $data->{'user_id'};
        $notif_id = $data->{'cmd_ctr'};
        $disaster_id = $data->{'disaster'};
        $reportin_id = $data->{'report_incident'};
        $date_read = date("Y-m-d H:i:s");

        $notifList = "";
        foreach ($notif_id as $key => $value) {
            $notifList.= $value. ",";
        }

        $notifList = rtrim($notifList, ",");

        $disasterList ="";
        foreach ($disaster_id as $key => $value) {
            $disasterList.= $value. ",";
        }

        $disasterList = rtrim($disasterList, ",");

        $reportIncidentList = "";
        foreach ($reportin_id as $key => $value) {
            $reportIncidentList.= $value. ",";
        }

        $reportIncidentList = rtrim($reportIncidentList, ",");

        if($notifList == "") { 
            $notifList = "NULL";
        }

        if($disasterList == "") {
            $disasterList = "NULL";
        }

        if($reportIncidentList == "") {
            $reportIncidentList = "NULL";
        }

        $result = $this->db->update("UPDATE broadcast_status SET is_read = 1, date_read = '$date_read' WHERE notification_id IN (".$notifList.") AND user_id = ".$user_id);
        $result = $this->db->update("UPDATE cmdctr_status SET is_read = 1, date_read = '$date_read' WHERE notification_id IN (".$disasterList.") AND user_id = ".$user_id);
        $result = $this->db->update("UPDATE report_incident_status SET is_read = 1, date_read = '$date_read' WHERE report_incident_id IN (".$reportIncidentList.") AND user_id = ".$user_id);

        return $result;
    }
}