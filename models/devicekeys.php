<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Devicekeys extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function save_device_key($input) {
        $user_id = $input->{'user_id'};
        $device_key = $input->{'device_key'};
        $os_type = $input->{'os_type'};

        $sql = "SELECT COUNT(`owner`) AS `existing` FROM devices WHERE `owner` = ".$user_id;
        $existing = $this->db->query($sql);

        if($existing[0]->{'existing'} == 0) {
            $sql = "INSERT INTO devices(owner, device_key, os_type) VALUES(".$user_id.", ".$this->db->quote($device_key).", ".$os_type.")";
			
			$result = $this->db->insert($sql);
			// $devices = $this->db->create("devices");
			// $devices->owner = $user_id;
			// $devices->device_key = $device_key;
			// $devices->os_type = $os_type;

			// $result = $this->db->insert($devices);
        } else {
            $sql = "UPDATE devices SET device_key = ".$this->db->quote($device_key) . ", os_type =  ".$os_type." WHERE owner = ". $user_id;
			
			$result = $this->db->update($sql);
			// $devices = $this->db->create("devices");
			// $devices->owner = $input->{'user_id'};
			// $devices->device_key = $input->{'device_key'};
			// $devices->os_type = $input->{'os_type'};
			
            // $result = $this->db->update($devices, array("user_id"=>$input->{'user_id'}));
        }
        
        return $result;
    }
}