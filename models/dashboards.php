<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Dashboards extends Model {
    public function __construct() {
        parent::__construct();
    }

    private $last_broadcast_id = 0;
	
    public function filter_signins($data) {
		$where_array = "";
		$comp_filter = "";
		$status = "empty";
    	foreach ($data as $key => $value) {
    		if ($key != "filter_type" && ($value === 0 || $value != "")) {
    			$value = $this->db->quote($value);
    			if ($key == "status") {
    				$status = $value;
    				$where_array .= " AND ci.status = " . $value . " ";
    			} else if ($key == "company_id") {
    				$where_array .= " AND emp.company_id = " . $value . " ";
    				$comp_filter .= " AND emp.company_id = " . $value . " ";
    			}
    		}
    	}

    	if($status == "'0'") {
    		$sql = "SELECT 0 AS id, emp.id AS user_id, CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name`, emp.img_path,
    				st.description AS `status`
			    	FROM employees emp 
					LEFT JOIN `status` st ON emp.status = st.id 
					LEFT JOIN companies comp ON emp.company_id = comp.id 
					WHERE emp.`status` = 0 AND emp.is_deleted = 0 AND emp.is_registered = 1 ". $comp_filter ." 
					ORDER BY emp_name ASC";
		} else {
			$sql = "SELECT ci.id,
					ci.user_id,
					CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name` ,
					emp.img_path,
					st.description AS `status`,
					ci.date_checkedin, ci.time_checkedin, ci.location, ci.message
					FROM checkins ci 
					LEFT JOIN employees emp ON ci.user_id = emp.id
					LEFT JOIN `status` st ON ci.status = st.id
					LEFT JOIN companies comp ON emp.company_id = comp.id
					WHERE ci.status <> 1 AND ci.date_checkedin 
					BETWEEN " . $this->db->quote($data['datefrom']) 
					. " AND " . $this->db->quote($data['dateto']) . " "
					. $where_array ."
					AND ci.is_deleted = 0 ORDER BY ci.id DESC";
		}

    	return $this->db->query($sql);
    }

    public function filter_incidents($data) {
    	$where_array = "";
    	$search = "";
    	foreach ($data as $key => $value) {
    		if ($key != "filter_type" && $value != "") {
    			if ($key == "company_id") {
    				$value = $this->db->quote($value);
    				$where_array .= " AND e.company_id = " . $value . " ";
    			} else if ($key == "search") {
    				$search .= " AND  (CONCAT(e.first_name,' ',e.last_name) LIKE '%$value%' OR ri.message LIKE '%$value%')";
    			}
    		}
    	}

    	$sql = "SELECT ri.id, ri.user_id, ri.longitude, ri.latitude,
				ri.location, ri.message, ri.date_reported, CONCAT(e.first_name,' ',e.last_name) AS emp_name
				FROM report_incidents AS ri LEFT JOIN employees AS e ON ri.user_id=e.id 
				WHERE DATE(ri.date_reported) BETWEEN " . $this->db->quote($data['datefrom']) 
				. " AND " . $this->db->quote($data['dateto']) . " "
				. $where_array . $search .
				"ORDER BY ri.id DESC";

    	return $this->db->query($sql);
    }

    public function filter_broadcasts($data) {
    	$where_array = "";
    	$search = "";
    	foreach ($data as $key => $value) {
    		if ($key != "filter_type" && $value != "") {
    			if ($key == "company_id") {
    				$value = $this->db->quote($value);
    				$where_array .= " AND bs.company_id = " . $value . " ";
    			} else if ($key == "search") {
    				$search .= " AND (name LIKE '%$value%' OR message LIKE '%$value%')";
    			}
    		}
    	}

		$sql = "SELECT DISTINCT broadcast_id AS id, b.user_id, b.name, b.date_created, b.message FROM broadcast_sites bs
				LEFT JOIN broadcasts b ON b.id = bs.broadcast_id
				WHERE active = 1 AND DATE(date_created) BETWEEN " . $this->db->quote($data['datefrom']) 
				. " AND " . $this->db->quote($data['dateto']) . " ".
				$where_array . $search ." ORDER BY id DESC";

    	return $this->db->query($sql);
    }

	public function get_screen_name() {
		$result = $this->db->query("SELECT twitter_screen_name AS screen_name FROM officialfeeds WHERE is_deleted <> 1");
		return $result;
	}
	
	public function get_last_disaster() {
		$result = $this->db->query("SELECT id, disaster_mode FROM cmdctr ORDER BY id DESC LIMIT 1");
		
		return $result;
	}

	public function save_to_notif_is_read($notif_id, $user_id) {
		$notification_status = $this->db->create(Table::BROADCAST_STATUS);
		$notification_status->notification_id = $notif_id;
		$notification_status->user_id = $user_id;

		$result = $this->db->insert($notification_status);
		
		return $result;
	}
	
	public function save_to_notif_is_read_disaster($notif_id, $user_id) {
		$notification_status = $this->db->create("cmdctr_status");
		$notification_status->notification_id = $notif_id;
		$notification_status->user_id = $user_id;

		$result = $this->db->insert($notification_status);
		
		return $result;
	}
	
	public function get_device_keys($id) {
		$ids = implode(",", $id);
		
		$result = $this->db->query("SELECT owner, device_key, os_type FROM devices WHERE owner IN(". $ids .") AND (device_key <> '' AND device_key <> 'No Token')");
		// $result = $this->db->query("SELECT owner, device_key, os_type FROM devices WHERE owner IN('15531,25892') AND (device_key <> '' AND device_key <> 'No Token')");
		return $result;
	}
	
	public function get_checkin() {
		$date = $this->db->quote(date("Y-m-d"));
		
		$result = $this->db->query("SELECT ci.id,
				ci.user_id,
				CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name` ,
				emp.img_path,
				st.description AS `status`,
				ci.date_checkedin, ci.time_checkedin, ci.location, ci.message
				FROM checkins ci 
				LEFT JOIN employees emp ON ci.user_id = emp.id
				LEFT JOIN `status` st ON ci.status = st.id
				LEFT JOIN companies comp ON emp.company_id = comp.id
				WHERE ci.date_checkedin=".$date." 
				AND ci.status <> 1 AND ci.is_deleted <> 1
				ORDER BY ci.id DESC");
		return $result;
	}
	
	public function get_max_checkin() {
		$result = $this->db->query("SELECT MAX(id) AS maxid FROM checkins");
		
		return $result;
	}
	
	public function get_last_checkin($input) {
		$result = $this->db->query("SELECT ci.id,
				ci.user_id,
				CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name` ,
				emp.img_path,
				st.description AS `status`,
				ci.date_checkedin, ci.time_checkedin, ci.location, ci.message
				FROM checkins ci 
				LEFT JOIN employees emp ON ci.user_id = emp.id
				LEFT JOIN `status` st ON ci.status = st.id
				LEFT JOIN companies comp ON emp.company_id = comp.id
				WHERE ci.status <> 1 AND ci.id = ". $this->db->quote($input->data['maxid']) ."");
		return $result;
	}
	
	public function get_emp_count() {
		$result = $this->db->query("SELECT COUNT(id) AS emp_count FROM employees");
		return $result;
	}
	
	public function get_emergency($data) {
		$disaster_id = $data['disaster_id'];

		$emp_status = $this->db->query("SELECT status FROM checkins WHERE disaster_id =".$disaster_id);
		$get_total = $this->db->query("SELECT COUNT(id) AS total FROM cmdctr_status WHERE notification_id = ".$disaster_id);
		$total = $get_total[0]->{'total'};

		$nostatus = $total - count($emp_status);
		$safe = 0;
		$threat = 0;
		$urgent = 0;

		foreach ($emp_status as $key => $value) {
			$status_id = $emp_status[$key]->status;
			if ($status_id == 1) {
				$safe++;
			} else if ($status_id == 2) {
				$threat++;
			} else if ($status_id == 3) {
				$urgent++;
			}
    	}

		$calamity = $this->db->query("SELECT COUNT(n.id) AS calamity_count FROM broadcasts AS n WHERE message_type=1");
		$reportin = $this->db->query("SELECT COUNT(ri.id) AS reportin_count FROM report_incidents AS ri");

		$arr = array(
			'nostatus_count' => $nostatus,
			'safe_count' => $safe,
			'threat_count' => $threat,
			'urgent_count' => $urgent,
			'employee_count' => $total,
			'calamity_count' => $calamity,
			'reportin_count' => $reportin
		);
		
		return $arr;
	}
	
	public function search_emp_db($input) {
		if($input->data['type'] == 'employees') {
			$result = $this->db->query("SELECT ci.id,
					ci.user_id,
					CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name` ,
					emp.img_path,
					st.description AS `status`,
					ci.date_checkedin, ci.time_checkedin, ci.location, ci.message
					FROM checkins ci 
					LEFT JOIN employees emp ON ci.user_id = emp.id
					LEFT JOIN `status` st ON ci.status = st.id
					LEFT JOIN companies comp ON emp.company_id = comp.id
					WHERE (ci.status <> 1)
					AND (emp.first_name LIKE '%".$input->data['search']."%'
					OR emp.last_name LIKE '%".$input->data['search']."%')
					ORDER BY ci.id DESC");
			return $result;
		} else if($input->data['type'] == 'report') {
			$result = $this->db->query("SELECT ri.id, 
				ri.user_id, ri.longitude, ri.latitude, ri.location, ri.message,  ri.date_reported,
				CONCAT(e.first_name,' ',e.last_name) AS emp_name 
				FROM report_incidents AS ri
				LEFT JOIN employees AS e ON ri.user_id=e.id
				WHERE 
				(e.status <> 1)
				AND (e.first_name LIKE '%".$input->data['search']."%'
				OR e.last_name LIKE '%".$input->data['search']."%')
				ORDER BY ri.id DESC");
			return $result;
		} else if($input->data['type'] == 'incident') {
			$result = $this->db->query("SELECT id, user_id, name, date_created, message FROM broadcasts WHERE (name LIKE '%".$input->data['search']."%') ORDER BY id DESC");
			return $result;
		} else if($input->data['type'] == 'search_disaster') {
			$disaster_id = $input->data['id'];
			$by_company = "";
			$by_status = "";
			$search_emp = "";
			$search_emp_ns = "";
			$is_no_status = 0;

			if(isset($input->data['search']) && strlen($input->data['search']) > 0) {
				$search = $input->data['search'];
				$search_emp = " AND emp_name LIKE '%$search%'";
				$search_emp_ns = " AND CONCAT(emp.first_name,' ',emp.last_name) LIKE '%$search%' ";
			}
			
			if(isset($input->data['company_id']) && $input->data["company_id"] > 0) {
				$company_id = $input->data['company_id'];
				$by_company = " AND e.company_id = ".$company_id;
			}

			if(isset($input->data['status']) && ctype_digit($input->data['status']) && $input->data['status'] > 0) {
				$status_id = $input->data['status'];
				if (empty($search_emp)) {
					$by_status = " AND st.id = ".$status_id;
				} else {
					$by_status = " AND st.id = ".$status_id;
				}
			} else {
				//no status
				if($input->data['status']=="nostatus") {
					$is_no_status = 1;
				}
			}

			$disaster_date_range = $this->db->query("SELECT disaster_mode,
													DATE(date_created) AS date_created,
													DATE(date_closed) AS date_closed
													FROM cmdctr WHERE id=".$disaster_id);
			$date_min = date("Y-m-d");
			$date_max = date("Y-m-d");
			if (count($disaster_date_range) > 0) {
				$date_min = $disaster_date_range[0]->{'date_created'};
				$date_max = $disaster_date_range[0]->{'date_closed'};
				if ($disaster_date_range[0]->{'disaster_mode'} == 1) {
					$date_max = date("Y-m-d");
				}
			}

			$datefrom = (empty($input->data['datefrom'])) ? $date_min : $input->data["datefrom"];
			$dateto = (empty($input->data['dateto'])) ? $date_max : $input->data["dateto"];

			if ($datefrom == $dateto && $datefrom == date("Y-m-d")) {
				$datefrom = $date_min;
				$dateto = $date_max;
			}

			$by_date = " WHERE date_checkedin BETWEEN '$datefrom' AND '$dateto' ";

			$arr = array();
			$company_list = "";

			$company_list_data = $this->db->query("SELECT company_id,company_name FROM cmdctr_sites cs 
												LEFT JOIN companies c ON c.id = cs.company_id
												WHERE disaster_id=$disaster_id GROUP BY company_id");

			foreach ($company_list_data as $key => $value) {
				$company_list .= '<option value="'. $company_list_data[$key]->company_id .'">'. $company_list_data[$key]->company_name .'</option>';	
			}

			if ($is_no_status == 0) {
				$result = $this->db->query("SELECT t2.id,user_id,emp_name,img_path,company_id,company_name,st.description AS status,
											date_checkedin,time_checkedin,location,message
											FROM
											(
												SELECT c.id,c.user_id,CONCAT(e.first_name,' ',e.last_name) AS `emp_name`,e.img_path,
												e.company_id,co.company_name,
												IF(c.is_rescued=1,1,t1.status) AS `status`,
												c.date_checkedin,c.time_checkedin,c.location,c.message
												FROM 
													( 
														SELECT ch.checkin_id,ch.status,ch.date_updated FROM checkin_history ch 
														ORDER BY ch.checkin_id,ch.date_updated DESC 
													)t1 
												LEFT JOIN checkins c ON c.id = t1.checkin_id 
												LEFT JOIN employees e ON e.id = c.user_id 
												LEFT JOIN companies co ON co.id = e.company_id 
												LEFT JOIN company_sites cs ON cs.id = e.site_id 
												WHERE c.disaster_id = ".$disaster_id." AND e.is_deleted = 0 
												$by_company
												GROUP BY checkin_id
											)t2 
											LEFT JOIN `status` st ON st.id = t2.status
											$by_date $search_emp $by_status AND user_id IN (SELECT user_id FROM cmdctr_status WHERE notification_id = $disaster_id)
											ORDER BY date_checkedin DESC,time_checkedin DESC");

				$get_total = $this->db->query("SELECT COUNT(cs.id) AS total FROM cmdctr_status cs LEFT JOIN employees e ON e.id = cs.user_id WHERE notification_id = ".$disaster_id." AND e.is_deleted = 0");
				$total = $get_total[0]->{'total'};
	
				$nostatus = $total - count($result);
				$safe = 0;
				$threat = 0;
				$urgent = 0;

				foreach ($result as $key => $value) {
					$status_id = $result[$key]->status;
					if ($status_id == "safe") {
						$safe++;
					} else if ($status_id == "threat") {
						$threat++;
					} else if ($status_id == "urgent") {
						$urgent++;
					}
				}
	
				$calamity = array();
				$reportin = array();
	
				$arr = array(
					'nostatus_count' => $nostatus,
					'safe_count' => $safe,
					'threat_count' => $threat,
					'urgent_count' => $urgent,
					'employee_count' => $total,
					'calamity_count' => $calamity,
					'reportin_count' => $reportin
				);
			} else {
				$result = $this->db->query("SELECT 0 AS id, t1.user_id, CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name`, emp.img_path, st.description AS `status`,c.id
												FROM(
													SELECT notification_id,user_id FROM cmdctr_status cs
													WHERE cs.notification_id = $disaster_id
												)t1 
											LEFT JOIN checkins c ON c.disaster_id = t1.notification_id AND c.user_id = t1.user_id
											LEFT JOIN employees emp ON emp.id = t1.user_id
											LEFT JOIN `status` st ON st.id = 0
											WHERE c.id IS NULL
											$search_emp_ns ");
			}

			$fresult['data'] = $result;
			$fresult['count'] = $arr;
			$fresult['company_list'] = $company_list;
			$fresult['date_min'] = $date_min;
			$fresult['date_max'] = $date_max;

			return $fresult;
		}
	}
	
	public function post_change_status($input) {
		$status = $this->db->quote($input->data['status']);
		$id = $this->db->quote($input->data['id']);

		$need_help = "";
		if($status == 0 || $status == 1) {
			$need_help = " ,need_help = 0 ";
		}

		if ($status == 1) {
			$this->db->update("DELETE FROM checkin_items WHERE checkin_id=".$id);
		}

		$result = $this->db->query("SELECT user_id FROM checkins WHERE id = ".$id);

		if(count($result) > 0) {
			$user_id = $result[0]->{'user_id'};
			$this->db->update("UPDATE employees SET status = ".$status." WHERE id=".$user_id);
		}

		$sql = "UPDATE checkins SET status = ".$status." ".$need_help." WHERE id=".$id;
    	$result = $this->db->update($sql);

    	$sql = "SELECT id FROM checkin_history WHERE checkin_id = ".$id." ORDER BY id DESC LIMIT 1";
    	$result = $this->db->query($sql);

    	$this->db->update("UPDATE checkin_history SET status = ".$status." WHERE id =".$result[0]->id);

		$update = $this->db->query("SELECT status, id FROM checkins WHERE id=".$id."");
		return $update;
	}

	public function reset_emp_status($id) {
		$this->db->update("UPDATE employees SET status = 0 WHERE id = $id");
	}
	
	public function get_reportin() {
		$date = date("Y-m-d");

		$sql = "SELECT ri.id, ri.user_id, ri.longitude, ri.latitude,
				ri.location, ri.message, ri.date_reported, ri.location, CONCAT(e.first_name,' ',e.last_name) AS emp_name
				FROM report_incidents AS ri LEFT JOIN employees AS e ON ri.user_id=e.id 
				WHERE DATE(ri.date_reported)=".$this->db->quote($date)." ORDER BY ri.id DESC";

		$result = $this->db->query($sql);
		return $result;
	}
	
	public function get_notif_count($message_template) {
		if($message_template['flag'] == "Disaster") {
			$result = $this->db->query("SELECT COUNT(id) AS count FROM disaster");
		} else {
			$result = $this->db->query("SELECT COUNT(id) AS count FROM broadcasts");
		}
		
		return $result;
	}
	
	public function get_company_list() {
		$result = $this->db->query("SELECT company_name, id FROM companies WHERE is_deleted='0' ORDER BY company_name ASC");
		return $result;
	}
	
	public function get_disaster_type() {
		$result = $this->db->query("SELECT id, type FROM disaster_type WHERE id IN (1,2,3,4)");
		
		return $result;
	}
	
	public function get_incident() {
		$date = date("Y-m-d");

		$sql = "SELECT id, user_id, name, date_created, message FROM broadcasts 
				WHERE DATE(date_created) = ".$this->db->quote($date)." AND active = 1 ORDER BY id DESC";
		$result = $this->db->query($sql);
		return $result;
	}
	
	public function get_status() {
		$result = $this->db->query("SELECT id, description FROM status WHERE id <> 1 AND id <> 4");
		return $result;
	}
	
	public function post_disaster_mode($input) {
		$date = $this->db->quote(date("Y-m-d H:i:sa"));
		$mode = $input;
		$query = 0;

		$user_id = $this->session->userdata("user_id");

		if($mode == 1) {
			$result = $this->db->insert("INSERT INTO cmdctr SET disaster_mode=". intval($input) .", date_created=". $date .", created_by=". $user_id);
			$query = $this->db->getLastInsertId();
		} else if($mode == 0) {
			$result = $this->db->query("SELECT id FROM cmdctr ORDER BY id DESC LIMIT 1");
			$lastDisasterId = $result[0]->{'id'};
			$result = $this->db->update("UPDATE cmdctr SET date_closed = '".date("Y-m-d H:i:s")."', disaster_mode = 0, closed_by=".$user_id." WHERE id = ". intval($lastDisasterId));
			$query = $lastDisasterId;
		}

		return $query;
	}
	
	public function post_disaster($emp_ids, $notif_ids) {
		$result = $this->db->insert("INSERT INTO cmdctr_status SET notification_id=". $notif_ids .", user_id=". $emp_ids);
		
		$query = $this->db->query("SELECT user_id FROM notification_status WHERE notification_id=". $notif_ids ."");
		return $result;
	}
	
	public function get_dev_keys() {
		//$result = $this->db->query("SELECT owner, device_key, os_type FROM devices GROUP BY device_key");
		$result = $this->db->query("SELECT owner, device_key, os_type FROM devices");
		return $result;
	}
	
	public function get_all_companies() {
		$result = $this->db->query("SELECT id FROM companies");
		return $result;
	}
	
	public function get_all_status() {
		$result = $this->db->query("SELECT id FROM status WHERE id <> 1");
		return $result;
	}
	
	public function get_latest_disaster() {
		$result = $this->db->query("SELECT");
	}

	public function archive() {
		$response = false;
		$result = $this->db->query("SELECT id FROM cmdctr ORDER BY id DESC LIMIT 1");

		if(count($result) > 0) {
			$last_disaster_id = $result[0]->{'id'};

			if($last_disaster_id != 0) {
				$statement = "UPDATE checkins SET is_deleted = 1 WHERE disaster_id = " .$last_disaster_id;
				$result = $this->db->update($statement);

				$statement = "UPDATE employees SET status = 0";
				$result = $this->db->update($statement);
				$response = $result;
			}
		}

		return $response;
	}
	
	public function get_twitter_interval() {
		$result = $this->db->query("SELECT twitter_screen_name AS screen_name FROM officialfeeds WHERE is_deleted <> 1");
		
		return $result;
	}
	
	public function insert_twitter_feed($input) {
		$twitter = $this->db->create('twitter_feed');
		$twitter->screen_name = $input->data['name'];
		$twitter->profile_img = $input->data['img'];
		$twitter->date_created = date("Y-m-d H:i:s", strtotime($input->data['date']));
		$twitter->message = $input->data['tweet'];
		$twitter->inserted_at = date("Y-m-d H:i");

		$result = $this->db->insert($twitter);
	}
	
	public function delete_twitter_feed($timenow) {
		$result = $this->db->update("DELETE FROM twitter_feed WHERE inserted_at <> '". $timenow ."'");
	}

	public function post_sms_broadcast($input) {
		/*
		 * fixed command center disaster broadcast
		 * added company_id
		 */

		$notification = $this->db->create(table::BROADCASTS);
		$notification->user_id = $this->session->userdata("user_id");
		$notification->name = $this->session->userdata("full_name");
		$notification->date_created = date("Y-m-d H:i:sa");
		$notification->message = $input->data['msg'];
		$notification->company_id = $input->data['company_id'];
		$notification->is_sent_sms = 1;

		$this->db->insert($notification);

		if($input->data['company_id'] == 0) {
			return $this->db->query("SELECT id FROM employees");
		} else {
			$employee = $this->db->create("employees");
			$employee->id = 0;
			return $this->db->query($employee,array("company_id" => $notification->company_id));
		}
	}
	
	public function get_last_broadcast_id() {
        return $this->last_broadcast_id;
    }

	public function post_broadcast($input) {
		$notification = $this->db->create(table::BROADCASTS);
		$notification->user_id = $this->session->userdata("user_id");
		$notification->name = $this->session->userdata("full_name");
		$notification->date_created = date("Y-m-d H:i:sa");
		$notification->message = $input->data['msg'];

		$this->db->insert($notification);
		
		$broadcast_max_id = $this->db->query("SELECT MAX(id) AS disaster_id FROM broadcasts");
		$this->last_broadcast_id = $broadcast_max_id[0]->{"disaster_id"};
		
		$this->activity = $this->db->create("activities");
		$this->activity->activity = $input->data['msg'];
		$this->activity->date_created = date('Y-m-d H:i:s');
		$this->activity->created_by = $this->session->userdata("user_id");

		$this->db->insert($this->activity);

		// $this->activity = null;
		
		if($input->data['sites'][0]['company_id'] == '0') {
			$broadcast_ids = $this->db->query("SELECT id AS site_id, company_id FROM company_sites");
			
			foreach($broadcast_ids as $sites) {
				$broadcasts = $this->db->create("broadcast_sites");
				$broadcasts->company_id = $sites->{'company_id'};
				$broadcasts->site_id = $sites->{'site_id'};
				$broadcasts->broadcast_id = $broadcast_max_id[0]->{'disaster_id'};
				$this->db->insert($broadcasts);
			}
		} else {
			$sitesArr = array();
			foreach($input->data['sites'] as $sites) {
				$broadcasts = $this->db->create("broadcast_sites");
				$broadcasts->company_id = $sites['company_id'];
				$broadcasts->site_id = $sites['site_id'];
				$broadcasts->broadcast_id = $broadcast_max_id[0]->{'disaster_id'};
				$this->db->insert($broadcasts);
				
				$sitesArr['company_id'][] = $sites['company_id'];
				$sitesArr['site_id'][] = $sites['site_id'];
			}
		}
		
		if($input->data['sites'][0]['company_id'] == '0') {
			return $this->db->query("SELECT id FROM employees WHERE is_deleted <> 1");
		} else {
			return $this->db->query("SELECT id FROM employees WHERE site_id IN (". implode(',',$sitesArr['site_id']) .") AND is_deleted <> 1");
		}
	}
	
	public function post_declare_disaster($input) {
		if($input->data['type'] == '1' || $input->data['type'] == '2' || $input->data['type'] == '3' || $input->data['type'] == '4') {
			$disaster_type_id = $input->data['type'];
		} else {
			$disaster_type = $this->db->create("disaster_type");
			$disaster_type->type = $input->data['type'];
			
			$this->db->insert($disaster_type);
			
			$query = $this->db->getLastInsertId();
			$disaster_type_id = $query;
		}
		
		$this->activity = $this->db->create("activities");
		$this->activity->activity = $input->data['name'];
		$this->activity->date_created = date('Y-m-d H:i:s');
		$this->activity->created_by = $this->session->userdata("user_id");

		$this->db->insert($this->activity);

		// $this->activity = null;
		
		$cmdctr = $this->db->create("cmdctr");
		$cmdctr->disaster_mode = 1;
		$cmdctr->disaster_name = $input->data['name'];
		$cmdctr->disaster_details = $input->data['details'];
		$cmdctr->disaster_type = $disaster_type_id;
		$cmdctr->date_created = date("Y-m-d H:i:sa");
		$cmdctr->created_by = $this->session->userdata("user_id");

		$this->db->insert($cmdctr);
		
		$cmdctr_max_id = $this->db->query("SELECT MAX(id) AS disaster_id FROM cmdctr");
		
		if($input->data['sites'][0]['company_id'] == '0') {
			$company_ids = $this->db->query("SELECT id AS site_id, company_id FROM company_sites");
			
			foreach($company_ids as $sites) {
				$disaster = $this->db->create("cmdctr_sites");
				$disaster->company_id = $sites->{'company_id'};
				$disaster->site_id = $sites->{'site_id'};
				$disaster->disaster_id = $cmdctr_max_id[0]->{'disaster_id'};
				$this->db->insert($disaster);
			}
		} else {
			$sitesArr = array();
			foreach($input->data['sites'] as $sites) {
				$disaster = $this->db->create("cmdctr_sites");
				$disaster->company_id = $sites['company_id'];
				$disaster->site_id = $sites['site_id'];
				$disaster->disaster_id = $cmdctr_max_id[0]->{'disaster_id'};
				$this->db->insert($disaster);
				
				$sitesArr['company_id'][] = $sites['company_id'];
				$sitesArr['site_id'][] = $sites['site_id'];
			}
		}

		if($input->data['sites'][0]['company_id'] == '0') {
			return $this->db->query("SELECT id FROM employees WHERE is_deleted = 0");
		} else {
			return $this->db->query("SELECT id FROM employees WHERE site_id IN (". implode(',',$sitesArr['site_id']) .") AND is_deleted = 0");
		}
	}
	
	public function get_current_disaster() {
		$result = $this->db->query("SELECT cc.id, cc.disaster_name, cc.disaster_details, cc.company_id, cc.date_created, cc.date_closed, cc.disaster_type, c.company_name, dt.type FROM cmdctr AS cc LEFT JOIN companies AS c ON c.id=cc.company_id LEFT JOIN disaster_type AS dt ON cc.disaster_type=dt.id WHERE cc.is_closed <> 1 ORDER BY cc.id DESC");
		
		return $result;
	}
	
	public function view_current_disaster($request) {
		if($request->data['mode'] == 'filter') {
			$result = $this->db->query("SELECT cc.id, cc.disaster_name, cc.disaster_details, cc.company_id, cc.date_created, cc.date_closed, cc.disaster_type, c.company_name, dt.type FROM cmdctr AS cc LEFT JOIN companies AS c ON c.id=cc.company_id LEFT JOIN disaster_type AS dt ON cc.disaster_type=dt.id WHERE cc.is_closed <> 1 AND disaster_type=". $request->data['disaster_type'] ." ORDER BY cc.id DESC");
		} else if($request->data['mode'] == 'search') {
			$result = $this->db->query("SELECT cc.id, cc.disaster_name, cc.disaster_details, cc.company_id, cc.date_created, cc.date_closed, cc.disaster_type, c.company_name, dt.type FROM cmdctr AS cc LEFT JOIN companies AS c ON c.id=cc.company_id LEFT JOIN disaster_type AS dt ON cc.disaster_type=dt.id WHERE cc.is_closed <> 1 AND disaster_name LIKE '%". $request->data['search'] ."%' ORDER BY cc.id DESC");
		} else {
			$result = $this->db->query("SELECT cc.id, cc.disaster_name, cc.disaster_details, cc.company_id, cc.date_created, cc.date_closed, cc.disaster_type, c.id AS company_id, c.company_name, dt.type, cs.site_id, css.site FROM cmdctr AS cc LEFT JOIN disaster_type AS dt ON cc.disaster_type=dt.id LEFT JOIN cmdctr_sites AS cs ON cs.disaster_id=cc.id LEFT JOIN companies AS c ON c.id=cs.company_id LEFT JOIN company_sites AS css ON cs.site_id=css.id WHERE cc.id=". $request->data['disaster_id'] ." AND cc.is_closed <> 1 ORDER BY cc.id DESC");
		}
		
		return $result;
	}
	
	public function edit_current_disaster($request) {
		if($request->data['disaster_type'] == '1' || $request->data['disaster_type'] == '2' || $request->data['disaster_type'] == '3' || $request->data['disaster_type'] == '4') {
			$disaster_type_id = $request->data['disaster_type'];
		} else {
			$query = $this->db->query("SELECT id FROM disaster_type WHERE type='". $request->data['disaster_type'] ."'");
			
			if($query) {
				$disaster_type_id = $query[0]->{'id'};
			} else {
				$disaster_type = $this->db->create("disaster_type");
				$disaster_type->type = $request->data['disaster_type'];
				
				$this->db->insert($disaster_type);
				
				$last_id = $this->db->getLastInsertId();
				$disaster_type_id = $last_id;
			}
		}
		
		$result = $this->db->update("UPDATE cmdctr SET disaster_name='". $request->data['name'] ."', disaster_details='". $request->data['details'] ."', disaster_type='". $disaster_type_id ."' WHERE id=". $request->data['disaster_id']. "");
		
		$this->db->update("DELETE FROM cmdctr_sites WHERE disaster_id=". $request->data['disaster_id'] ."");
		$this->db->update("DELETE FROM cmdctr_status WHERE notification_id=". $request->data['disaster_id'] ."");
		
		$sitesArr = array();
		$compArr = array();
		foreach($request->data['sites'] as $data) {
			$sites = $this->db->create("cmdctr_sites");
			$sites->company_id = $data['company_id'];
			$sites->site_id = $data['site_id'];
			$sites->disaster_id = $request->data['disaster_id'];
			$this->db->insert($sites);
			
			$sitesArr[] = $data['site_id'];
			$compArr[] = $data['company_id'];
		}
		
		return $this->db->query("SELECT id FROM employees WHERE site_id IN (". implode(',', $sitesArr) .") AND company_id IN (". implode(',', $compArr) .") AND is_deleted = 0");
	}
	
	public function get_disaster_company($request) {
		$result = $this->db->query("SELECT id, site FROM company_sites WHERE company_id=". $request->data['company_id'] ."");
		
		return $result;
	}
	
	public function delete_current_disaster($request) {
		$result = $this->db->update("UPDATE cmdctr SET is_closed = 1, disaster_mode = 0, date_closed = '".date("Y-m-d H:i:s")."' WHERE id=". $request->data['disaster_id'] ."");
		
		$query = $this->db->query("SELECT cs.company_id, cs.site_id, cs.disaster_id, e.id, cc.disaster_name FROM cmdctr_sites AS cs LEFT JOIN employees AS e ON e.site_id=cs.site_id LEFT JOIN cmdctr AS cc ON cc.id=cs.disaster_id WHERE disaster_id=". $request->data['disaster_id'] ."");
		
		return $query;
	}
	
	public function get_disaster() {
		$result = $this->db->query("SELECT id, disaster_name FROM cmdctr WHERE disaster_mode=1 AND is_closed=0 ORDER BY date_created DESC");
		
		return $result;
	}
	
	public function get_notifid_declare_disaster($request) {
		if($request->data['type'] == 'edit') {
			
		} else {
			$result = $this->db->query("SELECT MAX(id) FROM cmdctr");
			$query = $this->db->query("SELECT cc.id AS disaster_id, dt.type FROM cmdctr AS cc LEFT JOIN disaster_type AS dt ON cc.disaster_type=dt.id WHERE cc.id=". $result[0]->{'MAX(id)'} ."");
		}
		
		return $query;
	}
	
	public function get_twitter_feed_cmdctr($data) {
		$datenow = date("Y-m-d");
		$date_three_days_ago = date('Y-m-d', strtotime('-3 days'));

		$search_by = "";
		if (isset($data["search"])) {
			$search = $data["search"];
			$search_by = " AND (user_name LIKE '%$search%' OR screen_name LIKE '%$search%' OR `text` LIKE '%$search%')";
		}

		if (isset($data["screen_name"])) {
			$search = $data["screen_name"];
			$search_by = " AND screen_name ='$search'";
		}

		$result = $this->db->query("SELECT profile_image, user_name, created_at, screen_name, `text` FROM twitter_feed_cmdctr WHERE DATE(created_at) BETWEEN '$date_three_days_ago' AND '$datenow' $search_by ORDER BY created_at DESC");
		
		return $result;
	}
	
	
	
}