<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Newsfeeds extends Model {
    public function __construct() {
        parent::__construct();
    }

    //company id
    public function getEmployeeFeeds($data) {
    	$company_id = $this->db->quote($data->{'company_id'});
    	$user_id = $this->db->quote($data->{'user_id'});
    	$row_id = $data->{'row_id'};
    	$offset_row = "";
    	$limit = 10;
		$has_reached_last_row = 0;
		$date_three_days_ago = date('Y-m-d', strtotime('-3 days', strtotime(date("Y-m-d"))));

    	if ($row_id > 0) {
    		$offset_row = " AND ri.id < $row_id";
    	}

		$sql = "SELECT ri.id, ri.user_id, CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name`, emp.img_path,
				ri.location, ri.message, ri.date_reported, COUNT(DISTINCT con.id) AS confirms, 
				COUNT(DISTINCT rep.id) AS replies,
				IF(SUM(IF(con.user_id=$user_id,1,0))>=1,1,0) AS is_confirmed
				FROM report_incidents ri
				LEFT JOIN replies rep ON ri.id = rep.report_incident_id
				LEFT JOIN employees emp ON emp.id = ri.user_id
				LEFT JOIN confirms con ON con.report_incident_id = ri.id
				WHERE emp.company_id = " . $company_id . " AND emp.is_deleted <> 1 $offset_row
				AND DATE(ri.date_reported) >= '$date_three_days_ago'
				GROUP BY ri.id
				ORDER BY ri.date_reported DESC LIMIT $limit";

		$result = $this->db->query($sql);
		$result_array['message'] = $result;

		$limit += 1;

		$sql = "SELECT COUNT(id) AS count FROM (SELECT ri.id, ri.user_id, CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name`, emp.img_path,
				ri.location, ri.message, ri.date_reported, COUNT(DISTINCT con.id) AS confirms, 
				COUNT(DISTINCT rep.id) AS replies,
				IF(SUM(IF(con.user_id=$user_id,1,0))>=1,1,0) AS is_confirmed
				FROM report_incidents ri
				LEFT JOIN replies rep ON ri.id = rep.report_incident_id
				LEFT JOIN employees emp ON emp.id = ri.user_id
				LEFT JOIN confirms con ON con.report_incident_id = ri.id
				WHERE emp.company_id = " . $company_id . " AND emp.is_deleted <> 1 $offset_row
				AND DATE(ri.date_reported) >= '$date_three_days_ago'
				GROUP BY ri.id
				ORDER BY ri.date_reported DESC LIMIT $limit)test";

		$result = $this->db->query($sql);
		if ($result[0]->count < $limit) {
            $has_reached_last_row = 1;
        }

        $result_array['has_reached_last_row'] = $has_reached_last_row;

        return $result_array;
    }

    public function get_replies($data) {
    	$report_incident_id = $data->{'report_incident_id'};
    	$row_id = $data->{'row_id'};
    	$offset_row = "";
    	$limit = 10;
    	$has_reached_last_row = 0;

    	if ($row_id > 0) {
    		$offset_row = " AND r1.id < $row_id ";
    	}

    	$result = $this->db->query("SELECT replies.id,date_replied,message,
    								CONCAT(first_name,' ',last_name) AS employee_name,img_path FROM 
									(
									  SELECT r1.id,date_replied,message,user_id FROM replies r1 
									  WHERE report_incident_id = ".$report_incident_id." $offset_row
									  ORDER BY r1.id DESC LIMIT $limit
									) replies
									LEFT JOIN employees ON employees.id = replies.user_id
									ORDER BY replies.id ASC");

    	$result_array['message'] = $result;

		$limit += 1;

		$result = $this->db->query("SELECT COUNT(id) AS count FROM (SELECT replies.id,date_replied,message,
    								CONCAT(first_name,' ',last_name) AS employee_name,img_path FROM 
									(
									  SELECT r1.id,date_replied,message,user_id FROM replies r1 
									  WHERE report_incident_id = ".$report_incident_id." $offset_row
									  ORDER BY r1.id DESC LIMIT $limit
									) replies
									LEFT JOIN employees ON employees.id = replies.user_id
									ORDER BY replies.id ASC)test");

    	if ($result[0]->count < $limit) {
            $has_reached_last_row = 1;
        }

        $result_array['has_reached_last_row'] = $has_reached_last_row;

        return $result_array;
    }

    public function get_confirm_and_reply_count($data) {
    	$report_incident_id = $data->{'report_incident_id'};

    	$result = $this->db->query("SELECT
									(
										SELECT COUNT(id) FROM confirms 
										WHERE report_incident_id = ".$report_incident_id."
									) AS confirm_count,
									(
										SELECT COUNT(id) FROM replies 
										WHERE report_incident_id = ".$report_incident_id."
									) AS reply_count");
    	return $result;
    }
	
	public function get_screen_name() {
		$result = $this->db->query("SELECT twitter_screen_name AS screen_name FROM officialfeeds WHERE is_deleted <> 1");
		return $result;
	}
	
	public function get_twitter_feeds() {
		$result = $this->db->query("SELECT screen_name, profile_img, message, tweet_id, date_created FROM twitter_feed ORDER BY date_created DESC");
		
		return $result;
	}
	
	public function validate_date($date) {
		$result = $this->db->update("DELETE FROM twitter_feed WHERE date_created LIKE '%". $date ."%'");
	}
	
	public function get_date_diff() {
		$result = $this->db->query("SELECT MAX(inserted_at) AS date_inserted FROM twitter_feed");
		
		return $result;
	}
	
	public function get_twitter_interval() {
		$result = $this->db->query("SELECT twitter_screen_name AS screen_name FROM officialfeeds WHERE is_deleted <> 1");
		
		return $result;
	}
	
	public function insert_twitter_feed($data) {
		$twitter = $this->db->create('twitter_feed');
		$twitter->screen_name = $data->user->screen_name;
        $twitter->profile_img = $data->user->profile_image_url;
        $twitter->date_created = date("Y-m-d H:i:s", strtotime($data->created_at));
        $twitter->message = $data->text;
		$twitter->tweet_id = $data->id_str;
        $twitter->inserted_at = date("Y-m-d H:i");

        $result = $this->db->insert($twitter);
		
		$this->db->update("DELETE FROM twitter_feed WHERE inserted_at <> '". date("Y-m-d H:i") ."'");
	}
}