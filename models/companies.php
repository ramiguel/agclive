<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Companies extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function getCompanyList() {
        $result = $this->db->query("SELECT id, company_name, company_address, company_contact_no FROM companies WHERE is_deleted <> 1");
        return $result;
    }
	
	public function getSupervisor($data) {
		$result = $this->db->query("SELECT sc.id AS sc_id, sc.supervisor_id, sc.company_id, sc.department_id, c.id, c.company_name, c.company_address, c.company_contact_no 
			                        FROM supervisor_companies AS sc 
			                        LEFT JOIN companies AS c ON sc.company_id=c.id 
			                        WHERE sc.supervisor_id=".$this->db->quote($data->{'supervisor_id'})."");

		return $result;
	}
	
	public function getEmployees($data) {
		/*
		$result = $this->db->query("SELECT sr.id, sr.employee_id, sr.supervisor_id, e.status, e.id, CONCAT(e.first_name,' ',e.last_name) AS emp_name, e.company_id, e.img_path, e.employee_no 
			                        FROM supervisor_role AS sr 
			                        LEFT JOIN employees AS e ON sr.employee_id=e.id 
			                        WHERE sr.supervisor_id=".$this->db->quote($data->{'supervisor_id'})."");
			                        */

        $result = $this->db->query("SELECT id AS employee_id,employee_no,CONCAT(first_name,' ',last_name) AS emp_name,img_path,company_id,company_id AS id,status,supervisor_id FROM employees WHERE supervisor_id = " . intval($data->{'supervisor_id'}));
		return $result;
	}
	
	public function filterByStatus($data) {
		// $result = $this->db->query("SELECT sr.id, sr.employee_id, sr.supervisor_id, e.status, e.id, CONCAT(e.first_name,' ',e.last_name) AS emp_name, e.company_id, e.img_path, e.employee_no 
		// 	                        FROM supervisor_role AS sr 
		// 	                        LEFT JOIN employees AS e ON sr.employee_id=e.id 
		// 	                        WHERE e.status=".$this->db->quote($data->{'status'})." AND sr.supervisor_id=".$this->db->quote($data->{'supervisor_id'})."");
        $result = $this->db->query("SELECT id,supervisor_id,status,CONCAT(first_name,' ',last_name) AS emp_name,company_id, img_path, employee_no FROM employees WHERE status = " . intval($data->{'status'}) . " AND supervisor_id = " . intval($data->{'supervisor_id'}));
		return $result;
	}
}