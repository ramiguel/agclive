<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Incidents extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_device_keys($user_id, $company_id) {
        /* 
        $device_seek = $this->db->create("devices");
        $device_seek->owner = "";
        $device_seek->device_key = "";
        $result = $this->db->query($device_seek,array("owner" => $id));
        
        if (count($result) > 0) {
            $device_seek = $result[0];
            $result = $this->db->query("SELECT owner,device_key,os_type FROM devices WHERE device_key <> " . $this->db->quote($device_seek->device_key) . " GROUP BY device_key");
            return $result;
        }
        */

        $result = $this->db->query("SELECT `owner`, device_key, os_type, CONCAT(e.first_name, ' ',e.last_name) AS emp_name, 
                    e.company_id FROM devices d INNER JOIN employees e ON e.id = d.owner WHERE e.id <> ". $user_id ." AND 
                    e.company_id = ". $company_id);
        return $result;
    }

    public function get_information($user_id) {
        $result = $this->db->query("SELECT company_id,CONCAT(first_name,' ',last_name) AS emp_name FROM employees WHERE id = " . $user_id);
        return $result;
    }

    public function confirm($report_incident_id, $confirm, $user_id) {
        if ($confirm == 0) {
            return $this->db->update("DELETE FROM confirms WHERE report_incident_id = ".$report_incident_id." AND user_id=".$user_id);
        } else if ($confirm == 1){
            $confirm = $this->db->create("confirms");
            $confirm->user_id = $user_id;
            $confirm->report_incident_id = $report_incident_id;
            return $this->db->insert($confirm);
        } else {
            return false;
        }
    }

    public function reply($report_incident_id, $user_id, $message) {
        $reply = $this->db->create("replies");
        $reply->report_incident_id = $report_incident_id;
        $reply->user_id = $user_id;
        $reply->message = $message;
        $reply->date_replied = date('Y-m-d H:i:s');
        return $this->db->insert($reply);
    }

    public function save_to_report_incidents_is_read($report_incident_id, $user_id) {
        $incident = $this->db->create("report_incident_status");
        $incident->report_incident_id = $report_incident_id;
        $incident->user_id = $user_id;
        return $this->db->insert($incident);
    }

    public function save_report($data) {
    	$current_date = date('Y-m-d H:i:s');

        $report = $this->db->create("report_incidents");
        $report->user_id = $data->{'user_id'};
        $report->longitude = $data->{'longitude'};
        $report->latitude = $data->{'latitude'};
        $report->location = $data->{'location'};
        $report->message = $data->{'message'};
        $report->date_reported = $current_date;

        $this->db->insert($report);

        return $this->db->getLastInsertId();
    }
}