<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Rescues extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_emp_per_location($data) {
        // $sql =  "SELECT b.id AS `checkin_id`, emp.id AS `user_id`, CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name`,
        //         b.location, b.status, emp.blood_type, emp.img_path FROM (SELECT user_id, MAX(date_checkedin) AS max_date,
        //         MAX(time_checkedin) AS max_time 
        //         FROM checkins ci WHERE location LIKE '%".$data->{'location'}."%' GROUP BY user_id) AS a
        //         JOIN(SELECT * FROM checkins WHERE location LIKE '%".$data->{'location'}."%') AS b
        //         ON a.user_id = b.user_id AND a.max_date = b.date_checkedin AND a.max_time = b.time_checkedin
        //         LEFT JOIN employees emp ON emp.id = b.user_id";

        //latest post of employee per location
        $sql = "SELECT a.id AS `checkin_id`, emp.id AS `user_id`, CONCAT(emp.first_name,' ',emp.last_name) AS `emp_name`,
                a.location, a.latitude, a.longitude, a.status, emp.mobile_no, emp.blood_type, emp.img_path FROM checkins a INNER JOIN(
                SELECT t1.user_id, t1.date_checkedin, t1.time_checkedin, MAX(t1.time_checkedin) AS max_time
                FROM checkins t1 INNER JOIN(SELECT user_id, MAX(date_checkedin) AS max_date FROM checkins ci GROUP BY ci.user_id) t2
                ON t1.user_id = t2.user_id AND t1.date_checkedin = t2.max_date GROUP BY user_id)b ON a.user_id = b.user_id
                AND a.date_checkedin = b.date_checkedin AND a.time_checkedin = b.max_time 
                LEFT JOIN employees emp ON emp.id = a.user_id
                WHERE location LIKE '%".$data->{'location'}."%'";
        $result = $this->db->query($sql);
        return $result;
    }
}