<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Reportins extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function view_reportin($data) {        
        $user_id = isset($data->{'user_id'}) ? $data->{'user_id'} : 0;
        $checkin_id = isset($data->{'checkin_id'}) ? $data->{'checkin_id'} : 0;
        
        if (!ctype_digit($checkin_id)) {
            $checkin_id = 0;
        }

        if (!ctype_digit($user_id)) {
            $user_id = 0; 
        }

        if ($checkin_id > 0 && $user_id > 0) {
            $result = $this->db->query("SELECT c.id AS checkin_id,CONCAT(e.first_name,' ',e.last_name) AS emp_name,
                                        c.location,e.mobile_no,c.status,c.message,c.no_of_persons,e.blood_type,e.img_path 
                                        FROM checkins AS c 
                                        INNER JOIN employees AS e ON e.id = c.user_id
                                        WHERE c.user_id = $user_id AND c.id = $checkin_id");
        } else {
            $result = $this->db->query("SELECT c.id AS checkin_id,CONCAT(e.first_name,' ',e.last_name) AS emp_name,
                                        c.location,e.mobile_no,c.status,c.message,c.no_of_persons,e.blood_type,e.img_path 
                                        FROM checkins c
                                        INNER JOIN employees AS e ON e.id = c.user_id
                                        WHERE c.user_id = $user_id 
                                        ORDER BY c.date_checkedin DESC,
                                        c.time_checkedin DESC LIMIT 1");
        }

        return $result;
    }

    public function get_reportin_items($checkin_id) {
        $sql = "SELECT item_name 
                FROM checkin_items 
                LEFT JOIN items ON items.id = checkin_items.item_id
                WHERE checkin_id = " . $checkin_id;
        $result = $this->db->query($sql);
        return $result;
    }
}