<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Sample_Model extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function getAllUsers() {
        $result = $this->db->query("SELECT username FROM users");
        foreach ($result as $user) {
            print "{$user->username}<br>";
        }
    }
}