<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Employees extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function getInfo($data) {
        $sql = "SELECT emp.employee_no, 
                       emp.first_name, 
                       emp.last_name, 
                       emp.email, 
                       emp.address_street,
                       emp.address_line_2,
                       emp.address_city,
                       emp.address_state,
                       emp.address_country,
                       emp.address_zip_code, 
                       emp.mobile_no, 
                       emp.landline_no, 
                       emp.blood_type,
                       dep.department_name AS dept_name, 
                       com.company_name, 
                       com.company_address, 
                       com.company_contact_no,
                       emp.emp_company_address,
                       emp.emp_company_contact_no,
                       emp.emergency_name, 
                       emp.emergency_address,
                       emp.emergency_contact_no,
                       emp.emergency_relationship,
                       emp.emergency_name_two, 
                       emp.emergency_address_two, 
                       emp.emergency_contact_no_two,
                       emp.emergency_relationship_two, 
                       emp.img_path 
                    FROM employees emp
                    LEFT JOIN companies com ON emp.company_id = com.id
                    LEFT JOIN department dep ON emp.department_id = dep.id
                    WHERE emp.id = ". $this->db->quote($data->{'user_id'});

        return $this->db->query($sql);
    }

    public function updateInfo($data) {
        $employee = $this->db->create("employees");
        $employee->date_modified = date('Y-m-d H:i:s');
        $post_keys = array(
            "employee_no","address","landline_no","mobile_no",
            "address_street","address_line_2","address_city","address_state","address_zip_code","address_country",
            "emergency_name","emergency_address","emergency_contact_no","emergency_relationship",
            "emergency_name_two","emergency_address_two","emergency_contact_no_two","emergency_relationship_two",
            "emp_company_address","emp_company_contact_no","blood_type"
        );

        $result = false;

	      foreach ($post_keys as $key) {
            if (isset($data->{$key})) {
                if ($key == "address") {
                    $employee->address_street = $data->{$key};  
                    $result = true;
                } else if ($key == "mobile_no") {
                    if(ctype_digit($data->{$key}) && strlen($data->{$key}) == 11) {
                      $check_mobile = $this->db->query("SELECT count(id) AS total FROM employees WHERE mobile_no = ". $data->{$key} ." 
                        AND is_deleted <> 1 AND id <> ".$data->{'user_id'});

                      if(count($check_mobile) > 0) {
                        if($check_mobile[0]->total == "0") {
                          $result = true;
                          $employee->$key = $data->{$key};
                        }
                      }
                    }
                } else {
                    $employee->$key = $data->{$key};
                    $result = true;
                }

            }
	      }
        
        if ($result) {
          return $this->db->update($employee,array("id" => $data->{'user_id'}));
        }

       return $result;
    }

    public function updateImagePath($data) {
        $result = false;
        if (!empty($data)) {
            $employee = $this->db->create("employees");
            $employee->img_path = $data["img_path"];

            $result = $this->db->update($employee,array("id" => $data["user_id"]));
        }
        return $result;
    }
	
	public function getCheckIns($data) {
        $id = $data->{'user_id'};
		$result = $this->db->query("SELECT ci.id,
				                           ci.user_id,
				                           CONCAT(emp.first_name,' ',emp.last_name) AS emp_name ,
				                           st.description AS status,
				                           ci.date_checkedin, 
                                           ci.time_checkedin,
                                           ci.location,
                                           ci.message,
                                           emp.img_path
				                           FROM checkins ci
				                           LEFT JOIN employees emp ON ci.user_id = emp.id
				                           LEFT JOIN status st ON ci.status = st.id
				                           WHERE emp.id = $id ORDER BY ci.id DESC");
        return $result;
	}

    public function getCompanySiteEmployees($data) {
        $company_id = $data['company_id'];
        $site_id = $data['site_id'];

        $result = $this->db->query("SELECT e.id as id, e.mobile_no as mobile_no
                            FROM employees as e
                            INNER JOIN companies as c
                            ON e.company_id = c.id
                            INNER JOIN company_sites as cs
                            ON e.site_id = cs.id
                            WHERE cs.id = '" . $site_id . "'
                            AND c.id = '" . $company_id. "'");

        return $result;
    }

    public function check_prefix($data) {
        $mobile_no = $data;
        $result = $this->checking($mobile_no);

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    private function checking($passed_data) {

        $this->prefixes = $this->db->query("
                    SELECT number_range, start_msisdn,
                        end_msisdn, brand
                        FROM prefixes
                    ");

        $mobile_no = substr($passed_data, 1);

        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;

        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }
}