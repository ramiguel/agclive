<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Safezones extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_provinces() {
        $result = $this->db->query("SELECT id, province_name FROM provinces");
        return $result;
    }

    public function get_count_per_province() {
        $result = $this->db->query("SELECT province_id, province_name, COUNT(safezones.id) AS `count` FROM safezones 
                                    LEFT JOIN provinces ON provinces.id = safezones.province_id
                                    WHERE safezones.is_deleted <> 1
                                    GROUP BY province_id");
        return $result;
    }

    public function get_list($data) {
        $province_id = $data->{'province_id'};
        $category_id = $data->{'category_id'};
        $result = $this->db->query("SELECT s.id, safezone_name, 
                    CONCAT(street,', ',barangay,', ',city,', ',province_name) AS address,
                    information,
                    person_in_charge,
                    contact_no,
                    c.description AS category
                    FROM safezones s
                    LEFT JOIN provinces p ON p.id = s.province_id 
                    LEFT JOIN safezone_categories c ON c.id = s.category_id
                    WHERE s.province_id = " . $province_id . " 
                    AND s.category_id = " . $category_id . "
                    AND s.is_deleted <> 1");
        return $result;
    }

    public function get_safezone_supplies($safezone_id) {
        $safezone_supplies = $this->db->create(Table::SAFEZONE_SUPPLIES);
        $safezone_supplies->item_id = 0;
        $result = $this->db->query($safezone_supplies, array("safezone_id"=>$safezone_id));
        return $result;
    }

    public function save_safezone($data) {
        $safezone = $this->db->create(Table::SAFEZONES);
        $safezone->safezone_name = $data->{'safezone_name'};
        $safezone->street = $data->{'street'};
        $safezone->barangay = $data->{'barangay'};
        $safezone->province_id = $data->{'province_id'};
        $safezone->city = $data->{'city'};
        $safezone->zip_code = $data->{'zip_code'};
        $safezone->person_in_charge = $data->{'person_in_charge'};
        $safezone->contact_no = $data->{'contact_no'};
        $safezone->information = $data->{'information'};
        $safezone->nominated_by = $data->{'nominated_by'};
        $safezone->date_nominated = date('Y-m-d H:i:s');
        $safezone->is_nominated = 1;

        $result = $this->db->insert($safezone);

        if($result) {
            $safezone_id = $this->db->getLastInsertId();
            foreach ($data->{'supply_list'} as $key => $value) {
                $safezone_supplies = $this->db->create(Table::SAFEZONE_SUPPLIES);
                $safezone_supplies->safezone_id = $safezone_id;
                $safezone_supplies->item_id = intval($value);
                $this->db->insert($safezone_supplies);
            }
        }

        return $result;
    }    
}