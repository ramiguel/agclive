<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Checkins extends Model {
    public function __construct() {
        parent::__construct();
    }

    private $checkin_id = 0;
    
    public function getStatus() {
        $result = $this->db->query("SELECT id,description FROM status");
        return $result;
    }
    
    public function getItems() {
        $result = $this->db->query("SELECT id,item_name FROM items");
        return $result;
    }

    public function get_device_keys($input) {
        $id = intval($input->data->{'user_id'});
        $user_device_seek = $this->db->create("devices");
        $user_device_seek->device_key = "";

        $result = $this->db->query($user_device_seek,array("id" => $id));
        if (count($result) > 0) {
            $user_device_seek = $result[0];
            $result = $this->db->query("SELECT owner,device_key,os_type FROM devices WHERE device_key <> " . $this->db->quote($user_device_seek->device_key) . " GROUP BY device_key");
            return $result;
        } else {
            $user_device_seek = null;
            $result = $this->db->query("SELECT owner,device_key,os_type FROM devices GROUP BY device_key");
            return $result;
        }
    }

    public function getDisasterList($input) {
        $user_id = intval($input->{'user_id'});

        $result = $this->db->query("SELECT site_id FROM employees WHERE id = ".$user_id);

        if (count($result) > 0) {
            $site_id = $result[0]->{'site_id'};
            //$site_id = 0;

            $result = $this->db->query("SELECT c.id,c.disaster_name,c.disaster_details,c.disaster_type,dt.type 
                                        FROM cmdctr_sites cs
                                        LEFT JOIN cmdctr c ON c.id = cs.disaster_id
                                        LEFT JOIN disaster_type dt ON dt.id = c.disaster_type
                                        WHERE c.disaster_mode = 1 AND cs.site_id = ".$site_id);
        }
        return $result;
    }

    public function saveCheckin($data) {
        $date = date('Y-m-d');
        $time = date('H:i:s');

        $checkins = $this->db->create(Table::CHECKINS);
        $checkins->user_id = $data->{'user_id'};
        $checkins->status = $data->{'status'};
        $checkins->date_checkedin = $date;
        $checkins->time_checkedin = $time;
        $checkins->location = $data->{'location'};       
        $checkins->message = $data->{'message'};
        $checkins->no_of_persons = $data->{'no_of_persons'};
        $checkins->longitude = $data->{'longitude'};
        $checkins->latitude = isset($data->{'latitude'}) ? $data->{'latitude'} : 0;
        $checkins->disaster_id = count($data->{'disaster_id'}) > 0 ? $data->{'disaster_id'} : 0;
        $checkins->city = !empty($data->{'city'}) ? $data->{'city'} : "Others";
        $checkins->rescuer_id = 0;
        $checkins->date_rescued = "0000-00-00 00:00:00";
        $checkins->is_rescued = 0;

        $checkins->need_help = $data->{'need_help'};

        $checkin_id = 0;

        $cities_set = array("Binan","Dasmarinas","Las Pinas","Munoz","Paranaque");
        $cities_set_sc = array("Biñan","Dasmariñas","Las Piñas","Muñoz","Parañaque");
        $cities_set_sc_en = array("BiÃ±an","DasmariÃ±as","Las PiÃ±as","MuÃ±oz","ParaÃ±aque");

        $location_index = array_search($checkins->city, $cities_set);
        if ($location_index) {
            $checkins->city = $cities_set_sc_en[$location_index];
        }

        $location_index = array_search($checkins->city, $cities_set_sc);
        if ($location_index) {
            $checkins->city = $cities_set_sc_en[$location_index];
        }
        
        $result = false;

        if($data->{'disaster_id'} > 0) {
            foreach ($data->{'disaster_id'} as $value) {
                $signin_seek = $this->db->create(Table::CHECKINS);
                $signin_seek->id = 0;
                $previous_signin = $this->db->query($signin_seek,array("user_id"=>$checkins->user_id,"disaster_id"=>$value));

                if (count($previous_signin) > 0) {
                    $checkins->disaster_id = $value;
                    $result = $this->db->update($checkins,array("id"=>$previous_signin[0]->{'id'}));
                    $checkin_id = $previous_signin[0]->{'id'};
                } else {
                    $checkins->disaster_id = $value;
                    $result = $this->db->insert($checkins);
                    $checkin_id = $this->db->getLastInsertId();
                }

                $checkin_history_id = 0;
                //History
                $checkin_history = $this->db->create("checkin_history");
                $checkin_history->checkin_id = $checkin_id;
                $checkin_history->status = $checkins->status;
                $checkin_history->date_updated = date("Y-m-d H:i:s");
                $checkin_history->location = $checkins->location;
                $checkin_history->message = $checkins->message;
                $checkin_history->no_of_persons = $checkins->no_of_persons;
                $checkin_history->longitude = $checkins->longitude;
                $checkin_history->latitude = $checkins->latitude;
                $checkin_history->need_help = $checkins->need_help;
                $checkin_history->city = $checkins->city;
                $this->db->insert($checkin_history);

                $checkin_history_id = $this->db->getLastInsertId();

                if (isset($data->{'item_list'})) {
                    $items = $data->{'item_list'};
                    $get_checkin = $this->db->query("SELECT COUNT(checkin_id) AS count FROM checkin_items WHERE checkin_id=".$checkin_id);
                    if(count($get_checkin) > 0) {
                        $this->db->update("DELETE FROM checkin_items WHERE checkin_id = " . $checkin_id);
                    }

                    foreach ($items as $item_id) {
                        $checkin_items = $this->db->create("checkin_items");
                        $checkin_items->checkin_id = $checkin_id;
                        $checkin_items->item_id = $item_id;
                        $result = $this->db->insert($checkin_items);

                        $checkin_item_history = $this->db->create("checkin_item_history");
                        $checkin_item_history->checkin_history_id = $checkin_history_id;
                        $checkin_item_history->item_id = $item_id;
                        $result = $this->db->insert($checkin_item_history);
                    }
                }
            }
        }

        if ($result) {
            $disasterList ="";
            foreach ($data->{'disaster_id'} as $key => $value) {
                $disasterList.= $value. ",";
            }

            $disasterList = rtrim($disasterList, ",");

            if($disasterList == "") {
                $disasterList = "NULL";
            }

            $date_read = date("Y-m-d H:i:s");

            $result = $this->db->update("UPDATE cmdctr_status SET is_read = 1, date_read = '$date_read'
                                            WHERE notification_id IN (".$disasterList.") AND user_id = ".$data->{'user_id'});

            $status = $data->{'status'};

            //FOR SUPERVISOR UPDATE
            $employee = $this->db->create("employees");
            $employee->status = $status;
            $result = $this->db->update($employee,array("id" => $data->{'user_id'}));
        }

        return $result;
    }

    public function save_to_reportin_is_read($reportin_id, $user_id) {
        $reportin = $this->db->create(Table::REPORTIN_STATUS);
        $reportin->reportin_id = $reportin_id;
        $reportin->user_id = $user_id;
        return $this->db->insert($reportin);
    }

    public function getCheckinId() {
        return $this->checkin_id;
    }
}