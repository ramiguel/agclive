<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Contacts extends Model {
    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        $result = $this->db->query("SELECT id,contact_name,logo FROM emergency_hotline 
        							WHERE is_deleted <> 1 ORDER BY contact_name ASC");

        return $result;
    }

    public function get_contact_nos($id) {
        $result = $this->db->query("SELECT contact_no FROM emergency_hotline_num
									WHERE hotline_id = ". $id);
        return $result;
    }
}