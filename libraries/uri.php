<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Uri {
    private $uri;
    private $segments;

    public function __construct() {    	
    	$request_uri = $_SERVER["REQUEST_URI"];
    	$base_path = BASE . "/";
    	$base_path_length = strlen($base_path);
        $request_uri_length = strlen($request_uri);

        if (strpos($request_uri,$base_path) === false) {
            $this->uri = $request_uri;
        } else {
            $this->uri = substr($request_uri,$base_path_length,($request_uri_length - $base_path_length));
        }

        $this->segments = explode("/",$this->uri);
    }

    public function get_segments() {
        return $this->segments;
    }
}
