<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Helper {
    public function get_csv($file_path,$get_as_object) {
        $csv_data = array_map("str_getcsv",file($file_path));
        $row = array();
        if (count($csv_data)) {
            $keys = array();

            foreach ($csv_data[0] as $key) {
                $keys[] = trim($key); 
            }
      
            array_shift($csv_data); // skip headers

            if ($get_as_object) {
                foreach ($csv_data as $data) {
                    $line = new stdClass();
                    foreach ($keys as $index => $name) {
                        $line->$name = $data[$index];
                    }

                    $row[] = $line;
                }
            } else {
                foreach ($csv_data as $data) {
                    $line = array();
                    foreach ($keys as $index => $name) {
                        $line[$name] = $data[$index];
                    }

                    $row[] = $line;
                }
            }
        }

        return $row;
    }

    public function create_csv_stream($file_name,$upload_dir,$headers,$result) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$file_name);

        $file_output = fopen($upload_dir.$file_name,"wb");

        fputcsv($file_output,$headers);

        if (count($result) > 0) {
            foreach ($result as $data) {
                $line = array();
                foreach ($headers as $key) {
                    if ($key == "mobile_no") {
                        $line[$key] = ""+$data->$key;
                    } else {
                        $line[$key] = $data->$key;
                    }
                }
                fputcsv($file_output,$line);
            }
        }
        fclose($file_output);

        $file_output = fopen($upload_dir.$file_name,"rb");
        fpassthru($file_output);
    }

    public function xls_clear_data(&$str) {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

    public function create_xls($file_name, $upload_dir, $headers, $result) {
        $file_output = fopen($upload_dir.$file_name,"wb");
        fputcsv($file_output,$headers);

        $flag = false;
        $count = count($headers);
        $header = '';
        $data = '';

        for($i = 0; $i< $count; $i++):
            $header .= $headers[$i] . "\t";
        endfor;

        foreach($result as $key => $value):
            $line = '';
            foreach($value as $k => $v):
                $v =  '"' . $v . '"';

                if($line === '') {
                    $line = $v;
                } else {
                    $line .= "\t" . $v;
                }
            endforeach;

            $data .= $line . "\n";
            fputcsv($file_output,get_object_vars($value));
        endforeach;

        if($data == "") {
            $data = "No records available.";
        }
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=".$file_name.".xls");
        header("Pragma: no-cache");
        header("Expires: 0");


        fclose($file_output);
        $file_output = fopen($upload_dir.$file_name,"rb");
        fpassthru($file_output);

    }

}

if ( ! function_exists('redirect_this'))
{
    function redirect_this($uri = '', $method = 'location', $http_response_code = 302)
    {
        switch($method)
        {
            case 'refresh'	: header("Refresh:0;url=".$uri);
                break;
            default			: header("Location: ".$uri, TRUE, $http_response_code);
                break;
        }
        exit;
    }
}