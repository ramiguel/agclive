<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
/*
 * Form Validation Utility
 *
 * Supported rules for now:
 *
 *     key => array(
 *         required => true,
 *         type => type,
 *         extras => array(
 *             min => 0,
 *             max => 10
 *         )
 *     )
 *
 * Types available:
 *
 * text, email, mobile, int, bool
 */

class Form {
    const TYPE_TEXT = "text";
    const TYPE_EMAIL = "email";
    const TYPE_MOBILE = "mobile";
    const TYPE_INT = "int";
    const TYPE_BOOL = "bool";
    const TYPE_URL = "url";
    const TYPE_NUMBER = "number";

    const ERROR_ID_NOT_SET = 1;
    const ERROR_VALIDATION_FAILED = 2;

    public function __construct() {
    }

    /* Validators */

    public function validate_text($value,$extras=array()) {
        $range_min = isset($extras["min"]) ? $extras["min"] : 0;
        $range_max = isset($extras["max"]) ? $extras["max"] : 1;

        return ($this->in_range($value,$range_min,$range_max));
    }

    public function validate_email($value,$extras=array()) {
        if (filter_var($value,FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    public function validate_mobile($value,$extras=array()) {
        $range_min = isset($extras["min"]) ? $extras["min"] : 0;
        $range_max = isset($extras["max"]) ? $extras["max"] : 1;

        $verification_state = false;

        /* Mobile Number Foramt 
         *
         * + CC | NP | N7
         * ----------------------------------------------------------------------------------
         * CC = Country Code
         * NP = Network Prefix
         * N7 = Number
         *
         */

        
        if (ctype_digit($value) && $this->in_range($value,$range_min,$range_max)) {
            return true;
        } else {
            return false;
        }
    }

    public function validate_int($value,$extras=array()) {
        if (ctype_digit($value)) {
            return true;
        } else {
            return false;
        }
    }

    public function validate_bool($value,$extras=array()) {
        if ($value==true) {
            return true;
        } else {
            return false;
        }
    }

    public function validate_url($value,$exras=array()) {
        if (filter_var($value,FILTER_VALIDATE_URL)) {
            return true;
        } else {
            return false;
        }
    }

    public function validate_number($value,$exras=array()) {
        if (ctype_digit($value)) {
            return true;
        } else {
            return false;
        }
    }

    /* Sanitizers */

    public function sanitize_text($value) {
        return $value;
    }

    public function sanitize_email($value) {
        return filter_var($value,FILTER_SANITIZE_EMAIL);
    }

    public function sanitize_mobile($value) {
        if (strlen($value) == 10) {
            if ($value[0] != "0") {
                $value = "0" . $value;
            }   
        }
        return $value;
    }

    public function sanitize_int($value) {
        return $value;
    }

    public function sanitize_bool($value) {
        return $value == true ? 1 : 0;
    }

    public function sanitize_url($value) {
        return filter_var($value,FILTER_SANITIZE_URL);
    }

    public function sanitize_number($value) {
        return $value;
    }

    // This basically is the main validator

    public function validate($rules = array(),$form_data=array(),$callback,$error) {
        $error_count = 0;
        foreach ($rules as $key => $data) {
            if (isset($form_data[$key])) {
                $data_type = isset($data["type"]) ? $data["type"] : "";
                $rule_extras = isset($data["extras"]) ? $data["extras"] : array();
                $form_value = $form_data[$key];

                if ($data["required"] == true) {
                    $call = "validate_" . $data_type;
                    if ($this->$call($form_data[$key],$rule_extras) == false) {
                        $error_count++;
                        $error(Form::ERROR_VALIDATION_FAILED,$key);
                    } else {
                        $sanitize = "sanitize_" . $data_type;
                        $form_value = $this->$sanitize($form_value);

                        $callback($key,$form_value);
                    }
                } else {
                    $sanitize = "sanitize_" . $data_type;
                    $form_value = $this->$sanitize($form_value);

                    $callback($key,$form_value);
                }
            } else {
                if ($data["required"] == true) {
                    $error_count++;
                    $error(Form::ERROR_ID_NOT_SET,$key);  
                }
            }
        }
        return $error_count;
    }

    public function in_range($value,$range_min,$range_max) {
        $length = strlen($value);
        return ($length >= $range_min && $length <= $range_max);
    }
}