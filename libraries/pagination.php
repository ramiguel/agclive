<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Pagination {

    public static function compute($page,$range,$total,$items) {
        $pages = array();

        $total_pages = ceil($total / $items);

        $start_page = (($page-1)-(($page-1)%$range))+1;
        $start_page = $start_page > $total_pages ? $total_pages - $range : $start_page;

        $last = ceil((($start_page+ $range)/$range) + 1);

        $start_page = $start_page < 1 ? 1 : $start_page;
        $last = $last < $range ? $range : $last;
        $last += $start_page;

        $prev = $start_page-1;
        if ($prev < 1) {
            $prev = 1;
        }

        while ($start_page < $last) {
            $pages[] = array("id" => $start_page,"show" => $start_page > $total_pages ? 0 : 1);
            $start_page++;
        }

        $next = $start_page;
        if ($next > $total_pages) {
            $next = $total_pages;
        }

        return array(
            "first" => 1,
            "prev" => $prev,
            "next" => $next,
            "last" => $total_pages,
            "total" => $total_pages,
            "pages" => $pages);
    }
}