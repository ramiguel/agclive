<?php

class Ios_push_notif {
	public function pushBroadcast($reg_id, $message, $count) {
		foreach($reg_id as $key=>$value) {
			$deviceArray = $value;
			$broadcast_message = $message['message'];
			$mode = $message['disaster_mode'];
			$id = $message['notif_id'];
			$badge = $count;
			$sound = 'chime';
		
			$body = array();
			$body['aps'] = array('alert' => $broadcast_message);
			if ($badge)
				$body['aps']['badge'] = $badge;
			if ($sound)
				$body['aps']['sound'] = $sound;

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PUSH_URL);
			
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

			$payload = json_encode($body);

			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $deviceArray)) . pack("n",strlen($payload)) . $payload;

			fwrite($fp, $msg);

			fclose($fp);
		}
	}
	
	
	public function pushBroadcast_New($payload,$count) {
		foreach ($payload as $payload_data) {
			$body = array();
			$body['aps']['alert'] = $payload_data->message['message'];
			$body['aps']['badge'] = $count;
			$body['aps']['sound'] = "chime";
			
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PUSH_URL);
			
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
			
			$payload_encoded = json_encode($body);
			
			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $payload_data->device_key)) . pack("n",strlen($payload_encoded)) . $payload_encoded;
			
		    fwrite($fp, $msg);

			fclose($fp);
		}
	}
	
	public function pushDeclareDisaster($payload, $count) {
		foreach ($payload as $payload_data) {
			$body = array();
			$body['aps']['alert'] = $payload_data->message['message'];
			$body['aps']['badge'] = $count;
			$body['aps']['sound'] = "chime";
			
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PUSH_URL);
			
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
			
			$payload_encoded = json_encode($body);
			
			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $payload_data->device_key)) . pack("n",strlen($payload_encoded)) . $payload_encoded;
			echo json_encode($fp);
			fwrite($fp, $msg);

			fclose($fp);
		}
	}
	
	public function push_disaster($reg_id, $message, $count) {
		foreach($reg_id as $key=>$value) {
			$deviceArray = $value;
			$broadcast_message = $message['message'];
			$mode = $message['disaster_mode'];
			$id = $message['notif_id'];
			$badge = $count;
			$sound = 'chime';
		
			$body = array();
			$body['aps'] = array('alert' => $broadcast_message);
			if ($badge)
				$body['aps']['badge'] = $badge;
			if ($sound)
				$body['aps']['sound'] = $sound;

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PUSH_URL);
			
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

			$payload = json_encode($body);

			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $deviceArray)) . pack("n",strlen($payload)) . $payload;

			fwrite($fp, $msg);

			fclose($fp);
		}
	}
	
	public function pushCheckin($reg_id, $message) {
		foreach($reg_id as $key=>$value) {
			$deviceToken = $value;
			$broadcast_message = $message['message'];
			$sound = 'chime';
		
			$body = array();
			$body['aps'] = array('alert' => $broadcast_message);
			
			if ($sound)
				$body['aps']['sound'] = $sound;

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PUSH_URL);
			
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

			$payload = json_encode($body);

			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n",strlen($payload)) . $payload;

			fwrite($fp, $msg);

			fclose($fp);
		}
	}

	public function pushIncident($reg_id, $message) {
		foreach($reg_id as $key=>$value) {
			$deviceToken = $value;
			$broadcast_message = $message['message'];
			$sound = 'chime';
		
			$body = array();
			$body['aps'] = array('alert' => $broadcast_message);
			if ($badge)
				$body['aps']['badge'] = $badge;
			if ($sound)
				$body['aps']['sound'] = $sound;

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PUSH_URL);
			
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

			$payload = json_encode($body);

			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n",strlen($payload)) . $payload;

			fwrite($fp, $msg);

			fclose($fp);
		}
	}
}
?>	