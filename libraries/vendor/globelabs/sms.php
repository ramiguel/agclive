<?php
class Sms extends GlobeApi
{
    //PUBLIC VARIABLES
    public $version;
    public $recepient;
    public $message;
    public $description;
    public $address;
    public $senderName;
    public $clientCorrelator;
    public $criteria;
    public $subscriptionId;

    const CURL_URL = 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/4357/requests?';
    const RESC_URL = 'https://%s/smsmessaging/%s/inbound/registrations/%s/messages';

    public function __construct(
        $address = null,
        $version = null
    ) {
        $this->version = $version;
        $this->address = $address;
    }

    public function getMessage($request)
    {
        return (is_null($request)) ? array() : (is_array($request)) ? $request : json_decode($request);
    }

    public function getMessages($bodyOnly = true)
    {
        $url = sprintf(self::RESC_URL, GlobeApi::API_ENDPOINT, $this->version, $this->address);
        $response = $this->_curlGet($url, array());
        return $this->getReturn($response, $bodyOnly);
    }

    public function sendMessage(
        $number = null,
        $message = null,
        $bodyOnly = true
    ) {

        if($number) {
            $this->recepient = $number;
        }

        if($message) {
            $this->message = $message;
        }

        if(!$this->recepient) {
            throw new Exception('recepient is required');
        }

        if(!$this->message) {
            throw new Exception('message is required');
        }

        if(!$this->address) {
            throw new Exception('shortcode is required');
        }

        $url = sprintf(
            Sms::CURL_URL,
            GlobeAPI::API_ENDPOINT,
            $this->version,
            urlencode($this->address)
        );

        $format = "";

        $postFields = array(
            'app_id' => APP_ID,
            'app_secret' => APP_SECRET,
            'passphrase' => PASSPHRASE,
            'address' => $number,
            'message' => stripslashes($message),
            'clientCorrelator' => CORRELATOR
        );

        $postFields = array_filter($postFields);
        $response = $this->_curlPost($url, $postFields);

        return $this->getReturn($response, $bodyOnly);
    }
}
