<?php
class Auth extends GlobeApi
{
    const LOGIN_URL = 'https://%s/dialog/oauth';
    const AUTH_URL = 'https://%s/oauth/access_token';

    private $api_key;
    private $api_secret;

    public function __construct(
        $api_key,
        $api_secret
    ) {
        $this->api_key = $api_key;
        $this->api_secret = $api_secret;
    }

    public function getCode($request)
    {
        return (isset($request['code'])) ? $request['code'] : NULL;
    }

    public function getLoginUrl()
    {
        return sprintf(Auth::LOGIN_URL, GlobeApi::AUTH_POINT).'?app_id='.$this->api_key;
    }

    public function getAccessToken($code, $sms = false, $bodyOnly = true)
    {
        if($sms && $code['access_token']) {
            return $code;
        } else if($sms) {
            return NULL;
        }

        $url = sprintf(Auth::AUTH_URL, GlobeApi::AUTH_POINT);

        $fields = array(
            'app_id' => $this->api_key,
            'app_secret' => $this->api_secret,
            'code' => $code
        );

        $url.='?'.http_build_query($fields);

        $response = $this->_curlPost($url);

        return $this->getReturn($response, $bodyOnly);
    }
}
