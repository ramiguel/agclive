<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Security {
    public function checkpoint($root,$module_uri) {

        $module_uri_end = strlen($module_uri);
        $module_uri_end = $module_uri_end < 1 ? 1 : $module_uri_end;

        if ($module_uri[$module_uri_end-1] == "/") {
            $module_uri = substr($module_uri,0,$module_uri_end-1);
        }

        if (!$root->session->userdata("user_id")) {
            redirect(base_url() . "/");
        } else {
            $session = null;
            if (($session=$this->hasActiveSession($root)) == null) {
                redirect(base_url() . "/");
            } else {
                if (!$this->hasAccessRights($root,$root->session->userdata("user_id"),$module_uri)) {
                    redirect(base_url() . "/error/access-denied");
                } else {
                    $this->updateSession($root,$session);
                }
            }
        }
    }

    private function hasAccessRights($root,$user_id,$module_uri) {
        $result = $root->db->query("SELECT COUNT(um.user_id) as module_count
                          FROM user_module AS um
                          LEFT JOIN modules AS mo ON mo.id = um.module_id
                          WHERE um.user_id = ".$user_id." AND mo.module_uri = " . $root->db->quote($module_uri));

        $state = false;
        if (count($result) && $result[0]->module_count > 0) {
            $state = true;
        }
        return $state;
    }

    private function hasActiveSession($root) {
        $user_id = $root->session->userdata("user_id");
        $session_id = $root->session->get_session_id();

        $session = $root->db->create("session");
        $session->id = 0;
        $session->user_id = 0;
        $session->session_id = "";

        $result = $root->db->query($session,array(
            "user_id" => $user_id,
            "state" => 0
        ));

        $state = null;

        if (count($result) > 0 && $result[0]->id) {
            foreach ($result as $item) {
                if ($session_id == $item->session_id) {
                    $state = $result[0];
                    break;
                }
            }
        }

        return $state;
    }

    private function updateSession($root,$session) {

        $current_date = date('Y-m-d H:i:s');

        $user_session = $root->db->create("session");
        $user_session->last_update = $current_date;

        $root->db->update($user_session,array(
            "id" => $session->id
        ));
    }

    public function logout($root) {
        if ($root->session->userdata("user_id")) {
            $session = $root->db->create("session");
            $session->user_id = $root->session->userdata("user_id");
            $session->session_id = $root->session->get_session_id();
            $session->state = 1;

            $root->db->update($session,array(
                "user_id" => $session->user_id,
                "session_id" => $session->session_id
            ));
        }
        $root->session->destroy();
        redirect(base_url() . "/checkpoint/login");
    }
}