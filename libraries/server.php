<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Server {
	public function maintenance_check($parent,$bailout=false) {
	    $result = $parent->db->query("SELECT status FROM maintenance");
        if (count($result) > 0) {
        	$role = $parent->session->userdata("role_id");
        	$role = $role == null ? 0 : $role;
            
        	if ($result[0]->status == 1 && $bailout == false) {
        		if ($role != 1) {
        	        redirect(base_url() . "/maintenance");
        	    }
            }
        } 
	}
}