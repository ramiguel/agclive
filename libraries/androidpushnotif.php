<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
require_once LIBRARY_PATH . "vendor/GCM.php";

class AndroidPushNotif {
	public function push($registration_ids, $message) {
        $GCM = new GCM();
		$GCM->send_notification($registration_ids, $message);
	}
	
	public function push_disaster($registration_ids, $message) {
        $GCM = new GCM();
		$GCM->send_notification($registration_ids, $message);
	}
	
	public function pushBroadcast($payload) {
        $GCM = new GCM();
		$GCM->send_notification_new($payload);
	}
}
?>