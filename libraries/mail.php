<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
require_once LIBRARY_PATH . "vendor/phpmailer/class.pop3.php";
require_once LIBRARY_PATH . "vendor/phpmailer/class.smtp.php";
require_once LIBRARY_PATH . "vendor/phpmailer/class.phpmailer.php";

class Mail {
     public function send($recipient,$subject,$body) {
         $mail = new PHPMailer(false);

         $mail->isSMTP();

         $mail->Host = MAIL_HOST;
         $mail->CharSet = "UTF-8";
         $mail->SMTPAuth = true;
         $mail->SMTPSecure = "tls";
         $mail->SMTPDebug = 0;
         $mail->Port = MAIL_PORT;
         $mail->Username = MAIL_USER;
         $mail->Password = MAIL_PASS;

         $mail->setFrom(MAIL_FROM,MAIL_FROM_NAME);
         $mail->addReplyTo(MAIL_REPLY_TO,MAIL_REPLY_TO_NAME);

         $mail->Subject = $subject;
         $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";

         $mail_sent = false;

         $mail->isHTML(true);

         if (is_array($recipient)) {
             foreach ($recipient as $name => $email) {
                 $mail->addAddress($email,$name);
             }
         } else {
             $mail->addAddress($recipient,$recipient);
         }

         $mail->MsgHTML($body);
         if ($mail->Send()) {
             $mail_sent = true;
         }

         return $mail_sent;
     }
}