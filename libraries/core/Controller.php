<?php
class Controller {
    public $load;
    public $view;

    public function __construct() {
        $this->load = new sdgClass();

        $this->load->library = __declare($this,function($library) {
            if (is_array($library)) {
                foreach ($library as $lib) {
                    $this->$lib = load_class(LIBRARY_PATH, $lib);
                }
            } else {
                $this->$library = load_class(LIBRARY_PATH, $library);
            }
        });

        $this->load->model = __declare($this,function($model) {
            if (is_array($model)) {
                foreach ($model as $mod) {
                    $this->$mod = load_class(MODEL_PATH,$mod);
                }
            } else {
                $this->$model = load_class(MODEL_PATH, $model);
            }
        });
    }
}