<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

define ('TEMPLATE_EXT','.html');

class Themes {
    private $layout;
    private $template;
    private $vars;

    public function __construct() {
        $this->layout = "";
        $this->template = "";
        $this->vars = array();
    }

    public function layout($layout) {
        $this->layout = $layout;
    }

    public function template($template) {
        $this->template = $template;
    }

    public function set($name,$value) {
        $this->vars[$name] = $value;
    }

    public function render() {
        $request_uri = $_SERVER["REQUEST_URI"];
        $base_path = BASE . "/";
        $base_path_length = strlen($base_path);
        $request_uri_length = strlen($request_uri);

        if (strpos($request_uri,$base_path) === false) {
            $this->uri = $request_uri;
        } else {
            $this->uri = substr($request_uri,$base_path_length,($request_uri_length - $base_path_length));
        }

        $uri_segments = explode("/",$this->uri);
        $module = $uri_segments[0];

        $controller_themes_path = BASEPATH . "modules/$module/themes/";

        $themes_layout_file = $controller_themes_path . "_layouts/$this->layout" . TEMPLATE_EXT;
        $themes_content_template_file = $controller_themes_path . "_content/$this->template" . TEMPLATE_EXT;

        if (is_file($themes_layout_file) && is_file($themes_content_template_file)) {
            $container = file_get_contents($themes_layout_file);
            $content = file_get_contents($themes_content_template_file);

            $patterns = array();
            $values = array();

            foreach ($this->vars as $key => $value) {
                $patterns[] = "/{{\s*$key\s*}}/";
                $values[] = $value;
            }

            if (count($patterns) > 0) {
                $container = preg_replace($patterns, $values, $container);
                $content = preg_replace($patterns, $values, $content);
            }

            preg_match_all("/{%\s*block\s*(\w+)\s*%}(.*){%\s*endblock\s*%}/siU",$content,$content_map,PREG_PATTERN_ORDER);
            if (count($content_map) >= 2) {
                array_shift($content_map);
                foreach ($content_map[0] as $index => $key) {
                    $container = preg_replace("/{%\s*block\s*$key\s*%}\s*{%\s*endblock\s*%}/",trim($content_map[1][$index]),$container);
                }
            }

            print $container;
        }
    }
}