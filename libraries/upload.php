<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Upload {
    private $file;
    private $allowed_file_types;
    private $max_size;
    private $overwrite_file;

    private $uploaded_file_name;

    const TYPE_IMAGE = 2;
    const TYPE_BINARY = 1;

    public function __construct() {
        $this->file = $_FILES;

        $this->max_size = 1000000;
        $this->allowed_file_types = array();
        $this->overwrite_file = true;

        $this->uploaded_file_name = "";

        unset($_FILES);
    }

    public function get_uploaded_filename() {
        return $this->uploaded_file_name;
    }

    public function has($name) {
        return isset($this->file[$name]) ? true : false;
    }

    public function allowed($types=array()) {
        $this->allowed_file_types = $types;
    }

    public function is_uploaded($name) {
        if ($this->has($name) && is_uploaded_file($this->file[$name]["tmp_name"])) {
            return true;
        } else {
            return false;
        }
    }

    public function get_name($id) {
        if ($this->has($id)) {
            return $this->file[$id]["name"];
        } else {
            return null;
        }
    }

    public function get_base_path($id) {
        if ($this->has($id)) {
            return basename($this->file[$id]["name"]);
        } else {
            return "";
        }
    }

    public function get_error($id) {
        if ($this->has($id)) {
            return $this->file[$id]["error"] != 0;
        } else {
            return 1;
        }
    }

/*
 * File Upload Config
 *  
 * upload_dir
 * file_type
 * width, height  
 *
 */

    public function do_upload($id,$config=array()) {
        $file = $this->file[$id];

        $upload_dir = $config["upload_dir"];
        $file_type = $config["file_type"];
        $prefix = isset($config["file_prefix"]) ? $config["file_prefix"]."." : "";

        $file_upload_state = false;
        $this->uploaded_file_name = basename($file["name"]);
        $upload_file_path = $upload_dir . $this->uploaded_file_name;
        $temp_file_path = $file["tmp_name"];
        $image_upload = false;

        $file_check_error = true;
        if ($file_type == Upload::TYPE_IMAGE) {
            if (getimagesize($temp_file_path) !== false) {
                $file_check_error = true;   
                $image_upload = true;
            }
        }

        if ($file_check_error == true && is_uploaded_file($temp_file_path)) {
            $file_extension = pathinfo($upload_file_path,PATHINFO_EXTENSION);
            $file_id = uniqid();
            $this->uploaded_file_name = $prefix . $file_id . "." . $file_extension;  

            if (in_array(strtolower($file_extension),array_map('strtolower',$this->allowed_file_types))) {
                if ($file["size"] < $this->max_size) {
                    if ($image_upload == true && isset($config["sizes"])) {
                        // foreach ($config["sizes"] as $width => $height) {
                        //     list($w,$h) = getimagesize($temp_file_path);
                        //     $ratio = max($width/$w,$height/$h);
                        //     $h = ceil($height / $ratio);
                        //     $x = ($w - $width / $ratio) / 2;
                        //     $w = ceil($width / $ratio);

                        //     $image_file_path = $upload_dir . $prefix . $file_id . $width ."x" . $height;
                        //     $image_data = file_get_contents($temp_file_path);
                        //     $image = imagecreatefromstring($image_data);
                        //     $tmp = imagecreatetruecolor($width, $height);
                        //     imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
                        //     $file_type = $file["type"];
                        //     if ($file_type == "image/jpeg") {
                        //        imagejpeg($tmp,$image_file_path.".jpg",100); 
                        //     } else if ($file_type == "image/png") {
                        //        imagepng($tmp,$image_file_path.".png",0);
                        //     } else if ($file_type == "image/gif") {
                        //        imagegif($tmp,$image_file_path.".gif");
                        //     }

                        //     imagedestroy($image);
                        //     imagedestroy($tmp);
                        // }
                    }

                    $file_upload_state = move_uploaded_file($temp_file_path,$upload_dir . $this->uploaded_file_name);
                }
            }
        }
        return $file_upload_state;
    }
}