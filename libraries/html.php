<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Html {
    public function option($label,$value,$selected = false,$attr = array()) {
        if ($selected == true) {
            $attr["selected"] = "";
        }
        $attr["value"] = $value;
        return $this->create_tag("option",$label,true,$attr);
    }

    public function td($body,$attr=array()) {
        return $this->create_tag("td",$body,true,$attr);
    }

    public function tr($body,$attr=array()) {
        return $this->create_tag("tr",$body,true,$attr);
    }

    public function input($type,$name,$attr = array()) {
        $attr["type"] = $type;
        $attr["name"] = $name;
        return $this->create_tag("input","",false,$attr);
    }

    public function button($type,$body,$attr=array()) {
        $attr["type"] = $type;
        return $this->create_tag("button",$body,true,$attr);
    }

    public function i($body,$attr=array()) {
        return $this->create_tag("i",$body,true,$attr);
    }

    public function create_tag($tag,$body,$close,$attr=array()) {
        $attrs = $this->populate_attributes($attr);
        return "<$tag$attrs>$body" . ($close == true ? "</$tag>" : "");
    }

    private function populate_attributes($attr = array()) {
        $attrs = "";
        foreach ($attr as $key => $value) {
            $attrs .= " $key" . (empty($value) == true ? "" : "=\"$value\"");
        }
        return $attrs;
    }
}