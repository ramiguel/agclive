<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Input {
    private $data;
    private $type;

    const STREAM = 1;
    const POST = 2;
    
    public function __construct() {
        $this->data = array();
        $this->type = Input::POST;
    }

    public function get_data($type=Input::POST) {
        $request = new sdgClass();

        $this->type = $type;

        if ($this->request_method('POST')) {
            if ($type==Input::STREAM) {
                $request->data = json_decode(file_get_contents("php://input"));
            } else {
                foreach ($_POST as $key => $value) {
                    $this->data[$key] = $value;
                }
                $request->data = $this->data;

                unset($_POST);
            }
        }

        $request->get_data = __declare($request,function() {
            if (isset($this->data) && !empty($this->data)) {
                return $this->data;
            } else {
                return json_encode((object)null);
            }
        });

        $request->validate = __declare($request,function() {
            $accepted = false;
            if (isset($this->data) && (json_last_error() == JSON_ERROR_NONE)) {
                $accepted = true;
            }

            return $accepted;
        });

        $request->isValid = __declare($request,function() {
            return $this->validate();
        });

        return $request;
    }

    public function request_method($method) {
        return $_SERVER['REQUEST_METHOD'] == $method;
    }

    public function get_remote_addr() {
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "0.0.0.0";
    }

    public function get_proxy() {
        print implode(" ",array($_SERVER['HTTP_CLIENT_IP'],$_SERVER['HTTP_X_FORWARDED_FOR'],"127.0.0.1","::1"));
    }
}