<?php
class CMS_Controller extends Controller {
    private $active_url;
    private $module_name;

    const PAGE_TITLE = "AGC Employee Locator | CMS";

    const ROLE_SUPER_ADMINISTRATOR = 1;
    const ROLE_ADMINISTRATOR = 2;
    const ROLE_SUPERVISOR = 4;
    const ROLE_RESCUER = 5;
    const ROLE_EMPLOYEE = 6;

    private $audit;
    private $activity;

    public function __construct($active_url,$module_name) {
        parent::__construct();

        $this->setActive($active_url,$module_name);

        $this->load->library(array("session","security","themes","db","helper","pagination","alert","server"));
        $this->init();
    }

    private function init() {
        $this->server->maintenance_check($this);
        $this->initialize_page();
    }

    private function initialize_page() {
        $this->create_menu();
        $this->create_page();
    }

    private function create_menu() {
        $user_id = $this->session->userdata("user_id");

        $menu = $this->db->query("SELECT m.id,m.url,m.icon,m.label,m.has_submenu,m.is_submenu_of FROM admin_menu AS am
                                  LEFT JOIN cms_menu AS m ON m.id = am.menu_id
                                  WHERE am.user_id = " . $user_id);

        $this->active_url = base_url() . $this->active_url;

        $menu_content = "";

        foreach ($menu as $menu_item) {
            $menu_url = base_url() . $menu_item->url;
            if ($menu_item->has_submenu > 0) {
                $sub_menu = "";
                foreach ($menu as $sub_menu_item) {
                    if ($sub_menu_item->is_submenu_of == $menu_item->id) {
                        $sub_menu_url = base_url() . $sub_menu_item->url;
                        $sub_menu .= "<li>
                                          <a href=\"$sub_menu_url\">
                                              <i class=\"fa $sub_menu_item->icon\"></i>
                                              <span>$sub_menu_item->label</span>
                                          </a>
                                      </li>";
                    }
                }
                $menu_content.="<li>
                                <a href=\"#\" class=\"tree-toggle nav-header\">
                                    <i class=\"fa $menu_item->icon\"></i>
                                    <span>$menu_item->label <i class=\"fa fa-angle-left\"></i></span>
                                </a>
                                <ul class=\"nav nav-list tree\">
                                    $sub_menu
                                </ul>
                            </li>";
            } else if ($menu_item->is_submenu_of == 0) {
                $menu_content.="<li>
                                <a href=\"$menu_url\"".(($this->active_url == $menu_url) ? " class=\"active\"" : "") .">
                                    <i class=\"fa $menu_item->icon\"></i>
                                    <span>$menu_item->label</span>
                                </a>
                            </li>";
            }
        }
        $this->themes->set("menu_items",$menu_content);
    }

    public function render($layout,$template,$title) {
        $this->themes->set("title",$title);

        $this->themes->layout($layout);
        $this->themes->template($template);
        $this->themes->render();
    }

    public function set($name,$value) {
        $this->themes->set($name, $value);
    }

    private function create_page() {
        $this->themes->set('base_url',base_url());
        $this->themes->set("active_user_name",$this->session->userdata("full_name"));
        $this->themes->set('module_name',$this->module_name);

        $this->themes->set("AGC_EMPLOYEE_LOCATOR_CMS_VERSION",AGC_EMPLOYEE_LOCATOR_CMS_VERSION);
    }

    private function setActive($active_url,$module_name) {
        $this->active_url = $active_url;
        $this->module_name = $module_name;
    }

    public function checkpoint($module) {
        $this->security->checkpoint($this,$module);
    }

    public function log_open($component,$action,$description) {
        $this->audit = $this->db->create("admin_actions");
        $this->audit->component = $component;

        $this->audit->description = $description;
        if ($action == "update") {
            $this->audit->description = "Updating " . $this->audit->description;
        } else if ($action == "insert") {
            $this->audit->description = "Inserting into " . $this->audit->description;
        }

        $this->audit->date_created = date('Y-m-d H:i:s');
        $this->audit->action = $action;
        $this->audit->created_by = $this->session->userdata("user_id");
        $this->audit->delimiter = "";
    }

    public function log_changes($key,$from_data,$to_data) {
        if ($this->audit != null) {
            $this->audit->description .= $this->audit->delimiter . " {$key} from '{$from_data}' to '{$to_data}'";
            $this->audit->delimiter = ",";
        }
    }

    public function log_insert($key,$data) {
        if ($this->audit != null) {
            $this->audit->description .= $this->audit->delimiter . " {$key} '{$data}'";
            $this->audit->delimiter = ",";
        }
    }

    public function log_close() {
        if ($this->audit != null) {
            if (isset($this->audit->delimiter)) {
                unset($this->audit->delimiter);
            }
            $this->db->insert($this->audit);
        }
        $this->audit = null;
    }

    public function log_action($component,$action,$description) {
        $current_date = date('Y-m-d H:i:s');

        $audit = $this->db->create("admin_actions");
        $audit->component = $component;
        $audit->description = $description;
        $audit->date_created = $current_date;
        $audit->action = $action;
        $audit->created_by = $this->session->userdata("user_id");

        $this->db->insert($audit);
    }

    public function activity_insert($msg) {
        $current_date = date('Y-m-d H:i:s');
        $this->activity = $this->db->create("activities");
        $this->activity->activity = $msg;
        $this->activity->date_created = $current_date;
        $this->activity->created_by = $this->session->userdata("user_id");

        $this->db->insert($this->activity);

        $this->activity = null;
    }

    public function paginate($page,$total,$range,$link) {
        $pagination = $this->pagination->compute($page,5,$total,$range);

        $first = $link . $pagination["first"];
        $prev = $link . $pagination["prev"];
        $next = $link . $pagination["next"];
        $last = $link . $pagination["last"];

        $pages = "<li><a href=\"$first\">First</li>";
        $pages.= "<li><a href=\"$prev\">Prev</li>";

        foreach ($pagination["pages"] as $data) {
            $page_link = $link . $data["id"];
            $css = "";
            if ($data["show"] == 0) {
                $css = " class=\"disabled\"";
                $pages.= "<li$css><span>".$data["id"]."</span></li>";
            } else if ($data["id"] == $page) {
                $css = " class=\"active\"";
                $pages.= "<li$css><a href=\"$page_link\">".$data["id"]."</a></li>";
            }
        }

        $pages.= "<li><a href=\"$next\">Next</li>";
        $pages.= "<li><a href=\"$last\">Last</li>";

        $this->set("pages",$pages);
    }

    public function bind($sql,$fields=array(),$actions=array()) {
        $result = $this->db->query($sql);
        $content = "";
        if (count($result) > 0) {
            foreach ($result as $data) {
                $content.= "<tr>";
                foreach ($fields as $field) {
                    if ($field == "id") {

                    }
                    else if (is_int($data->$field) || is_double($data->$field)) {
                        $content .= "<td class=\"text-right\">" . $data->$field . "</td>";
                    } else {
                        $content .= "<td>" . $data->$field . "</td>";
                    }
                }

                if (count($actions) > 0) {
                    $content .= "<td class=\"text-center\">";
                    if (isset($actions["on"])) {
                        $icon_toggle = "icon-toggle-off";
                        if ($data->$actions["on"] == 1) {
                            $icon_toggle = "icon-toggle-on";
                        }
                        $content .= "<button type=\"button\" class=\"btn btn-default btn-tool-square btn-sm cmd_toggle\" rel=\"$data->id\"><i class=\"fa fa-power-off $icon_toggle\"></i></button>";
                    }
                    $content .= "</td>";
                }

                $content.="</tr>";
            }
        }

        return $content;
    }

    public function get_total($sql) {
        $total = 0;
        $result = $this->db->query($sql);
        if (count($result) > 0) {
            $total = $result[0]->total;
        }
        return $total;
    }

    public function get_current_date() {
        return date('Y-m-d H:i:s');
    }

    public function get_current_timestamp() {
        return date("YmdHis");
    }

    public function get_current_user() {
        return $this->session->userdata("user_id");
    }

    public function send($total,$result) {
        header("Content-Type: text/json");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Requested-With");

        print json_encode(array(
            "total" => intval($total),
            "rows" => $result),JSON_PRETTY_PRINT
        );
    }

    public function create_client_response($code,$message) {
        $response = new sdgClass();
        $response->code = $code;
        $response->message = $message;
        $response->redirect_url = "";

        $response->notify = __declare($response,function() {
            print json_encode(array("response_code" => $this->code,"response_msg" => $this->message));
        });

        $response->error = __declare($response,function($message,$code=1,$redirect_url="") {
            $this->message = $message;
            $this->code = $code;
            $this->redirect_url = $redirect_url;
        });

        $response->success = __declare($response,function($message,$redirect_url="") {
            $this->message = $message;
            $this->code = 0;
            $this->redirect_url = $redirect_url;
        });

        return $response;
    }

    function bulk_upload() {

    }

    public function export_data($prefix,$headers=array(),$statement) {
        $result = $this->db->query($statement);

        $file_name = $prefix.$this->get_current_timestamp().".csv";
        $file_path = BASEPATH . "assets/uploads/bulk/downloads/";
        //$file_path = BASEPATH . "bulk/";
        $this->helper->create_csv_stream($file_name,$file_path,$headers,$result);

        $file = $this->db->create("files");
        $file->date_created = $this->get_current_date();
        $file->created_by = $this->get_current_user();
        $file->flag = 1;
        $file->file_name = $file_name;
        $file->file_path = $file_path . $file_name;
        $file->file_size = filesize($file->file_path);
        $file->hash_value = sha1_file($file->file_path);

        $this->db->insert($file);
    }

    public function export_data_xls($prefix, $headers=array(), $statement) {
        $result = $this->db->query($statement);
        $file_name = $prefix.$this->get_current_timestamp().".xls";
        $file_path = BASEPATH . "bulk/";

        $this->helper->create_xls($file_name, $file_path, $headers, $result);
        $file = $this->db->create("files");
        $file->date_created = $this->get_current_date();
        $file->created_by = $this->get_current_user();
        $file->flag = 1;
        $file->file_name = $file_name;
        $file->file_path = $file_path . $file_name;
        $file->file_size = filesize($file->file_path);
        $file->hash_value = sha1_file($file->file_path);

        $this->db->insert($file);
    }

    public function route($method,$extras) {
        
    }
}