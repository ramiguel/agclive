<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Table {
	const ADMIN_ACTIONS           = "admin_actions";

	const ADMIN_MENU              = "admin_menu";
	
	const BLOOD_TYPES             = "blood_types";
	
	const CHANGE_PASSWORD_HISTORY = "change_password_history";
	
	const CHECKIN_ITEMS           = "checkin_items";
	
	const CHECKINS                = "checkins";
	
	const CITIES                  = "cities";
	
	const CMDCTR                  = "cmdctr";
	
	const CMDCTR_STATUS           = "cmdctr_status";
	
	const CMS_MENU                = "cms_menu";
	
	const COMPANIES               = "companies";
	
	const COMPANY_ADMINS          = "company_admins";
	
	const COMPANY_DEPARTMENT      = "company_department";
	
	const COMPANY_DICTIONARY      = "company_dictionary";
	
	const COMPONENT               = "component";
	
	const CONFIG                  = "config";
	
	const DEPARTMENT              = "department";
	
	const DEVICES                 = "devices";
	
	const EMAILS                  = "emails";
	
	const EMERGENCY_HOTLINE       = "emergency_hotline";
	
	const EMPLOYEES               = "employees";

	const EVENTS 				  = "events";

	const EVENT_SKILLS 			  = "event_skills";

	const EVENT_VOLUNTEERS 		  = "event_volunteers";

	const EVENT_VOLUNTEER_SKILLS  = "event_volunteer_skills";

	const SKILLS 				  = "skills";
	
	const FEEDTYPE                = "feedtype";
	
	const FILES                   = "files";
	
	const FORGOT_PASSWORD         = "forgot_password";
	
	const ITEMS                   = "items";
	
	const LOGIN_TYPES             = "login_types";
	
	const MAINTENANCE             = "maintenance";
	
	const MODULES                 = "modules";
	
	const BROADCASTS              = "broadcasts";
	
	const BROADCAST_STATUS        = "broadcast_status";
	
	const OFFICIALFEEDS           = "officialfeeds";
	
	const REGION                  = "region";
	
	const REPORT_INCIDENTS        = "report_incidents";
	
	const REPORT_INCIDENT_STATUS  = "report_incident_status";
	
	const REPORTIN_STATUS         = "reportin_status";
	
	const RESOURCES               = "resources";
	
	const ROLES                   = "roles";
	
	const SAFEZONE_AREAS          = "safezone_areas";
	
	const SAFEZONES               = "safezones";

	const SAFEZONE_SUPPLIES		  = "safezone_supplies";
	
	const SESSION                 = "session";
	
	const SMS                     = "sms";
	
	const STATUS                  = "status";
	
	const USER_MODULE             = "user_module";
	
	const USERS                   = "users";
}