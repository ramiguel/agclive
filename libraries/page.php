<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Page {
    public function __construct() {
    }

    public static function handle($post_page,$default_page) {
        if ($post_page != null) {
            $post_page();
        } else {
            if ($default_page != null) {
                $default_page();
            } else {
                Page::error_page();
            }
        }
    }

    public static function error_page() {
    }
}