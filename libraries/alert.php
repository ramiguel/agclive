<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Alert {
    public function __construct() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function set_message($type,$message) {
        $_SESSION["alert_message"] = $message;
        $_SESSION["alert_type"] = $type;
    }

    public function get_message() {
        return isset($_SESSION["alert_message"]) ? $_SESSION["alert_message"] : "";
    }

    public function get_type() {
        return isset($_SESSION["alert_type"]) ? $_SESSION["alert_type"] : "";
    }

    public function display_message_on_error($parent) {
        $error_message = $this->get_message();
        if (!empty($error_message)) {
            //$parent->set("error_modal","location.href=\"#modal\"");
            $parent->set("error_modal","$('#error_modal').modal('show');");
            $parent->set("error_message",$error_message);
        } else {
            $parent->set("error_modal","");
            $parent->set("error_message","");
        }
        $this->clear();
    }

    public function clear() {
        unset($_SESSION["alert_message"]);
        unset($_SESSION["alert_type"]);
    }
}