<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
require_once LIBRARY_PATH . "vendor/apnsphp/ios_push_notif.php";

class IOSPushNotif {
	public function push($message, $data) {
        $ios_push = new Ios_push_notif();
		$ios_push->push($message, $data);
	}
	
	public function pushBroadcast($reg_id, $message, $count) {
		$ios_push_broadcast = new Ios_push_notif();
		$ios_push_broadcast->pushBroadcast($reg_id, $message, $count);
	}
	
	public function pushBroadcast_New($payload, $count) {
		$ios_push_broadcast = new Ios_push_notif();
		$ios_push_broadcast->pushBroadcast_New($payload, $count);
	}
	
	public function pushDeclareDisaster($payload, $count) {
		$ios_push_disaster = new Ios_push_notif();
		$ios_push_disaster->pushDeclareDisaster($payload, $count);
	}
	
	public function push_disaster($reg_id, $message, $count) {
		$ios_push_broadcast = new Ios_push_notif();
		$ios_push_broadcast->push_disaster($reg_id, $message, $count);
	}
	
	public function pushCheckin($reg_id, $message) {
		$ios_push_broadcast = new Ios_push_notif();
		$ios_push_broadcast->pushCheckin($reg_id, $message);
	}

	public function pushIncident($reg_id,$message) {
		$ios_push_broadcast = new Ios_push_notif();
		$ios_push_broadcast->pushIncident($reg_id, $message);
	}
}
?>