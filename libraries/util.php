<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Util {
    public function generate_password() {
        $map = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $map_length = strlen($map)-1;

        $password_length = 6;
        $generated_password = "";

        $index = mt_rand(3,13);

        while ($index < 13) {
            $password_length = mt_rand(6, 10);
            $index++;
        }

        $index = 0;
        while ($index < $password_length) {
            $map_index = mt_rand(0,$map_length);
            $generated_password .= $map[$map_index];
            $index++;
        }

        return $generated_password;
    }
}