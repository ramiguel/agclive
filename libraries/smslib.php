<?php

require_once LIBRARY_PATH . "vendor/globelabs/globeapi.php";

class SmsLib extends Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array("db"));
    }

    public function sendMessage($data)
    {
        $globe = new GlobeApi(GL_VERSION);

        if($this->_check_prefix($data["mobile_no"])){
            $sms = $globe->Sms(SHORT_CODE_CROSS);
        } else {
            $sms = $globe->Sms(SHORT_CODE);
        }

        $response = $sms->sendMessage($data["mobile_no"], $data["message"]);

        return $response;
    }

    private function _check_prefix($mobile_no) {
        $result = $this->checking($mobile_no);

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    private function checking($passed_data) {
        $this->prefixes = $this->_get_all_prefixes();

        $mobile_no = substr($passed_data, 1);

        $number_range = $this->check_number_range($mobile_no);

        if($number_range) {
            if ($this->check_start_end_msisdn($number_range, $mobile_no)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function check_number_range($mobile_no) {

        foreach($this->prefixes as $key=>$value):

            $pref = $value->number_range;
            $pref_len = strlen($value->number_range);
            $mob_pref = substr($mobile_no, 0, $pref_len);

            if($mob_pref == $pref) :
                return $pref;
            endif;

        endforeach;

        return false;
    }

    private function check_start_end_msisdn($number_range, $mobile_no) {
        foreach($this->prefixes as $key=>$value):
            $start = $value->start_msisdn;
            $end = $value->end_msisdn;
            $brand = $value->brand;

            if(($number_range == $value->number_range)
                && ($mobile_no >= $start)
                && ($mobile_no <= $end)):

                return true;

            endif;
        endforeach;

        return false;
    }

    private function _get_all_prefixes() {
        return $this->db->query("SELECT number_range, start_msisdn, end_msisdn, brand FROM prefixes");
    }
}