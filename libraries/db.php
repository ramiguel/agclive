<?php

/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * User: Cris del Rosario
 * Date: 4/13/2015
 * Time: 9:24 AM
 *
 ************************************************************************************************************/

class Db {
    private $connection = null;
    private $lastInsertId = 0;

    const INNER = 0;
    const LEFT = 1;
    const RIGHT = 2;
    const OUTER = 3;

    public function connect() {
        if (!$this->checkConnection()) {
            $this->createNewConnection();
        }
    }

    private function checkConnection() {
        return $this->connection;
    }

    private function createNewConnection() {
        try {
            $this->connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        } catch (Exception $exception) {
        }
        if ($this->connection && $this->connection->connect_error) {
            die($this->connection->connect_error);
        }        
    }

    private function isObjectQuery($query) {
        if (is_object($query) && !is_string($query)) {
            return true;
        } else {
            return false;
        }
    }

    private function getFields($query) {
        $fields = "";
        $seperator = "";

        foreach ($query->getFields() as $key => $value) {
            $fields .= $seperator . $key; 
            $seperator = ",";
        }

        return $fields;
    }

    private function getConditions($condition) {
        $where = "";
        $seperator = "";
        if ($condition != null && is_array($condition)) {
            foreach ($condition as $key => $value) {
                $where.= $seperator . $key . "=" . $this->quote($value);
                $seperator = " AND ";
            }
            if (!empty($where)) {
                $where = " WHERE $where";
            }
        }
        return $where;
    }

    public function query($query,$condition=null) {
        $resultObjects = array();
        $this->connect();

        if ($this->checkConnection()) {
            if ($this->isObjectQuery($query)) {
                $table = $query->getTable();
                $fields = $this->getFields($query);
                $where = $this->getConditions($condition);
                $orderby  = $query->getOrderBy();

                $query = "SELECT $fields FROM $table $where $orderby";
            }

            $result = $this->connection->query($query);
            if ($result) {
                while ($object = $result->fetch_object()) {
                    $resultObjects[] = $object;
                }
                $result->free();
            } else {
                print $this->connection->error;
            }
        }

        $this->close();

        return $resultObjects;
    }

    public function delete($query,$condition=null) {
        $resultObjects = array();
        $this->connect();

        if ($this->checkConnection()) {
            if ($this->isObjectQuery($query)) {
                $table = $query->getTable();
                $fields = $this->getFields($query);
                $where = $this->getConditions($condition);
                $orderby  = $query->getOrderBy();

                $query = "SELECT $fields FROM $table $where $orderby";
            }

            $result = $this->connection->query($query);
            if ($result) {

            } else {
                print $this->connection->error;
            }
        }

        $this->close();

        return $resultObjects;
    }

    public function select($table,$fields,$condition=null) {
        $where = $this->getConditions($condition);
        return $this->query("SELECT $fields FROM $table $where");
    }

    public function quote($value) {
        $quoted_value = $value;
        if ($this->checkConnection()) {
            $quoted_value = $this->connection->real_escape_string($quoted_value);
        }
        $quoted_value = "'".$quoted_value."'";
        return $quoted_value;
    }

    public function insert($sql) {
        $result = false;
        $this->connect();
        if ($this->checkConnection()) {
            if (is_object($sql) && !is_string($sql)) {
                $table = $sql->getTable();

                $fields = "";
                $values = "";
                $sep = "";
                foreach ($sql->getFields() as $key => $value) {
                    $fields .= $sep.$key;
                    $values .= $sep.(is_int($value) ? $value : $this->quote($value));
                    $sep = ",";
                }

                $sql = "INSERT INTO $table ($fields) VALUES($values)";
            }
    
            if ($this->connection->query($sql)) {
                $result = true;
                $this->lastInsertId = $this->connection->insert_id;
            } else {
                print $this->connection->error;
            }
        }
        $this->close();
        return $result;
    }

    public function update($sql,$condition=null) {
        $result = false;
        $this->connect();
        if ($this->checkConnection()) {
            if (is_object($sql) && !is_string($sql)) {
                $table = $sql->getTable();

                $fields = "";
                $sep = "";
                foreach ($sql->getFields() as $key => $value) {
                    $fields .= $sep.$key."=".(is_int($value) ? $value : $this->quote($value));
                    $sep = ",";
                }

                $where = $this->getConditions($condition);

                $sql = "UPDATE $table SET $fields $where";
            }
            if ($this->connection->query($sql)) {
                $result = true;
            } else {
                print $this->connection->error;
            }
        }
        $this->close();

        return $result;
    }

    public function getLastInsertId() {
        return $this->lastInsertId;
    }

    public function close() {
        if ($this->connection) {
            $this->connection->close();
        }
        $this->connection = null;
    }

    public function add_fields($object,$fields) {
        $fields = explode(",",$fields);
        foreach ($fields as $field) {
            $object->$field = "";
        }
    }

    // misc stuff
    public function create($table,$alias="") {
        $object = new sdgClass();
        if ($table != null) {
            $object->__table = $table;
            $object->__orderby = "";
            $object->__direction = "DESC";
            $object->__as = $alias;

            $object->__join_left = null;
            $object->__join_right = null;
            $object->__join_type = Db::LEFT;

            $object->getTable = __declare($object,function() {
                return $this->__table;
            });

            $object->setOrderBy = __declare($object,function($fields,$order="DESC") {
                 $this->__orderby = $fields;
                 $this->__direction = $order;
            });

            $object->getOrderBy = __declare($object,function() {
                $orderby = "";
                if (!empty($this->__orderby)) {
                    $orderby = "ORDER BY " . $this->__orderby . " " . $this->__direction;
                }
                return $orderby;
            });

            $object->join = __declare($object,function($left,$right,$type) {
                $this->__join_left = $left;
                $this->__join_right = $right;
                $this->__join_type = $type;
            });

            $object->as = __declare($object,function($alias) {
                $this->__as = $alias;
            });

            $object->getFields = __declare($object,function() {
                $fields = get_object_vars($this);

                unset($fields["__table"]);
                unset($fields["__orderby"]);
                unset($fields["__direction"]);
                unset($fields["__as"]);

                unset($fields["__join_left"]);
                unset($fields["__join_right"]);
                unset($fields["__join_type"]);

                unset($fields["cleanup"]);
                unset($fields["getFields"]);
                unset($fields["getOrderBy"]);
                unset($fields["setOrderBy"]);
                unset($fields["getTable"]);

                unset($fields["join"]);
                unset($fields["as"]);

                return $fields;
            });
        }

        return $object;
    }
}