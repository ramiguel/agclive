<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

class Session {
    public function __construct() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function set_userdata($name,$value) {
        if (isset($_SESSION)) {
            $_SESSION[$name] = $value;            
        }
    }

    public function userdata($name) {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    public function get_session_id() {
        return session_id();
    }

    public function destroy() {
        if (isset($_SESSION)) {
            foreach($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }            
        }
        session_destroy();
        session_regenerate_id(TRUE);
    }
}