<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class Timeago {
	public function input_date($date) {
		if(empty($date)) {
			return "No date provided";
		}

		$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$lengths = array("60","60","24","7","4.35","12","10");
		$now = time();
		$unix_date = strtotime($date);

		// check validity of date
		if(empty($unix_date)) {
			return "Bad date";
		}

		// is it future date or past date\
		if($now > $unix_date) {
			$difference = $now - $unix_date;
			$tense = "ago";
		} else {
			$difference = $unix_date - $now;
			$tense = "from now";
		}

		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}

		$difference = round($difference);

		if($difference != 1) {
			$periods[$j] .= "s";
		}

		return "$difference $periods[$j] {$tense}";

	}
}
?>