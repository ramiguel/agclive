<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator / A.S.S.I.S.T
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/
 
class User {
    const MIN_USERNAME_LENGTH = 3;
    const MAX_USERNAME_LENGTH = 22;

    const MIN_PASSWORD_LENGTH = 6;
    const MAX_PASSWORD_LENGTH = 22;

    const MIN_MOBILE_LENGTH = 11;
    const MAX_MOBILE_LENGTH = 12;

    const MIN_EMAIL_LENGTH = 5;
    const MAX_EMAIL_LENGTH = 128;

    public function emailLogin($email,$password) {
        $email_length = strlen($email);
        $password_length = strlen($password);

        $state = false;
        if ($email_length >= User::MIN_EMAIL_LENGTH && $email_length <= User::MAX_EMAIL_LENGTH) {
            if ($password_length >= User::MIN_PASSWORD_LENGTH && $password_length <= User::MAX_PASSWORD_LENGTH) {
                if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
                    $state = true;
                }
            }
        }
        return $state;
    }

    public function mobileLogin($mobile,$password) {
        $mobile_length = strlen($mobile);
        $password_length = strlen($password);

        $state = false;
        if ($mobile_length >= User::MIN_MOBILE_LENGTH && $mobile_length <= User::MAX_MOBILE_LENGTH) {
            if ($password_length >= User::MIN_PASSWORD_LENGTH && $password_length <= User::MAX_PASSWORD_LENGTH) {
                if (ctype_digit($mobile)) {
                    $state = true;
                }
            }
        }
        return $state;
    }

    public function userLogin($username,$password) {

    }
}