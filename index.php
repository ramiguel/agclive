<?php
/************************************************************************************************************
 * AGC - Ayala Group of Companies Employee Locator
 * Web/CMS
 *
 * Developed by TOP-SDG/Yondu
 * User: Cris del Rosario
 * Date: 4/11/2015
 * Time: 7:39 PM
 *
 ************************************************************************************************************/

date_default_timezone_set('Asia/Hong_Kong');

/*
 * ENVIRONMENT
 *
 *      You can manually change the default ENVIRONMENT value depending on the type of environment you want.
 *
 *      development - This basically enables all error reporting
 *      production  - Be silent!!! disables error reporting, we don't want that in production right?
 */

define ("ENVIRONMENT","production");

if (defined("ENVIRONMENT") && ENVIRONMENT == "production") {
    error_reporting(0);
} else {
    error_reporting(E_ALL);
}

// You can change the path for each if you want
define ("MODEL_PATH","models/");
define ("LIBRARY_PATH", "libraries/");
define ("MODULES_PATH", "modules/");

// Path to our core libraries
define ("CORE_LIBRARY_PATH",LIBRARY_PATH . "core/");

// Main configuration file
define ("CONFIG_PATH",MODULES_PATH);

// constants
define ("BASEPATH",str_replace(pathinfo(__FILE__, PATHINFO_BASENAME),"",__FILE__));
define ("BASE",str_replace("/index.php","",$_SERVER["SCRIPT_NAME"]));
define ("EXT",".php");

// This framework's current version
define ("YONDU_SDG_FRAMEWORK_VERSION","0.1");

require_once CONFIG_PATH . "config.php";

require_once CORE_LIBRARY_PATH . "Controller.php";
require_once CORE_LIBRARY_PATH . "Model.php";

/***********************************************************************************************************
 *
 * we can load our custom extensions through here
 *
 **********************************************************************************************************/

require_once LIBRARY_PATH . "contrib/Extensions.php";

/***********************************************************************************************************
 *
 * Class loader
 *
 **********************************************************************************************************/

if (!function_exists('load_class')) {
    class Classes {
        public static $bucket = array();
    };

    function load_class($base,$class) {
        $library_class_path = BASEPATH . $base . $class . EXT;

        // we don't need to reload the same class
        if (!isset(Classes::$bucket[$class])) {
            if (!class_exists($class) && is_file($library_class_path)) {
                require_once $library_class_path;

                // we need to make sure that it contains the expected class
                if (class_exists($class)) {
                    Classes::$bucket[$class] = new $class();
                    return Classes::$bucket[$class];
                }
            }
        }
        return null;
    }
}

/***********************************************************************************************************
 *
 * This can be our first page/redirect page/first controller
 *
 **********************************************************************************************************/

if (!class_exists('sdgClass')) {
    class sdgClass {
        public function __construct() {
        }

        public function __call($method,$args) {
            if (is_callable(array($this, $method))) {
                return call_user_func_array($this->$method, $args);
            }
        }
    };
}

// helper function
if (!function_exists('__declare')) {
    function __declare($class,$closure) {
        return $closure->bindTo($class);
    }
}

/***********************************************************************************************************
 *
 * Some miscellaneous functions
 *
 **********************************************************************************************************/

if (!function_exists('redirect')) {
    function redirect($url='/') {
        header('Location: ' . $url);
        exit;
    }
}

if (!function_exists('base_url')) {
    function base_url() {
        return BASE;
    }
}

/***********************************************************************************************************
 *
 * We can use this to check how fast our page loads
 *
 **********************************************************************************************************/

if (!class_exists('PageBenchmark')) {
    class PageBenchmark {
        private static $time;

        public static function markStart() {
            PageBenchmark::$time = PageBenchmark::getTime();
        }

        public static function getTime() {
            $time = microtime();
            $time = explode(" ",$time);
            return $time[1] + $time[0];
        }

        public static function markEnd() {
            print "Page generated in " . (round(PageBenchmark::getTime() - PageBenchmark::$time,4)). " seconds";
        }
    }
}

/***********************************************************************************************************
 *
 * API Helper functions
 * This is how we send our response to the client
 *
 **********************************************************************************************************/

if (!function_exists('create_response')) {
    function create_response($code,$message) {
        $response = new stdClass();
        $response->code = $code;
        $response->data = null;
        $response->message = $message;
        return $response;
    }
}

if (!function_exists('notify')) {
    function notify($response) {
        $root = new stdClass();
        $root->response_code = $response->code;

        if ($response->data == null) {
            $root->data = array();
        } else {
            $root->data = $response->data;
        }

        $root->system = new stdClass();
        $root = sign($root);

        $root->system->message = $response->message;

        header("Content-Type: text/json");

        print json_encode($root);
    }
}

if (!function_exists('clear')) {
    function clear($response,$message="") {
        $response->code = 0;
        $response->message = $message;
        return $response;
    }
}

/***********************************************************************************************************
 *
 * API Signer
 *
 **********************************************************************************************************/

if (!function_exists('sign')) {
    function sign($root) {
        $root->system->created = date('Y-m-d H:i:s');
        $root->system->signature = sha1("yondu-top-sdg");
        return $root;
    }
}

/***********************************************************************************************************
 *
 * Default or error pages
 *
 **********************************************************************************************************/

if (!function_exists('method_not_supported')) {
    function method_not_supported() {
        $response = create_response(2,'Method not supported');
        notify($response);
    }
}

if (!function_exists('bad_request')) {
    function bad_request() {
        $response = create_response(3,'Bad request');
        notify($response);
    }
}

if (!function_exists('error_page')) {
    function error_page() {
        print "<h3>Error 404</h3>The page you're trying to view could not be found!";
    }
}

/***********************************************************************************************************
 *
 * Main framework entry
 *
 **********************************************************************************************************/

class Yondu {
    private $uri;

    public function __construct() {

        // We'll be using this class to parse the uri
        $this->uri = load_class(LIBRARY_PATH,"uri");
    }

    public function route() {
        $segment = $this->uri->get_segments();
        $num_segments = count($segment);

        $module = $num_segments > 0 ? $segment[0] : "error";

        if ($module == "" && ENTRY_POINT != "") {
            redirect(base_url() . ENTRY_POINT);
        }

        if ($num_segments > 1 && $segment[1] != "") {
            $class_name = $segment[1];
        } else {
            $class_name = $module;
        }

        $controller = $class_name . EXT;

        $method = "index";                 // default main entry point
        $route_method = "request_handler";

        $module_path = BASEPATH . "modules/";

        $page_not_found = true;

        if (is_dir($module_path.$module)) {

            $controller_path = $module_path.$module."/controllers/".$controller;

            $segment_index = 2;

            $controller_loaded = false;

            if (!file_exists($controller_path)) {
                $controller_path = $module_path.$module."/controllers/".$module.EXT;

                if (file_exists($controller_path) && is_readable($controller_path)) {
                    $controller_loaded = true;
                    $class_name = $module;
                    $segment_index = 1;
                    require_once $controller_path;
                }
            } else {
                $controller_loaded = true;
                require_once $controller_path;
            }

            if ($controller_loaded == true && class_exists($class_name)) {
                $class = new $class_name();

                if ($class instanceof Controller) {
                    // Auto inject libraries
                    foreach (Classes::$bucket as $__name => $__class) {
                        $class->$__name = $__class;
                    }

                    if (method_exists($class, $route_method) && is_callable(array($class, $route_method))) {
                        $request_method = "";
                        $request_params = array();

                        if (count($segment) > $segment_index) {
                            $request_method = $segment[$segment_index];
                            $request_params = array_slice($segment, $segment_index);
                        }

                        $class->$route_method($request_method, $request_params);

                        $page_not_found = false;
                    } else {
                        if (method_exists($class, $method) && is_callable(array($class, $method))) {
                            $class->$method();
                            $page_not_found = false;
                        }
                    }
                }
            }
        }

        if ($page_not_found) {
            error_page();
        }
    }
}

(new Yondu())->route();



